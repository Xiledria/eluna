/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "LogsDatabase.h"

void LogsDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_LOGSDATABASE_STATEMENTS);

    PrepareStatement(LOGS_INS_ITEM_GET, "INSERT INTO item_logs (guid, player, account, item, item_guid, state, position, target) VALUES (?, ?,?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_GM_COMMANDS, "INSERT INTO gm_logs (player, account, command, position, selected) VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_ARENA_TEAM, "INSERT INTO team_arena_logs (logs_id, start, end, winner_team_id, loser_team_id, winner_rat, loser_rat, winner_change, loser_change) VALUES(?, FROM_UNIXTIME(?), FROM_UNIXTIME(?), ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_ARENA_PLAYER, "INSERT INTO player_arena_logs (log_id, guid, `name`, damage, heal, kills, rating, rating_change, arena_team) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_SEL_ARENA_ID, "SELECT MAX(logs_id) FROM team_arena_logs", CONNECTION_SYNCH);
    PrepareStatement(LOGS_INS_LOGIN, "INSERT INTO character_logs (guid, `name`, `account`, ip, time, login) VALUES (?, ?, ?, ?,  FROM_UNIXTIME(?), ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_INTERRUPT, "INSERT INTO interrupt_logs (guid, `name`, `account`, ip, time, start, end, cast_time, spell_id) VALUES (?, ?, ?, ?, FROM_UNIXTIME(?), ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_DISPELL, "INSERT INTO dispell_logs (guid, `name`, `account`, ip, time, delay, duration, spell_id) VALUES (?, ?, ?, ?, FROM_UNIXTIME(?), ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_BOSS_KILLS, "INSERT INTO boss_kill_logs (log_id, entry, map, difficulty, players, fight_duration, time) VALUES (?, ?, ?, ?, ?, ?, FROM_UNIXTIME(?))", CONNECTION_ASYNC);
    PrepareStatement(LOGS_INS_BOSS_KILL_PLAYERS, "INSERT INTO boss_kill_player_logs (log_id, guid, `name`, account, ip) VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(LOGS_SEL_BOSS_KILL_ID, "SELECT MAX(log_id) FROM boss_kill_logs", CONNECTION_SYNCH);
}

/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_dk_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "CombatAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

enum DeathKnightSpells
{
    SPELL_DK_SUMMON_GARGOYLE_1      = 49206,
    SPELL_DK_SUMMON_GARGOYLE_2      = 50514,
    SPELL_DK_DISMISS_GARGOYLE       = 50515,
    SPELL_DK_SANCTUARY              = 54661,
    SPELL_GARGOYLE_STRIKE           = 51963,
};

#define POINT_INIT 666

class npc_pet_dk_ebon_gargoyle : public CreatureScript
{
    public:
        npc_pet_dk_ebon_gargoyle() : CreatureScript("npc_pet_dk_ebon_gargoyle") { }

        struct npc_pet_dk_ebon_gargoyleAI : CasterAI
        {
            npc_pet_dk_ebon_gargoyleAI(Creature* creature) : CasterAI(creature)
            {
                Initialize();
            }

            void Reset() override
            {
                CasterAI::Reset();
            }

            void Initialize()
            {
                me->InitCharmInfo();
                if (Unit* owner = me->GetOwner())
                {
                    if (!me->IsWithinLOSInMap(owner))
                        me->Relocate(*owner);
                }
                me->RelocateOffset(Position(0.0f, 0.0f, 15.0f));
                me->SetCanFly(true);
                Position pos(*me);
                pos.m_positionZ -= 15.0f;
                landPos = pos;
                me->GetMotionMaster()->MovePoint(POINT_INIT, landPos, false);
                me->SetReactState(REACT_PASSIVE);
                init = false;
            }

            void MovementInform(uint32 /*type*/, uint32 id) override
            {
                // start fight instantly on land
                if (id == POINT_INIT)
                {
                    if (me->GetDistance(landPos) < 0.2f)
                        StartFight();
                    else
                        me->GetMotionMaster()->MovePoint(POINT_INIT, landPos, false);
                }
            }

            void EnterCombat(Unit*) override
            {

            }

            void AttackStart(Unit* victim) override
            {
                AttackStartCaster(victim, 40.0f);
            }

            void InitializeAI() override
            {

            }

            void JustDied(Unit* /*killer*/) override
            {
                // Stop Feeding Gargoyle when it dies
                if (Unit* owner = me->GetOwner())
                    owner->RemoveAurasDueToSpell(SPELL_DK_SUMMON_GARGOYLE_2);
            }

            // Fly away when dismissed
            void SpellHit(Unit* source, SpellInfo const* spell) override
            {
                if (spell->Id != SPELL_DK_DISMISS_GARGOYLE || !me->IsAlive())
                    return;

                Unit* owner = me->GetOwner();
                if (!owner || owner != source)
                    return;

                me->DeleteCharmInfo();

                // Stop Fighting
                me->ApplyModFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE, true);

                // Sanctuary
                me->CastSpell(me, SPELL_DK_SANCTUARY, true);
                me->SetReactState(REACT_PASSIVE);

                //! HACK: Creature's can't have MOVEMENTFLAG_FLYING
                // Fly Away
                me->SetCanFly(true);
                me->AddUnitState(UNIT_STATE_IGNORE_PATHFINDING);
                me->SetSpeedRate(MOVE_FLIGHT, 0.75f);
                me->SetSpeedRate(MOVE_RUN, 0.75f);
                float x = me->GetPositionX() + 20 * std::cos(me->GetOrientation());
                float y = me->GetPositionY() + 20 * std::sin(me->GetOrientation());
                float z = me->GetPositionZ() + 40;
                me->DespawnOrUnsummon(4 * IN_MILLISECONDS);
                me->RemoveAllAuras();
                me->GetMotionMaster()->MovePoint(0, x, y, z, false);
                // disable default AI
                me->IsAIEnabled = false;
            }

            void StartFight()
            {
                me->SetCanFly(false);
                me->SetReactState(REACT_AGGRESSIVE);
                ObjectGuid ownerGuid = me->GetOwnerGUID();
                if (!ownerGuid)
                    return;

                // Find victim of Summon Gargoyle spell
                std::list<Unit*> targets;
                Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 30.0f);
                Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, targets, u_check);
                me->VisitNearbyObject(30.0f, searcher);
                for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
                {
                    if ((*iter)->HasAura(SPELL_DK_SUMMON_GARGOYLE_1, ownerGuid))
                    {
                        AttackStart(*iter);
                        break;
                    }
                }
                events.ScheduleEvent(SPELL_GARGOYLE_STRIKE, 0);
                init = true;
            }

            void UpdateAI(uint32 diff) override
            {
                if (init)
                {
                    if (!me->GetVictim() && !me->isMoving())
                        me->GetMotionMaster()->MoveFollow(me->GetOwner(), PET_FOLLOW_DIST, me->GetFollowAngle());

                    if (me->GetVictim())
                        events.Update(diff);

                    if (me->GetVictim() && me->EnsureVictim()->HasBreakableByDamageCrowdControlAura(me))
                    {
                        me->InterruptNonMeleeSpells(false);
                        return;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;

                    if (me->GetVictim())
                    if (uint32 spellId = events.ExecuteEvent())
                    {
                        DoCast(spellId);
                        uint32 casttime = me->GetCurrentSpellCastTime(spellId);
                        events.ScheduleEvent(spellId, casttime);
                    }
                }
                else if (!me->isMoving())
                {
                    me->GetMotionMaster()->MovePoint(POINT_INIT, landPos, false);
                }
            }

        private:
            bool init;
            Position landPos;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_dk_ebon_gargoyleAI(creature);
        }
};


class npc_pet_dk_guardian : public CreatureScript
{
    public:
        npc_pet_dk_guardian() : CreatureScript("npc_pet_dk_guardian") { }

        struct npc_pet_dk_guardianAI : public AggressorAI
        {
            npc_pet_dk_guardianAI(Creature* creature) : AggressorAI(creature) { }

            bool CanAIAttack(Unit const* target) const override
            {
                if (!target)
                    return false;
                Unit* owner = me->GetOwner();
                if (owner && !target->IsInCombatWith(owner))
                    return false;
                return AggressorAI::CanAIAttack(target);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_dk_guardianAI(creature);
        }
};

void AddSC_deathknight_pet_scripts()
{
    new npc_pet_dk_ebon_gargoyle();
    new npc_pet_dk_guardian();
}

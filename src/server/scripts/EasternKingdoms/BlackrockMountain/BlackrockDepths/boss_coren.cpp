#include "ScriptedCreature.h"
#include "ScriptMgr.h"
#include "Group.h"
#include "LFGMgr.h"
#include "Player.h"

enum Spells
{
    SPELL_DISARM            = 47310,
    SPELL_BARRELED          = 47442,
    SPELL_CHUCK_MUG         = 50276,
    SPELL_DRILL_EMERGE      = 50313,
    SPELL_CREATE_MUG        = 47345,
    SPELL_BREWMAIDEN_STUN   = 47340,
};

enum Summons
{
    NPC_DIREBREW_MINION     = 26776,
    NPC_TRIGGER             = 30298,
    NPC_URSULA              = 26822,
    NPC_ILSA                = 26764,
};

enum Events
{
    EVENT_DISARM            = 1,
    EVENT_DRILL_EMERGE      = 2,
};

class boss_coren_direbrew : public CreatureScript
{
public:
    boss_coren_direbrew() : CreatureScript("boss_coren_direbrew") { }

    struct boss_coren_direbrewAI : public ScriptedAI
    {
        boss_coren_direbrewAI(Creature* creature) : ScriptedAI(creature), _summons(me) { }

        void Reset() override
        {
            _summons.DespawnAll();
            _events.Reset();
        }

        void EnterCombat(Unit*) override
        {
            me->SummonCreature(NPC_URSULA, me->GetPositionX() + float(irand(-14, 14)), me->GetPositionY() + float(irand(-14, 14)), me->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 600000);
            me->SummonCreature(NPC_ILSA, me->GetPositionX() + float(irand(-14, 14)), me->GetPositionY() + float(irand(-14, 14)), me->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 600000);

            _events.ScheduleEvent(EVENT_DISARM, 1000);
            _events.ScheduleEvent(EVENT_DRILL_EMERGE, 5000);
        }

        void EnterEvadeMode(EvadeReason why) override
        {
            ScriptedAI::EnterEvadeMode(why);
            _summons.DespawnAll();
        }

        void JustSummoned(Creature* who) override
        {
            _summons.Summon(who);
        }

        void JustDied(Unit*) override
        {
            Map::PlayerList const& players = me->GetMap()->GetPlayers();

            if (!players.isEmpty())
            {
                if (Group* group = players.begin()->GetSource()->GetGroup())
                    if (group->isLFGGroup())
                        sLFGMgr->FinishDungeon(group->GetGUID(), 287);
            }
        }

        void SummonAdds()
        {
            me->CastSpell((Unit*)nullptr, SPELL_DRILL_EMERGE, false);

            uint8 numAdds = irand(1, 3);
            for(uint8 i = 0; i < numAdds; ++i)
            {
                if (TempSummon* add = me->SummonCreature(NPC_DIREBREW_MINION, me->GetPositionX() + float(irand(-14, 14)), me->GetPositionY() + float(irand(-14, 14)), me->GetPositionZ(), 0, TEMPSUMMON_TIMED_OR_DEAD_DESPAWN, 600000))
                {
                    add->SetReactState(REACT_AGGRESSIVE);
                    add->setFaction(me->getFaction());
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if(!UpdateVictim())
                return;

            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_DISARM:
                        DoCastAOE(SPELL_DISARM, false);
                        _events.ScheduleEvent(EVENT_DISARM, 7000);
                        break;

                    case EVENT_DRILL_EMERGE:
                        SummonAdds();
                        _events.ScheduleEvent(EVENT_DRILL_EMERGE, 15000);
                        break;

                    default:
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }

    private:
        EventMap _events;
        SummonList _summons;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetInstanceAI<boss_coren_direbrewAI>(creature);
    }
};

class npc_ursula_direbrew : public CreatureScript
{
public:
    npc_ursula_direbrew() : CreatureScript("npc_ursula_direbrew") { }

    struct npc_ursula_direbrewAI : public CreatureAI
    {
        npc_ursula_direbrewAI(Creature* creature) : CreatureAI(creature), _timer(0) { }

        void EnterCombat(Unit*) override
        {
            _timer = 1000;
        }

        void UpdateAI(uint32 diff) override
        {
            if(!UpdateVictim())
                return;

            if (_timer > diff)
            {
                _timer -= diff;
                return;
            }

            _timer += 18000;

            if(Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 50, true))
                me->CastSpell(target, SPELL_BARRELED, false);
        }

    private:
        uint16 _timer;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetInstanceAI<npc_ursula_direbrewAI>(creature);
    }
};

class npc_ilsa_direbrew : public CreatureScript
{
public:
    npc_ilsa_direbrew() : CreatureScript("npc_ilsa_direbrew") { }

    struct npc_ilsa_direbrewAI : public CreatureAI
    {
        npc_ilsa_direbrewAI(Creature* creature) : CreatureAI(creature), _timer(0), _mugGiven(false) { }

        void EnterCombat(Unit*) override
        {
            _timer = 1000;
        }

        void UpdateAI(uint32 diff) override
        {
            if(!UpdateVictim())
                return;

            if (_timer > diff)
            {
                _timer -= diff;
                return;
            }

            if(!_lastThrown)
            {
                if(Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 50, true))
                {
                    DoCast(target, SPELL_CHUCK_MUG);
                    _lastThrown = target->GetGUID();
                }

                _timer += 3000; // give mug after cast, too lazy create spellscript
            }
            else
            {
                Unit* target = ObjectAccessor::GetUnit(*me, _lastThrown);

                if(!target || _mugGiven)
                {
                    if(target)
                        DoCast(target, SPELL_BREWMAIDEN_STUN);

                    _timer += 10000;
                    _mugGiven = false;
                    _lastThrown.Clear();
                }
                else
                {
                    DoCast(target, SPELL_CREATE_MUG);
                    _timer += 5000; // give target like 13 seconds to drink
                    _mugGiven = true;
                }
            }
        }

    private:
        uint16 _timer;
        ObjectGuid _lastThrown;
        bool _mugGiven;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetInstanceAI<npc_ilsa_direbrewAI>(creature);
    }
};

void AddSC_boss_coren_direbrew()
{
    new boss_coren_direbrew();
    new npc_ursula_direbrew();
    new npc_ilsa_direbrew();
}

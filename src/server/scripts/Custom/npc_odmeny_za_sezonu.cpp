#include "npc_odmeny_za_sezonu.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"

class npc_odmeny_za_sezonu : CreatureScript
{
public:
    npc_odmeny_za_sezonu() : CreatureScript("npc_odmeny_za_sezonu") { }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        OdmenyZaSezonu::GossipHelloMainMenu(player, creature);
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        if (!player)
            return false;

        if (action == (MENU + 0))
        {
            OdmenyZaSezonu::GossipHelloMainMenu(player, creature);
            return true;
        }
        if (action == (MENU + 1))
        {
            OdmenyZaSezonu::NactiOdmeny();
            creature->Whisper("Odmeny reloadnuty.", LANG_UNIVERSAL, player);
            OdmenyZaSezonu::GossipHelloMainMenu(player, creature);
            return true;
        }
        if (action == (MENU + 2))
        {
            player->PlayerTalkClass->SendCloseGossip();
            return true;
        }
        if (action > TITUL)
        {
            uint32 title = action - TITUL;
            OdmenyZaSezonu::AplikujOdmenu(player, creature, title);
            OdmenyZaSezonu::GossipHelloMainMenu(player, creature);
            return true;
        }
        if ((action > MENU_GM) && (action < MENU))
        {
            uint32 nasobic = action - MENU_GM;
            OdmenyZaSezonu::VypisOdmenyProGM(player, creature, nasobic);
            return true;
        }
        return false;
    }
};

void AddSC_npc_odmeny_za_sezonu()
{
    new npc_odmeny_za_sezonu();
}

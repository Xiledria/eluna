#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "Player.h"
#include "Creature.h"
#include "ObjectAccessor.h"
#include "ThreatManager.h"

#define QUEST_EVENT_MARK 40051
class npc_event_mark_essence : public CreatureScript
{
public:
    npc_event_mark_essence() : CreatureScript("npc_event_mark_essence") { }
    struct npc_event_mark_essenceAI : public CreatureAI
    {
        std::map<const ObjectGuid, uint32> attackers;
        uint32 timer;
        uint32 delay;
        npc_event_mark_essenceAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 60 * IN_MILLISECONDS;
            Reset();
        }

        void Reset() override
        {
            delay = 0;
            attackers.clear();
            me->setActive(true);
            me->m_PlayerDamageReq = std::numeric_limits<uint32>::max();
        }

        void DamageTaken(Unit* attacker, uint32&) override
        {
            delay = timer;
            if (attacker->GetTypeId() == TYPEID_PLAYER)
                attackers[attacker->GetGUID()] = 30 * IN_MILLISECONDS;
            if (!me->isMoving())
            {
                Position pos = *me;
                me->MovePositionToFirstCollision(pos, static_cast<float>(10.0f * rand_norm()), static_cast<float>(rand_norm() * 2 * M_PI));
                pos.m_positionZ += 0.1f;
                if (pos.m_positionZ < me->GetPositionZ() - 1.0f || pos.m_positionZ > me->GetPositionZ() + 1.0f ||
                !me->IsWithinLOS(pos.m_positionX, pos.m_positionY, pos.m_positionZ) || me->GetMap()->IsInWater(pos.m_positionX, pos.m_positionY, pos.m_positionZ))
                    return;

                me->GetMotionMaster()->MovePoint(0, pos, false);
            }
        }


        void JustDied(Unit*) override
        {
            for (auto itr = attackers.begin(); itr != attackers.end(); ++itr)
            {
                Player* plr = ObjectAccessor::GetPlayer(*me, itr->first);
                if (!plr)
                    continue;

                if (plr->GetDistance(me) > 60.0f)
                    continue;
                uint32 count = 2;
                uint32 noSpaceForCount = 0;
                ItemPosCountVec dest;
                InventoryResult msg = plr->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, Deffender::EVENT_MARK, count, &noSpaceForCount);
                if (msg != EQUIP_ERR_OK)                               // convert to possible store amount
                    count -= noSpaceForCount;
                if (Item* item = plr->StoreNewItem(dest, Deffender::EVENT_MARK, true))
                    item->SetBinding(true);
                me->Whisper("You have received 2 event marks!", LANG_UNIVERSAL, plr, true);
                plr->KilledMonsterCredit(me->GetEntry());
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!delay)
                return;

            // remove inactive players
            for (auto itr = attackers.begin(); itr != attackers.end();)
            {
                if (itr->second <= diff)
                    attackers.erase(itr++);
                else
                {
                    ++itr;
                    itr->second -= diff;
                }
            }

            if (me->IsInCombat())
            {
                auto& hostileMgr = me->getHostileRefManager();
                for (auto itr = hostileMgr.begin(); itr != hostileMgr.end(); ++itr)
                {
                    if (Unit* hostile = itr->getTarget())
                        hostile->getHostileRefManager().deleteReference(me);
                }
                me->getThreatManager().clearReferences();
            }

            if (delay > diff)
                delay -= diff;
            else
                EnterEvadeMode();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_event_mark_essenceAI(creature);
    }
};

void AddSC_npc_event_mark_essence()
{
    new npc_event_mark_essence();
}

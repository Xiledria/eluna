#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "Player.h"
#include "Creature.h"
#include "ObjectAccessor.h"
#include "ThreatManager.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

enum Spells
{
    METEOR_DAMAGE_PH1               = 74648,
    METEOR_DAMAGE_PH3               = 74648, // 75878, initial value before nerf
    METEOR_DUMMY                    = 74641,
    SOUL_BURN                       = 33913,
    FIRE_BLAST                      = 43245,
    FLAMEBLADE                      =  7806,
    FLAMEBLADE_STACK_BONUS          =   750,
    FLAMEBLADE_STACK_BONUS_PERIODIC =   150,
    MARK_OF_FLAMES                  = 64023,
    FLAME_WHIRL                     = 43213,
    FIRE_SHIELD                     = 71515,
    SPELL_PYROBLAST                 = 70516,
};

enum Cooldowns
{
    COOLDOWN_METEOR     = 20000,
    COOLDOWN_WHIRL      = 25000,
    COOLDOWN_MARK       =  5000,
    COOLDOWN_BUFFS      =  5000,
    COOLDOWN_PYROBLAST  = 12000,
    COOLDOWN_FIREBLAST  =  2000,
};

enum Entries
{
    ENTRY_ELEMENTAL = 400058,
};

class boss_troll_npc_dummyAI : public CreatureAI
{
    uint8 phase = 0;
    bool locked = false;
    TaskScheduler scheduler;
public:
    boss_troll_npc_dummyAI(Creature* creature) : CreatureAI(creature) { }
    void Reset() override
    {
        scheduler.Schedule(Seconds(5), [this](TaskContext task)
        {
            me->CastSpell(me, phase < 3 ? METEOR_DAMAGE_PH1 : METEOR_DAMAGE_PH3, true, nullptr, nullptr, me->GetOwnerGUID());
            me->RemoveAura(METEOR_DUMMY);
            if (phase > 1)
            {
                task.Schedule(Seconds(1), [this](TaskContext)
                {
                    if (me->IsSummon())
                    if (Unit* owner = me->ToTempSummon()->GetSummoner())
                        owner->SummonCreature(ENTRY_ELEMENTAL, *me)->UpdateObjectVisibility();
                    me->DespawnOrUnsummon(Seconds(1));
                });
            }
        });
    }

    void DoAction(int32 action) override
    {
        if (!locked)
        {
            locked = true;
            phase = static_cast<uint8>(action);
        }
    }

    void UpdateAI(uint32 diff) override
    {
        scheduler.Update(diff);
    }
};

CreatureAI* GetDummyAI(Creature* creature)
{
    return new boss_troll_npc_dummyAI(creature);
}

void GetPlayerList(Creature* me, std::list<Player*>& players, float distance)
{
    Trinity::AnyVisibleAttackableUnitInObjectRangeCheck u_check(me, me, distance);
    Trinity::PlayerListSearcher<Trinity::AnyVisibleAttackableUnitInObjectRangeCheck> searcher(me, players, u_check);
    me->VisitNearbyObject(distance, searcher);
}

class boss_troll : public CreatureScript
{
public:
    boss_troll() : CreatureScript("boss_troll") { }
    struct boss_trollAI : public CreatureAI
    {
        uint8 phase;
        const Seconds meteorTimer;
        uint32 fireblastCooldown;
        TaskScheduler scheduler;
        SummonList summons;

        boss_trollAI(Creature* creature) : CreatureAI(creature), meteorTimer(Seconds(25)), summons(creature)
        {

        }

        void GetPlayerList(std::list<Player*>& players, float distance)
        {
            ::GetPlayerList(me, players, distance);
        }

        void ScheduleTasks()
        {
            scheduler.Schedule(Seconds(5), 1, [this](TaskContext task)
            {
                Aura* flameblade = me->GetAura(FLAMEBLADE);
                if (!flameblade)
                {
                    flameblade = me->AddAura(FLAMEBLADE, me);
                    flameblade->GetEffect(EFFECT_0)->SetAmount(FLAMEBLADE_STACK_BONUS);
                }
                else
                {
                    AuraEffect* eff = flameblade->GetEffect(EFFECT_0);
                    eff->SetAmount(eff->GetAmount() + FLAMEBLADE_STACK_BONUS_PERIODIC);
                }
                flameblade->SetMaxDuration(3600 * IN_MILLISECONDS);
                flameblade->SetDuration(flameblade->GetMaxDuration());
                task.Repeat();
            });
            scheduler.Schedule(Seconds(0), 1, [this](TaskContext task)
            {
                std::list<Player*> players;
                GetPlayerList(players, 60.0f);
                if (players.empty())
                {
                    task.Repeat(Seconds(5));
                    return;
                }
                float count = static_cast<float>(players.size() / 5.0f + 2.0f);
                uint32 targetsCount = static_cast<uint32>(std::ceil(count));
                for (uint32 i = 0; i < targetsCount; ++i)
                {
                    auto victim = players.begin();
                    std::advance(victim, urand(0, static_cast<uint32>(players.size() - 1)));
                    ObjectGuid victimGuid = (*victim)->GetGUID();
                    scheduler.Schedule(Seconds(0), Seconds(meteorTimer), 1, [this, victimGuid](TaskContext /*task*/)
                    {
                        Player* victim = ObjectAccessor::GetPlayer(*me, victimGuid);
                        if (!victim)
                            return;
                        float x,y,z;
                        victim->GetPosition(x,y,z);
                        if (Creature* dummy = me->SummonTrigger(x,y,z, 0.0f, 30000, GetDummyAI))
                        {
                            dummy->CastSpell(dummy, METEOR_DUMMY, true);
                            dummy->GetAI()->DoAction(phase);
                        }
                    });
                }
                task.Repeat(Seconds(meteorTimer));
            });

            scheduler.Schedule(Seconds(3), 1, [this](TaskContext task)
            {
                int32 bp = 800; // custom spell power bonus
                me->CastCustomSpell(me->GetVictim(), SPELL_PYROBLAST, &bp, &bp, &bp, false);
                task.Repeat(Seconds(12));
            });
        }

        void JustSummoned(Creature* creature) override
        {
            summons.Summon(creature);
        }

        void SummonedCreatureDespawn(Creature* creature) override
        {
            summons.Despawn(creature);
        }

        void EnterCombat(Unit* who) override
        {
            me->Yell("Y U DO DIS TO ME?", LANG_UNIVERSAL);
            CreatureAI::EnterCombat(who);
            ScheduleTasks();
        }

        void Reset() override
        {
            fireblastCooldown = 0;
            summons.DespawnAll();
            scheduler.CancelGroup(1);
            SetPhase(1);
        }

        class CheckDummy
        {
        public:
            bool operator()(ObjectGuid guid) { return guid == WORLD_TRIGGER; }
        };

        void SetPhase(uint8 phase)
        {
            this->phase = phase;
        }

        void DamageTaken(Unit* /*attacker*/, uint32&) override
        {
            float pctHp = me->GetHealthPct();
            switch (phase)
            {
            case 1:
                if (pctHp < 75.0f)
                {
                    SetPhase(2);
                    scheduler.Schedule(Seconds(5), 1, [this](TaskContext task)
                    {
                        if (Aura* mark = me->AddAura(MARK_OF_FLAMES, me->GetVictim()))
                        {
                            mark->SetMaxDuration(25 * IN_MILLISECONDS);
                            mark->SetDuration(mark->GetMaxDuration());
                        }
                        task.Repeat();
                    });
                }
                break;
            case 2:
                if (pctHp < 50.0f)
                    SetPhase(3);
                break;
            case 3:
                if (pctHp < 25.0f)
                {
                    SetPhase(4);
                    scheduler.Schedule(Seconds(8), Seconds(16), 1, [this](TaskContext task)
                    {
                        me->CastSpell(me->GetVictim(), FLAME_WHIRL);
                        me->Yell("YOLO", LANG_UNIVERSAL);
                        task.Repeat(Seconds(25));
                    });

                    scheduler.Schedule(Seconds(5), 1, [this](TaskContext task)
                    {
                        Aura* shield = me->GetAura(FIRE_SHIELD);
                        if (!shield)
                        {
                            if ((shield = me->AddAura(FIRE_SHIELD, me)))
                            {
                                shield->SetMaxDuration(3600 * IN_MILLISECONDS);
                                shield->SetDuration(shield->GetMaxDuration());
                            }
                        }
                        task.Repeat();
                    });
                }
            default:
                break;
            }
        }

        void KilledUnit(Unit* victim) override
        {
            if (!fireblastCooldown)
                fireblastCooldown = COOLDOWN_FIREBLAST;

            if (victim->GetTypeId() != TYPEID_PLAYER)
                return;

            me->Yell(victim->GetName() + " is not illuminati ._.", LANG_UNIVERSAL);
            std::list<Player*> players;
            GetPlayerList(players, 50.0f);
            for (auto plr : players)
            {
                if (!fireblastCooldown)
                    me->CastSpell(plr, FIRE_BLAST, true);
                me->CastSpell(plr, SOUL_BURN, true);
            }
            Aura* flameblade = me->GetAura(FLAMEBLADE);
            if (!flameblade)
            {
                flameblade = me->AddAura(FLAMEBLADE, me);
                if (flameblade) // if target is dead, there is no aura added, it might die because of DoT or something like that
                    flameblade->GetEffect(EFFECT_0)->SetAmount(FLAMEBLADE_STACK_BONUS);
            }
            else
            {
                AuraEffect* eff = flameblade->GetEffect(EFFECT_0);
                eff->SetAmount(FLAMEBLADE_STACK_BONUS + eff->GetAmount());
            }
        }

        class YoloEvent : public BasicEvent
        {
        public:
            YoloEvent(Creature* troll) : BasicEvent() { this->troll = troll; }
            bool Execute(uint64, uint32) override
            {

                std::list<Player*> players;
                ::GetPlayerList(troll, players, 50.0f);
                for (auto plr : players)
                    plr->Yell("YOLO TROLO!", LANG_UNIVERSAL);
                return true;
            }
            Creature* troll;
        };

        void JustDied(Unit*) override
        {
            me->Yell("Illuminati cnfrmd!", LANG_UNIVERSAL);
            me->m_Events.AddEvent(new YoloEvent(me), me->m_Events.CalculateTime(2 * IN_MILLISECONDS));
            Reset();
        }

        void UpdateAI(uint32 diff) override
        {
            scheduler.Update(diff, [this](){DoMeleeAttackIfReady();});

            if (!UpdateVictim())
                return;

            if (fireblastCooldown > diff)
                fireblastCooldown -= diff;
            else
                fireblastCooldown = 0;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new boss_trollAI(creature);
    }
};

class boss_troll_npc_elemental : public CreatureScript
{
public:
    boss_troll_npc_elemental() : CreatureScript("boss_troll_npc_elemental") { }
    struct boss_troll_npc_elementalAI : public CreatureAI
    {
        uint8 counter;
        uint32 timer;
        uint32 delay;
        
        enum Spells
        {
            FIREBALL_VALLEY = 29922,
            FLAME_BUFFET    = 64016,
        };

        boss_troll_npc_elementalAI(Creature* creature) : CreatureAI(creature)
        {

        }

        void JustRespawned() override
        {
            Reset();
        }

        void Reset() override
        {
            timer = 5000;
            delay = 0;
            counter = 0;
            std::string rng = "aeiou";
            std::string text;
            for (uint8 i = 0; i < 10; ++i)
                text += rng[urand(0, 4)];
            me->Yell(text, LANG_UNIVERSAL);
            Player* victim = me->SelectNearestPlayer(50.0f);
            me->Attack(victim, false);
        }

        void UpdateAI(uint32 diff) override
        {
            if (delay < diff)
            {
                me->CastSpell(me, FIREBALL_VALLEY, false, nullptr, nullptr, me->GetGUID());
                delay += timer;
                if (counter++ == 4)
                {
                    if (Unit* victim = me->GetVictim())
                        me->AddAura(FLAME_BUFFET, victim);
                    counter = 0;
                }
            }
            else
                delay -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new boss_troll_npc_elementalAI(creature);
    }
};

void AddSC_boss_troll()
{
    new boss_troll();
    new boss_troll_npc_elemental();
}

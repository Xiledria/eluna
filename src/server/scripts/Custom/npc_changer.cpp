#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "WorldSession.h"

enum ChangerEntries
{
    CHANGE_CUSTOMIZE,
    CHANGE_RACE,
    CHANGE_FACTION_TO_HORDE,
    CHANGE_FACTION_TO_ALLIANCE,
};

struct ChangerEntry
{
    uint32 goldFUN;
    uint32 goldBG;
    uint32 em;
    std::string text;
};

class ChangerMgr
{
    bool loaded = false;
public:
private:
    std::map<uint8, ChangerEntry> ChangerMap;
public:
    uint32 references;
    ChangerMgr()
    {
        Load();
    }

    std::map<uint8, ChangerEntry> GetChangerMap()
    {
        return ChangerMap;
    }

    bool IsLoaded()
    {
        return loaded;
    }

    void Reload()
    {
        loaded = false;
        Load();
    }

    void Load()
    {
        std::map<uint8, ChangerEntry> ChangerMap;
        QueryResult result = WorldDatabase.Query("SELECT id, gold_fun, gold_bg, em, allowed, comment FROM npc_changer");
        if (!result)
            return;

        do
        {
            Field *fields = result->Fetch();
            uint8 id = fields[0].GetInt8();
            if (id > 4)
                continue;

            if (!fields[4].GetBool())
                continue;

            auto& entry = ChangerMap[id];
            entry.goldFUN = fields[1].GetUInt32();
            entry.goldBG = fields[2].GetUInt32();
            entry.em = fields[3].GetUInt32();
            entry.text = fields[5].GetString();
        } while (result->NextRow());
        ChangerMap.swap(this->ChangerMap);
        loaded = true;
    }
};

std::mutex changerMgrLock;
ChangerMgr* changerMgr = nullptr;
void InitChangerMgr()
{
    std::lock_guard<std::mutex> guard(changerMgrLock);
    if (!changerMgr)
        changerMgr = new ChangerMgr();
    ++changerMgr->references;
}

void RemoveChangerReference()
{
    std::lock_guard<std::mutex> guard(changerMgrLock);
    if (!--changerMgr->references)
    {
        delete changerMgr;
        changerMgr = nullptr;
    }
}

class npc_changer : public CreatureScript
{
public:
    npc_changer() : CreatureScript("npc_changer")
    {
        InitChangerMgr();
    }

    ~npc_changer()
    {
        RemoveChangerReference();
    }

    bool CanShow(Player* plr)
    {
        if (plr->IsInCombat())
        {
            plr->GetSession()->SendNotification("You are in combat!");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        else if (!changerMgr->IsLoaded())
        {
            plr->GetSession()->SendNotification("Changer reloading, try again!");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        return true;
    }

    bool OnGossipHello(Player* plr, Creature* me)
    {
        if (!CanShow(plr))
            return true;

        if (plr->IsGameMaster())
            AddGossipItemFor(plr, 0, "~RELOAD CHANGER DATA~\n", GOSSIP_SENDER_MAIN, 666);

        for (auto& cm : changerMgr->GetChangerMap())
        {
            if (cm.first == CHANGE_FACTION_TO_HORDE && plr->GetTeamId() != TEAM_ALLIANCE)
                continue;

            if (cm.first == CHANGE_FACTION_TO_ALLIANCE && plr->GetTeamId() != TEAM_HORDE)
                continue;

            std::string text = std::to_string(plr->GetRealmId() == realm.Id.Realm ? cm.second.goldFUN : cm.second.goldBG) + "G "
                + std::to_string(cm.second.em) + "EM : " + cm.second.text;
            GossipMenu& menu = plr->PlayerTalkClass->GetGossipMenu();
            menu.AddMenuItem(-1, GOSSIP_ICON_MONEY_BAG, text, GOSSIP_SENDER_MAIN, cm.first, text, 0);
        }

        plr->PlayerTalkClass->SendGossipMenu(1, me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* plr, Creature* me, uint32 /*sender*/, uint32 action)
    {
        if (!CanShow(plr))
            return true;

        if (action == 666 && plr->IsGameMaster())
        {
            changerMgr->Reload();
            plr->PlayerTalkClass->SendCloseGossip();
            plr->GetSession()->SendNotification("Reloaded..");
            return true;
        }

        auto changerMap = changerMgr->GetChangerMap();
        auto itr = changerMap.find(action);
        if (itr == changerMap.end())
        {
            plr->PlayerTalkClass->SendCloseGossip();
            return true;
        }

        ChangerEntry& entry = itr->second;
        if (entry.em) // skip item check if no EM are required
        if (!plr->HasItemCount(Deffender::EVENT_MARK, entry.em))
        {
            me->Whisper("Nemas dostatek event marek!", LANG_UNIVERSAL, plr, true);
            plr->PlayerTalkClass->SendCloseGossip();
            return true;
        }
        int32 gold = (plr->GetRealmId() == realm.Id.Realm ? entry.goldFUN : entry.goldBG) * GOLD;

        if (!plr->HasEnoughMoney(gold))
        {
            me->Whisper("Nemas dostatek goldu!", LANG_UNIVERSAL, plr, true);
            plr->PlayerTalkClass->SendCloseGossip();
            return true;
        }
        plr->DestroyItemCount(Deffender::EVENT_MARK, entry.em, true);
        plr->ModifyMoney(-gold);

        switch (itr->first)
        {
        case CHANGE_CUSTOMIZE:
            plr->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
            break;
        case CHANGE_RACE:
            plr->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
            break;
        case CHANGE_FACTION_TO_HORDE:
        case CHANGE_FACTION_TO_ALLIANCE:
            plr->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
            break;
        default:
            plr->PlayerTalkClass->SendCloseGossip();
            return true;
        }
        me->Whisper("Relogni pro uplatneni zmen!", LANG_UNIVERSAL, plr, true);
        plr->PlayerTalkClass->SendCloseGossip();
        return true;
    }
};

void AddSC_npc_changer()
{
    new npc_changer();
}

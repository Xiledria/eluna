#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "ObjectMgr.h"
#include "MapManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SpellAuraEffects.h"

class box_spawner_base_class : public CreatureScript
{
public:

    box_spawner_base_class(const char* name) : CreatureScript(name) { }
};

struct box_spawner_base_classAI : public CreatureAI
{
#define BOX_ENTRY 400002
#define DELAYS_COUNT 11
#define OBJECTS_HOLE 20
#define DESPAWN_DELAY 1500
#define SAFE_FALL_AURA 18443
#define BUFF_INTERVAL 5 * IN_MILLISECONDS

    const uint16 delay[DELAYS_COUNT] = { 200, 300, 300, 350, 350, 400, 400, 400, 400, 400, 500 };
    Position last;
    std::vector<Position> waypoints;
    struct Box
    {
        Box(ObjectGuid guid, int16 delay)
        {
            this->guid = guid;
            this->delay = delay;
        }
        ObjectGuid guid;
        int16 delay;
    };
    std::list<Box> boxes;
    std::list<Box> deletingBoxes;
    uint16 currentDelay;
    uint16 spawnTimer;
    uint16 buffTimer;
    uint16 activeWP;
    Map* map;

    void DeleteBoxContainer(std::list<Box>& container, bool addDespawnDelay)
    {
        if (!me->FindMap()) // despawned
        {
            for (auto& box : container)
                if (GameObject* obj = map->GetGameObject(box.guid))
                    obj->Delete();
            return;
        }


        for (auto& box : container)
        {
            int16 delay = box.delay + (addDespawnDelay ? DESPAWN_DELAY : 0);
            int8 seconds = std::max(1, delay / 1000 + (delay % 1000 > 500 ? 1 : 0));
            if (GameObject* obj = map->GetGameObject(box.guid))
            {
                obj->SetRespawnTime(seconds);
                obj->SetSpellId(1);
                obj->SetLootState(GO_READY);

            }
        }
    }

    virtual ~box_spawner_base_classAI()
    {
        // save active waypoint
        me->SetMaxPower(POWER_HAPPINESS, activeWP);
        DeleteBoxContainer(boxes, true);
        DeleteBoxContainer(deletingBoxes, false);
    }

    box_spawner_base_classAI(Creature* creature, float x, float y, float z) : CreatureAI(creature), last(x, y, z)
    {
        buffTimer = BUFF_INTERVAL;
        map = me->GetMap();
        currentDelay = 500;
        spawnTimer = currentDelay;
        activeWP = me->GetMaxPower(POWER_HAPPINESS);
    }

    void AddWP()
    {
        waypoints.emplace_back(last);
    }

    void AddWPInDir(int8 x, int8 y, int8 z)
    {
        for (int8 i = 0; i < 10; ++i)
        {
            AddWP();
            last.m_positionX += static_cast<float>(x);
            last.m_positionY += static_cast<float>(y);
            last.m_positionZ += static_cast<float>(z);
        }

    }

    void UpdateBoxes(uint32 diff)
    {
        deletingBoxes.remove_if([diff, this](Box& box)
        {
            if (box.delay > static_cast<int16>(diff))
            {
                box.delay -= diff;
                return false;
            }
            else
            {
                GameObject* obj = map->GetGameObject(box.guid);
                if (obj)
                {
                    std::list<Player*> players;
                    obj->GetPlayerListInGrid(players, 0.2f);

                    for (auto p : players) // throw down
                        p->GetMotionMaster()->MovePoint(0, *p, false);
                    obj->Delete();
                }
                return true;
            }
        });

        boxes.remove_if([diff, this](Box& box)
        {
            if (box.delay > static_cast<int16>(diff))
            {
                box.delay -= diff;
                return false;
            }
            else
            {
                if (GameObject* obj = map->GetGameObject(box.guid))
                {
                    obj->SetLootState(GO_JUST_DEACTIVATED);
                    deletingBoxes.push_back(Box(box.guid, DESPAWN_DELAY));
                }
                return true;
            }
        });
    }

    GameObject* SpawnBox(const Position& p)
    {
        GameObject* object = new GameObject;
        if (!object->Create(map->GenerateLowGuid<HighGuid::GameObject>(), BOX_ENTRY, map, me->GetPhaseMask(), p, G3D::Quat(0.0f, 0.0f, 0.0f, 0.0f), 0, GO_STATE_READY))
        {
            delete object;
            return nullptr;
        }

        object->SetRespawnTime(5);
        object->SetSpawnedByDefault(false);
        map->AddToMap(object);
        return object;
    }

    void SpawnNextBoxes()
    {
        for (int16 i = activeWP; i >= 0; i -= OBJECTS_HOLE)
        {
            Position& p = waypoints[i];
            if (GameObject* obj = SpawnBox(p))
                boxes.emplace_back(obj->GetGUID(), (currentDelay != delay[0] ? currentDelay : delay[1]) * 4);
        }
        if (++activeWP >= waypoints.size())
            activeWP = activeWP - OBJECTS_HOLE;
        if (activeWP % 10 == 0)
            currentDelay = delay[irand(0, DELAYS_COUNT - 1)];
    }

    void UpdateBuff(uint32 diff)
    {
        if (buffTimer >= diff)
            buffTimer -= diff;
        else
        {
            buffTimer = BUFF_INTERVAL;
            std::list<Player*> players;
            me->GetPlayerListInGrid(players, 10.0f);
            for (auto p : players) // throw down
            {
                if (Aura* aur = me->AddAura(SAFE_FALL_AURA, p))
                    if (AuraEffect* eff1 = aur->GetEffect(EFFECT_0))
                        eff1->SetAmount(999);
            }
        }
    }

    void UpdateAI(uint32 diff) override
    {
        if (spawnTimer >= diff)
            spawnTimer -= diff;
        else
        {
            SpawnNextBoxes();
            spawnTimer += currentDelay;
            // lags might throw diff that is even higher than current delay
            if (spawnTimer >= diff)
                spawnTimer -= diff;
            else
                spawnTimer = 0;
        }
        UpdateBoxes(diff);
        UpdateBuff(diff);
    }
};

#include "npc_odmeny_za_sezonu.h"
#include "ScriptedGossip.h"
#include "Chat.h"
#include "Language.h"
#include "ObjectDefines.h"
#include "ObjectMgr.h"
#include "Player.h"

std::list<Odmena*> OdmenyZaSezonu::odmeny;
bool OdmenyZaSezonu::loaded;

void OdmenyZaSezonu::NactiOdmeny()
{
    QueryResult result = CharacterDatabase.Query("SELECT * FROM arena_odmeny ORDER BY guid");
    if (!result)
    {
        loaded = false;
        return;
    }

    odmeny.clear();

    do
    {
        Field* fields = result->Fetch();
        //guid hrace            id titulu               vyrizeno (0=ne, 1=ano)
        Odmena* odmena = new Odmena(fields[0].GetUInt32(), fields[1].GetUInt32(), fields[2].GetUInt32());
        odmeny.push_back(odmena);
    } while (result->NextRow());
    loaded = true;     
}

void OdmenyZaSezonu::AplikujOdmenu(Player* player, Creature* creature, uint32 titul)
{
    if (!loaded)
        return;
    if (!player)
        return;
    if (!creature)
        return;
    if (!titul || titul == 0)
        return;

    CharTitlesEntry const* titleInfo = sCharTitlesStore.LookupEntry(titul);
    if (!titleInfo)
    {
        creature->Whisper("CHYBA: Spatne ID titulu!",LANG_UNIVERSAL, player);
        return;
    }

    player->SetTitle(titleInfo);
    OdmenyZaSezonu::AplikujOdmenuDB(player, titul);
    OdmenyZaSezonu::AplikujOdmenuCore(player, titul);
    player->SaveToDB();

    creature->Whisper("Odmeny vybaveny.",LANG_UNIVERSAL, player);
    OdmenyZaSezonu::GossipHelloMainMenu(player, creature);
}

void OdmenyZaSezonu::AplikujOdmenuDB(Player* player, uint32 titul)
{
    CharacterDatabase.PExecute("UPDATE arena_odmeny SET done = '1' WHERE guid = '%u' AND title = '%u'", player->GetGUID().GetCounter(), titul);
}

void OdmenyZaSezonu::AplikujOdmenuCore(Player* player, uint32 titul)
{
    if (isEmpty())
        return;
    if (!loaded)
        return;
    if (!player)
        return;

    for (Odmeny::iterator itrBegin = odmeny.begin(), itrEnd = odmeny.end(); itrBegin != itrEnd; itrBegin++)
    {
        Odmena* odmena = *itrBegin;
        if (!odmena)
            continue;

        if (odmena->playerGUID == player->GetGUID().GetCounter() && odmena->title == titul)
        {
            odmeny.remove(odmena);
            odmena = new Odmena(player->GetGUID().GetCounter(), titul, 1);
            odmeny.push_back(odmena);
            return;
        }        
    }
}

void OdmenyZaSezonu::VypisOdmenyProHrace(Player* player)
{
    if (isEmpty())
        return;
    if (!loaded)
        return;
    if (!player)
        return;

    for (Odmeny::iterator itrBegin = odmeny.begin(), itrEnd = odmeny.end(); itrBegin != itrEnd; itrBegin++)
    {
        Odmena* odmena = *itrBegin;
        if (!odmena)
            continue;

        if (odmena->playerGUID == player->GetGUID() && odmena->done == 0)
        {
            CharTitlesEntry const* titleInfo = sCharTitlesStore.LookupEntry(odmena->title);
            if (!titleInfo)
            {
                AddGossipItemFor(player, 5, "Chybne ID titulu!", GOSSIP_SENDER_MAIN, MENU + 0);
                continue;
            }
            char titleNameStr[80];
            snprintf(titleNameStr, 80, player->getGender() == GENDER_MALE ? titleInfo->nameMale[player->GetSession()->GetSessionDbcLocale()] : titleInfo->nameFemale[player->GetSession()->GetSessionDbcLocale()], player->GetName().c_str());
            AddGossipItemFor(player, 8, titleNameStr, GOSSIP_SENDER_MAIN, TITUL + odmena->title);
        }
    }    
}

void OdmenyZaSezonu::VypisOdmenyProGM(Player* player, Creature* creature, uint32 nasobic)
{
    if (isEmpty())
        return;
    if (!loaded)
        return;
    if (!player)
        return;

    player->PlayerTalkClass->ClearMenus();
    uint32 start = 10 * nasobic - 10;
    uint32 fin = 10 * nasobic - 1;
    uint32 i = 0;
    for (Odmeny::iterator itrBegin = odmeny.begin(), itrEnd = odmeny.end(); itrBegin != itrEnd; itrBegin++)
    {
        if (i < start || i > fin)
        {
            i++;
            continue;
        }

        Odmena* odmena = *itrBegin;
        if (!odmena)
        {
            i++;
            continue;
        }

        QueryResult result = CharacterDatabase.PQuery("SELECT name FROM characters WHERE guid = %u", odmena->playerGUID);
        if (!result)
        {
            AddGossipItemFor(player, 8, OdmenyZaSezonu::BarevnyText(BARVA_FIALOVA, "Neexistujici character!") + " - " + OdmenyZaSezonu::UdelejText(odmena->playerGUID), GOSSIP_SENDER_MAIN, MENU + 0);
            i++;
            continue;
        }

        Field* fields = result->Fetch();
        std::string temp_nick = fields[0].GetString();

        CharTitlesEntry const* titleInfo = sCharTitlesStore.LookupEntry(odmena->title);
        if (!titleInfo)
        {
            AddGossipItemFor(player, 8, OdmenyZaSezonu::BarevnyText(BARVA_FIALOVA, "Chybne ID titulu!") + " - " + OdmenyZaSezonu::UdelejText(odmena->title), GOSSIP_SENDER_MAIN, MENU + 0);
            i++;
            continue;
        }
        char titleNameStr[80];
        snprintf(titleNameStr, 80, player->getGender() == GENDER_MALE ? titleInfo->nameMale[player->GetSession()->GetSessionDbcLocale()] : titleInfo->nameFemale[player->GetSession()->GetSessionDbcLocale()], temp_nick.c_str());
        std::string vyber = "";
        if (odmena->done == 0)
            vyber = OdmenyZaSezonu::BarevnyText(BARVA_CERVENA, "[NEVYBRANO]");
        else
        vyber = OdmenyZaSezonu::BarevnyText(BARVA_ZELENA, "[VYBRANO]");
        AddGossipItemFor(player, 8, OdmenyZaSezonu::UdelejText(odmena->playerGUID) + "\n" + OdmenyZaSezonu::BarevnyText(BARVA_SEDA, titleNameStr) + "\n" + vyber, GOSSIP_SENDER_MAIN, MENU_GM + nasobic);

        i++;
    }

    bool konecna;
    if ((fin + 1) >= odmeny.size())
        konecna = true;
    else
        konecna = false;

    if (nasobic != 1)
        AddGossipItemFor(player, 6, "< Strana vzad", GOSSIP_SENDER_MAIN, MENU_GM + (nasobic-1));
    if (!konecna)
        AddGossipItemFor(player, 6, "> Strana vpred", GOSSIP_SENDER_MAIN, MENU_GM + (nasobic + 1));
    AddGossipItemFor(player, 5, "<< Hlavni menu", GOSSIP_SENDER_MAIN, MENU + 0);
    SendGossipMenuFor(player, 3, creature->GetGUID());
}

void OdmenyZaSezonu::GossipHelloMainMenu(Player* player, Creature* creature)
{
    if (!player)
        return;
    if (!creature)
        return;

    player->PlayerTalkClass->ClearMenus();

    if (player->IsGameMaster())
    {
        AddGossipItemFor(player, 5, "[GM] Reload Odmen", GOSSIP_SENDER_MAIN, MENU + 1);
        AddGossipItemFor(player, 7, "[GM] Prehled titulu", GOSSIP_SENDER_MAIN, MENU_GM + 1);
    }

    OdmenyZaSezonu::VypisOdmenyProHrace(player);
    AddGossipItemFor(player, 5, "Odejit", GOSSIP_SENDER_MAIN, MENU + 2);
    SendGossipMenuFor(player, 3, creature->GetGUID());
}

std::string OdmenyZaSezonu::UdelejText(int32 hodnota)
{
    std::stringstream ss;
    std::string s;
    ss << hodnota;
    s = ss.str();

    return s;
}

std::string OdmenyZaSezonu::BarevnyText(uint32 barva, std::string text)
{
    std::stringstream ss;
    ss << "|cff";
    ss << Barvy[barva - 1];
    ss << text;
    ss << "|r";
    
    return ss.str();
}

class NactiOdmeny : public WorldScript
{
public:
    NactiOdmeny() : WorldScript("NactiOdmeny") { }

    void OnStartup()
    {
        OdmenyZaSezonu::NactiOdmeny();
    }
};

void AddSC_NactiOdmeny()
{
    new NactiOdmeny();
};

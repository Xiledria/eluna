#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Guild.h"
#include "Spell.h"

class npc_guild_vendor : public CreatureScript
{
public:
    npc_guild_vendor() : CreatureScript("npc_guild_vendor") { }

    bool OnGossipHello(Player *player, Creature *_creature)
    {
        if (!player || !_creature)
            return true;

        if (Guild* guild = player->GetGuild())
        {
            if (guild->HasLevelForBonus(GUILD_BONUS_VAULT))
                AddGossipItemFor(player, 5, "Dostat item Mobile Guild Vault.", GOSSIP_SENDER_MAIN, 1);
            /*if (guild->HasLevelForBonus(GUILD_BONUS_MOUNT_GRFLY))
            {
                nothing = false;
                AddGossipItemFor(player, 5, "Naucit se Fly Mounta.", GOSSIP_SENDER_MAIN, 3);
            }
            if (guild->HasLevelForBonus(GUILD_BONUS_MOUNT_GROUND_FLY))
            {
               AddGossipItemFor(player, 5, "Naucit se Ground/Fly Mounta.", GOSSIP_SENDER_MAIN, 4);
            }
            if (!guild->HasLevelForBonus(GUILD_BONUS_VAULT) && !guild->HasLevelForBonus(GUILD_BONUS_MOUNT_GRFLY) && !guild->HasLevelForBonus(GUILD_BONUS_MOUNT_GROUND_FLY))
            {
                AddGossipItemFor(player, 5, "Guilda nema dostatecny level.", GOSSIP_SENDER_MAIN, 5);
            }*/
            AddGossipItemFor(player, 5, "Zavrit.", GOSSIP_SENDER_MAIN, 5);
            SendGossipMenuFor(player, DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
            return true;
        } 

        AddGossipItemFor(player, 5, "Nejsem v Guilde.", GOSSIP_SENDER_MAIN, 5);
        SendGossipMenuFor(player, DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch (uiAction)
            {
            case 2: // Ground Mount
                CloseGossipMenuFor(player);
                if (!player->HasSpell(16059))
                {
                    player->LearnSpell(16059, false);
                    _creature->Whisper("Naucil jsi se Ground mounta.", LANG_UNIVERSAL, player, true);
                }
                else
                    _creature->Whisper("Tohoto mounta jiz umis.", LANG_UNIVERSAL, player, true);
                break;

            case 3: // Fly Mount
                CloseGossipMenuFor(player);
                if (!player->HasSpell(16060))
                {
                    player->LearnSpell(16060, false);
                    _creature->Whisper("Naucil jsi se Fly mounta.", LANG_UNIVERSAL, player, true);
                }
                else
                    _creature->Whisper("Tohoto mounta jiz umis.", LANG_UNIVERSAL, player, true);
                break;

            case 4: // Ground/Fly Mount
                CloseGossipMenuFor(player);
                if (!player->HasSpell(31700))
                {
                    player->LearnSpell(31700, false);
                    _creature->Whisper("Naucil jsi se Ground/Fly mounta.", LANG_UNIVERSAL, player, true);
                }
                else
                    _creature->Whisper("Tohoto mounta jiz umis.", LANG_UNIVERSAL, player, true);
                break;

            case 5: // Zavrit
                CloseGossipMenuFor(player);
                return true;

            default:
                break;
            }
        }
        return true;
    }
};

void AddSC_npc_guild_vendor()
{
    new npc_guild_vendor();
}

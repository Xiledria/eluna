#include "ScriptMgr.h"
#include "Battleground.h"
#include "BattlegroundMgr.h"
#include "ArenaTeam.h"
#include "Chat.h"
#include "Log.h"
#include "Player.h"
#include "ObjectAccessor.h"

class object_arena_crystal : public GameObjectScript
{
public:
    object_arena_crystal() : GameObjectScript("object_arena_crystal") { }

    bool OnGossipHello(Player* plr, GameObject* /*go*/)
    {
        if (!plr->GetBattleground() || !plr->GetBattleground()->isArena())
            return false;

        if (plr->HasAura(32727))
        {
            if (!plr->isSpectator())
                plr->m_clicked = true;

            Battleground* bg = plr->GetBattleground();
            uint8 numOfReadyPlrs = 0;
            for (Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().begin(); itr != bg->GetPlayers().end(); ++itr)
            {
                if (Player* plr = ObjectAccessor::FindPlayer(itr->first))
                {
                    if (plr->m_clicked == true)
                        ++numOfReadyPlrs;
                }
            }
            uint8 type = bg->GetArenaType();
            bool all = false;
            uint8 maxplrs = type * 2;
            if (numOfReadyPlrs == maxplrs)
                all = true;

            if (all)
                ChatHandler(plr->GetSession()).PSendSysMessage("Jsou pripraveni vsichni hraci.");
            else
                ChatHandler(plr->GetSession()).PSendSysMessage("Pocet pripravenych hracu: %u / %u.", numOfReadyPlrs, maxplrs);

            if (bg->GetStartDelayTime() <= 15 * IN_MILLISECONDS)
                plr->GetSession()->SendAreaTriggerMessage("Arena za chvili zacina");
            else
                plr->GetSession()->SendAreaTriggerMessage("Jste oznacen jako pripaven");
        }
        return true;
    }
};

void AddSC_object_arena_crystal()
{
    new object_arena_crystal();
}

#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "SpellAuraEffects.h"

struct grunt_guardian_base_classAI : public CreatureAI
{
    uint32 timer;
    uint32 delay;
    uint32 evadeTimer;
    struct StunnedPlayer
    {
        StunnedPlayer(Player* plr)
        {
            guid = plr->GetGUID();
            delay = 1000;
            phase = 0;
        }
        ObjectGuid guid;
        uint32 evadeTimer;
        uint32 delay;
        uint8 phase;
    };

    struct OrientedPoint
    {
        float x, y, z, o;
    };

    std::vector<OrientedPoint> points;
    std::list<StunnedPlayer> stunnedPlayers;
#define SPELL_CHARGE 74399
#define SPELL_STUN   54561
#define SPELL_GHOST  65250
    grunt_guardian_base_classAI(Creature* creature) : CreatureAI(creature)
    {
        timer = 1000;
        delay = timer;
        evadeTimer = 0;
        me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 30414);
    }

    ~grunt_guardian_base_classAI()
    {
        if (!me->FindMap())
            return;

        for (auto& splr : stunnedPlayers)
        {
            Player* plr = ObjectAccessor::FindPlayer(splr.guid);
            auto p = points.begin();
            std::advance(p, urand(0, static_cast<uint32>(points.size() - 1)));
            plr->NearTeleportTo(p->x, p->y, p->z, p->o);
        }
    }

    void GetVisiblePlayers(std::list<Player*>& players)
    {
        me->GetPlayerListInGrid(players, 15.0f);
        players.remove_if([this](Player* player)
        {
            if (player->IsGameMaster())
                return true;

            if (player->HasAura(SPELL_STUN))
                return true;

            if (!me->IsWithinLOSInMap(player))
                return true;

            if (me->isInFront(player))
                return false;

            return me->GetDistance(player) > 2.0f;
        });
    }

    virtual void HandleCatchPlayer(Player* plr)
    {
        plr->Dismount();
        me->CastSpell(plr, SPELL_CHARGE, true);
        plr->CastSpell(plr, SPELL_STUN, true);
    }

    void CatchPlayer(Player* plr)
    {
        HandleCatchPlayer(plr);
        stunnedPlayers.push_back(StunnedPlayer(plr));
        evadeTimer = 2000;
    }

    void UpdateAI(uint32 diff) override
    {
        stunnedPlayers.remove_if([&](StunnedPlayer& splr)
        {
            if (splr.delay > diff)
            {
                splr.delay -= diff;
                return false;
            }

            Player* plr = ObjectAccessor::FindPlayer(splr.guid);
            if (!plr)
                return true;

            if (!splr.phase)
            {
                Aura* aur = plr->AddAura(SPELL_GHOST, plr);
                if (!aur)
                    return true;

                aur->SetDuration(2500);
                splr.phase = 1;
                splr.delay = 1000;
                return false;
            }
            else
            {
                auto p = points.begin();
                std::advance(p, urand(0, static_cast<uint32>(points.size() - 1)));
                plr->NearTeleportTo(p->x, p->y, p->z, p->o);
                return true;
            }
        });

        if (evadeTimer)
        {
            if (evadeTimer > diff)
            {
                evadeTimer -= diff;
            }
            else
            {
                evadeTimer = 0;
                EnterEvadeMode();
            }
        }

        if (delay > diff)
        {
            delay -= diff;
            return;
        }

        delay += timer;

        std::list<Player*> players;
        GetVisiblePlayers(players);
        for (auto plr : players)
            CatchPlayer(plr);
    }
};

#include "DeffenderXmas.h"
#include "ScriptMgr.h"
#include "GameObjectAI.h"
#include "ObjectAccessor.h"
#include "TaskScheduler.h"
#include "Player.h"
#include "SpellAuraEffects.h"
#include "TemporarySummon.h"

enum privateXmasEnum
{
    ELY_LAYI_ACTION_SET_LEAVING     =       0,
    ELY_LAYI_NPC_ALENKA             =  450250,
    ELY_LAYI_QUEST_FIND_RUDOLF      =   40107,
    ELY_LAYI_NPC_SNOWMAN_CHILD      =  450261,
    ELY_LAYI_CHECKPOINT_AURA        =  100047,
    ELY_LAYI_CHECKPOINT_ID_FIRST    = 2200162,
    ELY_LAYI_CHECKPOINT_ID_LAST     = 2200182
};

class ElyLayiXmasGift : GameObjectScript
{
public:
    ElyLayiXmasGift() : GameObjectScript("ElyLayiXmasGift") { }
    struct ElyLayiXmasGiftAI : GameObjectAI
    {
        ElyLayiXmasGiftAI(GameObject* object) : GameObjectAI(object)
        {

        }

        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
                return !player->IsGameMaster() && player->HasAura(Deffender::Xmas::DEFFENDER_GIFT_AURA);
            return true;
            return false;
        }

        void OnStateChanged(uint32 state, Unit* looter) override
        {
            if (!looter || state != GO_ACTIVATED)
                return;

            Player* plrLooter = looter->ToPlayer();
            if (!plrLooter)
                return;

            if (Deffender::Xmas::GenerateGift(plrLooter))
            {
                looter->AddAura(Deffender::Xmas::DEFFENDER_GIFT_AURA, looter);
                looter->UpdateObjectVisibility();
            }
        }

    };

    GameObjectAI* GetAI(GameObject* object) const override
    {
        return new ElyLayiXmasGiftAI(object);
    }
};

class ElyLayiXmasQuestGiver : CreatureScript
{
public:
    ElyLayiXmasQuestGiver(const char* scriptName) : CreatureScript(scriptName) { }
    virtual bool OnQuestReward(Player* player, Creature* me, const Quest*, uint32) override
    {
        if (Deffender::Xmas::GenerateGift(player))
            me->SaySingleTarget("Veselé Vánoce!", LANG_UNIVERSAL, player);
        return false;
    }
};

class ElyLayiXmasSnowman : ElyLayiXmasQuestGiver
{
public:
    ElyLayiXmasSnowman() : ElyLayiXmasQuestGiver("ElyLayiXmasSnowman") { }
    bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32 rewardIndex) override
    {
        if (Creature* junior = me->SummonCreature(ELY_LAYI_NPC_SNOWMAN_CHILD, *me, TEMPSUMMON_TIMED_DESPAWN, 5 * MINUTE * IN_MILLISECONDS))
        {
            junior->SetRespawnRadius(8.0f);
            junior->SetDefaultMovementType(RANDOM_MOTION_TYPE);
            junior->GetMotionMaster()->Initialize();
        }
        return ElyLayiXmasQuestGiver::OnQuestReward(player, me, quest, rewardIndex);
    }
};

struct ElyLayiXmasWintergraspReindeerAI : CreatureAI
{
    bool leaving = false;

    ElyLayiXmasWintergraspReindeerAI(Creature* creature) : CreatureAI(creature) { }

    bool IsNeverVisibleFor(const WorldObject* seer) override
    {
        if (leaving)
            return false;

        if (const Player* player = seer->ToPlayer())
        {
            if (player->IsGameMaster())
                return false;

            return player->GetQuestStatus(ELY_LAYI_QUEST_FIND_RUDOLF) == QUEST_STATUS_REWARDED;
        }
        return true;
    }

    void DoAction(int32) override
    {
        leaving = true;
    }

    void UpdateAI(uint32) override { }
};

class ElyLayiXmasGiftAlenka : CreatureScript
{
public:
    ElyLayiXmasGiftAlenka() : CreatureScript("ElyLayiXmasGiftAlenka") { }
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new ElyLayiXmasWintergraspReindeerAI(creature);
    }
};

class ElyLayiXmasRudolf : ElyLayiXmasQuestGiver
{
public:
    ElyLayiXmasRudolf() : ElyLayiXmasQuestGiver("ElyLayiXmasRudolf") { }
    struct ElyLayiXmasRudolfAI : ElyLayiXmasWintergraspReindeerAI
    {
        TaskScheduler scheduler;
        ObjectGuid alenkaGuid;
        ElyLayiXmasRudolfAI(Creature* creature) : ElyLayiXmasWintergraspReindeerAI(creature)
        {
            Position pos = me->GetRandomNearPosition(10.0f);
            if (Creature* alenka = me->SummonCreature(ELY_LAYI_NPC_ALENKA, pos))
            {
                alenkaGuid = alenka->GetGUID();
                me->SetFacingToObject(alenka);
                alenka->SetFacingToObject(me);
            }
        }

        void sQuestReward(Player* player, const Quest*, uint32) override
        {
            leaving = true;
            if (Creature* alenka = ObjectAccessor::GetCreature(*me, alenkaGuid))
                alenka->GetAI()->DoAction(ELY_LAYI_ACTION_SET_LEAVING);
            me->Whisper("$GNasel:Nasla; si Rudolfa, vrat se za Santou!", LANG_UNIVERSAL, player, true);
            scheduler.Schedule(Seconds(5), 1, [this](TaskContext task)
            {
                me->Say("Dobrá tedy, vrátím se za Santou.", LANG_UNIVERSAL);
                task.Schedule(Seconds(5), 1, [this](TaskContext task)
                {
                    me->Say("Promiň Alenko, ale beze mě by nebyly Vánoce, což nemůžu dopustit!", LANG_UNIVERSAL);
                    task.Schedule(Seconds(5), 1, [this](TaskContext task)
                    {
                        me->Say("Slibuji ale, že se za tebou vrátím, jen co skončí Vánoce!", LANG_UNIVERSAL);
                        task.Schedule(Seconds(5), 1, [this](TaskContext)
                        {
                            Position leavePos = me->GetRandomNearPosition(50.0f);
                            leavePos.m_positionZ += 50.0f;
                            me->GetMotionMaster()->MovePoint(0, leavePos, false);
                            if (Creature* alenka = ObjectAccessor::GetCreature(*me, alenkaGuid))
                            {
                                alenka->Say("Já tu nezůstanu sama! Půjdu s tebou, Rudolfe!", LANG_UNIVERSAL);
                                alenka->DespawnOrUnsummon(Seconds(5));
                                alenka->GetMotionMaster()->MovePoint(0, leavePos, false);
                            }
                            me->DespawnOrUnsummon(Seconds(5));
                        });
                    });
                });
            });
        }

        void UpdateAI(uint32 diff) override
        {
            scheduler.Update(diff);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new ElyLayiXmasRudolfAI(creature);
    }
};

class ElyLayiXmasSavedReindeer : CreatureScript
{
public:
    ElyLayiXmasSavedReindeer() : CreatureScript("ElyLayiXmasSavedReindeer") { }
    struct ElyLayiXmasSavedReindeerAI : CreatureAI
    {
        ElyLayiXmasSavedReindeerAI(Creature* creature) : CreatureAI(creature) { }
        void UpdateAI(uint32) override { }
        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
            {
                if (player->IsGameMaster())
                    return false;

                return player->GetQuestStatus(ELY_LAYI_QUEST_FIND_RUDOLF) != QUEST_STATUS_REWARDED;
            }
            return true;
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new ElyLayiXmasSavedReindeerAI(creature);
    }
};

class ElyLayiXmasSantaHelper : CreatureScript
{
public:
    ElyLayiXmasSantaHelper() : CreatureScript("ElyLayiXmasSantaHelper") { }
    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (Aura* check = player->GetAura(ELY_LAYI_CHECKPOINT_AURA))
        {
            AuraEffect* counter = check->GetEffect(EFFECT_0);
            for (uint8 i = 0; i <= ELY_LAYI_CHECKPOINT_ID_LAST - ELY_LAYI_CHECKPOINT_ID_LAST; ++i)
            {
                if ((counter->GetAmount() & (1 << i)) == 0)
                {
                    me->SaySingleTarget("Neprošel si celou skákačku, podvodníci odměnu nedostanou!", LANG_UNIVERSAL, player);
                    return true;
                }
            }
        }

        player->KilledMonsterCredit(me->GetEntry());
        me->SaySingleTarget("Děkuji ti za záchranu!", LANG_UNIVERSAL, player);
        me->DespawnOrUnsummon(Seconds(5), Seconds(30));
        return true;
    }
};

class ElyLayiXmasCheckpoint : GameObjectScript
{
public:
    ElyLayiXmasCheckpoint() : GameObjectScript("ElyLayiXmasCheckpoint") { }
    struct ElyLayiXmasCheckpointAI : GameObjectAI
    {
        ElyLayiXmasCheckpointAI(GameObject* obj) : GameObjectAI(obj) { }
        void UpdateAI(uint32) override
        {
            std::list<Player*> players;
            go->GetPlayerListInGrid(players, 1.5f);
            for (Player* player : players)
            {
                // required for 3D check, because playerlistingrid returns 2D check for distance
                if (player->GetDistance(go) < 1.5f)
                {
                    int32 amount = 1 << (go->GetEntry() - ELY_LAYI_CHECKPOINT_ID_FIRST);
                    Aura* check = player->GetAura(ELY_LAYI_CHECKPOINT_AURA);
                    if (!check)
                        check = player->AddAura(ELY_LAYI_CHECKPOINT_AURA, player);

                    AuraEffect* counter = check->GetEffect(EFFECT_0);
                    counter->SetAmount(counter->GetAmount() | amount);
                }
            }
        }
    };

    GameObjectAI* GetAI(GameObject* obj) const override
    {
        return new ElyLayiXmasCheckpointAI(obj);
    }
};

class ElyLayiXmasSilenceHandler : GameObjectScript
{
public:
    ElyLayiXmasSilenceHandler() : GameObjectScript("ElyLayiXmasSilenceHandler") { }
    struct ElyLayiXmasSilenceHandlerAI : GameObjectAI
    {
        ElyLayiXmasSilenceHandlerAI(GameObject* obj) : GameObjectAI(obj) { }
        uint32 timer = 500;
        uint32 delay = 0;
        void UpdateAI(uint32 diff) override
        {
            if (delay > diff)
            {
                delay -= diff;
                return;
            }
            delay += timer;

            std::list<Player*> players;
            go->GetPlayerListInGrid(players, 25.0f);
            for (Player* player : players)
                player->AddAura(ELY_LAYI_CHECKPOINT_AURA, player);
        }
    };

    GameObjectAI* GetAI(GameObject* obj) const override
    {
        return new ElyLayiXmasSilenceHandlerAI(obj);
    }
};

void AddSC_ely_layi_xmas()
{
    new ElyLayiXmasGift();
    new ElyLayiXmasQuestGiver("ElyLayiXmasQuestGiver");
    new ElyLayiXmasGiftAlenka();
    new ElyLayiXmasRudolf();
    new ElyLayiXmasSavedReindeer();
    new ElyLayiXmasSnowman();
    new ElyLayiXmasSantaHelper();
    new ElyLayiXmasCheckpoint();
    new ElyLayiXmasSilenceHandler();
}

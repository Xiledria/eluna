#include "labyrinth_base_class.h"
#include "ScriptedGossip.h"
#include "ObjectAccessor.h"
#include "ScriptMgr.h"
#include "Player.h"

namespace event_383
{
    uint32 event_383_size;
    uint32 event_383_floors;
    uint32 event_383_floorHeight;
    std::mutex event_383_lock;

    void SetValues(uint32 size, uint32 floors, uint32 floorHeight)
    {
        event_383_lock.lock();
        event_383_size = size;
        event_383_floors = floors;
        event_383_floorHeight = floorHeight;
    }

    CreatureAI* GetLabyrintAI(Creature* creature)
    {
        CreatureAI* AI = new labyrint_base_classAI(creature, event_383_size, 0, true, true, event_383_floors, event_383_floorHeight);
        event_383_lock.unlock();
        return AI;
    }

    struct event_383_handler : public CreatureScript
    {
    public:
        event_383_handler() : CreatureScript("event_383_handler") { }
        struct event_383_handlerAI : CreatureAI
        {
            uint32 size;
            uint32 floors;
            uint32 floorHeight;
            ObjectGuid trigger;
            void UpdateAI(uint32) override { }
            event_383_handlerAI(Creature* creature) : CreatureAI(creature)
            {
                size = 27;
                floors = 2;
                floorHeight = 1;
            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {

                if (const Player* player = seer->ToPlayer())
                    return !player->IsGameMaster();
                return true;
            }

            void sGossipHello(Player* player) override
            {
                int32 i = 1;
                std::string sizestr = "nastavit velikost bludiste " + std::to_string(size);
                std::string floorstr = "nastavit pocet pater " + std::to_string(floors);
                std::string heightstr = "nastavit vysku pater " + std::to_string(floorHeight);
                ClearGossipMenuFor(player);
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, sizestr, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i++, sizestr, 0, true);
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, floorstr, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i++, floorstr, 0, true);
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, heightstr, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i++, heightstr, 0, true);
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, !trigger ? "spawn bludiste" : "despawn bludiste", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i++);
                SendGossipMenuFor(player, 1, me);
            }

            void sGossipSelect(Player* player, uint32, uint32 menuId) override
            {
                if (menuId == 3)
                {
                    if (trigger)
                    {
                        if (Creature* spawner = ObjectAccessor::GetCreature(*me, trigger))
                            spawner->DespawnOrUnsummon();
                        trigger = ObjectGuid::Empty;
                        me->SaySingleTarget("Bludiste bylo despawnute!", LANG_UNIVERSAL, player);
                    }
                    else
                    {
                        float x,y,z,o;
                        me->GetPosition(x,y,z,o);
                        SetValues(size, floors, floorHeight);
                        if (Creature* spawner = me->SummonTrigger(x,y,z,o, 0, GetLabyrintAI))
                            trigger = spawner->GetGUID();
                        else
                            event_383_lock.unlock();
                        me->SaySingleTarget("Nove bludiste bylo spawnute!", LANG_UNIVERSAL, player);
                    }

                    CloseGossipMenuFor(player);
                    return;
                }
            }

            void sGossipSelectCode(Player* player, uint32, uint32 menuId, const char* code) override
            {
                int32 value = atoi(code);
                if (value < 0)
                {
                    me->SaySingleTarget("Neplatna hodnota! Zadejte kladne cislo!", LANG_UNIVERSAL, player);
                    return;
                }
                std::string txt;
                switch(menuId)
                {
                    case 0:
                        txt = "velikost bludiste";
                        if (value < 3)
                            value = 3;
                        size = value;
                        break;
                    case 1:
                        txt = "pocet pater";
                        if (value == 0)
                            value = 1;
                        floors = value;
                        break;
                    case 2:
                        if (value == 0)
                            value = 1;
                        txt = "vyska patra";
                        floorHeight = value;
                        break;
                }

                txt += " = " + std::to_string(value);
                me->SaySingleTarget(txt, LANG_UNIVERSAL, player);
                sGossipHello(player);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_383_handlerAI(creature);
        }
    };
}

void AddSC_event_383()
{
    new event_383::event_383_handler();
}


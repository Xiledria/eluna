/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: achievement_commandscript
%Complete: 100
Comment: All achievement related commands
Category: commandscripts
EndScriptData */

#include "Chat.h"
#include "ScriptMgr.h"
#include "Player.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

namespace Trinity
{
    class EmoteChatBuilder
    {
        public:
            EmoteChatBuilder(Player const& player, uint32 text_emote, uint32 emote_num, Unit const* target)
                : i_player(player), i_text_emote(text_emote), i_emote_num(emote_num), i_target(target) { }

            void operator()(WorldPacket& data, LocaleConstant loc_idx)
            {
                std::string const name(i_target ? i_target->GetNameForLocaleIdx(loc_idx) : "");
                uint32 namlen = static_cast<uint32>(name.size());

                data.Initialize(SMSG_TEXT_EMOTE, 20 + namlen);
                data << i_player.GetGUID();
                data << uint32(i_text_emote);
                data << uint32(i_emote_num);
                data << uint32(namlen);
                if (namlen > 1)
                    data << name;
                else
                    data << uint8(0x00);
            }

        private:
            Player const& i_player;
            uint32        i_text_emote;
            uint32        i_emote_num;
            Unit const*   i_target;
    };
}


class trolling_commandscript : public CommandScript
{
public:
    trolling_commandscript() : CommandScript("bg_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> playerTrollingCommandTable =
        {
            { "emote", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingPlayerEmote, "" },
            { "say", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingPlayerSay, "" },
            { "textemote", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingPlayerTextEmote, "" },
            { "transmogrify", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingPlayerTransmogrify, "" },
            { "yell", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingPlayerYell, "" },
        };
        static std::vector<ChatCommand> trollingCommandTable =
        {
            { "evadeall", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingEvadeAll, "" },
            { "followall", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingFollowAll, "" },
            { "gm", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingGMCommand, "" },
            { "player", rbac::RBAC_PERM_TROLLING, false, nullptr, "", playerTrollingCommandTable },
            { "transmogrifyall", rbac::RBAC_PERM_TROLLING, false, &HandleTrollingTransmogrifyAll, "" },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "trolling", rbac::RBAC_PERM_TROLLING,  false, nullptr, "", trollingCommandTable },
        };
        return commandTable;
    }

    static bool HandleTrollingGMCommand(ChatHandler* handler, const char* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                if (Player* target = plr->GetSelectedPlayer())
                {
                    if (args && *args)
                    {
                        ChatHandler(target->GetSession()).ParseCommands(args);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static void TransmogrifyPlayer(Player* player, uint8 slot, uint32 itemEntry)
    {
        player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID + (slot * 2), itemEntry);
    }

    static bool HandleTrollingPlayerTransmogrify(ChatHandler* handler, const char* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                Player* selectedPlayer = plr->GetSelectedPlayer();
                if (!selectedPlayer)
                    return false;

                uint8 slot = 0;
                uint32 itemEntry = 0;
                if (char* str = strtok(const_cast<char*>(args), " "))
                    slot = static_cast<uint8>(atoi((char*)str));

                if (slot > 18)
                    return false;

                if (char* str = strtok(nullptr, " "))
                    itemEntry = static_cast<uint32>(atoi((char*)str));

                TransmogrifyPlayer(selectedPlayer, slot, itemEntry);
                return true;
            }
        }
        return false;
    }

    static bool HandleTrollingTransmogrifyAll(ChatHandler* handler, const char* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                float range = 0.0f;
                uint8 slot = 0;
                uint32 itemEntry = 0;
                if (char* str = strtok(const_cast<char*>(args), " "))
                    range = static_cast<float>(atof((char*)str));
                else return false;
                if (char* str = strtok(nullptr, " "))
                    slot = static_cast<uint8>(atoi((char*)str));

                if (slot > 18)
                    return false;

                if (char* str = strtok(nullptr, " "))
                    itemEntry = static_cast<uint32>(atoi((char*)str));

                std::list<Player*> players;
                Trinity::AnyUnitInObjectRangeCheck u_check(plr, range);
                Trinity::PlayerListSearcher<Trinity::AnyUnitInObjectRangeCheck> searcher(plr, players, u_check);
                plr->VisitNearbyObject(range, searcher);
                for (Player* selectedPlayer : players)
                    TransmogrifyPlayer(selectedPlayer, slot, itemEntry);
                return true;
            }
        }
        return false;
    }

    static bool HandleTrollingPlayerEmote(ChatHandler* handler, char const* args)
    {
        uint32 text_emote = atoi(args);
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                if (Player* target = plr->GetSelectedPlayer())
                {
                    EmotesTextEntry const* em = sEmotesTextStore.LookupEntry(text_emote);
                    if (!em)
                        return false;

                    uint32 emote_anim = em->textid;
                    switch (emote_anim)
                    {
                        case EMOTE_STATE_SLEEP:
                        case EMOTE_STATE_SIT:
                        case EMOTE_STATE_KNEEL:
                        case EMOTE_ONESHOT_NONE:
                            break;
                        default:
                            // Only allow text-emotes for "dead" entities (feign death included)
                            if (target->HasUnitState(UNIT_STATE_DIED))
                                break;
                            target->HandleEmoteCommand(emote_anim);
                            break;
                    }

                    Unit* unit = target->GetSelectedUnit();
                    CellCoord p = Trinity::ComputeCellCoord(target->GetPositionX(), target->GetPositionY());

                    Cell cell(p);
                    cell.SetNoCreate();
                    Trinity::EmoteChatBuilder emote_builder(*target, text_emote, text_emote, unit);
                    Trinity::LocalizedPacketDo<Trinity::EmoteChatBuilder > emote_do(emote_builder);
                    Trinity::PlayerDistWorker<Trinity::LocalizedPacketDo<Trinity::EmoteChatBuilder > > emote_worker(target, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_TEXTEMOTE), emote_do);
                    TypeContainerVisitor<Trinity::PlayerDistWorker<Trinity::LocalizedPacketDo<Trinity::EmoteChatBuilder> >, WorldTypeMapContainer> message(emote_worker);
                    cell.Visit(p, message, *target->GetMap(), *target, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_TEXTEMOTE));
                }
                else
                    return false;
            }
        }
        return true;
    }

    static bool HandleTrollingPlayerSay(ChatHandler* handler, char const* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                if (Player* target = plr->GetSelectedPlayer())
                    target->Say(args, LANG_UNIVERSAL);
                else
                    return false;
            }
        }
        return true;
    }

    static bool HandleTrollingPlayerTextEmote(ChatHandler* handler, char const* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                if (Player* target = plr->GetSelectedPlayer())
                    target->TextEmote(args);
                else
                    return false;
            }
        }
        return true;
    }

    static bool HandleTrollingPlayerYell(ChatHandler* handler, char const* args)
    {
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                if (Player* target = plr->GetSelectedPlayer())
                    target->Yell(args, LANG_UNIVERSAL);
                else
                    return false;
            }
        }
        return true;
    }

    static bool HandleTrollingFollowAll(ChatHandler* handler, char const* args)
    {
        float range = static_cast<float>(atof((char*)args));
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                std::list<Creature*> creatures;
                Trinity::AnyUnitInObjectRangeCheck u_check(plr, range);
                Trinity::CreatureListSearcher<Trinity::AnyUnitInObjectRangeCheck> searcher(plr, creatures, u_check);
                plr->VisitNearbyObject(range, searcher);
                for (Creature* creature : creatures)
                    creature->GetMotionMaster()->MoveFollow(plr, PET_FOLLOW_DIST, creature->GetFollowAngle());
                handler->PSendSysMessage("%u creatures is now following you.", creatures.size());
            }
        }
        return true;
    }

    static bool HandleTrollingEvadeAll(ChatHandler* handler, char const* args)
    {
        float range = static_cast<float>(atof((char*)args));
        if (WorldSession* session = handler->GetSession())
        {
            if (Player* plr = session->GetPlayer())
            {
                std::list<Creature*> creatures;
                Trinity::AnyUnitInObjectRangeCheck u_check(plr, range);
                Trinity::CreatureListSearcher<Trinity::AnyUnitInObjectRangeCheck> searcher(plr, creatures, u_check);
                plr->VisitNearbyObject(range, searcher);
                for (Creature* creature : creatures)
                {
                    creature->ClearUnitState(UNIT_STATE_EVADE);
                    if (CreatureAI* AI = creature->AI())
                        AI->EnterEvadeMode();
                    else if (Unit* owner = creature->GetCharmerOrOwner())
                    {
                        creature->GetMotionMaster()->Clear(false);
                        creature->GetMotionMaster()->MoveFollow(owner, PET_FOLLOW_DIST, creature->GetFollowAngle(), MOTION_SLOT_ACTIVE);
                    }
                    else
                    {
                        // Required to prevent attacking creatures that are evading and cause them to reenter combat
                        // Does not apply to MoveFollow
                        creature->AddUnitState(UNIT_STATE_EVADE);
                        creature->GetMotionMaster()->MoveTargetedHome();
                    }
                }
                handler->PSendSysMessage("%u creatures evaded.", creatures.size());
            }
        }
        return true;
    }
};

void AddSC_trolling_commandscript()
{
    new trolling_commandscript();
}

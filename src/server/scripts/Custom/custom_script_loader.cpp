/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:
void AddSC_arena_spectator_script();
void AddSC_arena_top_teams();
void AddSC_BagUpgrader();
void AddSC_battleground_rewards();
void AddSC_boss_troll();
void AddSC_command_mmr();
void AddSC_ely_layi();
void AddSC_ely_layi_xmas();
void AddSC_event_229();
void AddSC_event_271();
void AddSC_event_360();
void AddSC_event_363();
void AddSC_event_365();
void AddSC_event_381();
void AddSC_event_383();
void AddSC_event_season_bonus();
void AddSC_event_zone();
void AddSC_generic_events();
void AddSC_guildmaster();
void AddSC_lifetime_titles();
void AddSC_morph_shirt();
void AddSC_NactiOdmeny();
void AddSC_npc_argent_ring_restorer();
void AddSC_npc_bg_vendor();
void AddSC_npc_changer();
void AddSC_npc_dynamic_teleporter();
void AddSC_npc_enchantment();
void AddSC_npc_event_mark_essence();
void AddSC_npc_guild_vendor();
void AddSC_npc_hunterpetvendor();
void AddSC_npc_morph();
void AddSC_npc_odmeny_za_sezonu();
void AddSC_object_arena_crystal();
void AddSC_object_display_id_detect();
void AddSC_professions_npc();
void AddSC_seasonal_rewards();
void AddSC_start_mount();
void AddSC_Transmogrification();
void AddSC_trolling_commandscript();
void AddSC_wedding();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddCustomScripts()
{
    AddSC_arena_spectator_script();
    AddSC_arena_top_teams();
    AddSC_BagUpgrader();
    AddSC_battleground_rewards();
    AddSC_boss_troll();
    AddSC_command_mmr();
    AddSC_ely_layi();
    AddSC_ely_layi_xmas();
    AddSC_event_229();
    AddSC_event_271();
    AddSC_event_360();
    AddSC_event_363();
    AddSC_event_365();
    AddSC_event_381();
    AddSC_event_383();
    AddSC_event_season_bonus();
    AddSC_event_zone();
    AddSC_generic_events();
    AddSC_guildmaster();
    AddSC_lifetime_titles();
    AddSC_morph_shirt();
    AddSC_NactiOdmeny();
    AddSC_npc_argent_ring_restorer();
    AddSC_npc_bg_vendor();
    AddSC_npc_changer();
    AddSC_npc_dynamic_teleporter();
    AddSC_npc_enchantment();
    AddSC_npc_event_mark_essence();
    AddSC_npc_guild_vendor();
    AddSC_npc_hunterpetvendor();
    AddSC_npc_morph();
    AddSC_npc_odmeny_za_sezonu();
    AddSC_object_arena_crystal();
    AddSC_object_display_id_detect();
    AddSC_professions_npc();
    AddSC_seasonal_rewards();
    AddSC_start_mount();
    AddSC_Transmogrification();
    AddSC_trolling_commandscript();
    AddSC_wedding();
}

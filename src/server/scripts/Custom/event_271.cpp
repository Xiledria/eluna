#include "Player.h"
#include "ScriptMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CellImpl.h"

class event_271_stack_counter : public CreatureScript
{
public:
    event_271_stack_counter() : CreatureScript("event_271_stack_counter") { }

    bool OnGossipHello(Player* who, Creature* me) override
    {
        std::list<Player*> players;
        Trinity::AnyPlayerInObjectRangeCheck checker(me, 50);
        Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(me, players, checker);
        me->VisitNearbyWorldObject(50, searcher);

        std::map<uint32, std::string> auraCounts;
        for (auto player : players)
            if(player != who)
                if(Aura* aura = player->GetAura(110000))
                    auraCounts.insert(make_pair(aura->GetStackAmount(), player->GetName()));

        if(!auraCounts.empty())
        {
            me->Say("Hraci se stacky:", LANG_UNIVERSAL);
            for (auto pair : auraCounts)
                me->Say(pair.second + ": " + std::to_string(pair.first), LANG_UNIVERSAL);
        }

        return true;
    }
};

void AddSC_event_271()
{
    new event_271_stack_counter();
}

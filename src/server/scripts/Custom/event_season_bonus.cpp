#include "ScriptMgr.h"
#include "Player.h"
#include "Creature.h"
#include "GameEventMgr.h"
#include "Group.h"
#include <unordered_map>
namespace week_bonus
{
    enum Rewards
    {
        EMBLEM_OF_FROST = 49426,
        ARENA_POINT     = 43307,
        BONUS_EVENT_ID  =    99
    };

    struct Limit
    {
        Limit()
        {
            em = 0;
            eof = 0;
            ap = 0;
        }

        Limit(uint32 em, uint32 eof, uint32 ap)
        {
            this->em = em;
            this->eof = eof;
            this->ap = ap;
        }

        uint32 em;
        uint32 eof;
        uint32 ap;
    };

    std::unordered_map<ObjectGuid::LowType, Limit> limits;

    class event_seasonal_bonus_reset : public GameEventScript
    {
    public:
        event_seasonal_bonus_reset() : GameEventScript("event_seasonal_bonus_reset") { }

        void OnGameEventStart(uint32 eventId)
        {
            if (eventId != BONUS_EVENT_ID)
                return;

            CharacterDatabase.PExecute("truncate character_bonus_rewards");
            limits.clear();
        }
    };

    class event_season_bonus : public PlayerScript
    {
    private:
    public:
        event_season_bonus() : PlayerScript("event_season_bonus")
        {

        }

        void RewardEoF(Player* player)
        {
            uint32& eof = limits[player->GetGUID().GetCounter()].eof;
            if (eof >= 40)
                return;

            ++eof;
            player->AddItem(EMBLEM_OF_FROST, 1, true);
        }

        void RewardEM(Player* player)
        {
            uint32& em = limits[player->GetGUID().GetCounter()].em;
            if (em >= 25)
                return;

            ++em;
            player->AddItem(Deffender::EVENT_MARK, 1, true);
        }

        void RewardAP(Player* player, uint8 count)
        {
            uint32& ap = limits[player->GetGUID().GetCounter()].ap;
            if (ap >= 500)
                return;

            ap += count;
            player->AddItem(ARENA_POINT, count, true);
        }

        void OnLogin(Player* player, bool) override
        {
            QueryResult result = CharacterDatabase.Query(
                        std::string("SELECT em, eof, ap FROM character_bonus_rewards where guid = " + std::to_string(player->GetGUID().GetCounter())).c_str());
            if (!result)
                return;

            Field* fields = result->Fetch();
            limits[player->GetGUID().GetCounter()] = Limit(fields[0].GetUInt32(),fields[1].GetUInt32(),fields[2].GetUInt32());
        }

        void OnLogout(Player* player) override
        {
            Limit& l = limits[player->GetGUID().GetCounter()];
            CharacterDatabase.PExecute("REPLACE INTO character_bonus_rewards (guid, em, eof, ap) VALUES(%u, %u, %u, %u)", player->GetGUID().GetCounter(), l.em, l.eof, l.ap);
            limits.erase(player->GetGUID().GetCounter());
        }

        void OnKillCredit(Player* killer, Unit* killed) override
        {
            if (Player* pkilled = killed->ToPlayer())
                PlayerKilled(killer, pkilled);
            else if (Creature* ckilled = killed->ToCreature())
                CreatureKilled(killer, ckilled);
        }

        void PlayerKilled(Player* killer, Player* killed)
        {
            if (killed->getLevel() < killer->getLevel())
                return;

            if (!sGameEventMgr->IsActiveEvent(BONUS_EVENT_ID))
                return;

            if (roll_chance_f(2.0f))
                RewardEM(killer);

            if (roll_chance_f(25.0f))
                RewardAP(killer, urand(1,4));
        }

        void CreatureKilled(Player* killer, Creature* killed)
        {
            if (killed->getLevel() < killer->getLevel())
                return;

            if (killed->GetOwnerGUID())
                return;

            Map* map = killer->GetMap();
            float em_chance = 1.0f;
            float eof_chance = 5.0f;
            bool elite = killed->isElite() || killed->isWorldBoss();
            if (map->IsDungeon() || map->IsRaid())
            {
                if (!elite)
                    return;

                if (killed->isWorldBoss())
                {
                    em_chance *= 3.0f;
                    if (map->IsRaid())
                        em_chance *= 3.0f;
                }
            }
            else
            {
                if (elite)
                    em_chance += 0.5f;
                else
                    eof_chance /= 2.0f;

                if (Group* group = killer->GetGroup())
                {
                    float multiplier = floor((group->GetMembersCount() - 1) / 5.0f) + 1;
                    em_chance /= multiplier;
                    eof_chance /= multiplier;
                }
            }

            if (!sGameEventMgr->IsActiveEvent(BONUS_EVENT_ID))
                return;

            if (roll_chance_f(em_chance))
                RewardEM(killer);

            if (killer->getLevel() < 80)
                return;

            if (roll_chance_f(eof_chance))
                RewardEoF(killer);
        }
    };
}

void AddSC_event_season_bonus()
{
    new week_bonus::event_seasonal_bonus_reset();
    new week_bonus::event_season_bonus();
}

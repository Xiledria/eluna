#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "Language.h"
#include "Chat.h"
 
class professions_npc : public CreatureScript
{
public:
    professions_npc () : CreatureScript("professions_npc") {}
           
    void CreatureWhisperBasedOnBool(const char *text, Creature *creature, Player *player, bool value)
    {
    if (value) {
        creature->TextEmote(text, player);
    }
}

public:
    uint32 PlayerMaxLevel() const
    {
        return sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL);
    }

    bool OnGossipHello(Player *player, Creature* creature)
    {
        AddGossipItemFor(player, 3, "[Professions] ->", GOSSIP_SENDER_MAIN, 196);
        AddGossipItemFor(player, GOSSIP_ICON_TALK, "[Nevermind] ...", GOSSIP_SENDER_MAIN, 12);
        player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
        return true;
    }
           
    bool PlayerAlreadyHasTwoProfessions(const Player *player) const
    {
        uint32 skillCount = 0;

        if (player->HasSkill(SKILL_MINING)) {
            skillCount++;
        }
        if (player->HasSkill(SKILL_SKINNING)) {
            skillCount++;
        }
        if (player->HasSkill(SKILL_HERBALISM)) {
            skillCount++;
        }
        if (skillCount >= sWorld->getIntConfig(CONFIG_MAX_PRIMARY_TRADE_SKILL)) {
            return true;
            }
        for (uint32 i = 1; i < sSkillLineStore.GetNumRows(); ++i)
        {
            SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(i);
            if (!SkillInfo) {
                continue;
                }
            if (SkillInfo->categoryId == SKILL_CATEGORY_SECONDARY) {
                continue;
                }
            if ((SkillInfo->categoryId != SKILL_CATEGORY_PROFESSION) || !SkillInfo->canLink) {
                continue;
                }
            const uint32 skillID = SkillInfo->id;
            if (player->HasSkill(skillID)) {
                skillCount++;
                }
            if (skillCount >= sWorld->getIntConfig(CONFIG_MAX_PRIMARY_TRADE_SKILL)) {
                return true;
            }
        }
        return false;
    }

    bool LearnAllRecipesInProfession(Player *player, SkillType skill)
    {
        ChatHandler handler(player->GetSession());
        char* skill_name;

        SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(skill);
        skill_name = SkillInfo->name[handler.GetSessionDbcLocale()]; 

        LearnSkillRecipesHelper(player, SkillInfo->id);

        player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), 450, 450);
        handler.PSendSysMessage(LANG_COMMAND_LEARN_ALL_RECIPES, skill_name);
           
        return true;
    }
   
    void LearnSkillRecipesHelper(Player *player, uint32 skill_id)
    {
        uint32 classmask = player->getClassMask();
        for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j)
        {
            SkillLineAbilityEntry const *skillLine = sSkillLineAbilityStore.LookupEntry(j);
            if (!skillLine) {
                continue;
                }
            // wrong skill
            if (skillLine->skillId != skill_id) {
                continue;
            }
            // not high rank
            if (skillLine->forward_spellid) {
                continue;
                }
            // skip racial skills
            if (skillLine->racemask != 0) {
                continue;
                }
            // skip wrong class skills
            if (skillLine->classmask && (skillLine->classmask & classmask) == 0) {
                continue;
                }
            SpellInfo const * spellInfo = sSpellMgr->GetSpellInfo(skillLine->spellId);
            if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player, false)) {
                continue;
            }
            player->LearnSpell(skillLine->spellId, false);
        }
    }

    bool IsSecondarySkill(SkillType skill) const
       {
           return skill == SKILL_COOKING || skill == SKILL_FIRST_AID || skill == SKILL_FISHING;
       }
    void CompleteLearnProfession(Player *player, Creature *creature, SkillType skill)
    {
        if (PlayerAlreadyHasTwoProfessions(player) && !IsSecondarySkill(skill)) {
            creature->Whisper("You already know four professions!", LANG_UNIVERSAL, player);
        }
        else
        {
            if (!LearnAllRecipesInProfession(player, skill)) {
                creature->Whisper("Internal error occured!", LANG_UNIVERSAL, player);
            }
        }
    }
   
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
    {
        player->PlayerTalkClass->ClearMenus();
        if (uiSender == GOSSIP_SENDER_MAIN)
        {
            switch (uiAction)
            {
                case 196:
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_alchemy:30|t Alchemy.", GOSSIP_SENDER_MAIN, 1);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Ingot_05:30|t Blacksmithing.", GOSSIP_SENDER_MAIN, 2);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_LeatherScrap_02:30|t Leatherworking.", GOSSIP_SENDER_MAIN, 3);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Fabric_Felcloth_Ebon:30|t Tailoring.", GOSSIP_SENDER_MAIN, 4);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_wrench_01:30|t Engineering.", GOSSIP_SENDER_MAIN, 5);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_engraving:30|t Enchanting.", GOSSIP_SENDER_MAIN, 6);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_gem_01:30|t Jewelcrafting.", GOSSIP_SENDER_MAIN, 7);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Scroll_08:30|t Inscription.", GOSSIP_SENDER_MAIN, 8);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Bandage_Frostweave_Heavy:30|t First Aid.", GOSSIP_SENDER_MAIN, 13);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Herb_07:30|t Herbalism.", GOSSIP_SENDER_MAIN, 9);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_pelt_wolf_01:30|t Skinning.", GOSSIP_SENDER_MAIN, 10);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_mining:30|t Mining.", GOSSIP_SENDER_MAIN, 11);
                    AddGossipItemFor(player, GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Food_17:30|t Cooking", GOSSIP_SENDER_MAIN, 14);
                    AddGossipItemFor(player, GOSSIP_ICON_TALK, "|TInterface/ICONS/Thrown_1H_Harpoon_D_01Blue:30|t Nevermind!", GOSSIP_SENDER_MAIN, 12);
                    player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
                    break;

                // ALCHEMY
                case 1: 
                {
                    if(player->HasSkill(SKILL_ALCHEMY))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_ALCHEMY);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // BLACKSMITHING
                case 2: 
                {
                    if(player->HasSkill(SKILL_BLACKSMITHING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_BLACKSMITHING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // LEATHERWORKING
                case 3: 
                {
                    if(player->HasSkill(SKILL_LEATHERWORKING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_LEATHERWORKING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // TAILORING
                case 4: 
                {
                    if(player->HasSkill(SKILL_TAILORING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_TAILORING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // ENGINEERING
                case 5: 
                {
                    if(player->HasSkill(SKILL_ENGINEERING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_ENGINEERING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // ENCHANTING
                case 6: 
                {
                    if(player->HasSkill(SKILL_ENCHANTING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_ENCHANTING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // JEWELCRAFTING
                case 7: 
                {
                    if(player->HasSkill(SKILL_JEWELCRAFTING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_JEWELCRAFTING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // INSCRIPTION
                case 8: 
                {
                    if(player->HasSkill(SKILL_INSCRIPTION))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    CompleteLearnProfession(player, creature, SKILL_INSCRIPTION);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // HERBALISM
                case 9: 
                {
                    if(player->HasSkill(SKILL_HERBALISM))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
       
                    CompleteLearnProfession(player, creature, SKILL_HERBALISM);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // SKINNING
                case 10: 
                {
                    if(player->HasSkill(SKILL_SKINNING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                               
                    CompleteLearnProfession(player, creature, SKILL_SKINNING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // MINING
                case 11: 
                {
                    if(player->HasSkill(SKILL_MINING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                               
                    CompleteLearnProfession(player, creature, SKILL_MINING);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // FIRST AID
                case 13:
                {
                    if(player->HasSkill(SKILL_FIRST_AID))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                               
                    CompleteLearnProfession(player, creature, SKILL_FIRST_AID);
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }
                case 14:
                    if(player->HasSkill(SKILL_COOKING))
                    {
                        player->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                    CompleteLearnProfession(player, creature, SKILL_COOKING);

                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                // NEVERMIND
                case 12: 
                {
                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }
            }
        }
        return true;
    }
};
void AddSC_professions_npc()
{
    new professions_npc();
}

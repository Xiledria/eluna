#include "ScriptedCreature.h"
#include "ScriptMgr.h"
#include "Player.h"
#include "SpellAuraEffects.h"
#include "TemporarySummon.h"
#include "ScriptedGossip.h"
#include "GameObjectAI.h"
#include "SpellScript.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "TaskScheduler.h"
#include "DatabaseEnv.h"
#include "box_spawner_base_class.h"
#include "labyrinth_base_class.h"
#include "PassiveAI.h"
#include "Guild.h"

// @TODO:
// 42050 green nature portal
// knížky s nápovědama
// faerie dragon - 1267
// troll alternativa pro low lvly
// 40063 missing POI

// 1024 guild house phase
// boxes hacker check

using namespace std::string_literals;

enum EventZoneQuests
{
    EVENT_ZONE_QUEST_FINAL_PREQUEST             = 40054,
    EVENT_ZONE_QUEST_ELY_LAYI_LETTER            = 40056,
    EVENT_ZONE_QUEST_ASK_LAYI_ELY_FOR_HELP      = 40060,
    EVENT_ZONE_QUEST_GAIN_REPUTATION            = 40061,
    EVENT_ZONE_QUEST_APPLE_FARM_WORMS           = 40064,
    EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT  = 40065,
    EVENT_ZONE_QUEST_DOCTOR_DEATH_INVESTIGATION = 40068,
    EVENT_ZONE_QUEST_INVESTIGATION              = 40069,
    EVENT_ZONE_QUEST_FISHERMAN_HUNGER           = 40070,
    EVENT_ZONE_QUEST_INNKEEPER_GUESTS           = 40072,
    EVENT_ZONE_QUEST_FIND_LOST_DOG              = 40073,
    EVENT_ZONE_QUEST_FEED_DOG                   = 40074,
    EVENT_ZONE_QUEST_FEEDING_TURKEYS            = 40076,
    EVENT_ZONE_QUEST_CRYPT_INSPECTION           = 40078,
    EVENT_ZONE_QUEST_REPORT_CRYPT_TO_COUNCILOR  = 40080,
    EVENT_ZONE_QUEST_BLACKSMITH_MINE_CART       = 40081,
    EVENT_ZONE_QUEST_EXECUTION_REQUEST          = 40084,
    EVENT_ZONE_QUEST_KILL_TROLLISCUSAN          = 40085,
    EVENT_ZONE_QUEST_LIBRARY_QUESTIONS          = 40087,
    EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS     = 40090,
    EVENT_ZONE_QUEST_DAILY_FEEDING_TURKEYS      = 40093,
    EVENT_ZONE_QUEST_WEDDING                    = 40095,
    EVENT_ZONE_QUEST_WEDDING_DRESS              = 40098,
    EVENT_ZONE_QUEST_WEDDING_DRESS_CLOTH        = 40099,
    EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET     = 40100,
    EVENT_ZONE_QUEST_WEDDING_RIBBON             = 40101,
    EVENT_ZONE_QUEST_LOST_MARE                  = 40102,
    EVENT_ZONE_QUEST_WEDDING_RINGS              = 40104,
    EVENT_ZONE_QUEST_DAILY_COLLECT_WOOD         = 40112,
};

enum EventZoneCreatures
{
    EVENT_ZONE_CREATURE_ELY_LAYI                    = 450106,
    EVENT_ZONE_CREATURE_REVOLT_LEADER               = 450121,
    EVENT_ZONE_CREATURE_REVOLT_HEALER               = 450127,
    EVENT_ZONE_CREATURE_REVOLT_STRONG_SOLDIER       = 450123,
    EVENT_ZONE_CREATURE_REVOLT_SOLDIER              = 450133,
    EVENT_ZONE_CREATURE_CATHEDRAL_GUARDIAN          = 450135,
    EVENT_ZONE_CREATURE_EVILS_LAIR_EVIL_LEADER      = 450139,
    EVENT_ZONE_CREATURE_APPLE_FARM_WORM             = 450141,
    EVENT_ZONE_CREATURE_APPLE_FARM_WORM_HOLE        = 450142,
    EVENT_ZONE_CREATURE_APPLE_FARM_WORM_SPAWNER     = 450143,
    EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER = 450158,
    EVENT_ZONE_CREATURE_INVESTIGATING_DOCTOR        = 450161,
    EVENT_ZONE_CREATURE_SHARK                       = 450162,
    EVENT_ZONE_CREATURE_HAMMERHEAD_SHARK            = 450163,
    EVENT_ZONE_CREATURE_CORRECT_FISH_CREDIT         = 450164,
    EVENT_ZONE_CREATURE_INN_VIP_GUEST               = 450167,
    EVENT_ZONE_CREATURE_INN_IMPORTANT_GUEST         = 450168,
    EVENT_ZONE_CREATURE_INN_GUEST                   = 450169,
    EVENT_ZONE_CREATURE_INN_GUEST_SPAWNER           = 450170,
    EVENT_ZONE_CREATURE_INN_INNKEEPER_CREDIT        = 450171,
    EVENT_ZONE_CREATURE_WITCH                       = 450176,
    EVENT_ZONE_CREATURE_LOST_DOG_MOVING             = 450177,
    EVENT_ZONE_CREATURE_PUMPKIN_FARM_RABBIT         = 450180,
    EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_1         = 450181,
    EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_2         = 450182,
    EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_SUMMON_1  = 450183,
    EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_SUMMON_2  = 450184,
    EVENT_ZONE_CREATURE_GENERAL_SALVADOR_SUMMON     = 450185,
    EVENT_ZONE_CREATURE_CRYPT_APPLE_FARM_FARMER     = 450186,
    EVENT_ZONE_CREATURE_CRYPT_BLACKSMITH            = 450187,
    EVENT_ZONE_CREATURE_CRYPT_FISHERMAN             = 450188,
    EVENT_ZONE_CREATURE_CRYPT_INNKEEPER             = 450189,
    EVENT_ZONE_CREATURE_CRYPT_EVENT_DUMMY           = 450190,
    EVENT_ZONE_CREATURE_CRYPT_EVIL_ONE              = 450191,
    EVENT_ZONE_CREATURE_CRYPT_EVENT_PORTAL_OPENER   = 450192,
    EVENT_ZONE_CREATURE_INN_INNKEEPER_DAILY_QGIVER  = 450198,
    EVENT_ZONE_CREATURE_BLACKSMITH_MINE_CART        = 450201,
    EVENT_ZONE_CREATURE_REVOLT_WEAK_SOLDIER         = 450209,
    EVENT_ZONE_CREATURE_EVIL_DWARF                  = 450236,
    EVENT_ZONE_CREATURE_EVIL_DWARF_SPAWNER          = 450237,
    EVENT_ZONE_CREATURE_WOOD_COLLECTION_QUEST_GIVER = 450279,
    EVENT_ZONE_CREATURE_WOOD_COLLECTOR              = 450280,
    EVENT_ZONE_CREATURE_BRIGADIER_GENERALS_SOLDIER  = 450291,
    EVENT_ZONE_CREATURE_NECROMANCERS_ZOMBIE         = 450294,
};

enum EventZoneGameobjects
{
    EVENT_ZONE_GAMEOBJECT_PUMPKIN_FARM_PUMPKIN      = 2200041,
    EVENT_ZONE_GAMEOBJECT_INN_MEAT                  = 2200073,
    EVENT_ZONE_GAMEOBJECT_INN_BREAD                 = 2200074,
    EVENT_ZONE_GAMEOBJECT_INN_BEER                  = 2200075,
    EVENT_ZONE_GAMEOBJECT_INN_WINE                  = 2200076,
    EVENT_ZONE_GAMEOBJECT_CRYPT_INNER_PORTAL        = 2200103,
    EVENT_ZONE_GAMEOBJECT_CRYPT_OUTER_PORTAL        = 2200104,
    EVENT_ZONE_GAMEOBJECT_COLLECTABLE_WOOD          = 2200165,
};

enum EventZoneSpells
{
    EVENT_ZONE_SPELL_WITCH_ORB_KNOCKBACK         =  10689,
    EVENT_ZONE_SPELL_CRUSADER_STRIKE             =  14517,
    EVENT_ZONE_SPELL_INSPIRATION                 =  15359,
    EVENT_ZONE_SPELL_CLOUD_OF_DISEASE            =  17742,
    EVENT_ZONE_AURA_WELL_FED                     =  19705,
    EVENT_ZONE_SPELL_AURA_OF_COMMAND             =  25516,
    EVENT_ZONE_SPELL_LESSER_HEAL                 =  28306,
    EVENT_ZONE_SPELL_HEROIC_STRIKE               =  29567,
    EVENT_ZONE_SPELL_KILL_PRISONER               =  30273,
    EVENT_ZONE_AURA_PERMANENT_DEATH              =  31261,
    EVENT_ZONE_SPELL_CAMERA_SHAKE_SOFT           =  33271,
    EVENT_ZONE_SPELL_TAUNT                       =  36213,
    EVENT_ZONE_SPELL_FIREBALL_VOLLEY             =  36742,
    EVENT_ZONE_SPELL_HOLY_BOLT_VOLLEY            =  36743,
    EVENT_ZONE_SPELL_SLAYING_STRIKE              =  36838,
    EVENT_ZONE_SPELL_MORTAL_WOUNDS               =  38770,
    EVENT_ZONE_SPELL_CAMERA_SHAKE_HARD           =  39983,
    EVENT_ZONE_SPELL_SPIRIT_MEND                 =  42025,
    EVENT_ZONE_SPELL_STRIKE                      =  43298,
    EVENT_ZONE_SPELL_ROCKET_LAUNCH               =  46567,
    EVENT_ZONE_SPELL_FIRE_BLAST                  =  47721,
    EVENT_ZONE_AURA_APPLE_FARM_WORM_BURROW       =  50981,
    EVENT_ZONE_SPELL_EXPLOSION_SMOKE             =  52052,
    EVENT_ZONE_SPELL_HOLY_LIGHT                  =  52444,
    EVENT_ZONE_SPELL_CHAINS                      =  54324,
    EVENT_ZONE_SPELL_GREATER_HEAL                =  54337,
    EVENT_ZONE_SPELL_HOLY_BOLT                   =  57376,
    EVENT_ZONE_AURA_EVILS_LAIR_SCREEN_EFFECT     =  58548,
    EVENT_ZONE_SPELL_SHADOW_NOVA                 =  59435,
    EVENT_ZONE_SHADOW_BOLT_VOLLEY                =  21341,
    EVENT_ZONE_SPELL_ATTACK_VISUAL               =  61480,
    EVENT_ZONE_SPELL_CORPSE_EXPLOSION            =  61614,
    EVENT_ZONE_SPELL_FIRE_NOVA                   =  61654,
    EVENT_ZONE_SPELL_CHOP_TREE                   =  62990,
    EVENT_ZONE_AURA_KNEEL                        =  68442,
    EVENT_ZONE_SPELL_SMITE                       =  71841,
    EVENT_ZONE_SPELL_EXPLOSION_VISUAL            =  72137,
    EVENT_ZONE_SPELL_INVINCIBLE_GROUND           =  72282,
    EVENT_ZONE_SPELL_INVINCIBLE_FLYING           =  72284,
    EVENT_ZONE_SPELL_GREEN_BEAM_TO_RIFT          =  72589,
    EVENT_ZONE_AURA_INN_CARRYING_REQUESTED_FOOD  = 100032,
    EVENT_ZONE_AURA_SELECTED_QUESTION            = 100042,
    EVENT_ZONE_AURA_TAILOR_DRESS_TIMER           = 100043,
};

enum EventZoneDisplayIds
{
    EVENT_ZONE_DISPLAYID_BLACKSMITH_DISPLAY_ID = 10305
};

enum EventZoneGossipTextId
{
    EVENT_ZONE_GOSSIP_EXECUTIONER_HELLO         = 400034,
    EVENT_ZONE_GOSSIP_EXECUTIONER_WHAT_HAPPEND  = 400035,
    EVENT_ZONE_GOSSIP_EXECUTIONER_WHO_DID_IT    = 400036,
    EVENT_ZONE_GOSSIP_EXECUTIONER_SURPRISED     = 400037,
    EVENT_ZONE_GOSSIP_EXECUTIONER_GO_BACK       = 400038,
    EVENT_ZONE_GOSSIP_FLORIST_HELLO             = 400071,
    EVENT_ZONE_GOSSIP_TAILOR_HELLO              = 400071,
    EVENT_ZONE_GOSSIP_FLORIST_BEING_REQUESTED   = 400072,
    EVENT_ZONE_GOSSIP_TAILOR_WAYPOINT           = 400073,
    EVENT_ZONE_GOSSIP_TAILOR_BEING_REQUESTED    = 400074,
    EVENT_ZONE_GOSSIP_WITCH_FIND_STELLA         = 400075,
    EVENT_ZONE_GOSSIP_WITCH_FIND_RINGS          = 400076,
    EVENT_ZONE_GOSSIP_TAILOR_CHANGE             = 400077,
    EVENT_ZONE_GOSSIP_INN_DWARF_INTRO           = 400078,
    EVENT_ZONE_GOSSIP_INN_DWARF_GOOD_LUCK       = 400079,
    EVENT_ZONE_GOSSIP_INN_DWARF_YOU_CANNOT_HELP = 400080,
    EVENT_ZONE_GOSSIP_INN_DWARF_TELL_WHAT_I_NEED= 400081,
    EVENT_ZONE_GOSSIP_INN_DWARF_TELL_KICK_REASON= 400082,
    EVENT_ZONE_GOSSIP_INN_DWARF_TELL_REQUIREMENT= 400083,
};

enum EventZoneItemIds
{
    EVENT_ZONE_ITEM_TROLL_TACTIC_BOOK          =   4475,
    EVENT_ZONE_ITEM_EVENT_CLOTH                =   5014,
    EVENT_ZONE_ITEM_WEDDING_DRESS              =   6837,
    EVENT_ZONE_ITEM_INVITATION                 =  35558,
    EVENT_ZONE_ITEM_LOVE_SHIRT                 = 100033,
    EVENT_ZONE_ITEM_MORPH_TAUREN_M             = 150000,
    EVENT_ZONE_ITEM_MORPH_TAUREN_F             = 150001,
    EVENT_ZONE_ITEM_MORPH_HUMAN_M              = 150002,
    EVENT_ZONE_ITEM_MORPH_HUMAN_F              = 150003,
    EVENT_ZONE_ITEM_MORPH_GNOME_F              = 150004,
    EVENT_ZONE_ITEM_MORPH_GNOME_M              = 150005,
    EVENT_ZONE_ITEM_MORPH_BELF_F               = 150006,
    EVENT_ZONE_ITEM_MORPH_BELF_M               = 150007,
    EVENT_ZONE_ITEM_MORPH_GOBLIN_M             = 150015,
    EVENT_ZONE_ITEM_MORPH_GOBLIN_F             = 150016,
    EVENT_ZONE_ITEM_MORPH_DRAENEI_F            = 150017,
    EVENT_ZONE_ITEM_MORPH_TROLL_M              = 150019,
    EVENT_ZONE_ITEM_MORPH_DWARF_M              = 150020,
    EVENT_ZONE_ITEM_MORPH_ORC_F                = 150021,
    EVENT_ZONE_ITEM_MORPH_FEL_ORC              = 150022,
    EVENT_ZONE_ITEM_MORPH_NELF_M               = 150023,
    EVENT_ZONE_ITEM_MORPH_NELF_F               = 150024,
    EVENT_ZONE_ITEM_MORPH_UNDEAD_F             = 150025,
    EVENT_ZONE_ITEM_MORPH_UNDEAD_M             = 150026,
    EVENT_ZONE_ITEM_MORPH_DWARF_F              = 150027,
    EVENT_ZONE_ITEM_MORPH_TROLL_F              = 150028,
    EVENT_ZONE_ITEM_MORPH_DRAENEI_M            = 150029,
    EVENT_ZONE_ITEM_MORPH_ORC_M                = 150030,
    EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_M        = 150031,
    EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_F        = 150032,
    EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_M         = 150033,
    EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_F         = 150034,
    EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_F         = 150035,
    EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_M         = 150036,
    EVENT_ZONE_ITEM_LOVE_MORPH_BELF_F          = 150037,
    EVENT_ZONE_ITEM_LOVE_MORPH_BELF_M          = 150038,
    EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_M        = 150039,
    EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_F        = 150040,
    EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_F       = 150041,
    EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_M         = 150042,
    EVENT_ZONE_ITEM_LOVE_MORPH_DWARF_M         = 150043,
    EVENT_ZONE_ITEM_LOVE_MORPH_ORC_F           = 150044,
    EVENT_ZONE_ITEM_LOVE_MORPH_FEL_ORC         = 150045,
    EVENT_ZONE_ITEM_LOVE_MORPH_NELF_M          = 150046,
    EVENT_ZONE_ITEM_LOVE_MORPH_NELF_F          = 150047,
    EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_F        = 150048,
    EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_M        = 150049,
    EVENT_ZONE_ITEM_LOVE_ORPH_DWARF_F          = 150050,
    EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_F         = 150051,
    EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_M       = 150052,
    EVENT_ZONE_ITEM_LOVE_MORPH_ORC_M           = 150053,
};

enum EventZonePOI
{
    EVENT_ZONE_POI_BILLBOARD_START = 500,
    EVENT_ZONE_POI_INNKEEPER       = 500,
    EVENT_ZONE_POI_FISHERMAN       = 503,
    EVENT_ZONE_POI_FARMER          = 504,
    EVENT_ZONE_POI_WITCH           = 507,
    EVENT_ZONE_POI_BLACKSMITH      = 516,
    EVENT_ZONE_POI_BILLBOARD_MAX   = 517,
    EVENT_ZONE_POI_CLOTH_TRADER    = 518,
};

enum EventZoneEmotes
{
    EVENT_ZONE_EMOTE_SIT = 461,
};

struct Monolog
{
    std::chrono::milliseconds delay;
    std::string text;
};

class event_zone_old_priest : public CreatureScript
{
public:
    event_zone_old_priest() : CreatureScript("event_zone_old_priest") { }
    struct event_zone_old_priestAI : NullCreatureAI
    {
        event_zone_old_priestAI(Creature* creature) : NullCreatureAI(creature) { }
        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
            {
                if (player->IsGameMaster())
                    return false;

                return player->GetQuestStatus(EVENT_ZONE_QUEST_GAIN_REPUTATION) == QUEST_STATUS_REWARDED;
            }
            return true;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_zone_old_priestAI(creature);
    }
};

class event_zone_beggar : CreatureScript
{
public:
    event_zone_beggar() : CreatureScript("event_zone_beggar") { }
    struct event_zone_beggarAI : CreatureAI
    {
        TaskScheduler scheduler;
        std::unordered_set<ObjectGuid> beggedPlayers;
        std::unordered_set<ObjectGuid> ignoredPlayers;
        event_zone_beggarAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 5.0f;
        }

        void Reset() override
        {
            scheduler.CancelAll();
            beggedPlayers.clear();
            ignoredPlayers.clear();
        }

        void sGossipHello(Player* player) override
        {
            ObjectGuid guid = player->GetGUID();
            if (ignoredPlayers.find(guid) != ignoredPlayers.end())
            {
                me->Whisper(me->GetName() + " se odmita s tebou bavit!", LANG_UNIVERSAL, player, true);
                return CloseGossipMenuFor(player);
            }

            if (!player->HasItemCount(Deffender::EVENT_MARK))
                return;

            AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Venovat 1x Event Mark.", GOSSIP_SENDER_MAIN, 100);
            SendGossipMenuFor(player, 1, me);
        }

        void SendEmoteTo(Player* player, uint32 emoteId)
        {
            WorldPacket data(SMSG_EMOTE, 4 + 8);
            data << uint32(emoteId);
            data << uint64(me->GetGUID());
            player->SendDirectMessage(&data);
        }

        void sGossipSelect(Player* player, uint32, uint32) override
        {
            ObjectGuid guid = player->GetGUID();
            ignoredPlayers.insert(guid);
            scheduler.Schedule(Minutes(30), [this, guid](TaskContext)
            {
                ignoredPlayers.erase(guid);
            });

            player->DestroyItemCount(Deffender::EVENT_MARK, 1, true);
            scheduler.Schedule(Seconds(1), [this, guid](TaskContext)
            {
                if (Player* player = ObjectAccessor::GetPlayer(*me, guid))
                    SendEmoteTo(player, 4);
            });
            CloseGossipMenuFor(player);
        }

        void MoveInLineOfSight(Unit* who) override
        {
            if (who->GetTypeId() != TYPEID_PLAYER)
                return;

            ObjectGuid guid = who->GetGUID();

            if (beggedPlayers.find(guid) != beggedPlayers.end())
                return;

            if (ignoredPlayers.find(guid) != ignoredPlayers.end())
                return;

            beggedPlayers.insert(guid);
            scheduler.Schedule(Minutes(30), [this, guid](TaskContext)
            {
                beggedPlayers.erase(guid);
            });

            Player* player = who->ToPlayer();
            me->SaySingleTarget("Prosím, dej mi jednu event marku! Dám ti za to dárek!", LANG_UNIVERSAL, player);
            SendEmoteTo(player, 20);
        }

        void UpdateAI(uint32 diff) override
        {
            scheduler.Update(diff);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_zone_beggarAI(creature);
    }
};

class event_zone_portal : GameObjectScript
{
public:
    event_zone_portal() : GameObjectScript("event_zone_portal") { }
    const std::string code = "7+'Sz}4gdDfhfGS66Qj[4u'zF*9HAqeF13r§9G/Rd1,FtEi6.ml)4rD-e97";
    bool OnGossipHello(Player* player, GameObject* go) override
    {
        AddGossipItemFor(player, GOSSIP_ICON_INTERACT_1, "projit portalem", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF, "Chcete vlozit heslo z fora pro aktivaci portalu?", 0, true);
        SendGossipMenuFor(player, 1, go->GetGUID());
        return true;
    }

    bool OnGossipSelectCode(Player* player, GameObject*, uint32, uint32, const char* plrCode) override
    {
        if (std::string(plrCode) == code)
            player->TeleportTo(609, 1776.3f, -4943.9f, 81.5f, 4.4f);
        else
            player->Unit::Whisper("Spatne heslo, nepodarilo se ti aktivovat portal!\nHeslo lze najit na forum.deffender.eu v Event Zone topicu!", LANG_UNIVERSAL, player, true);
        return true;
    }

    struct event_zone_portalAI : GameObjectAI
    {
        event_zone_portalAI(GameObject* obj) : GameObjectAI(obj) { }
        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
            {
                if (player->IsGameMaster())
                    return false;

                return player->GetQuestStatus(EVENT_ZONE_QUEST_ELY_LAYI_LETTER) == QUEST_STATUS_NONE;
            }
            return true;
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new event_zone_portalAI(go);
    }
};

class event_zone_villager : public CreatureScript
{
public:
    event_zone_villager() : CreatureScript("event_zone_villager") { }

    struct event_zone_villagerAI : CreatureAI
    {
        enum VillagerEquipmentEnum
        {
            VILLAGER_EQUIP_DAY   = 1,
            VILLAGER_EQUIP_NIGHT = 2
        };

        bool checked;
        bool shouldEquip;
        uint32 timer;
        const uint32 checkDelay = 15 * MINUTE * IN_MILLISECONDS;
        event_zone_villagerAI(Creature* creature) : CreatureAI(creature)
        {
            checked = false;
            shouldEquip = false;
            timer = urand(0, checkDelay);
        }

        void sOnGameEvent(bool activated, int32) override
        {
            if (!activated) // reset model id, because it is saved to DB
            {
                me->SetNativeDisplayId(me->GetCreatureTemplate()->GetRandomValidModelId());
                me->SetDisplayId(me->GetNativeDisplayId());
                me->SaveToDB();
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer > diff)
            {
                timer -= diff;
                return;
            }
            timer = checkDelay;

            const time_t now = time(nullptr);
            tm* tm_struct = localtime(&now);
            uint8 h = tm_struct->tm_hour;
            bool night = h < 7 || h > 20;
            if (night && me->GetCurrentEquipmentId() == VILLAGER_EQUIP_DAY)
            {
                float x,y,z;
                me->GetPosition(x, y, z);
                // do not take torch inside of house
                if (!me->GetMap()->IsOutdoors(x, y, z))
                    return;

                if (!checked)
                {
                    checked = true;
                    if (roll_chance_i(75))
                        shouldEquip = true;
                }

                if (shouldEquip && roll_chance_i(10))
                {
                    me->Say("Začíná být nějak šero!", LANG_UNIVERSAL);
                    me->LoadEquipment(2);
                    me->SaveToDB();
                }
            }
            else if (!night && me->GetCurrentEquipmentId() == VILLAGER_EQUIP_NIGHT)
            {
                checked = false;
                me->LoadEquipment(1);
                me->SaveToDB();
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_zone_villagerAI(creature);
    }
};

class event_zone_cathedral_guardian_dummy : public CreatureScript
{
public:
    event_zone_cathedral_guardian_dummy() : CreatureScript("event_zone_cathedral_guardian_dummy") { }

    struct event_zone_cathedral_guardian_dummyAI : CreatureAI
    {
        event_zone_cathedral_guardian_dummyAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 5.0f;
        }

        bool CanSeeAlways(const WorldObject* who) override
        {
            return (who->GetPhaseMask() & me->GetPhaseMask()) != 0 && me->IsWithinLOSInMap(who) && me->GetDistance(who) < me->m_SightDistance;
        }

        void TeleportAway(Player* player)
        {
            if (player->IsQuestRewarded(EVENT_ZONE_QUEST_GAIN_REPUTATION))
                return;

            if (player->IsGameMaster())
                return;

            if (Creature* guardian = me->FindNearestCreature(EVENT_ZONE_CREATURE_CATHEDRAL_GUARDIAN, 20.0f))
                guardian->SaySingleTarget("Nemůžu Tě pustit k princezně Layi Ely, musíš nejprve získat přízeň města. Až získáš přízeň, vrať se. (musíš splnit všechny questy ve městě)", LANG_UNIVERSAL, player);
            player->NearTeleportTo(1612.96f, -5523.17f, 111.14f, 4.25f);
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            TeleportAway(unit->ToPlayer());
        }

        void UpdateAI(uint32 /*diff*/) override
        {

        }
    };

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (player->IsQuestRewarded(EVENT_ZONE_QUEST_GAIN_REPUTATION))
            return false;

        if (!player->IsQuestRewarded(EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT))
            return false;

        switch(player->GetQuestStatus(EVENT_ZONE_QUEST_GAIN_REPUTATION))
        {
            case QUEST_STATUS_NONE:
                player->AddQuest(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_GAIN_REPUTATION), me);
                // no break intended
            case QUEST_STATUS_INCOMPLETE:
                player->CompleteQuest(EVENT_ZONE_QUEST_GAIN_REPUTATION);
                me->Whisper("$GZiskal:Ziskala; si potrebnou reputaci na to, aby ses $Gdostal:dostala; za Layi Ely!", LANG_UNIVERSAL, player, true);
                break;
            default:
                break;
        }
        return false;
    }

    bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
    {
        if (quest->GetQuestId() == EVENT_ZONE_QUEST_GAIN_REPUTATION)
            me->Whisper("!!! WELCOME TO EVENT ZONE !!!\n$GOdemknul:Odemknula; sis daily questy a event zone vendory!", LANG_UNIVERSAL, player, true);
        return false;
    }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_zone_cathedral_guardian_dummyAI(creature);
    }
};

class event_zone_inviter : public CreatureScript
{
public:
    std::atomic<bool> processing;
    event_zone_inviter() : CreatureScript("event_zone_inviter")
    {
        processing.store(false);
    }

    bool IsProcessing(Player* player)
    {
        if (processing.load())
        {
            player->GetSession()->SendNotification("Sending mail in progress, please wait");
            CloseGossipMenuFor(player);
            return true;
        }
        return false;
    }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (IsProcessing(player))
            return true;

        if (!player->GetSession()->HasPermission(rbac::RBAC_PERM_CAN_SEND_EVENT_ZONE_INVITATION))
        {
            player->GetSession()->SendNotification("HEM / HGM / HDEV / admin only, sorry!");
            CloseGossipMenuFor(player);
            return true;
        }

        AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Pozvat hrace do event zony", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
        SendGossipMenuFor(player, 1, me);
        return true;
    }

    uint32 InvitePlayers()
    {
        processing.store(true);
        QueryResult result = CharacterDatabase.Query(std::string("SELECT guid FROM character_queststatus_rewarded where quest = " + std::to_string(EVENT_ZONE_QUEST_FINAL_PREQUEST)
                                                     + " AND guid NOT IN(SELECT guid FROM event_zone_invitations)").c_str());
        if (!result)
        {
            processing.store(false);
            return 0;
        }
        uint32 count = 0;
        bool first = true;
        std::string query = "INSERT INTO event_zone_invitations VALUES (";
        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        do
        {
            uint32 guid = result->Fetch()[0].GetUInt32();
            MailDraft draft("EVENT ZONE", "");
            Item* letter = Item::CreateItem(EVENT_ZONE_ITEM_INVITATION, 1);
            letter->SaveToDB(trans);
            draft.AddItem(letter).SendMailTo(trans, MailReceiver(guid), MailSender(MAIL_CREATURE, EVENT_ZONE_CREATURE_ELY_LAYI));
            if (!first)
                query += "),(" + std::to_string(guid);
            else
            {
                first = false;
                query += std::to_string(guid);
            }
            ++count;
        } while (result->NextRow());
        query += ")";
        CharacterDatabase.ExecuteOrAppend(trans, query.c_str());
        CharacterDatabase.CommitTransaction(trans);
        processing.store(false);
        return count;
    }

    bool OnGossipSelect(Player* player, Creature*, uint32, uint32) override
    {
        CloseGossipMenuFor(player);
        if (!IsProcessing(player))
        {
            uint32 invitedPlayers = InvitePlayers();
            if (!invitedPlayers)
            {
                player->GetSession()->SendNotification("Vsichni hraci splnujici podminky byli uz pozvani!");
                return true;
            }
            player->GetSession()->SendNotification("%s", (std::string("Pozvanky byly odeslany!\nOdeslano pozvanek: ") + std::to_string(invitedPlayers)).c_str());
        }
        return true;
    }

    class event_zone_inviterAI : public CreatureAI
    {
        event_zone_inviterAI(Creature* creature) : CreatureAI(creature) { }
        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
                return !player->GetSession()->HasPermission(rbac::RBAC_PERM_CAN_SEND_EVENT_ZONE_INVITATION);
            return false;
        }
    };
};

namespace Investigation
{
    enum InvestigationEnum
    {
        INVESTIGATION_APPLE_FARM_WORMS,
        INVESTIGATION_FISHERMAN_FISH,
        INVESTIGATION_BLACKSMITH,
        INVESTIGATION_INNKEEPER_GUESTS,
        INVESTIGATION_MAX,
    };

    class InvestigatedCreatureAI : public NullCreatureAI
    {
    public:
        InvestigatedCreatureAI(Creature* creature) : NullCreatureAI(creature) { }
        virtual bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
            {
                // always visible for GMe
                if (player->IsGameMaster())
                    return false;
                return player->GetQuestStatus(EVENT_ZONE_QUEST_INVESTIGATION) == QUEST_STATUS_REWARDED;
            }
            else
                return true;
        }
    };

    class InvestigatedCreature : public CreatureScript
    {
        struct InvestigationQuest
        {
            uint32 id;
            uint32 poi;
            std::string text;
        };

        std::vector<InvestigationQuest> const investigationQuests =
        {
            { EVENT_ZONE_QUEST_APPLE_FARM_WORMS,        EVENT_ZONE_POI_FARMER,      "farmáře na jablečné farmě" },
            { EVENT_ZONE_QUEST_FISHERMAN_HUNGER,        EVENT_ZONE_POI_FISHERMAN,   "rybářky u rybníka"  },
            { EVENT_ZONE_QUEST_BLACKSMITH_MINE_CART,    EVENT_ZONE_POI_BLACKSMITH,  "kovářky Leily v Avalonské kovárně" },
            { EVENT_ZONE_QUEST_INNKEEPER_GUESTS,        EVENT_ZONE_POI_INNKEEPER,   "hostinského" },
            { 0, 0, "" }
        };

        uint8 GetInvestigationMask(Player* player)
        {
#define IsRewarded(quest) player->GetQuestStatus(quest) == QUEST_STATUS_REWARDED
            uint8 mask = 0;
            if (IsRewarded(EVENT_ZONE_QUEST_APPLE_FARM_WORMS))
                mask |= 1 << INVESTIGATION_APPLE_FARM_WORMS;
            if (IsRewarded(EVENT_ZONE_QUEST_FISHERMAN_HUNGER))
                mask |= 1 << INVESTIGATION_FISHERMAN_FISH;
            if (IsRewarded(EVENT_ZONE_QUEST_BLACKSMITH_MINE_CART))
                mask |= 1 << INVESTIGATION_BLACKSMITH;
            if (IsRewarded(EVENT_ZONE_QUEST_INNKEEPER_GUESTS))
                mask |= 1 << INVESTIGATION_INNKEEPER_GUESTS;
            return mask;
        }

        void ShowNextInvestigationQuest(Player* player, Creature* me, InvestigationEnum triggeredBy)
        {
            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_INVESTIGATION);
            if (status == QUEST_STATUS_COMPLETE || status == QUEST_STATUS_REWARDED)
                return;

            uint8 mask = GetInvestigationMask(player);
            uint8 current = triggeredBy + 1;
            if (current == INVESTIGATION_MAX)
                current = 0;

            while (current != triggeredBy)
            {
                if (!(mask & (1 << current)))
                {
                    InvestigationQuest const& quest = investigationQuests[current];
                    me->SaySingleTarget("Sice nevím, kdo za to může, ale mohu ti poradit, kdo by mohl vědět. Zkus se zeptat " + quest.text + ", možná to bude vědět.", LANG_UNIVERSAL, player);
                    player->PlayerTalkClass->SendPointOfInterest(quest.poi);
                    return;
                }
                if (++current == INVESTIGATION_MAX)
                    current = 0;
            }

            me->SaySingleTarget("Čarodějka Vilma to určitě bude vědět! Říká se, že s jejím psem dokážou zjistit vše!", LANG_UNIVERSAL, player);
            player->PlayerTalkClass->SendPointOfInterest(EVENT_ZONE_POI_WITCH);
        }

    public:
        InvestigatedCreature(const char* name, InvestigationEnum investigationEnum, uint32 questId) : CreatureScript(name)
        {
            this->investigationEnum = investigationEnum;
            this->questId = questId;
        }


        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            if (action == GOSSIP_ACTION_INFO_DEF + 1)
            {
                ShowNextInvestigationQuest(player, me, investigationEnum);
                return true;
            }
            return false;
        }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(questId) != QUEST_STATUS_REWARDED)
                return false;

            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_INVESTIGATION);
            if (status == QUEST_STATUS_COMPLETE || status == QUEST_STATUS_REWARDED)
                return false;

            PlayerMenu* ptc = player->PlayerTalkClass;
            GossipMenu& menu = ptc->GetGossipMenu();
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Zjistit informace o Vickovo smrti", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0);
            ptc->SendGossipMenu(1, me->GetGUID());
            return true;
        }

        virtual CreatureAI* GetAI(Creature* creature) const override
        {
            return new InvestigatedCreatureAI(creature);
        }
    private:
        InvestigationEnum investigationEnum;
        uint32 questId;
    };

    namespace EvilsLair
    {
        class event_zone_witch_vision_orb : public GameObjectScript
        {
        public:
            event_zone_witch_vision_orb() : GameObjectScript("event_zone_witch_vision_orb") { }

            bool OnGossipHello(Player* player, GameObject* obj) override
            {
                QuestStatus investigationStatus = player->GetQuestStatus(EVENT_ZONE_QUEST_INVESTIGATION);
                if (investigationStatus == QUEST_STATUS_REWARDED || investigationStatus == QUEST_STATUS_COMPLETE)
                    return true;

                if (player->GetQuestStatus(EVENT_ZONE_QUEST_FIND_LOST_DOG) != QUEST_STATUS_REWARDED)
                {
                    if (Creature* witch = obj->FindNearestCreature(EVENT_ZONE_CREATURE_WITCH, 30.0f))
                    {
                        witch->SaySingleTarget("Nedotýkej se mého orbu!", LANG_UNIVERSAL, player);
                        witch->CastSpell(player, EVENT_ZONE_SPELL_WITCH_ORB_KNOCKBACK);
                    }
                    else
                        player->CastSpell(player, EVENT_ZONE_SPELL_WITCH_ORB_KNOCKBACK, true);
                    return true;
                }

                obj->GetMap()->LoadGrid(36, 21);
                if (Creature* evilLeader = player->SummonCreature(EVENT_ZONE_CREATURE_EVILS_LAIR_EVIL_LEADER, 2426.177f, -5552.696f, 444.193f))
                {
                    evilLeader->setActive(true);
                    evilLeader->SetOwnerGUID(player->GetGUID());
                    Aura* aur = player->AddAura(EVENT_ZONE_AURA_EVILS_LAIR_SCREEN_EFFECT, player);
                    aur->SetDuration(4 * IN_MILLISECONDS * MINUTE);
                    aur->SetMaxDuration(4 * IN_MILLISECONDS * MINUTE);
                    player->NearTeleportTo(2467.172f, -5603.652f, 414.306f, 2.184f);
                }

                return true;
            }
        };

        class event_zone_evil_one : public CreatureScript
        {
        public:
            event_zone_evil_one() : CreatureScript("event_zone_evil_one") { }
            struct event_zone_evil_oneAI : CreatureAI
            {
                uint8 id;
                TaskScheduler scheduler;
                event_zone_evil_oneAI(Creature* creature) : CreatureAI(creature) { }

                void Reset() override
                {
                    id = 0;
                    scheduler.Schedule(Seconds(10), [this](TaskContext task)
                    {
                        switch(id++)
                        {
                            case 0:
                                me->HandleEmoteCommand(4);
                                break;
                            case 1:
                                me->HandleEmoteCommand(15);
                                break;
                            case 2:
                                me->HandleEmoteCommand(54);
                                task.Repeat(Seconds(3));
                                return;
                            case 3:
                                me->HandleEmoteCommand(60);
                                task.Repeat(Seconds(3));
                                return;
                            case 4:
                                me->HandleEmoteCommand(66);
                                id = 0;
                        }

                        task.Schedule(Seconds(2), [this](TaskContext)
                        {
                            me->HandleEmoteCommand(0);
                        });

                        task.Repeat(Seconds(10));
                    });
                }

                void UpdateAI(uint32 diff) override
                {
                    scheduler.Update(diff);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_evil_oneAI(creature);
            }
        };

        class event_zone_evil_leader : public CreatureScript
        {
        public:
            event_zone_evil_leader() : CreatureScript("event_zone_evil_leader") { }

            struct event_zone_evil_leaderAI : public CreatureAI
            {
                TaskScheduler scheduler;
                uint32 id;
                const std::vector<Monolog> monolog
                {
                    { Seconds(15), "Bratři a sestry, sešli jsme se tu abychom naplnili rozkaz našeho všemohoucího pána!" },
                    { Seconds(18), "Je nás tu dostatek na to, abychom tento úkol zvádli a já mohl předat pozitivní zprávu našemu pánu!" },
                    { Seconds(20), "Nazáleží na tom, kolik z nás příjde o život. Důležitá věc, vlastně ta nejdůležitější, je vykonat rozkaz našeho pána!" },
                    { Seconds(25), "Když jsme zbavili ty nemožné lidi z města o jejich víru, budeme to mít mnohem lehčí. Nyní jsou oslabení bez náklonosti jejich Boha, přichází náš čas!" },
                    { Seconds(15), "Jsou tak oslabení, že výhra je už skoro naše. Ničeho se tedy nebojte a kráčejte vždy vpřed!" },
                    { Seconds(18), "Cítím že náš čas se brzy naplní a my budeme moc dokončit náš úkol a oslavit naše vítězství nad těmito odpornými lidmi!" },
                    { Seconds(15), "Jejich zrada, zrada jejich paní, za kterou všichni zaplatí vlastní krví, bude už brzy!" },
                    { Seconds(15), "Zničte všechno a zabijte všechny, na nikoho neberte ohled, s nikým nemějte slitování!" },
                    { Seconds(10), "Splňme tedy rozkaz našeho pána Trollicusana!" },
                    { Seconds(18), "Naučme je, že s naším pánem si není rando zahrávat a ukažme jim jaká pomsta za jejich lehkovážnost nastane!" },
                    { Seconds(15), "Dnes večer, bratři a sestry, vnikneme do města přes otevřený portál v kryptě!" },
                    { Seconds(15), "Povstaňte a dokažte svoji sílu a oddanost našemu pánu, následujte vaší víru!" }
                };

                event_zone_evil_leaderAI(Creature* creature) : CreatureAI(creature)
                {
                    id = 0;
                }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (const Player* player = seer->ToPlayer())
                        if (player->IsGameMaster())
                            return false;

                    return me->GetOwnerGUID() != seer->GetGUID();
                }

                void Reset() override
                {
                    scheduler.Schedule(Seconds(0), [this](TaskContext task)
                    {
                        if (id >= monolog.size())
                        {
                            if (Unit* owner = me->GetOwner())
                            {
                                if (Player* pOwner = owner->ToPlayer())
                                {
                                    pOwner->CompleteQuest(EVENT_ZONE_QUEST_INVESTIGATION);
                                    pOwner->RemoveAura(EVENT_ZONE_AURA_EVILS_LAIR_SCREEN_EFFECT);
                                    me->Whisper("$GZjistil:Zjistila; si, ze za Vickovu smrt mohou Trolliscusanovi zlouni!", LANG_UNIVERSAL, pOwner, true);
                                    pOwner->NearTeleportTo(1526.101f, -6109.907f, 117.279f, 0.831f);
                                }
                            }
                            me->DespawnOrUnsummon();
                            return;
                        }

                        me->YellSingleTarget(monolog[id].text, LANG_UNIVERSAL, me->GetCharmerOrOwnerPlayerOrPlayerItself());
                        task.Repeat(monolog[id++].delay);
                    });
                }

                void UpdateAI(uint32 diff) override
                {
                    scheduler.Update(diff);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_evil_leaderAI(creature);
            }
        };

        class event_zone_prophet : public CreatureScript
        {
        public:
            event_zone_prophet() : CreatureScript("event_zone_prophet") { }
            struct event_zone_prophetAI : public InvestigatedCreatureAI
            {
                uint32 timer;
                event_zone_prophetAI(Creature* creature) : InvestigatedCreatureAI(creature)
                {
                    timer = 0;
                }

                const std::vector<std::string> quotes =
                {
                    "Konec světa se blíží!",
                    "Brzy nastane konec!",
                    "Vše již brzy skončí!"
                };

                void UpdateAI(uint32 diff) override
                {
                    if (timer > diff)
                    {
                        timer -= diff;
                        return;
                    }

                    me->Say(quotes[urand(0, static_cast<uint32>(quotes.size() - 1))], LANG_UNIVERSAL);
                    timer = urand(25000, 120000);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_prophetAI(creature);
            }
        };
    }

    namespace PumpkinFarm
    {
        class event_zone_pumpkin_farm_pumpkin : GameObjectScript
        {
        public:
            event_zone_pumpkin_farm_pumpkin() : GameObjectScript("event_zone_pumpkin_farm_pumpkin") { }

            struct event_zone_pumpkin_farm_pumpkinAI : GameObjectAI
            {
                uint8 seeds;
                event_zone_pumpkin_farm_pumpkinAI(GameObject* obj) : GameObjectAI(obj) { seeds = urand(4, 10); }

                void DoAction(int32) override
                {
                    if (seeds)
                    {
                        if (roll_chance_i(60))
                            --seeds;
                    }
                    else
                    {
                        go->SetLootState(GO_JUST_DEACTIVATED);
                        seeds = urand(4, 10);
                    }
                }

                void OnStateChanged(uint32 state, Unit* unit) override
                {
                    if (state == GO_ACTIVATED)
                    {
                        if (Player* player = unit->ToPlayer())
                            player->KillCreditGO(go->GetEntry(), ObjectGuid::Empty, seeds);
                    }
                }
            };

            GameObjectAI* GetAI(GameObject* obj) const override
            {
                return new event_zone_pumpkin_farm_pumpkinAI(obj);
            }
        };

        class event_zone_pumpkin_farm_rabbit : CreatureScript
        {
        public:
            event_zone_pumpkin_farm_rabbit() : CreatureScript("event_zone_pumpkin_farm_rabbit") { }
            struct event_zone_pumpkin_farm_rabbitAI : CreatureAI
            {
                uint32 timer;
                event_zone_pumpkin_farm_rabbitAI(Creature* creature) : CreatureAI(creature)
                {
                    timer = urand(0, 500);
                    me->SetWalk(true);
                }
                void UpdateAI(uint32 diff) override
                {
                    if (timer > diff)
                    {
                        timer -= diff;
                        return;
                    }

                    timer += 500;
                    std::list<GameObject*> pumpkins;
                    me->GetGameObjectListWithEntryInGrid(pumpkins, EVENT_ZONE_GAMEOBJECT_PUMPKIN_FARM_PUMPKIN, 25.0f);
                    pumpkins.remove_if([this](GameObject* pumpkin) { return !pumpkin->isSpawned() || pumpkin->GetDistance(me->GetHomePosition()) > 25.0f; });
                    pumpkins.sort([this](GameObject* a, GameObject* b) { return a->GetDistance(me) < b->GetDistance(me); });
                    if (pumpkins.empty())
                    {
                        if (me->GetDistance(me->GetHomePosition()) > 15.0f)
                            me->GetMotionMaster()->MovePoint(0, me->GetHomePosition());
                        return;
                    }

                    GameObject* pumpkin = pumpkins.front();
                    if (pumpkin->GetDistance(me) > 0.5f)
                        me->GetMotionMaster()->MovePoint(0, *pumpkin);
                    else
                    {
                        pumpkin->AI()->DoAction(0);
                        if (roll_chance_i(5))
                            me->Say("nam nam nam", LANG_UNIVERSAL);
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_pumpkin_farm_rabbitAI(creature);
            }
        };
    }

    namespace TurkeyFarm
    {
        class event_zone_turkey_farm_farmer : CreatureScript
        {
        public:
            event_zone_turkey_farm_farmer() : CreatureScript("event_zone_turkey_farm_farmer") { }

            bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 sender) override
            {
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_FEED_DOG) != QUEST_STATUS_INCOMPLETE)
                    return true;

                if (sender != GOSSIP_ACTION_INFO_DEF)
                {
                    me->SaySingleTarget("Tu máš svého zaslouženého krocana!", LANG_UNIVERSAL, player);
                    player->CompleteQuest(EVENT_ZONE_QUEST_FEED_DOG);
                }
                else
                    me->SaySingleTarget("Neřvi na mě! Až mě slušně požádáš, tak ti dám krocana!", LANG_UNIVERSAL, player);
                CloseGossipMenuFor(player);

                return true;
            }

            bool OnGossipHello(Player* player, Creature* me) override
            {
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_FEED_DOG) != QUEST_STATUS_INCOMPLETE)
                    return false;

                if (player->GetQuestStatus(EVENT_ZONE_QUEST_FEEDING_TURKEYS) != QUEST_STATUS_REWARDED)
                    return false;

                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "DEJ MI MYHO KROCANA!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Pozadat chovatele o krocana.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
                SendGossipMenuFor(player, 1, me);
                return true;
            }
        };

        class event_zone_turkey_farm_turkey : CreatureScript
        {
        public:
            event_zone_turkey_farm_turkey() : CreatureScript("event_zone_turkey_farm_turkey") { }

            struct event_zone_turkey_farm_turkeyAI : CreatureAI
            {
                uint32 timer;
                event_zone_turkey_farm_turkeyAI(Creature* creature) : CreatureAI(creature) { timer = 1000; }

                void sGossipSelect(Player* player, uint32, uint32) override
                {
                    if (!me->GetAppliedAuras().empty())
                        return;

                    if (Aura* aur = me->AddAura(EVENT_ZONE_AURA_WELL_FED, me))
                    {
                        aur->SetDuration(MINUTE * IN_MILLISECONDS);
                        aur->SetMaxDuration(MINUTE * IN_MILLISECONDS);
                        player->KilledMonsterCredit(me->GetEntry());
                    }
                }

                void sGossipHello(Player* player) override
                {
                    if (!player->IsActiveQuest(EVENT_ZONE_QUEST_FEEDING_TURKEYS) && !player->IsActiveQuest(EVENT_ZONE_QUEST_DAILY_FEEDING_TURKEYS))
                        return;

                    if (!me->GetAppliedAuras().empty())
                        return CloseGossipMenuFor(player);

                    AddGossipItemFor(player, GOSSIP_ICON_VENDOR, "Nakrmit krocana", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "Nakrmit krocana", 0, false);
                    SendGossipMenuFor(player, 1, me);
                }

                void EnterEvadeMode(EvadeReason) override
                {

                }

                void UpdateAI(uint32 diff) override
                {
                    if (timer > diff)
                    {
                        timer -= diff;
                        return;
                    }
                    timer = urand(700, 1000);
                    std::list<Player*> players;
                    me->GetPlayerListInGrid(players, 6.0f);
                    if (players.empty())
                    {
                        if (me->GetDistance(me->GetHomePosition()) > 15.0f)
                            me->GetMotionMaster()->MovePoint(0, me->GetHomePosition());
                        return;
                    }

                    if (!me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_FLEEING))
                    {
                        me->GetMotionMaster()->MoveFleeing(players.front(), 1500);
                        me->SetInCombatState(true, players.front());
                    }

                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_turkey_farm_turkeyAI(creature);
            }
        };
    }

    namespace Witch
    {
        class event_zone_witch : CreatureScript
        {
        public:
            event_zone_witch() : CreatureScript("event_zone_witch") { }

            enum GossipActions
            {
                GOSSIP_ACTION_WHERE_IS_STELLA = 100,
                GOSSIP_ACTION_TANARIS,
                GOSSIP_ACTION_WINTERSPRING,
                GOSSIP_ACTION_STRANGLETHORN,
                GOSSIP_ACTION_WETLANDS,
                GOSSIP_ACTION_ARATHI,
                GOSSIP_ACTION_HILLSBRAD,
                GOSSIP_ACTION_WHERE_ARE_RINGS,
                GOSSIP_ACTION_DWARF
            };

            bool OnGossipHello(Player* player, Creature* me) override
            {
                if (player->IsActiveQuest(EVENT_ZONE_QUEST_LOST_MARE))
                {
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Potreboval bych najit kobylu Stellu, nekde se zatoulala, nevis prosim, kde ji mohu najit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_WHERE_IS_STELLA);
                    SendGossipMenuFor(player, 1, me);
                    return true;
                }

                if (player->IsActiveQuest(EVENT_ZONE_QUEST_WEDDING_RINGS))
                {
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Potrebuji sehnat ty nejlepsi prsteny na nasi svatbu! Nevis prosim, kde bych je mohl najit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_WHERE_ARE_RINGS);
                    SendGossipMenuFor(player, 1, me);
                    return true;
                }

                return false;
            }

            void AddTeleportItem(Player* player, std::string text, uint32 action)
            {
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, text, GOSSIP_SENDER_MAIN, action, "Chces teleportovat do oblasti " + text, 0, false);
            }

            bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
            {
                player->PlayerTalkClass->GetGossipMenu().ClearMenu();

                switch(action)
                {
                    case GOSSIP_ACTION_WHERE_IS_STELLA:
                        AddTeleportItem(player, "Tanaris", GOSSIP_ACTION_TANARIS);
                        AddTeleportItem(player, "Winterspirng", GOSSIP_ACTION_WINTERSPRING);
                        AddTeleportItem(player, "Stranglethorn Vale", GOSSIP_ACTION_STRANGLETHORN);
                        AddTeleportItem(player, "Wetlands", GOSSIP_ACTION_WETLANDS);
                        AddTeleportItem(player, "Arathi Highlands", GOSSIP_ACTION_ARATHI);
                        AddTeleportItem(player, "Hillsbrad Foothills", GOSSIP_ACTION_HILLSBRAD);
                        SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_WITCH_FIND_STELLA, me);
                        break;
                    case GOSSIP_ACTION_TANARIS:
                        player->TeleportTo(1, -7931.2f, -3414.28f, 80.74f, 0.0f);
                        break;
                    case GOSSIP_ACTION_WINTERSPRING:
                        player->TeleportTo(1, 6759.18f, -4419.63f, 763.21f, 0.0f);
                        break;
                    case GOSSIP_ACTION_STRANGLETHORN:
                        player->TeleportTo(0, -12644.30f, -377.41f, 10.10f, 0.0f);
                        break;
                    case GOSSIP_ACTION_WETLANDS:
                        player->TeleportTo(0, -3242.81f, -2469.04f, 15.92f, 0.0f);
                        break;
                    case GOSSIP_ACTION_ARATHI:
                        player->TeleportTo(0, -1508.51f, -2732.06f, 32.50f, 0.0f);
                        break;
                    case GOSSIP_ACTION_HILLSBRAD:
                        player->TeleportTo(0, -437.60f, -588.20f, 53.65f, 0.0f);
                        break;
                    case GOSSIP_ACTION_WHERE_ARE_RINGS:
                        AddTeleportItem(player, "Bludiste", GOSSIP_ACTION_DWARF);
                        SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_WITCH_FIND_RINGS, me);

                        break;
                    case GOSSIP_ACTION_DWARF:
                        player->NearTeleportTo(1257.54f, -5452.60f, 140.16f, 0.09f);
                        break;
                    default:
                        return false;
                }
                return true;
            }

            struct event_zone_witchAI : NullCreatureAI
            {
                event_zone_witchAI(Creature* creature) : NullCreatureAI(creature)
                {
                    me->m_SightDistance = 5.0f;
                }

                void MoveInLineOfSight(Unit* unit) override
                {
                    if (unit->GetEntry() != EVENT_ZONE_CREATURE_LOST_DOG_MOVING)
                        return;

                    if (Player* owner = unit->GetCharmerOrOwnerPlayerOrPlayerItself())
                        me->SaySingleTarget("Děkuji ti za nalezení mého Šmudly!", LANG_UNIVERSAL, owner);
                    unit->ToCreature()->DespawnOrUnsummon();
                }

                void sQuestReward(Player* player, const Quest* quest, uint32) override
                {
                    if (quest->GetQuestId() != EVENT_ZONE_QUEST_FIND_LOST_DOG)
                        return;

                    me->SaySingleTarget("Můj kouzelný orb by měl mít teď dostatek energie na propůjčení vidiny toho, kdo může za Víčkovu smrt, použij ho!", LANG_UNIVERSAL, player);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_witchAI(creature);
            }
        };

        class event_zone_witch_dog : CreatureScript
        {
        public:
            event_zone_witch_dog() : CreatureScript("event_zone_witch_dog") { }

            struct event_zone_witch_dogAI : NullCreatureAI
            {
                event_zone_witch_dogAI(Creature* creature) : NullCreatureAI(creature) { }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (seer->GetTypeId() != TYPEID_PLAYER)
                        return true;

                    const Player* plr = seer->ToPlayer();
                    if (plr->IsGameMaster())
                        return false;

                    return plr->GetQuestStatus(EVENT_ZONE_QUEST_FIND_LOST_DOG) != QUEST_STATUS_REWARDED;
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_witch_dogAI(creature);
            }
        };

        class event_zone_lost_dog : CreatureScript
        {
        public:
            event_zone_lost_dog() : CreatureScript("event_zone_lost_dog") { }

            struct event_zone_lost_dogAI : CreatureAI
            {
                event_zone_lost_dogAI(Creature* creature) : CreatureAI(creature) { }
                void UpdateAI(uint32) override { }
                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (seer->GetTypeId() != TYPEID_PLAYER)
                        return true;

                    const Player* plr = seer->ToPlayer();
                    if (plr->IsGameMaster())
                        return false;

                    return plr->GetQuestStatus(EVENT_ZONE_QUEST_FIND_LOST_DOG) != QUEST_STATUS_INCOMPLETE;
                }

                void sQuestReward(Player* player, const Quest* quest, uint32) override
                {
                    if (quest->GetQuestId() != EVENT_ZONE_QUEST_FEED_DOG)
                        return;

                    if (Creature* dog = player->SummonCreature(EVENT_ZONE_CREATURE_LOST_DOG_MOVING, *me))
                    {
                        dog->SetOwnerGUID(player->GetGUID());
                        dog->GetMotionMaster()->MoveJump(1340.131f, -6178.524f, -1.232f, 4.687f, 5.0f, 10.0f);
                        dog->SaySingleTarget("Mmm, dobře tedy, tak pojďme!", LANG_UNIVERSAL, player);
                        player->CompleteQuest(EVENT_ZONE_QUEST_FIND_LOST_DOG);
                    }
                    player->UpdateObjectVisibility();
                }
            };

            bool OnGossipHello(Player* player, Creature* me) override
            {
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_FEED_DOG) != QUEST_STATUS_REWARDED)
                    return false;

                me->AI()->sQuestReward(player, sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_FEED_DOG), 0);
                CloseGossipMenuFor(player);
                return true;
            }

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_lost_dogAI(creature);
            }
        };

        class event_zone_lost_moving_dog : CreatureScript
        {
        public:
            event_zone_lost_moving_dog() : CreatureScript("event_zone_lost_moving_dog") { }

            struct event_zone_lost_moving_dogAI : CreatureAI
            {
                uint32 timer;
                event_zone_lost_moving_dogAI(Creature* creature) : CreatureAI(creature) { timer = 2000; }
                void UpdateAI(uint32 diff)
                {
                    if (timer > diff)
                    {
                        timer -= diff;
                        return;
                    }

                    if (!me->isMoving())
                        me->GetMotionMaster()->MoveFollow(me->GetOwner(), 1.0f, 0.0f);
                }

                bool IsNeverVisibleFor(const WorldObject* seer)
                {
                    if (const Player* player = seer->ToPlayer())
                    {
                        if (player->IsGameMaster())
                            return false;
                        return me->GetOwnerGUID() != seer->GetGUID();
                    }
                    return false;
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_lost_moving_dogAI(creature);
            }
        };
    }

    namespace Tavern
    {
        enum GuestHappiness
        {
            GUEST_AURA_HAPPY = 58499,
            GUEST_AURA_SAD   = 58496,
            GUEST_AURA_ANGRY = 58500,
        };

        enum GuestSpawnerActions
        {
            GUEST_SPAWNER_ACTION_SAD,
            GUEST_SPAWNER_ACTION_ANGRY,
            GUEST_SPAWNER_ACTION_GUEST_QUIT,
            GUEST_SPAWNER_ACTION_DESPAWN,
        };

        enum GuestGuidActions
        {
            GUID_ACTION_SET_OWNER
        };

        class event_zone_innkeeper : public InvestigatedCreature
        {
        public:
            event_zone_innkeeper() : InvestigatedCreature("event_zone_innkeeper", INVESTIGATION_INNKEEPER_GUESTS, EVENT_ZONE_QUEST_INNKEEPER_GUESTS) { }

            struct event_zone_innkeeperAI : public InvestigatedCreatureAI
            {
                event_zone_innkeeperAI(Creature* creature) : InvestigatedCreatureAI(creature) { }
                void sQuestAccept(Player* player, const Quest* quest)
                {
                    if (quest->GetQuestId() != EVENT_ZONE_QUEST_INNKEEPER_GUESTS)
                        return;

                    if (Creature* spawner = player->SummonCreature(EVENT_ZONE_CREATURE_INN_GUEST_SPAWNER, *me))
                    {
                        me->Whisper("Hoste prichazi, postarej se o ne!", LANG_UNIVERSAL, player, true);
                        spawner->SetOwnerGUID(player->GetGUID());
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_innkeeperAI(creature);
            }
        };

        class event_zone_inn_guest_spawner : public CreatureScript
        {
        public:
            event_zone_inn_guest_spawner() : CreatureScript("event_zone_inn_guest_spawner") { }

            // summon guests until quest failed / quest completed
            // count points - decrease pts for every unsatisfied customer / (points over time for being late with food / drink)
            struct event_zone_inn_guest_spawnerAI : CreatureAI
            {
                uint32 delay;
                int32 points;
                uint32 guests;
                event_zone_inn_guest_spawnerAI(Creature* creature) : CreatureAI(creature)
                {
                    points = 100;
                    guests = 0;
                    delay = 0;
                }

                struct GuestChair
                {
                    GuestChair(float o, std::vector<G3D::Vector3> path, uint8 additionalparts)
                    {
                        orientation = Position::NormalizeOrientation(o);
                        // begin of generic path to inn

                        m_path =
                        {
                            { 1739.774f + frand(-0.5f, 0.5f), -5876.911f + frand(-0.5f, 0.5f), 116.666f },
                            { 1740.053f + frand(-0.5f, 0.5f), -5871.718f + frand(-0.5f, 0.5f), 116.652f },
                            { 1739.870f + frand(-0.5f, 0.5f), -5878.635f + frand(-0.5f, 0.5f), 116.667f },
                        };

                        if (additionalparts)
                        {
                            m_path.emplace_back(1744.93f + frand(-0.5f, 0.5f), -5878.42f + frand(-0.5f, 0.5f), 116.67f);
                            m_path.emplace_back(1749.58f + frand(-0.5f, 0.5f), -5878.33f + frand(-0.5f, 0.5f), 116.04f);
                        }

                        if (additionalparts > 1)
                        {
                            m_path.emplace_back(1759.612f + frand(-0.5f, 0.5f), -5878.502f + frand(-0.5f, 0.5f), 116.665f);
                        }


                        // copy specific path to chair
                        m_path.insert(m_path.end(), path.begin(), path.end());
                    }
                    float orientation;
                    std::vector<G3D::Vector3> m_path;
                };

                std::vector<GuestChair> emptyGuestChairs
                {
                    {
                        3.156f,
                        {
                            { 1744.594f, -5883.555f, 116.668f },
                            { 1743.740f, -5886.215f, 116.874f }
                        },
                        0
                    },
                    {
                        4.654f,
                        {
                            { 1740.880f, -5884.566f, 116.670f },
                            { 1742.487f, -5884.659f, 116.876f }
                        },
                        0
                    },
                    {
                        6.281f,
                        {
                            { 1740.880f, -5884.566f, 116.670f },
                            { 1740.823f, -5885.957f, 116.877f }
                        },
                        0
                    },
                    {
                        1.608f,
                        {
                            { 1739.246f, -5887.297f, 116.668f },
                            { 1742.164f, -5887.427f, 116.875f }
                        },
                        0
                    },
                    {
                        1.575f,
                        {
                            { 1749.419f, -5875.357f, 116.042f },
                            { 1751.685f, -5874.564f, 116.232f }
                        },
                        1
                    },
                    {
                        3.085f,
                        {
                            { 1753.815f, -5876.064f, 116.042f },
                            { 1753.759f, -5873.118f, 116.230f }
                        },
                        1
                    },
                    {
                        3.063f,
                        {
                            { 1749.118f, -5885.479f, 116.259f },
                        },
                        1
                    },
                    {
                        1.602f,
                        {
                            { 1751.578f, -5887.067f, 116.041f },
                            { 1747.648f, -5886.708f, 116.259f }
                        },
                        1
                    },
                    {
                        0.021f,
                        {
                            { 1753.219f, -5880.302f, 116.042f },
                            { 1754.059f, -5882.524f, 116.232f }
                        },
                        1
                    },
                    {
                        4.710f,
                        {
                            { 1753.219f, -5880.302f, 116.042f },
                            { 1755.501f, -5881.393f, 116.232f }
                        },
                        1
                    },
                    {
                        1.604f,
                        {
                            { 1752.692f, -5884.442f, 116.042f },
                            { 1755.414f, -5883.864f, 116.232f },
                        },
                        1
                    },
                    {
                        1.564f,
                        {
                            { 1758.769f, -5870.170f, 116.665f },
                            { 1760.635f, -5869.755f, 116.889f }
                        },
                        2
                    },
                    {
                        6.184f,
                        {
                            { 1758.769f, -5870.170f, 116.665f },
                            { 1759.345f, -5868.248f, 116.889f }
                        },
                        2
                    }
                };

                std::unordered_map<ObjectGuid, GuestChair> fullGuestChairs;

                void SummonGuests(uint32 entry, uint8 count = 1)
                {
                    for (uint8 i = 0; i < count; ++i)
                    {
                        // do not summon more than allowed
                        if (emptyGuestChairs.empty())
                            return;

                        float xrng =  1727.679f + frand(-2.5f, 2.5f);
                        float yrng = -5871.776f + frand(-2.5f, 2.5f);
                        Position pos(xrng, yrng, 116.126f);
                        if (Creature* guest = me->SummonCreature(entry, pos))
                        {
                            guest->AI()->SetGUID(me->GetOwnerGUID(), GUID_ACTION_SET_OWNER);
                            guest->SetCritterGUID(me->GetGUID());
                            std::vector<GuestChair>::const_iterator itr = emptyGuestChairs.begin();
                            std::advance(itr, urand(0, static_cast<uint32>(emptyGuestChairs.size() - 1)));
                            guest->GetMotionMaster()->MoveSmoothPath(1000, &*itr->m_path.begin(), itr->m_path.size(), true);
                            GuestChair chair = *itr;
                            emptyGuestChairs.erase(itr);
                            fullGuestChairs.insert(std::pair<ObjectGuid, GuestChair>(guest->GetGUID(), chair));
                        }
                    }
                }

                class AllGuestsOwnedBy
                {
                public:
                    AllGuestsOwnedBy(ObjectGuid const owner, Unit const* searcher, float range) : i_owner(owner), i_searcher(searcher), i_range(range) { }
                    bool operator()(Unit* u)
                    {
                        if (!u->IsAIEnabled || u->GetAI()->GetGUID(GUID_ACTION_SET_OWNER) != i_owner || !i_searcher->IsWithinDistInMap(u, i_range))
                            return false;
                        return true;
                    }
                private:
                    ObjectGuid const i_owner;
                    Unit const* i_searcher;
                    float i_range;
                };

                void DespawnAllGuests()
                {
                    std::list<Creature*> guests;
                    AllGuestsOwnedBy checker(me->GetOwnerGUID(), me, 50.0f);
                    Trinity::CreatureListSearcher<AllGuestsOwnedBy> searcher(me, guests, checker);
                    me->VisitNearbyObject(50.0f, searcher);
                    for (Creature* guest : guests)
                        guest->DespawnOrUnsummon();
                    me->DespawnOrUnsummon();
                }


                void DoAction(int32 action) override
                {
                    switch(action)
                    {
                        case GUEST_SPAWNER_ACTION_SAD:
                            points -= 5;
                            break;
                        case GUEST_SPAWNER_ACTION_ANGRY:
                            points -= 10;
                            break;
                        case GUEST_SPAWNER_ACTION_GUEST_QUIT:
                            points -= 20;
                            break;
                        case GUEST_SPAWNER_ACTION_DESPAWN:
                        {
                            Creature* innkeeper = me->FindNearestCreature(EVENT_ZONE_CREATURE_INN_INNKEEPER_DAILY_QGIVER, 0.0f);
                            if (!innkeeper)
                                innkeeper = me->FindNearestCreature(EVENT_ZONE_CREATURE_INN_INNKEEPER_CREDIT, 0.0f);
                            if (innkeeper)
                                innkeeper->SaySingleTarget("Krása, děkuji ti za tvojí pomoc!", LANG_UNIVERSAL, me->GetCharmerOrOwnerPlayerOrPlayerItself());
                            DespawnAllGuests();
                            return;
                        }
                    }

                    if (points < 0)
                    {
                        if (Player* owner = ObjectAccessor::GetPlayer(*me, me->GetOwnerGUID()))
                        {
                            if (owner->IsActiveQuest(EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS) || owner->IsActiveQuest(EVENT_ZONE_QUEST_INNKEEPER_GUESTS))
                                owner->FailQuest(EVENT_ZONE_QUEST_INNKEEPER_GUESTS);
                        }
                        DespawnAllGuests();
                    }
                }

                void SetGUID(ObjectGuid guid, int32 spawning) override
                {
                    if (spawning)
                    {
                        auto itr = fullGuestChairs.find(guid);
                        if (itr == fullGuestChairs.end())
                            return;

                        GuestChair& c = itr->second;
                        if (Creature* guest = ObjectAccessor::GetCreature(*me, guid))
                        {
                            if (c.m_path.empty())
                                return;

                            Position pos(c.m_path.back());
                            pos.SetOrientation(c.orientation);
                            guest->SetUInt32Value(UNIT_NPC_EMOTESTATE, EVENT_ZONE_EMOTE_SIT);
                            guest->HandleEmoteCommand(EVENT_ZONE_EMOTE_SIT);
                            guest->NearTeleportTo(pos);
                        }
                    }
                    else
                    {
                        auto itr = fullGuestChairs.find(guid);
                        if (itr == fullGuestChairs.end())
                            return;

                        emptyGuestChairs.push_back(itr->second);
                        fullGuestChairs.erase(itr);
                    }
                }

                void UpdateAI(uint32 diff) override
                {
                    if (delay > diff)
                        delay -= diff;
                    else
                    {
                        uint32 guest = urand(0,7);
                        switch(guest)
                        {
                            case 0:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_GUEST);
                                break;
                            case 1:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_GUEST, 2);
                                delay += 5000;
                                break;
                            case 2:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_GUEST, 3);
                                delay += 12000;
                                break;
                            case 3:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_GUEST, 4);
                                delay += 17000;
                                break;
                            case 4:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_VIP_GUEST);
                                break;
                            case 5:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_VIP_GUEST, 2);
                                delay += 10000;
                                break;
                            case 6:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_IMPORTANT_GUEST);
                                break;
                            case 7:
                                SummonGuests(EVENT_ZONE_CREATURE_INN_IMPORTANT_GUEST, 2);
                                delay += 10000;
                                break;
                        }
                        delay += urand(15000, 20000);
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_inn_guest_spawnerAI(creature);
            }
        };

        enum GuestRequest : uint8
        {
            GUEST_REQUEST_MEAT,
            GUEST_REQUEST_BEER,
            GUEST_REQUEST_WINE,
            GUEST_REQUEST_BREAD,
            GUEST_REQUEST_END,
            GUEST_REQUEST_MAX = GUEST_REQUEST_END - 1
        };

        const std::string requestStrings[GUEST_REQUEST_END] =
        {
            "Chci pořádný kus masa!",
            "Přines mi 1 pivo!",
            "Dones mi sklenku vína!",
            "Mám hlad, dones mi nějaký chleba!"
        };

        const std::string angryStrings[GUEST_REQUEST_END] =
        {
            "Dočkám se toho masa?",
            "Mohu už dostat to pivo?!",
            "Kde mám to víno!",
            "Halo, chci ten chleba už!"
        };

        const std::string giveStrings[GUEST_REQUEST_END] =
        {
            "Dat hostovi maso",
            "Dat hostovi pivo",
            "Dat hostovi vino",
            "Dat hostovi chleba"
        };

        const std::string carryString[GUEST_REQUEST_END] =
        {
            " maso!",
            " pivo!",
            " vino!",
            " chleba!"
        };

        class event_zone_inn_food : public GameObjectScript
        {
        public:
            event_zone_inn_food() : GameObjectScript("event_zone_inn_food") { }

            bool OnGossipHello(Player* player, GameObject* obj) override
            {
                if (!player->IsActiveQuest(EVENT_ZONE_QUEST_INNKEEPER_GUESTS) && !player->IsActiveQuest(EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS))
                    return true;

                uint8 carry = 0;
                switch(obj->GetEntry())
                {
                    case EVENT_ZONE_GAMEOBJECT_INN_BEER:
                        carry = GUEST_REQUEST_BEER;
                        break;
                    case EVENT_ZONE_GAMEOBJECT_INN_BREAD:
                        carry = GUEST_REQUEST_BREAD;
                        break;
                    case EVENT_ZONE_GAMEOBJECT_INN_MEAT:
                        carry = GUEST_REQUEST_MEAT;
                        break;
                    case EVENT_ZONE_GAMEOBJECT_INN_WINE:
                        carry = GUEST_REQUEST_WINE;
                        break;
                }

                Aura* aur = player->GetAura(EVENT_ZONE_AURA_INN_CARRYING_REQUESTED_FOOD);
                if (aur)
                {
                    int32 amount = aur->GetEffect(EFFECT_0)->GetAmount();
                    if (amount == carry)
                        return true;
                    std::string str = player->getGender() == GENDER_FEMALE ? "Ztratila si" : "Ztratil si";
                    str += carryString[amount];
                    player->Unit::Whisper(str, LANG_UNIVERSAL, player, true);
                }
                else
                    aur = player->AddAura(EVENT_ZONE_AURA_INN_CARRYING_REQUESTED_FOOD, player);

                if (aur)
                {
                    std::string str = player->getGender() == GENDER_FEMALE ? "Obdrzela si" : "Obdrzel si";
                    aur->GetEffect(EFFECT_0)->SetAmount(carry);
                    player->Unit::Whisper(str + carryString[carry], LANG_UNIVERSAL, player, true);
                }
                return true;
            }
        };

        class event_zone_inn_guest : public CreatureScript
        {
        public:
            event_zone_inn_guest() : CreatureScript("event_zone_inn_guest") { }
            struct event_zone_inn_guestAI : public CreatureAI
            {
                uint8 phase;
                bool talked;
                bool served;
                uint8 request;
                ObjectGuid ownerGuid;
            public:
                event_zone_inn_guestAI(Creature* creature) : CreatureAI(creature)
                {
                    talked = false;
                    served = false;
                    phase = 3;
                }

                class ServedHostEvent : public BasicEvent
                {
                public:
                    ServedHostEvent(Creature* creature, bool angry) : BasicEvent()
                    {
                        this->angry = angry;
                        owner = creature;
                    }
                    bool Execute(uint64, uint32)
                    {
                        if (Creature* spawner = ObjectAccessor::GetCreature(*owner, owner->GetCritterGUID()))
                            spawner->GetAI()->SetGUID(owner->GetGUID(), false);
                        if (!angry)
                            owner->Say("Děkuji, na shledanou!", LANG_UNIVERSAL);
                        owner->DespawnOrUnsummon();
                        return true;
                    }
                private:
                    Creature* owner;
                    bool angry;
                };

                ObjectGuid GetGUID(int32 action) const override
                {
                    if (action == GUID_ACTION_SET_OWNER)
                        return ownerGuid;
                    return ObjectGuid::Empty;
                }

                void SetGUID(ObjectGuid guid, int32 action) override
                {
                    if (action == GUID_ACTION_SET_OWNER)
                        ownerGuid = guid;
                }

                void DoSpawnerAction(GuestSpawnerActions action)
                {
                    if (Creature* spawner = ObjectAccessor::GetCreature(*me, me->GetCritterGUID()))
                        spawner->GetAI()->DoAction(action);
                }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (const Player* pseer = seer->ToPlayer())
                    {
                        if (pseer->IsGameMaster())
                            return false;

                        if (seer->GetGUID() != ownerGuid)
                            return true;

                        if (!pseer->IsActiveQuest(EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS) && !pseer->IsActiveQuest(EVENT_ZONE_QUEST_INNKEEPER_GUESTS))
                            return true;

                        return false;
                    }
                    return true;
                }

                void Despawn(uint32 time)
                {
                    me->m_Events.AddEvent(new ServedHostEvent(me, time == 0), me->m_Events.CalculateTime(time));
                }

                void MovementInform(uint32, uint32) override
                {
                    if (Creature* spawner = ObjectAccessor::GetCreature(*me, me->GetCritterGUID()))
                        spawner->GetAI()->SetGUID(me->GetGUID(), true);
                }

                void sGossipSelect(Player* player, uint32, uint32 menuId) override
                {
                    if (menuId)
                    {
                        uint32 effAmount = player->PlayerTalkClass->GetGossipOptionAction(menuId) - GOSSIP_ACTION_INFO_DEF;
                        if (effAmount == request)
                        {
                            served = true;
                            me->Say("Děkuji Ti!", LANG_UNIVERSAL);
                            me->RemoveAllAuras();
                            player->RemoveAura(EVENT_ZONE_AURA_INN_CARRYING_REQUESTED_FOOD);
                            player->KilledMonsterCredit(EVENT_ZONE_CREATURE_INN_INNKEEPER_CREDIT);
                            if (player->GetQuestStatus(EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS) == QUEST_STATUS_COMPLETE
                                || player->GetQuestStatus(EVENT_ZONE_QUEST_INNKEEPER_GUESTS) == QUEST_STATUS_COMPLETE)
                                if (Creature* spawner = ObjectAccessor::GetCreature(*me, me->GetCritterGUID()))
                                    spawner->GetAI()->DoAction(GUEST_SPAWNER_ACTION_DESPAWN);
                            CloseGossipMenuFor(player);
                            Despawn(urand(12000, 25000));
                        }
                        else
                        {
                            // switch to next anger level O.o
                            me->RemoveAllAuras();
                            me->Say("Tohle nebylo v mé objednávce!", LANG_UNIVERSAL);
                            CloseGossipMenuFor(player);
                        }
                    }
                    else
                        me->Say(requestStrings[request], LANG_UNIVERSAL);
                }

                void sGossipHello(Player* player) override
                {
                    if (served || me->isMoving())
                    {
                        CloseGossipMenuFor(player);
                        return;
                    }

                    if (!talked)
                    {
                        talked = true;
                        request = urand(0, GUEST_REQUEST_MAX);
                        me->Say(requestStrings[request], LANG_UNIVERSAL);
                        return;
                    }
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Zeptat se hosta co chce.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
                    if (Aura* carryAura = player->GetAura(EVENT_ZONE_AURA_INN_CARRYING_REQUESTED_FOOD))
                    {
                        uint32 effAmount = carryAura->GetEffect(EFFECT_0)->GetAmount();
                        AddGossipItemFor(player, GOSSIP_ICON_CHAT, giveStrings[effAmount], GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + effAmount);
                    }
                    SendGossipMenuFor(player, 1, me);
                }

                void AddAura(uint32 id)
                {
                    uint32 duration = 0;
                    switch(me->GetEntry())
                    {
                        case EVENT_ZONE_CREATURE_INN_VIP_GUEST:
                            duration = 25000;
                            break;
                        case EVENT_ZONE_CREATURE_INN_IMPORTANT_GUEST:
                            duration = 40000;
                            break;
                        case EVENT_ZONE_CREATURE_INN_GUEST:
                            duration = 60000;
                            break;
                    }

                    if (Aura* aur = me->AddAura(id, me))
                    {
                        aur->SetMaxDuration(duration);
                        aur->SetDuration(duration);
                    }

                    --phase;
                }

                void UpdateAI(uint32) override
                {
                    if (served)
                        return;

                    if (me->isMoving())
                        return;

                    if (me->GetAppliedAuras().empty())
                    {
                        switch(phase)
                        {
                            case 3:
                                AddAura(GUEST_AURA_HAPPY);
                                me->Say("Chci si něco objednat, mohl by prosím někdo přijít?", LANG_UNIVERSAL);
                                break;
                            case 2:
                                AddAura(GUEST_AURA_SAD);
                                if (talked)
                                    me->Say(angryStrings[request], LANG_UNIVERSAL);
                                else
                                    me->Say("Halo! Chci si objednat!", LANG_UNIVERSAL);
                                DoSpawnerAction(GUEST_SPAWNER_ACTION_SAD);
                                break;
                            case 1:
                                AddAura(GUEST_AURA_ANGRY);
                                if (talked)
                                    me->Say(angryStrings[request], LANG_UNIVERSAL);
                                else
                                    me->Say("Tak je tu někdo! Chci si už objednat!", LANG_UNIVERSAL);
                                DoSpawnerAction(GUEST_SPAWNER_ACTION_ANGRY);
                                break;
                            case 0:
                                me->Say("Co je tohle za hostinec! Tu se ani nedočkám, sbohem!", LANG_UNIVERSAL);
                                DoSpawnerAction(GUEST_SPAWNER_ACTION_GUEST_QUIT);
                                me->DespawnOrUnsummon(0);
                                return;
                        }
                        return;
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_inn_guestAI(creature);
            }
        };
    }

    namespace Blacksmith
    {
        class event_zone_blacksmith : public InvestigatedCreature
        {
        public:
            event_zone_blacksmith() : InvestigatedCreature("event_zone_blacksmith", INVESTIGATION_BLACKSMITH, EVENT_ZONE_QUEST_BLACKSMITH_MINE_CART) { }
            struct event_zone_blacksmithAI : public InvestigatedCreatureAI
            {
                event_zone_blacksmithAI(Creature* creature) : InvestigatedCreatureAI(creature)
                {
                    // forced display id because of trinitycore random display id selection
                    me->SetNativeDisplayId(EVENT_ZONE_DISPLAYID_BLACKSMITH_DISPLAY_ID);
                    me->SetDisplayId(EVENT_ZONE_DISPLAYID_BLACKSMITH_DISPLAY_ID);
                    me->m_SightDistance = 2.0f;
                }

                void MoveInLineOfSight(Unit* unit) override
                {
                    if (unit->GetEntry() != EVENT_ZONE_CREATURE_BLACKSMITH_MINE_CART)
                        return;

                    Unit* owner = unit->GetOwner();
                    if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                    {
                        Player* powner = owner->ToPlayer();
                        powner->KilledMonsterCredit(EVENT_ZONE_CREATURE_BLACKSMITH_MINE_CART);
                    }
                    unit->ToCreature()->DespawnOrUnsummon();
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_blacksmithAI(creature);
            }
        };

        class event_zone_blacksmith_mine_car : CreatureScript
        {
        public:
            event_zone_blacksmith_mine_car() : CreatureScript("event_zone_blacksmith_mine_car") { }
            struct event_zone_blacksmith_mine_carAI : CreatureAI
            {
                uint32 timer;
                event_zone_blacksmith_mine_carAI(Creature* creature) : CreatureAI(creature) { }

                void Reset() override
                {
                    timer = 30000;
                    me->SetOwnerGUID(ObjectGuid::Empty);
                }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (const Player* player = seer->ToPlayer())
                    {
                        if (player->IsGameMaster())
                            return false;
                        return me->GetOwnerGUID() && player->GetGUID() != me->GetOwnerGUID();
                    }
                    return false;
                }

                void UpdateAI(uint32 diff) override
                {
                    if (timer > diff)
                    {
                        timer -= diff;
                        return;
                    }
                    me->DespawnOrUnsummon();
                }

                void sGossipHello(Player* player) override
                {
                    if (!me->GetOwnerGUID())
                        me->SetOwnerGUID(player->GetGUID());
                    else if (me->GetOwnerGUID() != player->GetGUID())
                        return;

                    me->GetMotionMaster()->MovePoint(0, *player, false);
                    timer = 30000;
                    CloseGossipMenuFor(player);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_blacksmith_mine_carAI(creature);
            }
        };

        class event_zone_blacksmith_mine_car_spawner : CreatureScript
        {
        public:
            event_zone_blacksmith_mine_car_spawner() : CreatureScript("event_zone_blacksmith_mine_car_spawner") { }
            struct event_zone_blacksmith_mine_car_spawnerAI : CreatureAI
            {
                uint32 timer;
                event_zone_blacksmith_mine_car_spawnerAI(Creature* creature) : CreatureAI(creature)
                {
                    timer = 5000;
                }
                void UpdateAI(uint32 diff)
                {
                    if (timer > diff)
                        timer -= diff;

                    timer = 5000;
                    if (me->FindNearestCreature(EVENT_ZONE_CREATURE_BLACKSMITH_MINE_CART, 20.0f))
                        return;

                    me->SummonCreature(EVENT_ZONE_CREATURE_BLACKSMITH_MINE_CART, *me);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_blacksmith_mine_car_spawnerAI(creature);
            }
        };
    }

    namespace FishingPool
    {
        class event_zone_fisherman : public InvestigatedCreature
        {
        public:
            event_zone_fisherman() : InvestigatedCreature("event_zone_fisherman", INVESTIGATION_FISHERMAN_FISH, EVENT_ZONE_QUEST_FISHERMAN_HUNGER) { }
        };

        class event_zone_fish : public CreatureScript
        {
            enum FishSpells
            {
                SHARK_SPELL_BITE_ANIMATION        = 45774,
                HAMMERHEAD_SHARK_SPELL_HEAD_SMASH = 14102,
                SHARK_SPELL_INFECTED_WOUNDS       = 58181
            };
        public:
            event_zone_fish() : CreatureScript("event_zone_fish") { }
            bool OnGossipHello(Player* player, Creature* me) override
            {
                // disable interaction while being stunned
                if (player->HasUnitState(UNIT_STATE_STUNNED))
                    return true;

                if (roll_chance_i(15))
                {
                    if (roll_chance_i(60))
                    {
                        player->KilledMonsterCredit(EVENT_ZONE_CREATURE_CORRECT_FISH_CREDIT);
                        me->DespawnOrUnsummon(0, Seconds(30));
                        me->Whisper("Chytil si rybu!", LANG_UNIVERSAL, player, true);
                        return true;
                    }
                    me->CastSpell(player, SHARK_SPELL_BITE_ANIMATION, true);
                    me->AddAura(me->GetEntry() == EVENT_ZONE_CREATURE_HAMMERHEAD_SHARK ? HAMMERHEAD_SHARK_SPELL_HEAD_SMASH : SHARK_SPELL_INFECTED_WOUNDS, player);
                }
                me->GetMotionMaster()->MoveFleeing(player, 1000);
                return true;
            }
        };
    }

    namespace AppleFarm
    {
        class event_zone_apple_farm_farmer : public InvestigatedCreature
        {
        public:
            event_zone_apple_farm_farmer() : InvestigatedCreature("event_zone_apple_farm_farmer", INVESTIGATION_APPLE_FARM_WORMS, EVENT_ZONE_QUEST_APPLE_FARM_WORMS) { }

            bool OnQuestAccept(Player* player, Creature* me, const Quest* quest) override
            {
                if (quest->GetQuestId() != EVENT_ZONE_QUEST_APPLE_FARM_WORMS)
                    return true;

                if (TempSummon* spawner = me->SummonCreature(EVENT_ZONE_CREATURE_APPLE_FARM_WORM_SPAWNER, *me, TEMPSUMMON_MANUAL_DESPAWN, 15 * MINUTE * IN_MILLISECONDS))
                    spawner->SetOwnerGUID(player->GetGUID());

                me->Say("Pozor, tamhle vylejzá červ! Zašlápni ho, než se vynoří ze země!", LANG_UNIVERSAL);

                return true;
            }
        };

        class event_zone_apple_farm_worm_spawner : public CreatureScript
        {
        public:
            event_zone_apple_farm_worm_spawner() : CreatureScript("event_zone_apple_farm_worm_spawner") { }

            struct event_zone_apple_farm_worm_spawnerAI : CreatureAI
            {
                uint32 timer;
                bool spawned;
                event_zone_apple_farm_worm_spawnerAI(Creature* creature) : CreatureAI(creature)
                {
                    timer = 5000;
                    spawned = false;
                }

                void SummonedCreatureDespawn(Creature*) override
                {
                    spawned = false;
                }

                void JustSummoned(Creature*) override
                {
                    spawned = true;
                }

                void UpdateAI(uint32 diff) override
                {
                    // worm is spawned, wait until it dies
                    if (spawned)
                        return;

                    if (timer > diff)
                        timer -= diff;
                    else
                        timer = 5000;

                    Player* plrTrigger = ObjectAccessor::GetPlayer(*me, me->GetOwnerGUID());
                    if (!plrTrigger)
                        return;

                    std::list<Creature*> creatures;
                    me->GetCreatureListWithEntryInGrid(creatures, EVENT_ZONE_CREATURE_APPLE_FARM_WORM_HOLE);
                    if (creatures.empty())
                        return;

                    auto itr = creatures.begin();
                    std::advance(itr, urand(0, static_cast<uint32>(creatures.size() - 1)));
                    Creature* hole = *itr;
                    if (Creature* worm = me->SummonCreature(EVENT_ZONE_CREATURE_APPLE_FARM_WORM, *hole, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, MINUTE * IN_MILLISECONDS))
                    {
                        uint8 lvl = plrTrigger->getLevel();
                        worm->SetLevel(lvl);
                        uint32 hp = static_cast<uint32>(lvl < 20 ? (lvl * 100) : (lvl * 100 * float(lvl) / 20.0f));
                        float minDamage = float(hp) / 14.0f;
                        minDamage *= 1.0f + (float(lvl) / 80.0f);
                        float maxDamage = minDamage * 1.4f;
                        worm->SetMaxHealth(hp);
                        worm->SetHealth(hp);
                        worm->ResetPlayerDamageReq();
                        worm->SetStatFloatValue(UNIT_FIELD_MINDAMAGE, minDamage);
                        worm->SetStatFloatValue(UNIT_FIELD_MAXDAMAGE, maxDamage);
                        worm->m_SightDistance = 1.0f;
                        worm->SetOwnerGUID(me->GetGUID());
                        if (Aura* burrow = worm->AddAura(EVENT_ZONE_AURA_APPLE_FARM_WORM_BURROW, worm))
                            burrow->SetDuration(8000);

                        spawned = true;
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_apple_farm_worm_spawnerAI(creature);
            }
        };

        class event_zone_apple_farm_worm : public CreatureScript
        {
        public:
            event_zone_apple_farm_worm() : CreatureScript("event_zone_apple_farm_worm") { }

            struct event_zone_apple_farm_wormAI : CreatureAI
            {
                event_zone_apple_farm_wormAI(Creature* creature) : CreatureAI(creature) { }

                void JustDied(Unit* killer) override
                {
                    me->DespawnOrUnsummon(Seconds(5));
                    Creature* spawner = me->GetOwner() ? me->GetOwner()->ToCreature() : nullptr;
                    if (!spawner)
                        return;

                    if (Player* pkiller = killer->GetCharmerOrOwnerPlayerOrPlayerItself())
                    {
                        if (pkiller->GetQuestStatus(EVENT_ZONE_QUEST_APPLE_FARM_WORMS) == QUEST_STATUS_COMPLETE)
                        {
                            spawner->DespawnOrUnsummon(Seconds(0));
                            return;
                        }
                    }
                    spawner->SetPetGUID(ObjectGuid::Empty);
                }

                void DamageTaken(Unit* attacker, uint32&) override
                {
                    AttackStart(attacker);
                }

                void SpellHit(Unit* caster, const SpellInfo*) override
                {
                    AttackStart(caster);
                }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (!seer->isType(TYPEMASK_UNIT))
                        return false;

                    if (const Player* plr = seer->ToUnit()->ToPlayer())
                        if (plr->IsGameMaster())
                            return false;

                    if (Unit* owner = me->GetOwner())
                    {
                        Unit* questStarter = owner->GetOwner();
                        Player* seerPlr = seer->ToUnit()->GetCharmerOrOwnerPlayerOrPlayerItself();
                        return seerPlr && questStarter != seerPlr && !seerPlr->IsGameMaster();
                    }

                    return false;
                }

                void MoveInLineOfSight(Unit* unit) override
                {
                    if (!unit->CanSeeOrDetect(me))
                        return;

                    if (me->HasAura(EVENT_ZONE_AURA_APPLE_FARM_WORM_BURROW))
                    {
                        if (unit->GetTypeId() != TYPEID_PLAYER)
                            return;

                        Player* player = unit->ToPlayer();

                        if (!player->IsActiveQuest(EVENT_ZONE_QUEST_APPLE_FARM_WORMS))
                            return;

                        player->KilledMonsterCredit(me->GetEntry());
                        me->KillSelf();
                        return;
                    }

                    me->m_SightDistance = me->m_CombatDistance;
                    CreatureAI::MoveInLineOfSight(unit);
                }

                void UpdateAI(uint32 /*diff*/) override
                {
                    DoMeleeAttackIfReady();
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_apple_farm_wormAI(creature);
            }
        };
    }

    namespace Doctor
    {
        const std::vector<G3D::Vector3> waypoints
        {
            { 1756.17f, -5874.65f, 123.52f },
            { 1757.16f, -5874.77f, 123.52f },
            { 1753.39f, -5879.07f, 123.52f },
            { 1756.59f, -5881.95f, 123.52f },
            { 1762.37f, -5881.98f, 119.89f },
            { 1763.81f, -5884.79f, 119.89f },
            { 1761.90f, -5887.27f, 119.87f },
            { 1755.01f, -5886.80f, 116.04f },
            { 1749.64f, -5879.37f, 116.04f },
            { 1741.14f, -5878.65f, 116.67f },
            { 1739.98f, -5872.06f, 116.65f },
            { 1729.63f, -5871.93f, 116.13f },
            { 1703.03f, -5871.62f, 116.13f },
            { 1693.95f, -5880.46f, 116.14f },
            { 1665.94f, -5897.00f, 116.15f },
            { 1652.30f, -5910.55f, 116.54f },
            { 1633.93f, -5913.20f, 116.22f },
            { 1593.03f, -5908.47f, 117.27f },
            { 1539.44f, -5894.66f, 128.43f },
            { 1509.86f, -5885.87f, 131.07f },
            { 1493.29f, -5860.47f, 131.22f },
            { 1488.23f, -5822.01f, 131.21f },
            { 1483.31f, -5804.17f, 131.21f },
            { 1442.87f, -5756.43f, 131.21f },
            { 1424.99f, -5748.32f, 131.21f },
            { 1397.75f, -5710.30f, 135.06f },
            { 1390.38f, -5704.28f, 136.04f },
            { 1386.99f, -5701.68f, 137.90f },
            { 1363.74f, -5685.48f, 138.66f },
            { 1364.32f, -5683.41f, 138.66f },
            { 1364.51f, -5680.54f, 138.66f }
        };

        static std::vector<Monolog> monolog
        {
            { Milliseconds(30000), "No dobrá $N, půjdeme se na to spolu podívat. Musíš mě ale vést, nevím kde si mrtvolu $Gnašel:našla;." },
            { Milliseconds(20600), "Tak krásný den je dneska..." },
            { Milliseconds(29000), "Tebe jsem tu ještě neviděla, ty tu budeš pravděpodobně $Gněkdo nový:nová;..." },
            { Milliseconds(25400), "Je to dlouhá cesta, tak si můžeme spolu klidně povídat." },
            { Milliseconds(20600), "Moc toho nenamluvíš..." },
            { Milliseconds(20600), "Tak kde si $Gviděl:viděla; tu mrtvolu?" },
            { Milliseconds(20600), "Zajímalo by mě jak umřel." },
            { Milliseconds(29000), "Ještě že jsem zůstala ve městě. Měla jsem mít cestu do jiného města. $GPřišel:Přišla; si včas." },
            { Milliseconds(25400), "Doufám, že si se moc $Gnevyděsil:nevyděsila;, když si $Gviděl:viděla; mrtvolu..." },
            {  Milliseconds(4000), "Prokrista..." },
            { Milliseconds(10000), "Kdo to mohl udělat? Co se tady mohlo stát?" },
            { Milliseconds(15000), "Musím říct, že Víčko byl zavražděn velmi krutým způsobem." },
            {  Milliseconds(8000), "To nemůže nikomu projít! Někdo musí najít jeho vraha!" },
            { Milliseconds(15000), "Utíkej to říct na radnici. On radní Josef už bude vědět co má dělat." }
        };

        class event_zone_investigating_doctor : CreatureScript
        {

        public:
            event_zone_investigating_doctor() : CreatureScript("event_zone_investigating_doctor") { }
            struct event_zone_investigating_doctorAI : CreatureAI
            {
                TaskScheduler scheduler;
                uint32 talkIndex;
                bool kneel;
                event_zone_investigating_doctorAI(Creature* creature) : CreatureAI(creature) { }

                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (const Player* pseer = seer->ToPlayer())
                    {
                        if (pseer->IsGameMaster())
                            return false;
                        return me->GetOwnerGUID() != seer->GetGUID();
                    }
                    return true;
                }

                void ScheduleEvents()
                {
                    scheduler.Schedule(Seconds(0), [this](TaskContext task)
                    {
                        if (talkIndex >= monolog.size())
                        {
                            me->DespawnOrUnsummon();
                            if (Player* owner = me->GetCharmerOrOwnerPlayerOrPlayerItself())
                            {
                                if (owner->IsActiveQuest(EVENT_ZONE_QUEST_DOCTOR_DEATH_INVESTIGATION))
                                    owner->CompleteQuest(EVENT_ZONE_QUEST_DOCTOR_DEATH_INVESTIGATION);
                            }
                            return;
                        }

                        me->Say(monolog[talkIndex].text, LANG_UNIVERSAL, me->GetOwner());
                        task.Repeat(monolog[talkIndex++].delay);
                        if (!kneel && !me->isMoving())
                        {
                            kneel = true;
                            me->AddAura(EVENT_ZONE_AURA_KNEEL, me);
                        }
                    });
                }

                void Reset() override
                {
                    kneel = false;
                    ScheduleEvents();
                    talkIndex = 0;
                    me->GetMotionMaster()->MoveSmoothPath(0, &waypoints.front(), waypoints.size(), true);
                }

                void UpdateAI(uint32 diff) override
                {
                    scheduler.Update(diff);
                }
            };
            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_investigating_doctorAI(creature);
            }
        };

        class event_zone_doctor : CreatureScript
        {
        public:
            event_zone_doctor() : CreatureScript("event_zone_doctor") { }

            struct event_zone_doctorAI : NullCreatureAI
            {
                event_zone_doctorAI(Creature* creature) : NullCreatureAI(creature) { }
                bool IsNeverVisibleFor(const WorldObject* seer) override
                {
                    if (seer->GetTypeId() != TYPEID_PLAYER)
                        return true;
                    // if completed or accepted - then invisible, if no quest status, then visible
                    return seer->ToPlayer()->GetQuestStatus(EVENT_ZONE_QUEST_DOCTOR_DEATH_INVESTIGATION) != QUEST_STATUS_NONE;
                }

                void sQuestAccept(Player* player, const Quest*) override
                {
                    if (Creature* doctor = player->SummonCreature(EVENT_ZONE_CREATURE_INVESTIGATING_DOCTOR,*me))
                    {
                        doctor->SetOwnerGUID(player->GetGUID());
                        doctor->SetTarget(player->GetGUID());
                    }
                    player->UpdateObjectVisibility();
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_doctorAI(creature);
            }
        };
    }
}

namespace CryptEvent
{
    enum GUIDActions
    {
        GUID_SET_GENERAL,
        GUID_ADD_SOLDIER,
        GUID_SET_PRISONER,
        GUID_SET_INNKEEPER,
        GUID_ENEMY_SPAWNED,
        GUID_SET_FAKE_OWNER,
    };

    enum Actions
    {
        ACTION_EVENT_PROGRESS,
        ACTION_DESPAWN_PORTAL,
        ACTION_END_EVENT_PHASE,
        ACTION_SOLDIER_MOVE_INSIDE_CRYPT,
    };

    enum DataActions
    {
        DATA_SET_INDEX,
        DATA_SET_SOLDIERS_COUNT,
    };

    enum SoldierPhases
    {
        PHASE_NONE,
        PHASE_CHASING,
        PHASE_CHAINING,
        PHASE_GOING_TO_JAIL,
    };

    const Position blacksmithPos
    {
        1885.878f, -5795.466f, 101.907f, 1.438f
    };

    const Position farmerPos
    {
        1850.974f, -5782.096f, 103.213f, 1.490f
    };

    const Position fishermanPos
    {
        1874.457f, -5772.673f, 104.179f, 1.503f
    };

    const Position innkeeperPos
    {
        1879.360f, -5771.632f, 84.020f, 4.093f
    };

    const Position insideCrypt
    {
        1876.865f, -5762.930f, 83.855f, 4.725f
    };

    const Position openerPos
    {
        1876.698f, -5776.679f, 87.514f, 1.585f
    };

    const Position evilSpawnPos
    {
        1876.698f, -5776.679f, 85.014f, 1.585f
    };

    class event_zone_general : public CreatureScript
    {
    public:
        event_zone_general() : CreatureScript("event_zone_general") { }
        struct event_zone_generalAI : NullCreatureAI
        {
            event_zone_generalAI(Creature* creature) : NullCreatureAI(creature) { }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;
                    return pseer->GetQuestStatus(EVENT_ZONE_QUEST_CRYPT_INSPECTION) == QUEST_STATUS_INCOMPLETE;
                }
                return true;
            }

            void SummonCreature(uint32 entry, Position& pos, std::list<Creature*>& summons)
            {
                summons.push_back(me->SummonCreature(entry, pos));
            }

            void sQuestAccept(Player* player, const Quest* quest) override
            {
                if (quest->GetQuestId() == EVENT_ZONE_QUEST_CRYPT_INSPECTION)
                {
                    std::list<Creature*> summons;
                    std::list<Creature*> soldiers;
                    GetCreatureListWithEntryInGrid(soldiers, me, EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_1, 20.0f);
                    for (auto soldier : soldiers)
                        SummonCreature(EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_SUMMON_1, *soldier, summons);
                    soldiers.clear();
                    GetCreatureListWithEntryInGrid(soldiers, me, EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_2, 20.0f);
                    for (auto soldier : soldiers)
                        SummonCreature(EVENT_ZONE_CREATURE_SALVADOR_CRUSADER_SUMMON_2, *soldier, summons);
                    Creature* general = me->SummonCreature(EVENT_ZONE_CREATURE_GENERAL_SALVADOR_SUMMON, *me);
                    general->AI()->SetGUID(player->GetGUID(), GUID_SET_FAKE_OWNER);
                    uint32 index = 0;
                    for (Creature* summon : summons)
                    {
                        general->AI()->SetGUID(summon->GetGUID(), GUID_ADD_SOLDIER);
                        summon->AI()->SetGUID(general->GetGUID(), GUID_SET_GENERAL);
                        summon->AI()->SetData(index++, DATA_SET_INDEX);
                        summon->AI()->SetData(static_cast<uint32>(summons.size()), DATA_SET_SOLDIERS_COUNT);
                        summon->AI()->SetGUID(player->GetGUID(), GUID_SET_FAKE_OWNER);
                    }
                    player->UpdateObjectVisibility();
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_generalAI(creature);
        }
    };

    class event_zone_general_soldier : public CreatureScript
    {
    public:
        event_zone_general_soldier() : CreatureScript("event_zone_general_soldier") { }
        struct event_zone_general_soldierAI : NullCreatureAI
        {
            event_zone_general_soldierAI(Creature* creature) : NullCreatureAI(creature) { }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;
                    return pseer->GetQuestStatus(EVENT_ZONE_QUEST_CRYPT_INSPECTION) == QUEST_STATUS_INCOMPLETE;
                }
                return false;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_general_soldierAI(creature);
        }
    };

    class GuiltyScript : public CreatureScript
    {
    public:
        GuiltyScript(const char* scriptName) : CreatureScript(scriptName) { }
        struct GuiltyAI : NullCreatureAI
        {
            GuiltyAI(Creature* creature) : NullCreatureAI(creature)
            {

            }

            void Reset() override
            {
                me->AddAura(EVENT_ZONE_AURA_KNEEL, me);
            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;

                    return me->GetOwnerGUID() != pseer->GetGUID();
                }
                return false;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new GuiltyAI(creature);
        }
    };

    class event_zone_crypt_farmer : public GuiltyScript
    {
    public:
        event_zone_crypt_farmer() : GuiltyScript("event_zone_crypt_farmer") { }
    };

    class event_zone_crypt_innkeeper : public CreatureScript
    {
    public:
        event_zone_crypt_innkeeper() : CreatureScript("event_zone_crypt_innkeeper") { }
        struct event_zone_crypt_innkeeperAI : NullCreatureAI
        {
            event_zone_crypt_innkeeperAI(Creature* creature) : NullCreatureAI(creature) { }
            void Reset() override
            {
                me->RemoveAllAuras();
            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;

                    return me->GetOwnerGUID() != pseer->GetGUID();
                }
                return false;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_crypt_innkeeperAI(creature);
        }
    };

    class event_zone_crypt_fisherman : public GuiltyScript
    {
    public:
        event_zone_crypt_fisherman() : GuiltyScript("event_zone_crypt_fisherman") { }
    };

    class event_zone_crypt_blacksmith : public GuiltyScript
    {
    public:
        event_zone_crypt_blacksmith() : GuiltyScript("event_zone_crypt_blacksmith") { }
        struct event_zone_crypt_blacksmithAI : GuiltyAI
        {
            event_zone_crypt_blacksmithAI(Creature* creature) : GuiltyAI(creature)
            {
                // forced display id because of trinitycore random display id selection
                creature->SetNativeDisplayId(EVENT_ZONE_DISPLAYID_BLACKSMITH_DISPLAY_ID);
                creature->SetDisplayId(EVENT_ZONE_DISPLAYID_BLACKSMITH_DISPLAY_ID);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_crypt_blacksmithAI(creature);
        }
    };

    class event_zone_crypt_event_dummy : public CreatureScript
    {
    public:
        event_zone_crypt_event_dummy() : CreatureScript("event_zone_crypt_event_dummy") { }
        struct event_zone_crypt_event_dummyAI : NullCreatureAI
        {
            event_zone_crypt_event_dummyAI(Creature* creature) : NullCreatureAI(creature)
            {
                me->m_SightDistance = 2.0f;
            }

            void MoveInLineOfSight(Unit* unit) override
            {
                if (unit->GetEntry() != EVENT_ZONE_CREATURE_GENERAL_SALVADOR_SUMMON)
                    return;

                if (std::abs(unit->GetPositionZ() - me->GetPositionZ()) < 3.0f)
                    unit->ToCreature()->AI()->DoAction(ACTION_EVENT_PROGRESS);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_crypt_event_dummyAI(creature);
        }
    };

    class event_zone_crypt_event_portal_opener : public CreatureScript
    {
    public:
        event_zone_crypt_event_portal_opener() : CreatureScript("event_zone_crypt_event_portal_opener") { }
        struct event_zone_crypt_event_portal_openerAI : CreatureAI
        {
            ObjectGuid innkeeperGuid, generalGuid, ownerGuid;
            TaskScheduler scheduler;
            uint8 counter;
            event_zone_crypt_event_portal_openerAI(Creature* creature) : CreatureAI(creature)
            {

            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (seer->GetTypeId() != TYPEID_PLAYER)
                    return false;

                const Player* player = seer->ToPlayer();
                if (player->IsGameMaster())
                    return false;

                return player->GetGUID() != ownerGuid;
            }

            void Reset() override
            {
                counter = 0;
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;

                    if (Creature* creature = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                        me->CastSpell(creature, EVENT_ZONE_SPELL_GREEN_BEAM_TO_RIFT);

                    task.Repeat(Seconds(1));
                });
            }

            void DoAction(int32 action) override
            {
                if (action == ACTION_DESPAWN_PORTAL)
                {
                    me->TextEmote("Portal začíná praskat");
                    scheduler.Schedule(Seconds(1), [this](TaskContext task)
                    {
                        if (Creature* evil = me->SummonCreature(EVENT_ZONE_CREATURE_CRYPT_EVIL_ONE, evilSpawnPos))
                        {
                            if (Creature* general = ObjectAccessor::GetCreature(*me, generalGuid))
                            {
                                general->AI()->SetGUID(evil->GetGUID(), GUID_ENEMY_SPAWNED);
                                evil->GetMotionMaster()->MoveFollow(general, 0.0f, 0.0f);
                            }
                        }
                        task.Schedule(Seconds(1), [this](TaskContext task)
                        {
                            DoCastSelf(EVENT_ZONE_SPELL_EXPLOSION_VISUAL, true);
                            task.Schedule(Seconds(15), [this](TaskContext)
                            {
                                if (Creature* general = ObjectAccessor::GetCreature(*me, generalGuid))
                                    general->AI()->DoAction(ACTION_END_EVENT_PHASE);
                                me->CastSpell(me, EVENT_ZONE_SPELL_EXPLOSION_SMOKE, true);
                                me->DespawnOrUnsummon();
                            });
                        });
                    });
                }
            }

            void SetGUID(ObjectGuid guid, int32 action) override
            {
                switch(action)
                {
                    case GUID_SET_GENERAL:
                    {
                        generalGuid = guid;
                        if (Creature* general = ObjectAccessor::GetCreature(*me, generalGuid))
                        {
                            G3D::Quat rot = G3D::Matrix3::fromEulerAnglesZYX(me->GetOrientation(), 0.f, 0.f);
                            if (GameObject* obj = me->SummonGameObject(EVENT_ZONE_GAMEOBJECT_CRYPT_INNER_PORTAL, evilSpawnPos, rot, 0))
                            {
                                // because of trinitycore abort() fn in SetOwnerGUID
                                obj->SetOwnerGUID(ObjectGuid::Empty);
                                obj->SetOwnerGUID(general->GetAI()->GetGUID(GUID_SET_FAKE_OWNER));
                            }
                            if (GameObject* obj = me->SummonGameObject(EVENT_ZONE_GAMEOBJECT_CRYPT_OUTER_PORTAL, evilSpawnPos, rot, 0))
                            {
                                // because of trinitycore abort() fn in SetOwnerGUID
                                obj->SetOwnerGUID(ObjectGuid::Empty);
                                obj->SetOwnerGUID(general->GetAI()->GetGUID(GUID_SET_FAKE_OWNER));
                            }
                        }
                        break;
                    }
                    case GUID_SET_INNKEEPER:
                        innkeeperGuid = guid;
                        break;
                    case GUID_SET_FAKE_OWNER:
                        ownerGuid = guid;
                        break;
                }
            }

            void UpdateAI(uint32 diff) override
            {
                scheduler.Update(diff);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_crypt_event_portal_openerAI(creature);
        }
    };

    class event_zone_crypt_event_portal : GameObjectScript
    {
    public:
        event_zone_crypt_event_portal() : GameObjectScript("event_zone_crypt_event_portal") { }

        struct event_zone_crypt_event_portalAI : GameObjectAI
        {
            event_zone_crypt_event_portalAI(GameObject* obj) : GameObjectAI(obj) { }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* player = seer->ToPlayer())
                    if (player->IsGameMaster())
                        return false;

                return seer->GetGUID() != go->GetOwnerGUID();
            }
        };

        GameObjectAI* GetAI(GameObject* obj) const override
        {
            return new event_zone_crypt_event_portalAI(obj);
        }
    };

    namespace CryptSoldier
    {
        enum Waypoints
        {
            SOLDIER_WAYPOINTS_COUNT = 14,
            GENERAL_WAYPOINTS_COUNT = 28,
            SOLDIER_WAYPOINT_ID_OUTSIDE  = 1,
            SOLDIER_WAYPOINT_CHASE_TARGET,
            GENERAL_WAYPOINT_ID_AT_CRYPT,
            GENERAL_WAYPOINT_ID_IN_CRYPT,
            SOLDIER_WAYPOINT_ID_AT_JAIL,
            SOLDIER_WAYPOINT_ID_IN_CRYPT,
            SOLDIER_CHARGE_EVENT,
        };

        const std::vector<G3D::Vector3> waypoints
        {
            { 1603.428f, -5307.077f,  91.104f },
            { 1606.437f, -5308.623f,  89.811f },
            { 1618.189f, -5325.378f,  89.810f },
            { 1612.729f, -5331.885f,  89.811f },
            { 1598.543f, -5324.765f,  84.709f },
            { 1605.811f, -5302.044f,  84.709f },
            { 1595.162f, -5296.334f,  79.817f },
            { 1590.009f, -5307.210f,  75.888f },
            { 1605.194f, -5314.572f,  75.888f },
            { 1597.545f, -5328.839f,  75.888f },
            { 1608.336f, -5334.240f,  75.888f },
            { 1613.744f, -5323.233f,  75.889f },
            { 1617.897f, -5314.722f,  74.388f },
            { 1661.700f, -5335.090f,  73.612f },
            { 1675.854f, -5360.371f,  73.612f },
            { 1664.325f, -5399.437f,  74.507f },
            { 1637.704f, -5474.436f,  99.702f },
            { 1671.342f, -5520.934f, 100.926f },
            { 1678.570f, -5571.879f, 101.126f },
            { 1696.528f, -5622.814f, 100.937f },
            { 1697.668f, -5706.496f, 102.747f },
            { 1699.139f, -5773.047f, 112.829f },
            { 1700.287f, -5808.924f, 115.894f },
            { 1707.100f, -5814.154f, 116.092f },
            { 1710.541f, -5821.409f, 116.120f },
            { 1775.473f, -5818.480f, 115.400f },
            { 1829.028f, -5817.968f, 101.808f },
            { 1860.454f, -5817.149f, 100.100f },
        };

        struct CryptSoldierAI : CreatureAI
        {
            TaskScheduler scheduler;
            ObjectGuid ownerGuid;
            CryptSoldierAI(Creature* creature) : CreatureAI(creature) { }
            virtual void UpdateAI(uint32 diff) override { scheduler.Update(diff); }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;

                    return ownerGuid != pseer->GetGUID();
                }
                return false;
            }
        };

        class event_zone_general_crypt_soldier : public CreatureScript
        {
        public:
            event_zone_general_crypt_soldier() : CreatureScript("event_zone_general_crypt_soldier") { }

            struct event_zone_general_crypt_soldierAI : CryptSoldierAI
            {
                float xOffset, yOffset;
                uint32 index, soldiersCount, phase, timer;
                ObjectGuid generalGuid, prisonerGuid;

                event_zone_general_crypt_soldierAI(Creature* creature) : CryptSoldierAI(creature)
                {
                    xOffset = frand(-1.0f, 1.0f);
                    yOffset = frand(-1.0f, 1.0f);
                    index = 0;
                    soldiersCount = 0;
                    phase = 0;
                    timer = 0;

                    // copy 14 waypoints and offset them
                    std::vector<G3D::Vector3> cpy(waypoints.begin(), waypoints.begin() + SOLDIER_WAYPOINTS_COUNT);
                    for (G3D::Vector3& pos : cpy)
                    {
                        pos.x += xOffset;
                        pos.y += yOffset;
                    }

                    me->GetMotionMaster()->MoveSmoothPath(SOLDIER_WAYPOINT_ID_OUTSIDE, &cpy.front(), SOLDIER_WAYPOINTS_COUNT, false);
                }

                void SetData(uint32 value, uint32 action) override
                {
                    switch(action)
                    {
                        case DATA_SET_INDEX:
                            index = value;
                            break;
                        case DATA_SET_SOLDIERS_COUNT:
                            soldiersCount = value;
                            break;
                        default:
                            break;
                    }
                }

                void DoAction(int32 action) override
                {
                    if (action == ACTION_SOLDIER_MOVE_INSIDE_CRYPT)
                    {
                        Position pos = insideCrypt;
                        Position offset(frand(-2.0f, 2.0f), frand(-2.0f, 2.0f));
                        pos.RelocateOffset(offset);
                        me->GetMotionMaster()->MovePoint(SOLDIER_WAYPOINT_ID_IN_CRYPT, pos);
                    }
                }

                void SetGUID(ObjectGuid guid, int32 action) override
                {
                    switch(action)
                    {
                        case GUID_SET_GENERAL:
                            generalGuid = guid;
                            break;
                        case GUID_SET_PRISONER:
                            if (Creature* prisoner = ObjectAccessor::GetCreature(*me, guid))
                            {
                                me->GetMotionMaster()->Clear();
                                prisonerGuid = guid;
                                float x,y,z;
                                prisoner->GetPosition(x,y,z);
                                me->GetMotionMaster()->MoveCharge(x, y, z, 8.0f, SOLDIER_WAYPOINT_CHASE_TARGET, false);
                                phase = PHASE_CHASING;
                            }
                            break;
                        case GUID_ENEMY_SPAWNED:
                            if (Creature* enemy = ObjectAccessor::GetCreature(*me, guid))
                            {
                                me->GetMotionMaster()->Clear();
                                me->Yell("Postarám se o něj!", LANG_UNIVERSAL);
                                prisonerGuid = guid;
                                float x,y,z;
                                enemy->GetPosition(x,y,z);
                                me->GetMotionMaster()->MoveCharge(x, y, z, 42.0f, SOLDIER_CHARGE_EVENT, true);
                            }
                            break;
                            case GUID_SET_FAKE_OWNER:
                                ownerGuid = guid;
                            break;
                        default:
                            break;
                    }
                }

                void MovementInform(uint32, uint32 point) override
                {
                    switch(point)
                    {
                        case SOLDIER_WAYPOINT_ID_OUTSIDE:
                            if (Creature* general = ObjectAccessor::GetCreature(*me, generalGuid))
                                me->GetMotionMaster()->MoveFollow(general, 1.0f, 2 * G3D::pif() * (float)index / soldiersCount);
                        case SOLDIER_CHARGE_EVENT:
                            if (Creature* enemy = ObjectAccessor::GetCreature(*me, prisonerGuid))
                            {
                                if (Creature* general = ObjectAccessor::GetCreature(*me, generalGuid))
                                    me->GetMotionMaster()->MoveFollow(general, 1.0f, 0.0f);
                                me->CastSpell(enemy, EVENT_ZONE_SPELL_ATTACK_VISUAL);
                                prisonerGuid = ObjectGuid::Empty;
                                me->Kill(enemy);
                                enemy->DespawnOrUnsummon(Seconds(1));
                            }
                    }
                }

                const std::vector<std::string> quotes
                {
                    "Ani se nehni, nebo tě na místě zabiju!",
                    "Stuj, nebo je po tobě!",
                    "Teď tě odvedu do vězení!",
                    "Ve jménu Layi Ely tě zatýkám!"
                };

                void UpdateAI(uint32 diff) override
                {
                    CryptSoldierAI::UpdateAI(diff);
                    switch(phase)
                    {
                        case PHASE_CHASING:
                            if (me->isMoving())
                                return;

                            me->Say(quotes[urand(0, static_cast<uint32>(quotes.size() - 1))], LANG_UNIVERSAL);
                            phase = PHASE_CHAINING;
                            scheduler.Schedule(Seconds(3), [this](TaskContext task)
                            {
                                if (Creature* prisoner = ObjectAccessor::GetCreature(*me, prisonerGuid))
                                {
                                    me->CastSpell(prisoner, EVENT_ZONE_SPELL_CHAINS);
                                    prisoner->RemoveAura(EVENT_ZONE_AURA_KNEEL);
                                    prisoner->GetMotionMaster()->MoveFollow(me, 1.0f, static_cast<float>(G3D::halfPi()));
                                }
                                phase = PHASE_GOING_TO_JAIL;
                                task.Schedule(Seconds(5), [this](TaskContext task)
                                {
                                    me->GetMotionMaster()->MovePoint(SOLDIER_WAYPOINT_ID_AT_JAIL, 1347.427f + frand(-2.0f, 2.0f), -5744.422f + frand(-2.0f, 2.0f), 136.415f);
                                    task.Schedule(Seconds(15), [this](TaskContext)
                                    {
                                        if (Creature* prisoner = ObjectAccessor::GetCreature(*me, prisonerGuid))
                                            prisoner->DespawnOrUnsummon();
                                        me->DespawnOrUnsummon();
                                    });
                                });
                            });
                            break;
                        default:
                            break;
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_general_crypt_soldierAI(creature);
            }
        };

        class event_zone_general_crypt : public CreatureScript
        {
        public:
            event_zone_general_crypt() : CreatureScript("event_zone_general_crypt") { }

            struct event_zone_general_cryptAI : CryptSoldierAI
            {
                std::vector<ObjectGuid> soldiers;
                uint32 eventPhase;
                uint32 talkIndex;
                ObjectGuid innkeeperGuid, openerGuid;

                const std::vector<Monolog> monolog
                {
                    { Seconds(6), "Zvládnuli jsme to!" },
                    { Seconds(11), "Podařilo se nám zavřít jejich portal do našeho města!" },
                    { Seconds(15), "Tím to ale nekončí, jejich vůdce bude chtít zaútočit znovu, proto se ho budeme muset zbavit." },
                    { Seconds(15), "To ale počká, nejprve budeme muset popravit zrádce, kteří dopomohli k otevření portálu." },
                };

                event_zone_general_cryptAI(Creature* creature) : CryptSoldierAI(creature)
                {
                    me->GetMotionMaster()->MoveSmoothPath(GENERAL_WAYPOINT_ID_AT_CRYPT, &waypoints.front(), GENERAL_WAYPOINTS_COUNT, false);
                    eventPhase = 0;
                    talkIndex = 0;
                }

                Player* GetOwner()
                {
                    return ObjectAccessor::GetPlayer(*me, ownerGuid);
                }

                void DoAction(int32 action) override
                {
                    switch(action)
                    {
                        case ACTION_EVENT_PROGRESS:
                            switch(eventPhase++)
                            {
                                case 0:
                                    if (Player* plrOwner = GetOwner())
                                    {
                                        plrOwner->CastSpell(plrOwner, EVENT_ZONE_SPELL_CAMERA_SHAKE_SOFT, true);
                                        if (Creature* opener = me->SummonCreature(EVENT_ZONE_CREATURE_CRYPT_EVENT_PORTAL_OPENER, evilSpawnPos))
                                        {
                                            openerGuid = opener->GetGUID();
                                            opener->SetOwnerGUID(ObjectGuid::Empty);
                                            opener->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
                                            opener->AI()->SetGUID(innkeeperGuid, GUID_SET_INNKEEPER);
                                            opener->AI()->SetGUID(me->GetGUID(), GUID_SET_GENERAL);
                                            opener->AI()->SetGUID(ownerGuid, GUID_SET_FAKE_OWNER);
                                        }

                                        if (Creature* innkeeper = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                                            innkeeper->YellSingleTarget("In nomine omnipotentis Trollicusana aperui nri in porta !", LANG_UNIVERSAL, plrOwner);
                                    }
                                    break;
                                case 1:
                                    if (Player* plrOwner = GetOwner())
                                    {
                                        plrOwner->CastSpell(plrOwner, EVENT_ZONE_SPELL_CAMERA_SHAKE_HARD, true);
                                        if (Creature* innkeeper = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                                            innkeeper->YellSingleTarget("Omnipotens deus tenebrarum , da mihi virtutem ostium aperire !", LANG_UNIVERSAL, plrOwner);
                                    }

                                    scheduler.Schedule(Seconds(1), [this](TaskContext)
                                    {
                                        // because of delayed event, we have to get owner player again, so we cannot use plrOwner ptr (player might logout)
                                        me->SaySingleTarget("Pospěšte, ať to stihneme, než to otevře!",
                                                            LANG_UNIVERSAL, GetOwner());
                                    });
                                    break;
                                case 2:

                                    if (Player* plrOwner = GetOwner())
                                    {
                                        plrOwner->CastSpell(plrOwner, EVENT_ZONE_SPELL_CAMERA_SHAKE_HARD, true);
                                        if (Creature* innkeeper = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                                            innkeeper->YellSingleTarget("Venite , fratres ! Recharge eorum imperio !", LANG_UNIVERSAL, plrOwner);
                                    }
                                    break;
                                case 3:
                                    if (Creature* innkeeper = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                                    {
                                        innkeeper->TextEmote(innkeeper->GetName() + " padá k zemi vyčerpáním.");
                                        innkeeper->AddAura(EVENT_ZONE_AURA_PERMANENT_DEATH, innkeeper);
                                    }
                                    break;
                            }
                            break;
                        case ACTION_END_EVENT_PHASE:
                            scheduler.CancelAll();
                            scheduler.Schedule(Seconds(4), [this](TaskContext task)
                            {
                                if (talkIndex >= monolog.size())
                                {
                                    if (Player* owner = GetOwner())
                                    {
                                        owner->CompleteQuest(EVENT_ZONE_QUEST_CRYPT_INSPECTION);
                                        me->SaySingleTarget(owner->GetName() + ", děkuji ti za tvojí pomoc!", LANG_UNIVERSAL, owner);
                                        me->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER);
                                    }
                                    return;
                                }

                                me->Say(monolog[talkIndex].text, LANG_UNIVERSAL);
                                task.Repeat(monolog[talkIndex++].delay);
                            });
                    }
                }

                void DespawnAll(std::chrono::milliseconds delay)
                {
                    for (auto soldierGuid : soldiers)
                        if (Creature* soldier = ObjectAccessor::GetCreature(*me, soldierGuid))
                            soldier->DespawnOrUnsummon(delay);
                    if (Creature* innkeeper = ObjectAccessor::GetCreature(*me, innkeeperGuid))
                        innkeeper->DespawnOrUnsummon(delay);
                    me->DespawnOrUnsummon(delay);
                }

                void sQuestReward(Player* player, const Quest*, uint32) override
                {
                    player->AddQuestAndCheckCompletion(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_REPORT_CRYPT_TO_COUNCILOR), me);
                    me->SaySingleTarget("Prosím, utíkej oznámit radnímu Josefovi co se tu stalo!", LANG_UNIVERSAL, player);
                    DespawnAll(Seconds(30));
                }

                void SpellHitTarget(Unit* target, const SpellInfo* spell) override
                {
                    if (spell->Id == EVENT_ZONE_SPELL_ROCKET_LAUNCH)
                    {
                        target->GetAI()->DoAction(ACTION_DESPAWN_PORTAL);
                    }
                }

                void SendSoldierTo(ObjectGuid guid, int32 action)
                {
                    if (soldiers.empty())
                        return;

                    if (Creature* soldier = ObjectAccessor::GetCreature(*me, soldiers[0]))
                    {
                        soldier->AI()->SetGUID(guid, action);
                        if (action == GUID_SET_PRISONER)
                            soldiers.erase(soldiers.begin());
                    }
                }

                void SetGUID(ObjectGuid guid, int32 action) override
                {
                    switch(action)
                    {
                        case GUID_ADD_SOLDIER:
                            soldiers.push_back(guid);
                            break;
                        case GUID_ENEMY_SPAWNED:
                            SendSoldierTo(guid, action);
                            break;
                        case GUID_SET_FAKE_OWNER:
                            ownerGuid = guid;
                            break;
                        default:
                            break;
                    }
                }

                ObjectGuid GetGUID(int32 action) const override
                {
                    switch(action)
                    {
                        case GUID_SET_FAKE_OWNER:
                            return ownerGuid;
                        default:
                            return ObjectGuid::Empty;
                    }
                }

                void SummonGuiltyCreature(uint32 entry, const Position& pos, std::list<Creature*>& summons)
                {
                    if (Creature* summon = me->SummonCreature(entry, pos))
                    {
                        summon->SetOwnerGUID(ownerGuid);
                        summons.push_back(summon);
                        SendSoldierTo(summon->GetGUID(), GUID_SET_PRISONER);
                    }
                }

                void MovementInform(uint32, uint32 point) override
                {
                    if (point == GENERAL_WAYPOINT_ID_AT_CRYPT)
                    {
                        std::list<Creature*> summons;
                        SummonGuiltyCreature(EVENT_ZONE_CREATURE_CRYPT_APPLE_FARM_FARMER, farmerPos, summons);
                        SummonGuiltyCreature(EVENT_ZONE_CREATURE_CRYPT_BLACKSMITH, blacksmithPos, summons);
                        SummonGuiltyCreature(EVENT_ZONE_CREATURE_CRYPT_FISHERMAN, fishermanPos, summons);
                        if (Creature* innkeeper = me->SummonCreature(EVENT_ZONE_CREATURE_CRYPT_INNKEEPER, innkeeperPos))
                        {
                            innkeeperGuid = innkeeper->GetGUID();
                            innkeeper->SetOwnerGUID(ownerGuid);
                        }
                        if (Player* owner = GetOwner())
                        {
                            me->YellSingleTarget("Tamhle jsou! Chyťte je!", LANG_UNIVERSAL, owner);
                            owner->UpdateObjectVisibility();
                        }

                        scheduler.Schedule(Seconds(3), [this](TaskContext)
                        {
                            me->GetMotionMaster()->MovePoint(GENERAL_WAYPOINT_ID_IN_CRYPT, insideCrypt);
                            me->YellSingleTarget("Zbytek za mnou do krypty! Musíme je zastavit, než bude pozdě!",
                                LANG_UNIVERSAL, GetOwner());

                            for (auto soldierGuid : soldiers)
                                if (Creature* soldier = ObjectAccessor::GetCreature(*me, soldierGuid))
                                    soldier->AI()->DoAction(ACTION_SOLDIER_MOVE_INSIDE_CRYPT);
                        });
                    }

                    if (point == GENERAL_WAYPOINT_ID_IN_CRYPT)
                    {
                        me->Yell("Ne! Sem se nedostanete!", LANG_UNIVERSAL);
                        scheduler.Schedule(Seconds(1), [this](TaskContext task)
                        {
                            if (me->HasUnitState(UNIT_STATE_CASTING))
                                return;

                            Player* owner = GetOwner();
                            if (!owner)
                            {
                                DespawnAll(Seconds(0));
                                return;
                            }
                            // wait until player owner is staying near so they see what happends
                            if (owner->GetDistance(me) > 6.0f)
                            {
                                task.Repeat(Seconds(1));
                                return;
                            }
                            if (Creature* opener = ObjectAccessor::GetCreature(*me, openerGuid))
                                me->CastSpell(opener, EVENT_ZONE_SPELL_ROCKET_LAUNCH);
                            task.Repeat(Seconds(1));
                        });
                    }
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_general_cryptAI(creature);
            }
        };
    }
}

namespace Execution
{
    class event_zone_executioner : CreatureScript
    {
    public:
        event_zone_executioner() : CreatureScript("event_zone_executioner") { }

        enum ExecutionerActions
        {
            EXECUTIONER_ACTION_JOSEF_SENT_ME    = GOSSIP_ACTION_INFO_DEF + 1,
            EXECUTIONER_ACTION_TELL_ABOUT_CRYPT,
            EXECUTIONER_ACTION_TELL_WHO_DID_IT,
            EXECUTIONER_ACTION_THEY_WERE_CAUGHT,

        };

        bool OnGossipHello(Player* player, Creature* me) override
        {
            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_EXECUTION_REQUEST);
            if (status != QUEST_STATUS_COMPLETE && status != QUEST_STATUS_REWARDED)
            {
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Posila me radni Josef se zadosti o popravu zajatcu.", GOSSIP_SENDER_MAIN, EXECUTIONER_ACTION_JOSEF_SENT_ME);
                SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_EXECUTIONER_HELLO, me);
                return true;
            }
            return false;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32 sender, uint32 action) override
        {
            if (sender != GOSSIP_SENDER_MAIN)
                return false;

            ClearGossipMenuFor(player);
            switch(action)
            {
                case EXECUTIONER_ACTION_JOSEF_SENT_ME:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Pomahali Trolliscusanovym vojakum dostat se do mesta pres portal v krypte.", GOSSIP_SENDER_MAIN, EXECUTIONER_ACTION_TELL_ABOUT_CRYPT);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_EXECUTIONER_WHAT_HAPPEND, me);
                    break;
                case EXECUTIONER_ACTION_TELL_ABOUT_CRYPT:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Muze za to vice lidi. Kovarka Leila, rybarka Jasmina, farmar Pepino a hostinsky Franta, ktery na miste zemrel pri otevirani portalu.", GOSSIP_SENDER_MAIN, EXECUTIONER_ACTION_TELL_WHO_DID_IT);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_EXECUTIONER_WHO_DID_IT, me);
                    break;
                case EXECUTIONER_ACTION_TELL_WHO_DID_IT:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Obe byly prichyceny u krypty pri cinu!", GOSSIP_SENDER_MAIN, EXECUTIONER_ACTION_THEY_WERE_CAUGHT);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_EXECUTIONER_SURPRISED, me);
                    break;
                case EXECUTIONER_ACTION_THEY_WERE_CAUGHT:
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_EXECUTIONER_GO_BACK, me);
                    player->KilledMonsterCredit(me->GetEntry());
                    break;
            }
            return true;
        }
    };

    class event_zone_prisoner_visibility :CreatureScript
    {
    public:
        event_zone_prisoner_visibility() : CreatureScript("event_zone_prisoner_visibility") { }
        struct event_zone_prisoner_visibilityAI : NullCreatureAI
        {
            event_zone_prisoner_visibilityAI(Creature* creature) : NullCreatureAI(creature) { }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                {
                    if (pseer->IsGameMaster())
                        return false;

                    QuestStatus status = pseer->GetQuestStatus(EVENT_ZONE_QUEST_KILL_TROLLISCUSAN);
                    return status == QUEST_STATUS_COMPLETE || status == QUEST_STATUS_REWARDED;
                }
                return true;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_prisoner_visibilityAI(creature);
        }
    };
}

namespace EventZoneQuestions
{
    struct EventZoneQuestion
    {
        EventZoneQuestion(uint32 gossip, std::string correct, std::string wrong1, std::string wrong2, std::string wrong3)
        {
            gossipId = gossip;
            correctAnswer = correct;
            wrongAnswers[0] = wrong1;
            wrongAnswers[1] = wrong2;
            wrongAnswers[2] = wrong3;
        }

        uint32 gossipId;
        std::string correctAnswer;
        std::string wrongAnswers[3];
    };

    class EventZoneQuestionsMgr
    {
        bool loaded = false;
    public:
        uint32 references = 0;
        std::vector<EventZoneQuestion> questions;
        EventZoneQuestionsMgr()
        {
            Load();
        }

        bool IsLoaded()
        {
            return loaded && !questions.empty();
        }

        void Reload()
        {
            loaded = false;
            Load();
        }

        void Load()
        {
            questions.clear();
            QueryResult result = WorldDatabase.Query("SELECT question_gossip_text_id, correct_answer, wrong_answer_1, wrong_answer_2, wrong_answer_3 FROM event_zone_questions");
            if (!result)
                return;

            do
            {
                Field *fields = result->Fetch();
                questions.emplace_back(fields[0].GetUInt32(), fields[1].GetString(), fields[2].GetString(), fields[3].GetString(), fields[4].GetString());
            } while (result->NextRow());
            loaded = true;
        }
    };

    std::mutex questionMgrLock;
    EventZoneQuestionsMgr* questionsMgr = nullptr;
    void InitQuestionMgr()
    {
        std::lock_guard<std::mutex> guard(questionMgrLock);
        if (!questionsMgr)
            questionsMgr = new EventZoneQuestionsMgr();
        ++questionsMgr->references;
    }

    void RemoveQuestionMgrReference()
    {
        std::lock_guard<std::mutex> guard(questionMgrLock);
        if (!--questionsMgr->references)
        {
            delete questionsMgr;
            questionsMgr = nullptr;
        }
    }

    class event_zone_library_questions : public CreatureScript
    {
    public:
        event_zone_library_questions() : CreatureScript("event_zone_library_questions")
        {
            InitQuestionMgr();
        }

        ~event_zone_library_questions()
        {
            RemoveQuestionMgrReference();
        }

        enum QuestionsGossipAction
        {
            QUESTIONS_GOSSIP_ACTION_REQUEST_BOOK = 999,
        };

        void GenerateQuestion(Player* player, Creature* me)
        {
            bool handled = false;
            auto itr = questionsMgr->questions.begin();
            if (Aura* aur = player->GetAura(EVENT_ZONE_AURA_SELECTED_QUESTION))
            {
                uint32 offset = aur->GetEffect(EFFECT_0)->GetAmount();
                if (offset < questionsMgr->questions.size())
                {
                    std::advance(itr, offset);
                    handled = true;
                }
            }

            if (!handled)
            {
                uint32 offset = urand(0, static_cast<uint32>(questionsMgr->questions.size() - 1));
                std::advance(itr, offset);
                if (Aura* aur = player->AddAura(EVENT_ZONE_AURA_SELECTED_QUESTION, player))
                    aur->GetEffect(EFFECT_0)->SetAmount(offset);
            }

            EventZoneQuestion& question = *itr;
            typedef std::vector<std::pair<bool, std::string>> answerVect;
            answerVect answers;
            answers.reserve(4);
            answerVect tmp = {{ true, question.correctAnswer },
                { false, question.wrongAnswers[0] },
                { false, question.wrongAnswers[1] },
                { false, question.wrongAnswers[2]}};

            while (!tmp.empty())
            {
                uint32 rng = urand(0, static_cast<uint32>(tmp.size() - 1));
                answers.push_back(tmp[rng]);
                auto itr = tmp.begin();
                std::advance(itr, rng);
                tmp.erase(itr);
            }

            ClearGossipMenuFor(player);

            for (auto answer : answers)
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, answer.second, GOSSIP_SENDER_MAIN, answer.first ? GOSSIP_ACTION_INFO_DEF : (GOSSIP_ACTION_INFO_DEF + 1));
            SendGossipMenuFor(player, question.gossipId, me->GetGUID());
        }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS);
            if (status != QUEST_STATUS_INCOMPLETE)
            {
                if (status == QUEST_STATUS_REWARDED && !player->HasItemCount(EVENT_ZONE_ITEM_TROLL_TACTIC_BOOK, 1, true))
                {
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Poprosit o dalsi kopii Trolliscusan Tactic Book.", GOSSIP_SENDER_MAIN, QUESTIONS_GOSSIP_ACTION_REQUEST_BOOK);
                    SendGossipMenuFor(player, 1, me->GetGUID());
                }
                return false;
            }
            if (!questionsMgr->IsLoaded())
                return true;

            GenerateQuestion(player, me);
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            if (action == QUESTIONS_GOSSIP_ACTION_REQUEST_BOOK && player->GetQuestStatus(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS) == QUEST_STATUS_REWARDED)
            {
                player->AddItem(EVENT_ZONE_ITEM_TROLL_TACTIC_BOOK, 1, true);
                me->SaySingleTarget("Už jí prosím tě neztrať! Už mám jen posledních pár výtisku tohoto unikátu!", LANG_UNIVERSAL, player);
                CloseGossipMenuFor(player);
                return true;
            }
            if (!questionsMgr->IsLoaded())
            {
                CloseGossipMenuFor(player);
                return true;
            }

            if (action == GOSSIP_ACTION_INFO_DEF)
            {
                std::vector<std::string> answers =
                {
                    "Správná odpověď!",
                    "Velmi dobře!",
                    "Hmm, dobře studujete!",
                    "Ano! Správná odpověď!"
                };

                auto itr = answers.begin();
                std::advance(itr, urand(0, static_cast<uint32>(answers.size() - 1)));
                me->SaySingleTarget(*itr, LANG_UNIVERSAL, player);
                // correct answer, add 1 credit
                player->KilledMonsterCredit(me->GetEntry());
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS) != QUEST_STATUS_COMPLETE)
                {
                    if (Aura* aur = player->GetAura(EVENT_ZONE_AURA_SELECTED_QUESTION))
                        aur->GetEffect(EFFECT_0)->SetAmount(urand(0, static_cast<uint32>(questionsMgr->questions.size() - 1)));
                    GenerateQuestion(player, me);
                }
                else
                {
                    player->RemoveAura(EVENT_ZONE_AURA_SELECTED_QUESTION);
                    player->PlayerTalkClass->SendQuestGiverOfferReward(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS), me->GetGUID(), true);
                    CloseGossipMenuFor(player);
                }
            }
            else // failed answer, fail whole quest and start again
            {
                std::vector<std::string> answers =
                {
                    "Bohužel, špatná odpověď, máš ještě co studovat!",
                    "Ne, musíš se ještě učit!",
                    "Máš se ještě co učit! Takhle to není!",
                    "No to snad nemyslíš vážně!? To je naprosto špatně!",
                    "Ne, ne, ne a ne! Přijď za mnou, až se vše doučíš!"
                };

                auto itr = answers.begin();
                std::advance(itr, urand(0, static_cast<uint32>(answers.size() - 1)));
                me->SaySingleTarget(*itr, LANG_UNIVERSAL, player);
                if (player->IsActiveQuest(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS))
                    player->FailQuest(EVENT_ZONE_QUEST_LIBRARY_QUESTIONS);
                CloseGossipMenuFor(player);
            }
            return true;
        }
    };
}

namespace Revolt
{
    enum RevoltGUIDActions
    {
        GUID_ACTION_SET_TRIGGERED_PLAYER,
        GUID_ACTION_SET_ENEMY_GUID,
    };

    enum RevoltActions
    {
        ACTION_END_EVENT,
    };

    struct RevoltSpell
    {
        std::chrono::milliseconds delay;
        std::chrono::milliseconds cooldown;
        uint32 spellId;
        bool triggered;
        std::function<void(RevoltSpell*)> fn;
    };

    class RevoltAI : public CreatureAI
    {
    public:
        TaskScheduler scheduler;
        ObjectGuid plrTriggerGuid;
        RevoltAI(Creature* creature) : CreatureAI(creature) { }
        virtual void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            scheduler.Update(diff, std::bind(&CreatureAI::DoMeleeAttackIfReady, this));
        }

        void KilledUnit(Unit* victim) override
        {
            if (victim->GetEntry() != EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER)
                return;

            Unit* leader = me->GetOwner();
            if (!leader)
                leader = me;

            if (leader->IsAIEnabled)
                leader->GetAI()->DoAction(ACTION_END_EVENT);
        }

        void EnterEvadeMode(EvadeReason) override { }

        void MoveInLineOfSight(Unit* who) override
        {
            if (who->GetEntry() != EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER)
                return;

            if (me->CanSeeOrDetect(who))
                AttackStart(who);
        }

        virtual void SetGUID(ObjectGuid guid, int32 action) override
        {
            switch(action)
            {
                case GUID_ACTION_SET_TRIGGERED_PLAYER:
                    plrTriggerGuid = guid;
                    break;
            }
        }

        ObjectGuid GetGUID(int32 action) const override
        {
            switch(action)
            {
                case GUID_ACTION_SET_TRIGGERED_PLAYER:
                    return plrTriggerGuid;
                default:
                    return ObjectGuid::Empty;
            }
        }

        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
            {
                if (player->IsGameMaster())
                    return false;
                return player->GetGUID() != plrTriggerGuid;
            }

            if (const Creature* creature = seer->ToCreature())
                return creature->GetOwnerGUID() != plrTriggerGuid;
            return true;
        }

        void DamageDealt(Unit* victim, uint32& damage, DamageEffectType) override
        {
            if (victim->GetEntry() != EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER)
                damage = 0;
        }

        bool CanAIAttack(const Unit* victim) const override
        {
            return victim->GetEntry() != EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER;
        }
    };

    class event_zone_revolt_weak_soldier : public CreatureScript
    {
    public:
        event_zone_revolt_weak_soldier() : CreatureScript("event_zone_revolt_weak_soldier") { }
        struct event_zone_revolt_weak_soldierAI : RevoltAI
        {
            event_zone_revolt_weak_soldierAI(Creature* creature) : RevoltAI(creature) { }

            void Reset() override
            {
                scheduler.CancelAll();
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    DoCastVictim(EVENT_ZONE_SPELL_STRIKE);
                    task.Repeat(Seconds(5));
                });
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_weak_soldierAI(creature);
        }
    };

    class event_zone_revolt_soldier : public CreatureScript
    {
    public:
        event_zone_revolt_soldier() : CreatureScript("event_zone_revolt_soldier") { }
        struct event_zone_revolt_soldierAI : RevoltAI
        {
            event_zone_revolt_soldierAI(Creature* creature) : RevoltAI(creature) { }

            void Reset() override
            {
                scheduler.CancelAll();
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    DoCastVictim(EVENT_ZONE_SPELL_SLAYING_STRIKE);
                    task.Repeat(Seconds(10));
                });
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_soldierAI(creature);
        }
    };

    class event_zone_revolt_strong_soldier : public CreatureScript
    {
    public:
        event_zone_revolt_strong_soldier() : CreatureScript("event_zone_revolt_strong_soldier") { }
        struct event_zone_revolt_strong_soldierAI : RevoltAI
        {
            event_zone_revolt_strong_soldierAI(Creature* creature) : RevoltAI(creature) { }

            void Reset() override
            {
                scheduler.CancelAll();
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    DoCastVictim(EVENT_ZONE_SPELL_MORTAL_WOUNDS);
                    task.Repeat(Seconds(14));
                });

                scheduler.Schedule(Seconds(3), [this](TaskContext task)
                {
                    DoCastVictim(EVENT_ZONE_SPELL_HEROIC_STRIKE);
                    task.Repeat(Seconds(7));
                });
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_strong_soldierAI(creature);
        }
    };

    class event_zone_revolt_healer : public CreatureScript
    {
    public:
        event_zone_revolt_healer() : CreatureScript("event_zone_revolt_healer") { }
        struct event_zone_revolt_healerAI : RevoltAI
        {
            event_zone_revolt_healerAI(Creature* creature) : RevoltAI(creature) { }

            class MostHPFriendlyMissingInRange
            {
                public:
                    MostHPFriendlyMissingInRange(Unit const* obj, float range, uint32 hp) : i_obj(obj), i_range(range), i_hp(hp) { }
                    bool operator()(Unit* u)
                    {
                        if (u->IsAlive() && u->getFaction() == i_obj->getFaction() && i_obj->IsWithinDistInMap(u, i_range) && u->GetMaxHealth() - u->GetHealth() > i_hp)
                        {
                            i_hp = u->GetMaxHealth() - u->GetHealth();
                            return true;
                        }
                        return false;
                    }
                private:
                    Unit const* i_obj;
                    float i_range;
                    uint32 i_hp;
            };

            Unit* DoSelectLowestHpFriendly(float range, uint32 minHPDiff)
            {
                Unit* unit = nullptr;
                MostHPFriendlyMissingInRange u_check(me, range, minHPDiff);
                Trinity::UnitLastSearcher<MostHPFriendlyMissingInRange> searcher(me, unit, u_check);
                me->VisitNearbyObject(range, searcher);

                return unit;
            }

            void Reset() override
            {
                scheduler.CancelAll();
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (Unit* target = DoSelectLowestHpFriendly(40.0f, 0))
                        me->AddAura(EVENT_ZONE_SPELL_SPIRIT_MEND, target);
                    task.Repeat(Seconds(10));
                });
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_healerAI(creature);
        }
    };

    class event_zone_revolt_leader : public CreatureScript
    {
    public:
        event_zone_revolt_leader() : CreatureScript("event_zone_revolt_leader") { }
        struct event_zone_revolt_leaderAI : RevoltAI
        {
            TaskScheduler noncombatScheduler;
            SummonList summons;
            std::vector<ObjectGuid> enemies;
            uint32 phase;
            event_zone_revolt_leaderAI(Creature* creature) : RevoltAI(creature), summons(creature)
            {
                noncombatScheduler.CancelAll();
                phase = 0;
            }

            void SummonSoldier(uint32 entry)
            {
                Position pos = me->GetPosition();
                pos.RelocateOffset(Position(frand(-2.0f, 2.0f), frand(-2.0f, 2.0f)));
                me->SummonCreature(entry, pos);
            }

            void SetGUID(ObjectGuid guid, int32 action) override
            {
                if (action == GUID_ACTION_SET_ENEMY_GUID)
                    enemies.push_back(guid);
                else RevoltAI::SetGUID(guid, action);
            }

            void DoAction(int32 action) override
            {
                if (action == ACTION_END_EVENT)
                    Despawn();
            }

            void SummonNextPhase(Player* player)
            {
                switch (phase++)
                {
                    case 0:
                        for (uint8 i = 0; i < 10; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_WEAK_SOLDIER);
                        me->YellSingleTarget("Tamhle jdou vojáci, postarejte se o ně!", LANG_UNIVERSAL, player);
                        break;
                    case 1:
                        for (uint8 i = 0; i < 4; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_SOLDIER);
                        break;
                    case 2:
                        for (uint8 i = 0; i < 2; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_STRONG_SOLDIER);
                        break;
                    case 3:
                        for (uint8 i = 0; i < 4; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_WEAK_SOLDIER);
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_HEALER);
                        break;
                    case 4:
                        for (uint8 i = 0; i < 4; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_WEAK_SOLDIER);
                        for (uint8 i = 0; i < 2; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_SOLDIER);
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_HEALER);
                        me->YellSingleTarget("Zabte je!", LANG_UNIVERSAL, player);
                        break;
                    case 5:
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_STRONG_SOLDIER);
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_SOLDIER);
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_HEALER);
                        break;
                    case 6:
                        for (uint8 i = 0; i < 6; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_WEAK_SOLDIER);
                        SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_STRONG_SOLDIER);
                        break;
                    case 7:
                        for (uint8 i = 0; i < 3; ++i)
                            SummonSoldier(EVENT_ZONE_CREATURE_REVOLT_SOLDIER);
                        me->GetMotionMaster()->MoveFollow(player, 2.0f, 0.0f);
                        me->YellSingleTarget("Na tohle se už nemohu dívat!", LANG_UNIVERSAL, player);
                        break;
                }
            }

            void CheckForNextPhase(Player* player)
            {
                if (summons.size() < 2)
                    SummonNextPhase(player);
            }

            void Despawn()
            {
                scheduler.CancelAll();
                summons.DespawnAll();

                for (auto enemyGuid : enemies) // despawn all controllable soldiers
                    if (Creature* enemy = ObjectAccessor::GetCreature(*me, enemyGuid))
                        enemy->DespawnOrUnsummon();
                me->DespawnOrUnsummon(Seconds(1));
            }

            void DamageDealt(Unit* target, uint32& damage, DamageEffectType) override
            {
                if (target->GetTypeId() == TYPEID_PLAYER)
                    damage = 1;
            }

            void JustDied(Unit* killer) override
            {
                Player* owner = killer->GetCharmerOrOwnerPlayerOrPlayerItself();
                if (!owner)
                    return;

                me->YellSingleTarget("Nee! Tohle.. se... nestaloooooooo...", LANG_UNIVERSAL, owner);
                owner->KilledMonsterCredit(me->GetEntry());
                Despawn();
            }

            void Reset() override
            {
                scheduler.CancelAll();
                noncombatScheduler.Schedule(Seconds(5), [this](TaskContext task)
                {
                    Player* player = ObjectAccessor::GetPlayer(*me, plrTriggerGuid);
                    if (!player)
                    {
                        Despawn();
                        return;
                    }

                    if (player->GetDistance2d(me) < 200.0f)
                        CheckForNextPhase(player);
                    task.Repeat(Seconds(5));
                });

                scheduler.Schedule(Seconds(10), [this](TaskContext task)
                {
                    DoCastAOE(EVENT_ZONE_SPELL_FIRE_NOVA, true);
                    task.Repeat(Seconds(10));
                });

                scheduler.Schedule(Seconds(5), [this](TaskContext task)
                {
                    DoCastAOE(EVENT_ZONE_SPELL_FIREBALL_VOLLEY, false);
                    task.Repeat(Seconds(10));
                    me->Say("Shořte!", LANG_UNIVERSAL);
                });

                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    DoCastVictim(EVENT_ZONE_SPELL_FIRE_BLAST);
                    task.Repeat(Seconds(5));
                });

                scheduler.Schedule(Seconds(3), [this](TaskContext task)
                {
                    std::list<Creature*> creatures;
                    GetCreatureListWithEntryInGrid(creatures, me, 0, 20.0f);
                    for (Creature* creature : creatures)
                        if (creature->IsAIEnabled)
                            if (creature->AI()->GetGUID(GUID_ACTION_SET_TRIGGERED_PLAYER) ==  plrTriggerGuid)
                                me->AddAura(EVENT_ZONE_SPELL_AURA_OF_COMMAND, creature);
                    task.Repeat(Seconds(30));
                });
            }

            void SummonedCreatureDies(Creature* summon, Unit*) override
            {
                summon->DespawnOrUnsummon(Seconds(30));
                summons.Despawn(summon);
            }

            void SummonedCreatureDespawn(Creature* summon) override
            {
                summons.Despawn(summon);
            }

            void JustSummoned(Creature* summon) override
            {
                Player* player = ObjectAccessor::GetPlayer(*me, plrTriggerGuid);
                if (!player)
                {
                    Despawn();
                    return;
                }

                summon->AI()->SetGUID(plrTriggerGuid, GUID_ACTION_SET_TRIGGERED_PLAYER);
                summon->GetMotionMaster()->MoveFollow(player, 2.0f, 0.0f);
                summons.Summon(summon);
            }

            void UpdateAI(uint32 diff) override
            {
                noncombatScheduler.Update(diff);
                RevoltAI::UpdateAI(diff);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_leaderAI(creature);
        }
    };

    struct GridPos
    {
        uint32 x;
        uint32 y;
    };

    const GridPos leaderGrid
    {
        36, 20
    };

    const Position leaderSpawnPos
    {
        2197.28f, -6143.42f, 5.47f, 2.38f
    };

    class event_zone_revolt_controllable_soldier : public CreatureScript
    {
    public:
        event_zone_revolt_controllable_soldier() : CreatureScript("event_zone_revolt_controllable_soldier") { }

        struct event_zone_revolt_controllable_soldierAI : ScriptedAI
        {
            enum RevoltSoldierSpells
            {
                SPELL_TAUNT,
                SPELL_LESSER_HEAL,
                SPELL_HOLY_LIGHT,
                SPELL_GREAT_HEAL,
                SPELL_SMITE,
                SPELL_CRUSADER_STRIKE,
                SPELL_HOLY_BOLT_VOLLEY,
                SPELL_HOLY_BOLT,
                SPELLS_MAX,
                SPELL_HOLY_ZONE_VISUAL  = 70571, //@TODO: use me somehow, cool visual, lol
                SPELL_HOLY_CHAMPION     = 71954, // buff - 25% damage, 25% hp
            };

            enum HealPriority : uint8
            {
                HEAL_PREFER_BEGIN,
                HEAL_PREFER_TARGET_ANY = HEAL_PREFER_BEGIN,
                HEAL_PREFER_TARGET_LOWEST_HP,
                HEAL_PREFER_TARGET_SELF,
                HEAL_PREFER_MAX = HEAL_PREFER_TARGET_SELF
            };

            std::string GetHealPriorityString(HealPriority priority)
            {
                switch(priority)
                {
                    case HEAL_PREFER_TARGET_ANY:
                        return "Nahodny cil";
                    case HEAL_PREFER_TARGET_LOWEST_HP:
                        return "cil s nejmene hp";
                    case HEAL_PREFER_TARGET_SELF:
                        return "pouze sebe";
                    default: // wtf is Visual Studio after, there is no other value for HealPriority - HEAL_PREFER_BEGIN = HEAL_PREFER_TARGET_ANY and HEAL_PREFER_MAX = HEAL_PREFER_TARGET_SELF..
                        return "";
                }
            }

            enum DamagePriority : uint8
            {
                DAMAGE_PREFER_BEGIN,
                DAMAGE_PREFER_LOCK_ON_RANDOM_TARGET = DAMAGE_PREFER_BEGIN,
                DAMAGE_PREFER_LOWEST_PCT_HP,
                DAMAGE_PREFER_HIGHEST_PCT_HP,
                DAMAGE_PREFER_STRONGEST_ENEMY,
                DAMAGE_PREFER_WEAKEST_ENEMY,
                DAMAGE_PREFER_MAX = DAMAGE_PREFER_WEAKEST_ENEMY
            };

            std::string GetDamagePriorityString(DamagePriority priority)
            {
                switch(priority)
                {
                    case DAMAGE_PREFER_LOCK_ON_RANDOM_TARGET:
                        return "Nahodny cil do zabiti";
                    case DAMAGE_PREFER_LOWEST_PCT_HP:
                        return "cil s nejmene hp";
                    case DAMAGE_PREFER_HIGHEST_PCT_HP:
                        return "cil s nejvice hp";
                    case DAMAGE_PREFER_STRONGEST_ENEMY:
                        return "nejsilnejsi cil";
                    case DAMAGE_PREFER_WEAKEST_ENEMY:
                        return "nejslabsi cil";
                    default:
                        return "";
                }
            }

            enum SpellModes : uint8
            {
                SPELL_MODE_NO_SPELLS    = 0x0,
                SPELL_MODE_HEAL         = 0x1,
                SPELL_MODE_DAMAGE       = 0x2,
                SPELL_MODE_TAUNT        = 0x4,
            };

            struct RevoltSoldierSpell
            {
                RevoltSoldierSpell(std::string description, uint32 spellId, int8 powerCost, float pctHp = -1.0f, bool minPct = false)
                {
                    this->internalDescription = description + "\nStoji " + std::to_string(powerCost) + " power.\n";
                    this->spellId = spellId;
                    this->powerCost = powerCost;
                    minPowerToCast = powerCost;
                    this->pctHp = pctHp;
                    this->minPct = minPct;
                    UpdateInternalDescription();
                }

                void UpdateInternalDescription()
                {
                    description = internalDescription + "Minimalni power pro seslani: " + std::to_string(minPowerToCast);

                    if (pctHp >= 0.0f)
                        description += (minPct ? "\nMinimalni % hp pro seslani: " : "\nMaximalni % hp pro seslani: ") + std::to_string(pctHp) + ".";
                }

                int8 GetMinPowerToCast()
                {
                    return minPowerToCast;
                }

                float GetPctHp()
                {
                    return pctHp;
                }

                void SetMinPowerToCast(int8 minPower)
                {
                    minPowerToCast = minPower;
                    UpdateInternalDescription();
                }

                void SetPctHp(float pctHp)
                {
                    this->pctHp = pctHp;
                    UpdateInternalDescription();
                }

                std::string description;
                uint32 spellId;
                int8  powerCost;
                bool minPct;
                // user configurable
            private:
                int8  minPowerToCast;
                float pctHp;
                std::string internalDescription;
            };

#define REVOLT_SOLDIER_SPELLS_COUNT 8
            RevoltSoldierSpell spells[REVOLT_SOLDIER_SPELLS_COUNT] =
            {
                { "Taunt\nVyprovokuje nepratele, aby utocili na tohoto vojaka.\nSnizuje veskery damage taken o 10% po seslani na 15 sekund.", EVENT_ZONE_SPELL_TAUNT, 10, 100.0f, true },
                { "Lesser Heal\nHealne za 19-22% hp.\n2s cast time.", EVENT_ZONE_SPELL_LESSER_HEAL, 20, 0.0f },
                { "Holy Light\nHealne za 27-34% hp.\nInstant cast time. ", EVENT_ZONE_SPELL_HOLY_LIGHT, 50, 0.0f },
                { "Great Heal\nHealne za 33-38% hp.\n2s cast time.", EVENT_ZONE_SPELL_GREATER_HEAL, 35, 0.0f },
                { "Smite\nUbere nepriteli 1591-1786 hp a healne sebe za 25% hp.\nInstant cast time.", EVENT_ZONE_SPELL_SMITE, 55, 0.0f },
                { "Crusader Strike\nUderi nepritele za 100% weapon damage a nahodi debuff +10% damage taken. Stackuje se do 5", EVENT_ZONE_SPELL_CRUSADER_STRIKE, 25 },
                { "Holy Bolt Volley\nUbere vsem nepratelum ve 35 yardech 1063-1438 damage.\n1.5s cast time.", EVENT_ZONE_SPELL_HOLY_BOLT_VOLLEY, 40 },
                { "Holy Bolt\nUbere nepriteli 2357-2644 hp do 35 yardu.\n2s cast time.", EVENT_ZONE_SPELL_HOLY_BOLT, 20 }
            };
            uint16 gcd;
            uint16 regenTimer;
            HealPriority healPriority;
            DamagePriority damagePriority;
            SpellModes spellModes;
            uint8 minEnemiesForAoEDamage;
            bool castCheaperHealWhileNoMana;
            float followAngle;
#define GLOBAL_COOLDOWN 1500
            event_zone_revolt_controllable_soldierAI(Creature* creature) : ScriptedAI(creature)
            {
                regenTimer = IN_MILLISECONDS;
                castCheaperHealWhileNoMana = false;
                minEnemiesForAoEDamage = 0;
                me->setPowerType(POWER_FOCUS);
                me->SetMaxPower(POWER_FOCUS, 100);
                me->SetPower(POWER_FOCUS, 100);
                healPriority = HEAL_PREFER_TARGET_SELF;
                damagePriority = DAMAGE_PREFER_LOCK_ON_RANDOM_TARGET;
                spellModes = SPELL_MODE_NO_SPELLS;
                gcd = GLOBAL_COOLDOWN;
                // for internal use
                me->SetMaxPower(POWER_MANA, 9999999);
                me->SetPower(POWER_MANA, me->GetMaxPower(POWER_MANA));
            }

            void DoAction(int32 action) override
            {
                followAngle = action * G3D::pif() / 4.0f + 2 * G3D::pif() / 3.0f;
                me->GetMotionMaster()->MoveFollow(me->GetOwner(), 0.3f, followAngle);
            }

            bool CanAIAttack(const Unit* victim) const override
            {
                return victim->GetTypeId() != TYPEID_PLAYER;
            }

            void JustDied(Unit* /*killer*/) override
            {
                if (Unit* owner = me->GetOwner())
                    if (Player* plrOwner = owner->ToPlayer())
                        if (plrOwner->IsActiveQuest(EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT))
                            plrOwner->FailQuest(EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT);
            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (seer->GetTypeId() == TYPEID_PLAYER)
                    return !seer->ToPlayer()->IsGameMaster() && seer != me->GetOwner();

                if (const Creature* useer = seer->ToCreature())
                {
                    if (useer->GetOwner() == me->GetOwner())
                        return false;
                    if (useer->IsAIEnabled)
                        return useer->AI()->GetGUID(GUID_ACTION_SET_TRIGGERED_PLAYER) != me->GetOwnerGUID();
                }
                return false;
            }
#define REVOLT_SOLDIER_REGEN_TIMER  300
#define MenuAddItemSettings(text, action) menu.AddMenuItem(-1, GOSSIP_ICON_INTERACT_1, text, GOSSIP_SENDER_MAIN, action, "", 0)
#define MenuAddItemSettingsPopup(text, action) menu.AddMenuItem(-1, GOSSIP_ICON_INTERACT_1, text, GOSSIP_SENDER_MAIN, action, "", 0, true)
#define MenuAddItemSpell(text, action) menu.AddMenuItem(-1, GOSSIP_ICON_TRAINER, text, GOSSIP_SENDER_MAIN, action, "", 0)
#define MenuAddItemDescription(text, action) menu.AddMenuItem(-1, GOSSIP_ICON_TRAINER, text, GOSSIP_SENDER_MAIN, action, "", 0)
#define MenuMove(text, action) menu.AddMenuItem(-1, GOSSIP_ICON_TALK, text, GOSSIP_SENDER_MAIN, action, "", 0)
#define TRIGGER_MASK 0 /*TRIGGERED_IGNORE_GCD | TRIGGERED_IGNORE_POWER_AND_REAGENT_COST | TRIGGERED_IGNORE_EQUIPPED_ITEM_REQUIREMENT*/
            void sGossipHello(Player* player) override
            {
                if (player != me->GetOwner() && !player->IsGameMaster())
                    return;

                PlayerMenu* ptc = player->PlayerTalkClass;
                GossipMenu& menu = ptc->GetGossipMenu();
                menu.ClearMenu();
                MenuAddItemSettings(std::string("Povolene sesilani heal spellu: ") + ((spellModes & SPELL_MODE_HEAL) ? "ANO" : "NE"), GOSSIP_ACTION_INFO_DEF + 100);
                MenuAddItemSettings(std::string("Povolene sesilani damage spellu: ") + ((spellModes & SPELL_MODE_DAMAGE) ? "ANO" : "NE"), GOSSIP_ACTION_INFO_DEF + 200);
                MenuAddItemSettings(std::string("Povolene sesilani tauntu: ") + ((spellModes & SPELL_MODE_TAUNT) ? "ANO" : "NE"), GOSSIP_ACTION_INFO_DEF + 300);
                MenuAddItemSettings(std::string("Priorita healovani: ") + GetHealPriorityString(healPriority), GOSSIP_ACTION_INFO_DEF + 400);
                MenuAddItemSettings(std::string("Priorita utoceni: ") + GetDamagePriorityString(damagePriority), GOSSIP_ACTION_INFO_DEF + 500);
                MenuAddItemSettings(std::string("Povolene sesilani levnejsich healu pri nedostatku energie: ")
                                    + (castCheaperHealWhileNoMana ? "ANO" : "NE"), GOSSIP_ACTION_INFO_DEF + 600);
                MenuAddItemSettings(std::string("Minimalni pocet nepratel pro seslani AoE: ") + std::to_string(minEnemiesForAoEDamage), GOSSIP_ACTION_INFO_DEF + 700);
                MenuMove(std::string("Nastaveni jednotlivych kouzel"), GOSSIP_ACTION_INFO_DEF + 800);
                ptc->SendGossipMenu(1, me->GetGUID());
            }

            void ShowSpells(Player* player)
            {
                PlayerMenu* ptc = player->PlayerTalkClass;
                GossipMenu& menu = ptc->GetGossipMenu();
                menu.ClearMenu();
                for (uint8 i = 0; i < REVOLT_SOLDIER_SPELLS_COUNT; ++i)
                    MenuAddItemSpell(spells[i].description, GOSSIP_ACTION_INFO_DEF + i + 1);
                MenuMove(std::string("Zpet do hlavniho menu"), GOSSIP_ACTION_INFO_DEF);
                ptc->SendGossipMenu(1, me->GetGUID());
            }

            void ShowSpellGossip(Player* player, uint32 action)
            {
                PlayerMenu* ptc = player->PlayerTalkClass;
                GossipMenu& menu = ptc->GetGossipMenu();
                menu.ClearMenu();
                if (action < REVOLT_SOLDIER_SPELLS_COUNT)
                {
                    RevoltSoldierSpell& spell = spells[action];
                    MenuAddItemDescription(spell.description, GOSSIP_ACTION_INFO_DEF + action + 1);
                    MenuAddItemSettingsPopup("Zmenit minimalni power pro seslani spellu", action);
                    if (spell.GetPctHp() >= 0.0f)
                        MenuAddItemSettingsPopup(spell.minPct ? "Zmenit minimalni % hp pro seslani spellu" : "Zmenit maximalni % hp pro seslani spellu", 100 + action);
                    MenuMove(std::string("Zpet do nastaveni jednotlivych kouzel"), GOSSIP_ACTION_INFO_DEF + 800);
                    ptc->SendGossipMenu(1, me->GetGUID());
                }
            }

            void sGossipSelectCode(Player* player, uint32, uint32 menuId, const char* code) override
            {
                if (player != me->GetOwner() && !player->IsGameMaster())
                    return;

                uint32 action = player->PlayerTalkClass->GetGossipOptionAction(menuId);

                if (action >= 100)
                {
                    action -= 100;
                    if (action < REVOLT_SOLDIER_SPELLS_COUNT)
                    {
                        float pctHp = static_cast<float>(atof(code));
                        if (pctHp >= 0.0f && pctHp <= 100.0f)
                            spells[action].SetPctHp(pctHp);
                        else
                            me->Whisper("Procenta hp lze nastavit pouze v rozmezí od 0.0 do 100.0!", LANG_UNIVERSAL, player, true);
                    }
                }
                else if (action < REVOLT_SOLDIER_SPELLS_COUNT)
                {
                    RevoltSoldierSpell& spell = spells[action];
                    int32 minPower = atoi(code);
                    if (minPower >= spell.powerCost && minPower <= 100)
                        spell.SetMinPowerToCast(minPower);
                    else
                        me->Whisper("Minimalni power pro seslani lze nastavit v rozmezi od " + std::to_string(spell.powerCost) + " do 100.", LANG_UNIVERSAL, player, true);
                }

                ShowSpellGossip(player, action);
            }

            void sGossipSelect(Player* player, uint32, uint32 menuId) override
            {
                if (player != me->GetOwner() && !player->IsGameMaster())
                    return;

                uint32 action = player->PlayerTalkClass->GetGossipOptionAction(menuId);

                if (action < GOSSIP_ACTION_INFO_DEF)
                    return;
                action -= GOSSIP_ACTION_INFO_DEF;
                switch (action)
                {
                    case 0:
                        break;
                    case 100:
                    case 200:
                    case 300:
                        spellModes = SpellModes(spellModes ^ (1 << ((action / 100) - 1)));
                        break;
                    case 400:
                        if (healPriority == HEAL_PREFER_MAX)
                            healPriority = HEAL_PREFER_BEGIN;
                        else
                            healPriority = HealPriority(uint8(healPriority + 1));
                        break;
                    case 500:
                        if (damagePriority == DAMAGE_PREFER_MAX)
                            damagePriority = DAMAGE_PREFER_BEGIN;
                        else
                            damagePriority = DamagePriority(uint8(damagePriority + 1));
                        break;
                    case 600:
                        castCheaperHealWhileNoMana = !castCheaperHealWhileNoMana;
                        break;
                    case 700:
                        if (++minEnemiesForAoEDamage > 10)
                            minEnemiesForAoEDamage = 0;
                        break;
                    case 800:
                        ShowSpells(player);
                        return;
                    default:
                        ShowSpellGossip(player, action - 1);
                        return;
                }
                sGossipHello(player);
            }

            void OnSuccessfullSpellCast(SpellInfo const* spell) override
            {

                if (spell->Id == EVENT_ZONE_SPELL_TAUNT)
                    me->AddAura(EVENT_ZONE_SPELL_INSPIRATION, me);

                for (uint8 i = 0; i < REVOLT_SOLDIER_SPELLS_COUNT; ++i)
                {
                    if (spell->Id == spells[i].spellId)
                    {
                        me->ModifyPower(POWER_FOCUS, -spells[i].powerCost);
                        return;
                    }
                }
            }

            class AllHostileCreaturesInRange
            {
            public:
                AllHostileCreaturesInRange(Unit const* obj, float range) : i_obj(obj), i_range(range) { }
                bool operator()(Unit* u)
                {
                    if (!u->IsAlive() || !i_obj->IsWithinDistInMap(u, i_range) || u->IsFriendlyTo(i_obj) || !u->CanSeeOrDetect(i_obj) || !i_obj->CanSeeOrDetect(u))
                        return false;
                    return true;
                }
            private:
                Unit const* i_obj;
                float i_range;
            };

            void GetEnemyList(std::list<Creature*>& enemies, float range = 35.0f)
            {
                AllHostileCreaturesInRange checker(me, range);
                Trinity::CreatureListSearcher<AllHostileCreaturesInRange> searcher(me, enemies, checker);
                me->VisitNearbyObject(range, searcher);
            }

            void GetFriendsList(std::list<Creature*>& friends)
            {
                Trinity::AllCreaturesOfEntryInRange checker(me, me->GetEntry(), 30.0f);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(me, friends, checker);
                me->VisitNearbyObject(30.0f, searcher);
            }

            Unit* GetFriendlyTarget()
            {
                Unit* target = nullptr;

                switch (healPriority)
                {
                    case HEAL_PREFER_TARGET_ANY:
                    {
                        std::list<Creature*> friends;
                        GetFriendsList(friends);
                        if (!friends.empty())
                        {
                            auto itr = friends.begin();
                            std::advance(itr, urand(0, static_cast<uint32>(friends.size() - 1)));
                            target = *itr;
                        }
                        break;
                    }
                    case HEAL_PREFER_TARGET_LOWEST_HP:
                    {
                        std::list<Creature*> friends;
                        GetFriendsList(friends);
                        for (Creature* current : friends)
                            if (!target || target->GetHealthPct() > current->GetHealthPct())
                                target = current;
                        break;
                    }
                    case HEAL_PREFER_TARGET_SELF:
                    {
                        target = me;
                        break;
                    }
                }

                return target;
            }

            bool TryCastHeal()
            {
                if (Unit* target = GetFriendlyTarget())
                {
                    float pctHp = target->GetHealthPct();
                    if (pctHp <= spells[SPELL_HOLY_LIGHT].GetPctHp())
                    {
                        if (uint8 res = CastHeal(SPELL_HOLY_LIGHT, target))
                            return res - 1 > 0;
                    }

                    if (pctHp <= spells[SPELL_GREAT_HEAL].GetPctHp())
                    {
                        if (uint8 res = CastHeal(SPELL_GREAT_HEAL, target))
                            return res - 1 > 0;
                    }

                    if (pctHp <= spells[SPELL_LESSER_HEAL].GetPctHp())
                    {
                        if (uint8 res = CastHeal(SPELL_LESSER_HEAL, target))
                            return res - 1 > 0;
                    }
                }
                return false;
            }

            bool CastSpell(RevoltSoldierSpells index, Unit* target, bool setGcd)
            {
                RevoltSoldierSpell& spell = spells[index];
                if (spell.GetMinPowerToCast() <= int32(me->GetPower(POWER_FOCUS)))
                {
                    me->CastSpell(target, spell.spellId, TRIGGER_MASK);
                    if (setGcd)
                        gcd = GLOBAL_COOLDOWN;
                    return true;
                }
                return false;
            }

            uint8 CastHeal(RevoltSoldierSpells index, Unit* target)
            {
                if (CastSpell(index, target, true))
                    return 2;
                else if (!castCheaperHealWhileNoMana)
                    return 1;
                return 0;
            }

            bool CastDamage(RevoltSoldierSpells index, Unit* target)
            {
                AttackStart(target);
                return CastSpell(index, target, true);
            }

            Unit* GetHostileTarget()
            {
                Unit* target = nullptr;
                std::list<Creature*> enemies;
                GetEnemyList(enemies);
                switch(damagePriority)
                {
                    case DAMAGE_PREFER_LOCK_ON_RANDOM_TARGET:
                    {
                        // target is already set
                        target = me->GetVictim();
                        if (!target)
                        {
                            if (!enemies.empty())
                            {
                                auto itr = enemies.begin();
                                std::advance(itr, urand(0, static_cast<uint32>(enemies.size() - 1)));
                                target = *itr;
                            }
                        }
                        break;
                    }
                    case DAMAGE_PREFER_LOWEST_PCT_HP:
                    {
                        for (Creature* current : enemies)
                            if (!target || target->GetHealthPct() > current->GetHealthPct())
                                target = current;
                        break;
                    }
                    case DAMAGE_PREFER_HIGHEST_PCT_HP:
                    {
                        for (Creature* current : enemies)
                            if (!target || target->GetHealthPct() < current->GetHealthPct())
                                target = current;
                        break;
                    }
                    case DAMAGE_PREFER_STRONGEST_ENEMY:
                    {
                        for (Creature* current : enemies)
                        {
                            if (!target || target->GetMaxHealth() < current->GetMaxHealth())
                            {
                                target = current;
                                continue;
                            }

                            if (target->GetMaxHealth() == current->GetMaxHealth() && target->GetHealth() < current->GetHealth())
                                target = current;
                        }
                        break;
                    }
                    case DAMAGE_PREFER_WEAKEST_ENEMY:
                    {
                        for (Creature* current : enemies)
                        {
                            if (!target || target->GetMaxHealth() > current->GetMaxHealth())
                            {
                                target = current;
                                continue;
                            }
                            if (target->GetMaxHealth() == current->GetMaxHealth() && target->GetHealth() > current->GetHealth())
                                target = current;
                        }
                        break;
                    }
                }

                return target;
            }

            bool CanCastAoEDamage()
            {
                std::list<Creature*> enemies;
                GetEnemyList(enemies);
                return enemies.size() >= minEnemiesForAoEDamage;
            }

            bool TryCastDamage()
            {
                if (CanCastAoEDamage())
                    return CastDamage(SPELL_HOLY_BOLT_VOLLEY, nullptr);
                if (Unit* target = GetHostileTarget())
                {
                    if (me->GetHealthPct() <= spells[SPELL_SMITE].GetPctHp() && CastDamage(SPELL_SMITE, target))
                        return true;

                    if (CastDamage(SPELL_CRUSADER_STRIKE, target))
                        return true;

                    return CastDamage(SPELL_HOLY_BOLT, target);
                }
                return false;
            }

            bool TryCastTaunt()
            {
                if (me->GetHealthPct() > spells[SPELL_TAUNT].GetPctHp())
                {
                    std::list<Creature*> enemies;
                    GetEnemyList(enemies, 15.0f);

                    if (enemies.empty())
                        return false;

                    if (!me->HasAura(EVENT_ZONE_SPELL_INSPIRATION))
                        return CastSpell(SPELL_TAUNT, nullptr, false);

                    for (Creature* enemy : enemies)
                        if (enemy->GetVictim() != me)
                            return CastSpell(SPELL_TAUNT, nullptr, false);
                }
                return false;
            }

            void UpdateAI(uint32 diff) override
            {
                if (gcd > diff)
                    gcd -= diff;
                else gcd = 0;

                if (regenTimer > diff)
                    regenTimer -= diff;
                else
                {
                    regenTimer += REVOLT_SOLDIER_REGEN_TIMER;
                    // check for high lag diff
                    if (regenTimer > diff)
                        regenTimer -= diff;
                    else
                        regenTimer = 0;
                    me->ModifyPower(POWER_FOCUS, 1);
                }

                DoMeleeAttackIfReady();

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if ((spellModes & SPELL_MODE_TAUNT))
                    TryCastTaunt();

                if (gcd)
                    return;

                if (!me->GetOwner())
                    return me->DespawnOrUnsummon();

                if ((spellModes & SPELL_MODE_HEAL) && TryCastHeal())
                    return;

                if (Unit* victim = GetHostileTarget())
                    AttackStart(victim);
                else if (!me->isMoving())
                    me->GetMotionMaster()->MoveFollow(me->GetOwner(), 0.3f, followAngle);

                if ((spellModes & SPELL_MODE_DAMAGE) && TryCastDamage())
                    return;

                // prevent calling TryCastXY every update loop if no targets are found
                gcd = 500;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_controllable_soldierAI(creature);
        }
    };

    class event_zone_revolt_brigadier_general : public CreatureScript
    {
        enum BrigadierGeneralEnum
        {
            BRIGADIER_GENERAL_SPAWN_COUNT = 4,
            PLAYER_WAYPOINTS_COUNT        = 19
        };

        G3D::Vector3 const playerWaypoints[PLAYER_WAYPOINTS_COUNT]
        {
            { 1647.84f, -6032.19f, 150.1f },
            { 1648.77f, -6021.67f, 148.8f },
            { 1660.99f, -6012.62f, 148.8f },
            { 1667.72f, -6017.25f, 148.8f },
            { 1666.63f, -6027.43f, 143.7f },
            { 1661.76f, -6031.41f, 143.7f },
            { 1645.19f, -6030.56f, 143.7f },
            { 1641.14f, -6033.90f, 143.7f },
            { 1640.27f, -6040.37f, 139.1f },
            { 1642.91f, -6044.56f, 138.8f },
            { 1648.09f, -6044.85f, 134.9f },
            { 1652.36f, -6038.38f, 134.9f },
            { 1646.28f, -6031.52f, 134.9f },
            { 1637.98f, -6028.12f, 134.9f },
            { 1626.82f, -6023.27f, 134.9f },
            { 1630.40f, -6014.99f, 134.9f },
            { 1640.03f, -6015.85f, 134.9f },
            { 1649.67f, -6010.74f, 133.4f },
            { 1650.62f, -5998.84f, 133.7f }
        };

    public:
        event_zone_revolt_brigadier_general() : CreatureScript("event_zone_revolt_brigadier_general") { }

        struct event_zone_revolt_brigadier_generalAI : NullCreatureAI
        {
            event_zone_revolt_brigadier_generalAI(Creature* creature) : NullCreatureAI(creature) { }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (seer->GetTypeId() != TYPEID_PLAYER)
                    return true;

                const Player* plr = seer->ToPlayer();
                if (plr->IsGameMaster())
                    return false;

                return plr->GetQuestStatus(EVENT_ZONE_QUEST_ASK_LAYI_ELY_FOR_HELP) == QUEST_STATUS_REWARDED;
            }
        };

        class SummonSoldiersEvent : public BasicEvent
        {
        public:
            SummonSoldiersEvent(Player* owner) : BasicEvent()
            {
                this->owner = owner;
            }

            class OwnedControllableRevoltSoldierSelector
            {
            public:
                OwnedControllableRevoltSoldierSelector(WorldObject const* obj) : i_obj(obj) { }
                bool operator()(Unit* u)
                {
                    if (u->GetEntry() != EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER || u->GetOwner() != i_obj)
                        return false;
                    return true;
                }
            private:
                WorldObject const* i_obj;
            };

            void DespawnOwnedSoldiers(float distance)
            {
                std::list<Creature*> creatures;
                OwnedControllableRevoltSoldierSelector u_check(owner);
                Trinity::CreatureListSearcher<OwnedControllableRevoltSoldierSelector> searcher(owner, creatures, u_check);
                owner->VisitNearbyObject(distance, searcher);
                for (Creature* creature : creatures)
                    creature->DespawnOrUnsummon();
            }

            bool Execute(uint64, uint32)
            {
                // players might try cheating and accept quest multiple times
                DespawnOwnedSoldiers(500.0f);
                owner->SetControlled(false, UNIT_STATE_CONFUSED);
                owner->GetMotionMaster()->MovePoint(0, 1650.62f, -5998.84f, 133.7f, false);
                owner->GetMap()->LoadGrid(static_cast<float>(leaderGrid.x), static_cast<float>(leaderGrid.y));
                if (Creature* leader = owner->SummonCreature(EVENT_ZONE_CREATURE_REVOLT_LEADER, leaderSpawnPos))
                {
                    leader->setActive(true);
                    leader->AI()->SetGUID(owner->GetGUID(), GUID_ACTION_SET_TRIGGERED_PLAYER);
                    Creature* soldier;
                    for (uint8 i = 0; i < BRIGADIER_GENERAL_SPAWN_COUNT; ++i)
                    {
                        soldier = owner->SummonCreature(EVENT_ZONE_CREATURE_REVOLT_CONTROLLABLE_SOLDIER, *owner);
                        soldier->SetOwnerGUID(owner->GetGUID());
                        soldier->setFaction(owner->getFaction());
                        soldier->AI()->DoAction(i);
                        leader->AI()->SetGUID(soldier->GetGUID(), GUID_ACTION_SET_ENEMY_GUID);
                    }
                    soldier->Say("Očekáváme vaše rozkazy!", LANG_UNIVERSAL);
                    // despawn old leaders
                    std::list<Creature*> oldLeaders;
                    leader->GetCreatureListWithEntryInGrid(oldLeaders, leader->GetEntry(), 100.0f);
                    for (Creature* oldLeader : oldLeaders)
                    {
                        if (oldLeader == leader)
                            continue;
                        if (oldLeader->AI()->GetGUID(GUID_ACTION_SET_TRIGGERED_PLAYER) == leader->AI()->GetGUID(GUID_ACTION_SET_TRIGGERED_PLAYER))
                            oldLeader->DespawnOrUnsummon();
                    }
                }
                return true;
            }
        private:
            Player* owner;
        };

        void CompleteReputationQuest(Player* player, Creature* me)
        {
            switch(player->GetQuestStatus(EVENT_ZONE_QUEST_GAIN_REPUTATION))
            {
                case QUEST_STATUS_NONE:
                    player->AddQuest(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_GAIN_REPUTATION), me);
                    // no break intended
                case QUEST_STATUS_INCOMPLETE:
                    player->CompleteQuest(EVENT_ZONE_QUEST_GAIN_REPUTATION);
                    me->Whisper("$GZiskal:Ziskala; si potrebnou reputaci na to, aby ses $Gdostal:dostala; za Layi Ely!", LANG_UNIVERSAL, player, true);
                    break;
                default:
                    break;
            }
        }

        bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
        {
            if (quest->GetQuestId() != EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT)
                return false;
            CompleteReputationQuest(player, me);
            return true;
        }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT) == QUEST_STATUS_REWARDED)
                CompleteReputationQuest(player, me);
            return false;
        }

        bool OnQuestAccept(Player* player, Creature* me, const Quest* quest) override
        {
            if (quest->GetQuestId() == EVENT_ZONE_QUEST_SUPPRESSION_OF_THE_REVOLT)
            {
                me->SaySingleTarget("Pospěš si, venku už na tebe čeká družina mých nejlepších bojovníků!", LANG_UNIVERSAL, player);
                player->GetMotionMaster()->MoveSmoothPath(0, playerWaypoints, PLAYER_WAYPOINTS_COUNT, true);
                player->SetControlled(true, UNIT_STATE_CONFUSED);
                player->m_Events.AddEvent(new SummonSoldiersEvent(player), player->m_Events.CalculateTime(70000));
            }
            return true;
        }

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_revolt_brigadier_generalAI(creature);
        }
    };
}

namespace Daily
{
    class event_zone_daily_billboard : public GameObjectScript
    {
    public:
        event_zone_daily_billboard() : GameObjectScript("event_zone_daily_billboard") { }

        void AddGossipPOIFor(Player* player, uint32 icon, std::string const& text, uint32 sender, uint32 action, uint32 poi)
        {
            AddGossipItemFor(player, icon, text, sender, action);
            player->PlayerTalkClass->GetGossipMenu().AddGossipMenuItemData(sender, action, poi);
        }

        bool OnGossipHello(Player* player, GameObject* go) override
        {
            for (uint32 poiId = EVENT_ZONE_POI_BILLBOARD_START; poiId <= EVENT_ZONE_POI_BILLBOARD_MAX; ++poiId)
            {
                PointOfInterest const* poi = sObjectMgr->GetPointOfInterest(poiId);
                AddGossipPOIFor(player, GOSSIP_ICON_DOT, poi->icon_name, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + poiId, poiId);
            }
            SendGossipMenuFor(player, 1, go->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, GameObject*, uint32, uint32 action) override
        {
            player->PlayerTalkClass->SendPointOfInterest(action - GOSSIP_ACTION_INFO_DEF);
            CloseGossipMenuFor(player);
            return true;
        }
    };

    namespace Tavern
    {
        class event_zone_innkeeper_daily : public CreatureScript
        {
        public:
            event_zone_innkeeper_daily() : CreatureScript("event_zone_innkeeper_daily") { }

            bool OnQuestAccept(Player* player, Creature* me, const Quest* quest)
            {
                if (quest->GetQuestId() != EVENT_ZONE_QUEST_DAILY_INNKEEPER_GUESTS)
                    return true;

                if (Creature* spawner = player->SummonCreature(EVENT_ZONE_CREATURE_INN_GUEST_SPAWNER, *me))
                {
                    me->Whisper("Hoste prichazi, postarej se o ne!", LANG_UNIVERSAL, player, true);
                    spawner->SetOwnerGUID(player->GetGUID());
                }
                return true;
            }
        };
    }

    namespace Hospital
    {

    }

    namespace Lumberjacks
    {

        class spell_event_zone_chop_wood : public SpellScript
        {
            PrepareSpellScript(spell_event_zone_chop_wood);

            // if we explicitly provide target for our custom quest, then use that target, otherwise use random target as before
            void SelectTree(WorldObject*& target)
            {
                if (GetExplTargetUnit())
                    target = GetExplTargetUnit();
            }

            void Register() override
            {
                OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_event_zone_chop_wood::SelectTree, EFFECT_0, TARGET_UNIT_NEARBY_ENTRY);
            }
        };

        class event_zone_choppable_tree : public CreatureScript
        {
        public:
            event_zone_choppable_tree() : CreatureScript("event_zone_choppable_tree") { }

            struct event_zone_choppable_treeAI : NullCreatureAI
            {
                event_zone_choppable_treeAI(Creature* creature) : NullCreatureAI(creature) { }
                void SpellHit(Unit* caster, const SpellInfo* spell) override
                {
                    if (spell->Id == EVENT_ZONE_SPELL_CHOP_TREE && roll_chance_i(70))
                        caster->DealDamage(me, 1);
                }

                void SpawnWood()
                {
                    uint32 maximum = std::min<uint32>(5, ceil(me->GetMaxHealth() /  2.0f));
                    uint32 counter = urand(ceil(maximum / 2.0f), maximum);
                    for (uint32 i = 0; i < counter; ++i)
                        me->SummonGameObject(EVENT_ZONE_GAMEOBJECT_COLLECTABLE_WOOD, me->GetRandomNearPosition(5 + me->GetCreatureTemplate()->scale * 2), G3D::Quat(), 15 * IN_MILLISECONDS);
                }

                void JustDied(Unit*) override
                {
                    SpawnWood();
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_choppable_treeAI(creature);
            }

            bool OnGossipHello(Player* player, Creature* me) override
            {
                if (player->IsActiveQuest(EVENT_ZONE_QUEST_DAILY_COLLECT_WOOD))
                    player->CastSpell(me, EVENT_ZONE_SPELL_CHOP_TREE);
                return true;
            }
        };

        class event_zone_wood_collector : public CreatureScript
        {
        public:
            event_zone_wood_collector() : CreatureScript("event_zone_wood_collector") { }
            struct event_zone_wood_collectorAI : CreatureAI
            {
                uint32 timer = 2000;
                uint32 delay = 2000;
                uint8  troll = 5;
                std::vector<std::string> dialog;
                event_zone_wood_collectorAI(Creature* creature) : CreatureAI(creature)
                {
                    me->setPowerType(POWER_FOCUS);
                    me->SetMaxPower(POWER_FOCUS, 5);
                    dialog.push_back("Ty budeš kácet stromy a já nosit dříví!");
                }

                void UpdateAI(uint32 diff) override
                {
                    if (delay > diff)
                    {
                        delay -= diff;
                        return;
                    }

                    delay = timer;

                    Player* owner = me->GetOwner() ? me->GetOwner()->ToPlayer() : nullptr;
                    if (!owner)
                        return me->DespawnOrUnsummon();

                    if (--troll == 0)
                    {
                        troll = 15;
                        dialog.push_back(RAND("Jednou bych chtěl být farmářem, ale Vojta mě nechce pustit odsud."s,
                                              "Strašně moc se mi líbí hostinská Eliška, ale ještě nikdy jsem jí to neřekl."s,
                                              "Někde jsem tě už viděl, ale nemohu si vzpomenout kdy a kde..."s,
                                              "Chutnají ti čokoládové bonbóny? Já je miluji!"s,
                                              "Chtěl bych si založit vlastní rodinu na farmě s Eliškou."s,
                                              "Mám hlad, mohli bychom už jít domu?"s,
                                              "Mám hlad..."s,
                                              "Nemáš něco k pití?"s,
                                              "Tohle je nuda, nemůžeme jít už zpět?"s,
                                              "Už začínám být unavený, pojďme pryč."s,
                                              "Ach, Eliška..."s));
                    }

                    if (!dialog.empty())
                    {
                        me->SaySingleTarget(dialog.back(), LANG_UNIVERSAL, owner);
                        dialog.pop_back();
                    }

                    bool kneel = me->HasAura(EVENT_ZONE_AURA_KNEEL);
                    if (!kneel && owner->GetDistance(me) > 10.0f)
                    {
                        me->GetMotionMaster()->MoveFollow(owner, 0.0f, 0.0f);
                        return;
                    }

                    if (me->GetPower(POWER_FOCUS) > 0)
                    {
                        if (Creature* questGiver = me->FindNearestCreature(EVENT_ZONE_CREATURE_WOOD_COLLECTION_QUEST_GIVER, 8.0f))
                        {
                            if (questGiver->GetDistance(me) < 2.0f)
                            {
                                owner->KilledMonsterCredit(me->GetEntry(), ObjectGuid::Empty, -me->ModifyPower(POWER_FOCUS, -2));
                                if (owner->GetQuestStatus(EVENT_ZONE_QUEST_DAILY_COLLECT_WOOD) == QUEST_STATUS_COMPLETE)
                                {
                                    questGiver->SaySingleTarget("Děkuji ti za tvé služby!", LANG_UNIVERSAL, owner);
                                    return me->DespawnOrUnsummon();
                                }
                            }
                            else
                                me->GetMotionMaster()->MoveFollow(questGiver, 0.0f, 0.0f);
                            return;
                        }
                    }

                    if (me->GetPower(POWER_FOCUS) < me->GetMaxPower(POWER_FOCUS))
                    {
                        if (GameObject* wood = me->FindNearestGameObject(EVENT_ZONE_GAMEOBJECT_COLLECTABLE_WOOD, 8.0f))
                        {
                            if (wood->GetDistance(me) < 2.0f)
                            {
                                if (kneel)
                                {
                                    wood->Delete();
                                    me->ModifyPower(POWER_FOCUS, 1);
                                    if (me->GetPower(POWER_FOCUS) == me->GetMaxPower(POWER_FOCUS))
                                        dialog.push_back(RAND("Víc už toho nepoberu, pojďme zpět!"s, "Pane bože, to je ale tíha, pojďme zpět, víc toho nepoberu!"s, "Kdo to má nosit! Pojď zpět, víc toho už brát nebudu"s));
                                    else
                                        dialog.push_back(RAND("Fuha, to je ale tíha!"s, "Nevím, jestli to unesu!"s, "Ještě pár kousků a měli bychom jít zpět."s));
                                }
                                else
                                {
                                    me->AddAura(EVENT_ZONE_AURA_KNEEL, me);
                                    return;
                                }
                            }
                            else
                            {
                                float x,y,z;
                                wood->GetContactPoint(me,x,y,z);
                                me->GetMotionMaster()->MovePoint(1, x, y, z, true);
                            }

                            if (kneel)
                                me->RemoveAura(EVENT_ZONE_AURA_KNEEL);
                            return;
                        }
                    }

                    if (kneel)
                        me->RemoveAura(EVENT_ZONE_AURA_KNEEL);
                    if (!me->isMoving())
                        me->GetMotionMaster()->MoveFollow(owner, 0.0f, 0.0f);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_wood_collectorAI(creature);
            }
        };

        class event_zone_wood_collection_quest_giver : public CreatureScript
        {
        public:
            event_zone_wood_collection_quest_giver() : CreatureScript("event_zone_wood_collection_quest_giver") { }

            bool HasCollector(Player* player)
            {
                std::list<Creature*> collectors;
                player->GetCreatureListWithEntryInGrid(collectors, EVENT_ZONE_CREATURE_WOOD_COLLECTOR, 50.0f);
                for (Creature* collector : collectors)
                    if (collector->GetOwnerGUID() == player->GetGUID())
                        return true;
                return false;
            }

            void SummonCollectorFor(Player* player)
            {
                if (Creature* collector = player->SummonCreature(EVENT_ZONE_CREATURE_WOOD_COLLECTOR, player->GetRandomNearPosition(12.0f)))
                {
                    collector->setActive(true);
                    collector->SetOwnerGUID(player->GetGUID());
                    collector->UpdateObjectVisibility();
                    collector->GetMotionMaster()->MoveFollow(player, 0.0f, 0.0f);
                }
            }

            bool OnQuestAccept(Player* player, Creature* me, const Quest* quest) override
            {
                if (quest->GetQuestId() != EVENT_ZONE_QUEST_DAILY_COLLECT_WOOD)
                    return true;

                if (!HasCollector(player))
                {
                    SummonCollectorFor(player);
                    me->SaySingleTarget("Jirko, pojď pomoct se dřívím!", LANG_UNIVERSAL, player);
                }

                return true;
            }

            bool OnGossipHello(Player* player, Creature* me) override
            {
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_DAILY_COLLECT_WOOD) != QUEST_STATUS_INCOMPLETE)
                    return false;

                if (HasCollector(player))
                    return false;

                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Jirka se mi ztratil", GOSSIP_SENDER_MAIN, 100);
                SendGossipMenuFor(player, 1, me);
                return true;
            }

            bool OnGossipSelect(Player* player, Creature* me, uint32, uint32) override
            {
                CloseGossipMenuFor(player);
                if (!HasCollector(player))
                {
                    SummonCollectorFor(player);
                    me->SaySingleTarget("Jirko, kde se flákáš, mazej pracovat!", LANG_UNIVERSAL, player);
                    return true;
                }
                return true;
            }
        };
    }

    namespace EvilsLair
    {
        class event_zone_necromancers_zombie : public CreatureScript
        {
        public:
            event_zone_necromancers_zombie() : CreatureScript("event_zone_necromancers_zombie") { }
            struct event_zone_necromancers_zombieAI : ScriptedAI
            {
                event_zone_necromancers_zombieAI(Creature* creature) : ScriptedAI(creature) { }
                TaskScheduler scheduler;

                void Reset() override
                {
                    if (me->ToTempSummon())
                    {
                        scheduler.Schedule(Seconds(3), [this](TaskContext)
                        {
                            me->SetReactState(REACT_AGGRESSIVE);
                            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                            if (me->ToTempSummon())
                                if (Creature* summoner = me->ToTempSummon()->GetSummonerCreatureBase())
                                    me->GetMotionMaster()->MoveFollow(summoner, 0.0f, 0.0f);
                        });
                    }
                }

                void EnterEvadeMode(EvadeReason why) override
                {
                    _EnterEvadeMode(why);
                    me->GetMotionMaster()->MoveTargetedHome();

                }

                void UpdateAI(uint32 diff) override
                {
                    UpdateVictim();
                    scheduler.Update(diff, std::bind(&CreatureAI::DoMeleeAttackIfReady, this));
                }
                void JustDied(Unit*) override
                {
                    DoCastAOE(EVENT_ZONE_SPELL_CORPSE_EXPLOSION, true);
                    DoCastAOE(EVENT_ZONE_SPELL_CLOUD_OF_DISEASE, true);
                }

                void KilledUnit(Unit* victim) override
                {
                    if (me->ToTempSummon())
                        if (Creature* necromancer = me->ToTempSummon()->GetSummonerCreatureBase())
                            necromancer->AI()->KilledUnit(victim);
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_necromancers_zombieAI(creature);
            }
        };

        class event_zone_necromancer_boss : public CreatureScript
        {
        public:

            event_zone_necromancer_boss() : CreatureScript("event_zone_necromancer_boss") { }
            struct event_zone_necromancer_bossAI : CreatureAI
            {
                std::vector<Position> spawnPoints =
                {
                    { 2787.8f, -5533.0f, 162.9f, 3.11f },
                    { 2782.2f, -5518.3f, 162.9f, 3.91f },
                    { 2767.2f, -5531.5f, 161.8f, 5.47f },
                    { 2751.5f, -5545.3f, 162.9f, 0.77f },
                    { 2756.8f, -5552.1f, 162.9f, 1.54f },
                };

                TaskScheduler scheduler;
                SummonList summons;
                event_zone_necromancer_bossAI(Creature* creature) : CreatureAI(creature), summons(creature) { }

                void KilledUnit(Unit* victim) override
                {
                    if (me->IsAlive() && victim->GetTypeId() == TYPEID_PLAYER)
                    {
                        me->Whisper("Tvá duše je má!", LANG_UNIVERSAL, victim->ToPlayer());
                        me->Say("Muhahahaha!", LANG_UNIVERSAL);
                    }
                }

                void JustSummoned(Creature* summon) override
                {
                    summon->SetReactState(REACT_PASSIVE);
                    summon->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                    summon->HandleEmoteCommand(EMOTE_ONESHOT_EMERGE);
                    summons.Summon(summon);
                }

                void SummonedCreatureDespawn(Creature* creature) override
                {
                    summons.Despawn(creature);
                }

                void EnterEvadeMode(EvadeReason why) override
                {
                    summons.DespawnAll();
                    CreatureAI::EnterEvadeMode(why);
                }

                void Reset() override
                {
                    scheduler.CancelAll();
                    scheduler.Schedule(Seconds(1), [this](TaskContext volley)
                    {
                        DoCastAOE(EVENT_ZONE_SHADOW_BOLT_VOLLEY, true);
                        volley.Repeat();
                    }).Schedule(Seconds(10), Seconds(30), [this](TaskContext nova)
                    {
                        DoCastAOE(EVENT_ZONE_SPELL_SHADOW_NOVA);
                        nova.Repeat();
                    }).Schedule(Milliseconds(1500), [this](TaskContext summon)
                    {
                        const Position& pos = Trinity::Containers::SelectRandomContainerElement(spawnPoints);
                        DoSummon(EVENT_ZONE_CREATURE_NECROMANCERS_ZOMBIE, pos);
                        summon.Repeat();
                    });
                }

                void UpdateAI(uint32 diff) override
                {
                    if (UpdateVictim())
                        scheduler.Update(diff);
                }
            };
            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_necromancer_bossAI(creature);
            }
        };

        class event_zone_mounted_brigadier_general : public CreatureScript
        {
        public:
            event_zone_mounted_brigadier_general() : CreatureScript("event_zone_mounted_brigadier_general") { }

            struct event_zone_mounted_brigadier_generalAI : CreatureAI
            {
                enum MountedBrigadierGeneralEnum
                {
                    SOLDIERS_COUNT = 2
                };

                ObjectGuid soldierGuids[SOLDIERS_COUNT];
                uint32 mapId;
                uint32 instanceId;
                TaskScheduler scheduler;

                event_zone_mounted_brigadier_generalAI(Creature* creature) : CreatureAI(creature)
                {
                    me->setActive(true);
                    me->SetWalk(true);
                    mapId = me->GetMapId();
                    instanceId = me->GetInstanceId();

                    for (uint8 i = 0; i < SOLDIERS_COUNT; ++i)
                    {
                        if (TempSummon* soldier = me->SummonCreature(EVENT_ZONE_CREATURE_BRIGADIER_GENERALS_SOLDIER, *me))
                        {
                            soldier->SetWalk(true);
                            soldier->GetMotionMaster()->MoveFollow(me, 0.001f, static_cast<float>((1+i*2)*M_PI/2));
                            soldierGuids[i] = soldier->GetGUID();
                        }
                    }
                }

                void Reset() override
                {
                    scheduler.Schedule(Seconds(1), [this](TaskContext movementCheck)
                    {
                        if (me->IsAlive() && !me->isMoving())
                        {
                            movementCheck.Schedule(Seconds(5), [this, movementCheck](TaskContext)
                            {
                                me->GetMotionMaster()->MovementExpired();
                            });
                            movementCheck.Repeat(Seconds(5));
                        }
                        else
                            movementCheck.Repeat();
                    });
                }

                void UpdateAI(uint32 diff) override
                {
                    scheduler.Update(diff);
                }

                ~event_zone_mounted_brigadier_generalAI()
                {
                    if (Map* map = sMapMgr->FindMap(mapId, instanceId))
                        for (uint8 i = 0; i < SOLDIERS_COUNT; ++i)
                            if (Creature* creature = map->GetCreature(soldierGuids[i]))
                                creature->DespawnOrUnsummon();
                }
            };

            CreatureAI* GetAI(Creature* creature) const override
            {
                return new event_zone_mounted_brigadier_generalAI(creature);
            }
        };
    }
}

namespace Wedding
{
    class event_zone_cloth_trader : public CreatureScript
    {
    public:
        event_zone_cloth_trader() : CreatureScript("event_zone_cloth_trader") { }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            uint32 money = player->GetRealmId() == realm.Id.Realm ? 500 : 25;
            money *= GOLD;
            AddGossipItemFor(player, GOSSIP_ICON_MONEY_BAG, "Chci si neco koupit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_TRADE, "Mohu ti ukazat me zbozi pouze pokud mi zaplatis!", money, false);
            SendGossipMenuFor(player, 1, me);
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            if (action == GOSSIP_ACTION_TRADE)
            {
                int32 gold = (player->GetRealmId() == realm.Id.Realm ? 500 : 25);
                int32 money = gold * GOLD;
                if (player->GetMoney() >= static_cast<uint32>(money))
                {
                    player->ModifyMoney(-money);
                    player->GetSession()->SendListInventory(me->GetGUID());
                }
                else
                    me->SaySingleTarget("Ne, nedám ti slevu, buďto mi dáš " + std::to_string(gold) + "g, nebo nic nebude!", LANG_UNIVERSAL, player);
            }
            return true;
        }
    };

    class event_zone_boxes : public box_spawner_base_class
    {
    public:
        event_zone_boxes() : box_spawner_base_class("event_zone_boxes") { }

        struct event_zone_boxesAI : public box_spawner_base_classAI
        {
            enum
            {
                BOXES_TOTAL = 300
            };

            event_zone_boxesAI(Creature* creature) : box_spawner_base_classAI(creature, creature->GetPositionX(), creature->GetPositionY(), creature->GetPositionZ())
            {
                me->RemoveAllGameObjects();
                waypoints.reserve(BOXES_TOTAL);
                for (uint8 i = 0; i < 3; ++i)
                {
                    AddWPInDir(0, 1, 1);
                    AddWPInDir(0, 1, 0);
                    AddWPInDir(0, 1, 0);
                    AddWPInDir(1, 0, 0);
                    AddWPInDir(1, 0, 0);
                    AddWPInDir(1, 0, 0);
                    AddWPInDir(0, -1, 0);
                    AddWPInDir(0, -1, 0);
                    AddWPInDir(0, -1, 0);
                    if (i < 2)
                    {
                        AddWPInDir(-1, 0, 0);
                        AddWPInDir(-1, 0, 0);
                        AddWPInDir(-1, 0, i);
                    }
                }
                Position& p = waypoints[waypoints.size() - 1];
                me->SummonGameObject(BOX_ENTRY, p, G3D::Quat(0.0f, 0.0f, 0.0f, 0.0f), 0);

            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_boxesAI(creature);
        }
    };

    class event_zone_florist : public CreatureScript
    {
        enum Gossips
        {
            GOSSIP_ACTION_REQUEST_BOUQUET = 100,
            GOSSIP_ACTION_AGREE_WITH_HELP,
            GOSSIP_ACTION_HURRY,
        };

    public:
        event_zone_florist() : CreatureScript("event_zone_florist") { }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET) != QUEST_STATUS_INCOMPLETE)
                return false;

            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_RIBBON))
                return false;

            std::string str = player->getGender() == GENDER_FEMALE ? "Potrebovala bych uvazat kytici na svatbu, bylo by to mozne?" :
                                                                     "Potreboval bych uvazat kytici na svatbu, bylo by to mozne?";
            AddGossipItemFor(player, GOSSIP_ICON_CHAT, str, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_REQUEST_BOUQUET);
            SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_FLORIST_HELLO, me);
            return true;
        }

        bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET) != QUEST_STATUS_INCOMPLETE)
                return false;

            if (quest->GetQuestId() != EVENT_ZONE_QUEST_WEDDING_RIBBON)
                return false;

            me->SaySingleTarget("Tady je tva kytice! Dekuji ti za tvoji pomoc!", LANG_UNIVERSAL, player);
            player->KilledMonsterCredit(me->GetEntry());
            return false;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET) != QUEST_STATUS_INCOMPLETE)
                return false;

            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_RIBBON))
                return false;

            ClearGossipMenuFor(player);
            switch(action)
            {
                case GOSSIP_ACTION_REQUEST_BOUQUET:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Dobra, pomohu ti.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_AGREE_WITH_HELP);
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Na to nemam cas! Potrebuji tu kytici co nejrychleji!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_HURRY);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_FLORIST_BEING_REQUESTED, me);
                    break;
                case GOSSIP_ACTION_AGREE_WITH_HELP:
                    player->AddQuest(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_WEDDING_RIBBON), me);
                    me->SaySingleTarget("Děkuji Ti za tvojí ochotu! Najdi ji prosím co nejdřív!", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    break;
                case GOSSIP_ACTION_HURRY:
                    me->SaySingleTarget("Pokud ty nemáš čas na mě, tak já nemám čas na tebe. Je to prosté. Přijď, až mi budeš ochotný pomoct a já pak na oplátku pomohu tobě.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    break;
            }
            return true;
        }
    };

    class event_zone_tailor : public CreatureScript
    {
        enum Gossips
        {
            GOSSIP_ACTION_REQUEST_DRESS   = 100,
            GOSSIP_ACTION_WHERE_TO_FIND,
            GOSSIP_ACTION_HURRY,
            GOSSIP_ACTION_OK,
            GOSSIP_ACTION_ASK_FOR_DRESS,
            GOSSIP_ACTION_REFUSE,
        };

    public:
        event_zone_tailor() : CreatureScript("event_zone_tailor") { }

        const std::map<uint32, uint32> shirtsToLove =
        {
            { EVENT_ZONE_ITEM_MORPH_TAUREN_M,   EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_M     },
            { EVENT_ZONE_ITEM_MORPH_TAUREN_F,   EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_F     },
            { EVENT_ZONE_ITEM_MORPH_HUMAN_M,    EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_M      },
            { EVENT_ZONE_ITEM_MORPH_HUMAN_F,    EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_F      },
            { EVENT_ZONE_ITEM_MORPH_GNOME_F,    EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_F      },
            { EVENT_ZONE_ITEM_MORPH_GNOME_M,    EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_M      },
            { EVENT_ZONE_ITEM_MORPH_BELF_F,     EVENT_ZONE_ITEM_LOVE_MORPH_BELF_F       },
            { EVENT_ZONE_ITEM_MORPH_BELF_M,     EVENT_ZONE_ITEM_LOVE_MORPH_BELF_M       },
            { EVENT_ZONE_ITEM_MORPH_GOBLIN_M,   EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_M     },
            { EVENT_ZONE_ITEM_MORPH_GOBLIN_F,   EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_F     },
            { EVENT_ZONE_ITEM_MORPH_DRAENEI_F,  EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_F    },
            { EVENT_ZONE_ITEM_MORPH_TROLL_M,    EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_M      },
            { EVENT_ZONE_ITEM_MORPH_DWARF_M,    EVENT_ZONE_ITEM_LOVE_MORPH_DWARF_M      },
            { EVENT_ZONE_ITEM_MORPH_ORC_F,      EVENT_ZONE_ITEM_LOVE_MORPH_ORC_F        },
            { EVENT_ZONE_ITEM_MORPH_FEL_ORC,    EVENT_ZONE_ITEM_LOVE_MORPH_FEL_ORC      },
            { EVENT_ZONE_ITEM_MORPH_NELF_M,     EVENT_ZONE_ITEM_LOVE_MORPH_NELF_M       },
            { EVENT_ZONE_ITEM_MORPH_NELF_F,     EVENT_ZONE_ITEM_LOVE_MORPH_NELF_F       },
            { EVENT_ZONE_ITEM_MORPH_UNDEAD_F,   EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_F     },
            { EVENT_ZONE_ITEM_MORPH_UNDEAD_M,   EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_M     },
            { EVENT_ZONE_ITEM_MORPH_DWARF_F,    EVENT_ZONE_ITEM_LOVE_ORPH_DWARF_F       },
            { EVENT_ZONE_ITEM_MORPH_TROLL_F,    EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_F      },
            { EVENT_ZONE_ITEM_MORPH_DRAENEI_M,  EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_M    },
            { EVENT_ZONE_ITEM_MORPH_ORC_M,      EVENT_ZONE_ITEM_LOVE_MORPH_ORC_M        }
        };

        const std::map<uint32, uint32> loveToShirts =
        {
            { EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_M,   EVENT_ZONE_ITEM_MORPH_TAUREN_M     },
            { EVENT_ZONE_ITEM_LOVE_MORPH_TAUREN_F,   EVENT_ZONE_ITEM_MORPH_TAUREN_F     },
            { EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_M,    EVENT_ZONE_ITEM_MORPH_HUMAN_M      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_HUMAN_F,    EVENT_ZONE_ITEM_MORPH_HUMAN_F      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_F,    EVENT_ZONE_ITEM_MORPH_GNOME_F      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_GNOME_M,    EVENT_ZONE_ITEM_MORPH_GNOME_M      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_BELF_F,     EVENT_ZONE_ITEM_MORPH_BELF_F       },
            { EVENT_ZONE_ITEM_LOVE_MORPH_BELF_M,     EVENT_ZONE_ITEM_MORPH_BELF_M       },
            { EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_M,   EVENT_ZONE_ITEM_MORPH_GOBLIN_M     },
            { EVENT_ZONE_ITEM_LOVE_MORPH_GOBLIN_F,   EVENT_ZONE_ITEM_MORPH_GOBLIN_F     },
            { EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_F,  EVENT_ZONE_ITEM_MORPH_DRAENEI_F    },
            { EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_M,    EVENT_ZONE_ITEM_MORPH_TROLL_M      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_DWARF_M,    EVENT_ZONE_ITEM_MORPH_DWARF_M      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_ORC_F,      EVENT_ZONE_ITEM_MORPH_ORC_F        },
            { EVENT_ZONE_ITEM_LOVE_MORPH_FEL_ORC,    EVENT_ZONE_ITEM_MORPH_FEL_ORC      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_NELF_M,     EVENT_ZONE_ITEM_MORPH_NELF_M       },
            { EVENT_ZONE_ITEM_LOVE_MORPH_NELF_F,     EVENT_ZONE_ITEM_MORPH_NELF_F       },
            { EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_F,   EVENT_ZONE_ITEM_MORPH_UNDEAD_F     },
            { EVENT_ZONE_ITEM_LOVE_MORPH_UNDEAD_M,   EVENT_ZONE_ITEM_MORPH_UNDEAD_M     },
            { EVENT_ZONE_ITEM_LOVE_ORPH_DWARF_F,     EVENT_ZONE_ITEM_MORPH_DWARF_F      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_TROLL_F,    EVENT_ZONE_ITEM_MORPH_TROLL_F      },
            { EVENT_ZONE_ITEM_LOVE_MORPH_DRAENEI_M,  EVENT_ZONE_ITEM_MORPH_DRAENEI_M    },
            { EVENT_ZONE_ITEM_LOVE_MORPH_ORC_M,      EVENT_ZONE_ITEM_MORPH_ORC_M        }
        };


        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_DRESS) != QUEST_STATUS_INCOMPLETE)
            {
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING) == QUEST_STATUS_REWARDED)
                {
                    if (player->HasItemCount(EVENT_ZONE_ITEM_LOVE_SHIRT))
                    {
                        if (Item* shirt = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY))
                        {
                            auto itr = shirtsToLove.find(shirt->GetEntry());
                            if (itr != shirtsToLove.end())
                            {
                                std::string shirtName = sObjectMgr->GetItemTemplate(shirt->GetEntry())->Name1;
                                std::string text = "Vymenit Married Shirt a " + shirtName + " za Love " + shirtName + " morph. STOJI 10 EVENT CLOTH";
                                AddGossipItemFor(player, GOSSIP_ICON_MONEY_BAG, text, GOSSIP_SENDER_MAIN, shirt->GetEntry(), text, 0, false);
                                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Odmítnout.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_REFUSE);
                            }
                        }
                    }
                    else if (Item* shirt = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY))
                    {
                        auto itr = loveToShirts.find(shirt->GetEntry());
                        if (itr != loveToShirts.end())
                        {
                            std::string shirtName = sObjectMgr->GetItemTemplate(itr->second)->Name1;
                            std::string text = "Vymenit Love " + shirtName + " morph za Married Shirt a " + shirtName + ". STOJI 5 EVENT CLOTH";
                            AddGossipItemFor(player, GOSSIP_ICON_MONEY_BAG, text, GOSSIP_SENDER_MAIN, shirt->GetEntry(), text, 0, false);
                            AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Odmítnout.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_REFUSE);
                        }
                    }
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_CHANGE, me);
                    return true;
                }
                return false;
            }
            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_DRESS_CLOTH);

            switch(status)
            {
                case QUEST_STATUS_NONE:
                {
                    std::string str = player->getGender() == GENDER_FEMALE ? "Potrebovala bych usit svatebni saty a spolecensky oblek, bylo by to mozne?" :
                                                                             "Potreboval bych usit svatebni saty a spolecensky oblek, bylo by to mozne?";
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, str, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_REQUEST_DRESS);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_HELLO, me);
                    break;
                }
                case QUEST_STATUS_INCOMPLETE:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "A kde ty latky sezenu?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_WHERE_TO_FIND);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_BEING_REQUESTED, me);
                    break;
                case QUEST_STATUS_REWARDED:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Uz jsou me saty hotove?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_ASK_FOR_DRESS);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_HELLO, me);
                    break;
                default:
                    return false;
                    break;
            }
            return true;
        }

        bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
        {
            if (quest->GetQuestId() == EVENT_ZONE_QUEST_WEDDING_DRESS_CLOTH)
            {
                if (Aura* aur = player->AddAura(EVENT_ZONE_AURA_TAILOR_DRESS_TIMER, player))
                {
                    aur->SetMaxDuration(DAY * IN_MILLISECONDS);
                    aur->SetDuration(aur->GetMaxDuration());
                }
                me->SaySingleTarget("Chvili bude trvat, nez to usiju, vrat se za nejaky cas.", LANG_UNIVERSAL, player);
            }
            return false;
        }


        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_DRESS) != QUEST_STATUS_INCOMPLETE)
                if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING) != QUEST_STATUS_REWARDED)
                    return false;

            QuestStatus status = player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_DRESS_CLOTH);

            ClearGossipMenuFor(player);
            switch(action)
            {
                case GOSSIP_ACTION_REQUEST_DRESS:
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Na to nemam cas, nemohl bys mi to prosim proste usit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_HURRY);
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "A kde ty latky sezenu?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_WHERE_TO_FIND);
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Dobre, vratim se jen co je sezenu!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_OK);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_BEING_REQUESTED, me);
                    break;
                case GOSSIP_ACTION_WHERE_TO_FIND:
                    if (!status || status == QUEST_STATUS_INCOMPLETE)
                    {
                        AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Na to nemam cas, nemohl bys mi to prosim proste usit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_HURRY);
                        AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Dobre, vratim se jen co je sezenu!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_OK);
                    }
                    player->PlayerTalkClass->SendPointOfInterest(EVENT_ZONE_POI_CLOTH_TRADER);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_TAILOR_WAYPOINT, me);
                    break;
                case GOSSIP_ACTION_OK:
                    player->AddQuestAndCheckCompletion(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_WEDDING_DRESS_CLOTH), me);
                    me->SaySingleTarget("Budu čekat.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    break;
                case GOSSIP_ACTION_HURRY:
                    me->SaySingleTarget("To bych bohužel nemohl. Promiň, ale nemám potřebné látky.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    break;
                case GOSSIP_ACTION_ASK_FOR_DRESS:
                    if (player->HasAura(EVENT_ZONE_AURA_TAILOR_DRESS_TIMER))
                    {
                        me->SaySingleTarget("Ne tak rychle! Ještě to nemám dokončené, přijď jindy!", LANG_UNIVERSAL, player);
                        CloseGossipMenuFor(player);
                        break;
                    }
                    me->SaySingleTarget("Poslal jsem svoji sestru, saty i oblek by mely byt uz na ceste, ja je u sebe uz nemam.", LANG_UNIVERSAL, player);
                    me->Whisper("Vrat se zpet za maminkou!", LANG_UNIVERSAL, player, true);
                    if (!player->IsActiveQuest(EVENT_ZONE_QUEST_WEDDING_DRESS))
                        player->AddQuest(sObjectMgr->GetQuestTemplate(EVENT_ZONE_QUEST_WEDDING_DRESS), me);
                    player->KilledMonsterCredit(me->GetEntry());
                    CloseGossipMenuFor(player);
                    break;
                case GOSSIP_ACTION_REFUSE:
                    CloseGossipMenuFor(player);
                    break;
                default:
                {
                    if (Item* shirt = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY))
                    {
                        auto itr = loveToShirts.find(shirt->GetEntry());
                        if (itr != loveToShirts.end())
                        {
                            if (!player->HasItemCount(EVENT_ZONE_ITEM_EVENT_CLOTH, 5))
                            {
                                me->SaySingleTarget("Bude to stát 5x Event Cloth. Dojdi, až jej budeš mít.", LANG_UNIVERSAL, player);
                            }
                            else
                            {
                                ItemPosCountVec dest;
                                if (player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, EVENT_ZONE_ITEM_WEDDING_DRESS, 2) == EQUIP_ERR_OK)
                                {
                                    player->DestroyItemCount(EVENT_ZONE_ITEM_EVENT_CLOTH, 5, true);
                                    player->DestroyItemCount(shirt->GetEntry(), 1, true, true);
                                    player->AddItem(EVENT_ZONE_ITEM_LOVE_SHIRT, 1, true);
                                    player->AddItem(itr->second, 1, true);
                                    me->SaySingleTarget("Zde jsou tvé původní trička.", LANG_UNIVERSAL, player);
                                }
                                else
                                    me->SaySingleTarget("Nemáš dostatek místa v batohu, prosím, zbav se nějakých zbytečností.", LANG_UNIVERSAL, player);
                            }
                        }
                        else
                        {
                            auto itr = shirtsToLove.find(shirt->GetEntry());
                            if (itr != shirtsToLove.end())
                            {
                                if (!player->HasItemCount(EVENT_ZONE_ITEM_EVENT_CLOTH, 10))
                                {
                                    me->SaySingleTarget("Bude to stát 10x Event Cloth. Dojdi, až jej budeš mít.", LANG_UNIVERSAL, player);
                                }
                                // no need to check for slot count, because we are destroying one shirt that must be in bag
                                // so it is 100% that there is empty slot
                                else if (player->HasItemCount(EVENT_ZONE_ITEM_LOVE_SHIRT))
                                {
                                    player->DestroyItemCount(EVENT_ZONE_ITEM_EVENT_CLOTH, 10, true);
                                    player->DestroyItemCount(shirt->GetEntry(), 1, true, true);
                                    player->DestroyItemCount(EVENT_ZONE_ITEM_LOVE_SHIRT, 1, true);
                                    player->AddItem(itr->second, 1, true);
                                    me->SaySingleTarget("Tady je tvé nové tričko. Ať ti slouží!", LANG_UNIVERSAL, player);
                                }
                                else
                                {
                                    me->SaySingleTarget("Nemáš Married Shirt!", LANG_UNIVERSAL, player);
                                }
                            }
                            else
                                me->SaySingleTarget("Musíš mít nasazené triko na morph, které chceš vyměnit!", LANG_UNIVERSAL, player);
                        }
                    }
                    else
                        me->SaySingleTarget("Musíš mít nasazené triko na morph, které chceš vyměnit!", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                }
            }
            return true;
        }
    };

    class ParentScript : CreatureScript
    {
    public:
        ParentScript(const char* aiName) : CreatureScript(aiName) { }
        struct parentAI : ScriptedAI
        {
            parentAI(Creature* creature) : ScriptedAI(creature) { }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* plrSeer = seer->ToPlayer())
                    return !plrSeer->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING);
                return true;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new parentAI(creature);
        }
    };

    class event_zone_mum : public ParentScript
    {
    public:
        event_zone_mum() : ParentScript("event_zone_mum") { }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET) == QUEST_STATUS_REWARDED)
                player->KilledMonsterCredit(me->GetEntry());
            return false;
        }

        bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
        {
            if (quest->GetQuestId() != EVENT_ZONE_QUEST_WEDDING_FLOWER_BOUQUET)
                return false;

            player->KilledMonsterCredit(me->GetEntry());
            me->Whisper("Ziskal si pozehnani od matky!", LANG_UNIVERSAL, player, true);
            me->SaySingleTarget("Úzasné! To bude dokonalá svatba! Nemohu se už dočkat!", LANG_UNIVERSAL, player);
            return false;
        }
    };

    class event_zone_lost_mare : public CreatureScript
    {
    public:
        event_zone_lost_mare() : CreatureScript("event_zone_lost_mare") { }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->IsActiveQuest(EVENT_ZONE_QUEST_LOST_MARE))
            {
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Poslat ztracenou kobylu domu.", GOSSIP_SENDER_MAIN, 100);
                SendGossipMenuFor(player, 1, me);
                return true;
            }
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32) override
        {
            me->Whisper("Kobyla odesla domu, vrat se za tatinkem!", LANG_UNIVERSAL, player, true);
            player->KilledMonsterCredit(me->GetEntry());
            me->DespawnOrUnsummon();
            return true;
        }
    };

    class event_zone_labyrint_spawner : public CreatureScript
    {
    public:
        event_zone_labyrint_spawner() : CreatureScript("event_zone_labyrint_spawner") { }

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new labyrint_base_classAI(creature, 17, EVENT_ZONE_CREATURE_EVIL_DWARF_SPAWNER, true, false, 3);
        }
    };

    class event_zone_evil_dwarf : public CreatureScript
    {
    public:
        event_zone_evil_dwarf() : CreatureScript("event_zone_evil_dwarf") { }

        struct event_zone_evil_dwarfAI : CreatureAI
        {
            event_zone_evil_dwarfAI(Creature* creature) : CreatureAI(creature) { }

            ObjectGuid ownerGuid;

            ObjectGuid GetGUID(int32) const override
            {
                return ownerGuid;
            }

            void Reset() override
            {
                me->m_PlayerDamageReq = me->GetMaxHealth() * 2;
            }

            void JustDied(Unit* killer) override
            {
                if (Player* player = killer->GetCharmerOrOwnerPlayerOrPlayerItself())
                    player->KilledMonsterCredit(me->GetEntry());
            }

            void SetGUID(ObjectGuid guid, int32) override
            {
                ownerGuid = guid;
            }

            bool CanAIAttack(const Unit* who) const override
            {
                if (Player* player = who->GetCharmerOrOwnerPlayerOrPlayerItself())
                    return player->GetGUID() == ownerGuid;
                return false;
            }

            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (!seer->isType(TYPEMASK_UNIT))
                    return false;

                if (const Player* plr = seer->ToUnit()->ToPlayer())
                    if (plr->IsGameMaster())
                        return false;

                if (Player* player = seer->ToUnit()->GetCharmerOrOwnerPlayerOrPlayerItself())
                {
                    return player->GetGUID() != ownerGuid;
                }

                return false;
            }
            void UpdateAI(uint32) override
            {
                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_evil_dwarfAI(creature);
        }
    };

    class event_zone_evil_dwarf_spawner : public CreatureScript
    {
    public:
        event_zone_evil_dwarf_spawner() : CreatureScript("event_zone_evil_dwarf_spawner") { }

        struct event_zone_evil_dwarf_spawnerAI : NullCreatureAI
        {
            event_zone_evil_dwarf_spawnerAI(Creature* creature) : NullCreatureAI(creature)
            {
                me->m_SightDistance = 1.0f;
            }

            class DwarfOwnedBy
            {
            public:
                DwarfOwnedBy(ObjectGuid const owner, Unit const* searcher, float range) : i_owner(owner), i_searcher(searcher), i_range(range) { }
                bool operator()(Unit* u)
                {
                    if (u->GetEntry() != EVENT_ZONE_CREATURE_EVIL_DWARF || u->GetAI()->GetGUID() != i_owner || !i_searcher->IsWithinDistInMap(u, i_range))
                        return false;
                    return true;
                }
            private:
                ObjectGuid const i_owner;
                Unit const* i_searcher;
                float i_range;
            };


            void TrySpawnDwarf(Player* plrTrigger)
            {
                if (!plrTrigger)
                    return;

                std::list<Creature*> dwarves;
                DwarfOwnedBy checker(plrTrigger->GetGUID(), me, 50.0f);
                Trinity::CreatureListSearcher<DwarfOwnedBy> searcher(me, dwarves, checker);
                me->VisitNearbyObject(50.0f, searcher);
                if (!dwarves.empty())
                    return;

                if (TempSummon* dwarf = me->SummonCreature(EVENT_ZONE_CREATURE_EVIL_DWARF, *plrTrigger, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, 2 * MINUTE * IN_MILLISECONDS))
                {
                    uint8 lvl = plrTrigger->getLevel();
                    dwarf->SetLevel(lvl);
                    uint32 hp = static_cast<uint32>(lvl < 20 ? (lvl * 500) : (lvl * 500 * float(lvl) / 20.0f));
                    float minDamage = float(hp) / 420.0f;
                    minDamage *= 1.0f + (float(lvl) / 80.0f);
                    float maxDamage = minDamage * 1.4f;
                    dwarf->SetMaxHealth(hp);
                    dwarf->SetHealth(hp);
                    dwarf->ResetPlayerDamageReq();
                    dwarf->SetStatFloatValue(UNIT_FIELD_MINDAMAGE, minDamage);
                    dwarf->SetStatFloatValue(UNIT_FIELD_MAXDAMAGE, maxDamage);
                    dwarf->GetAI()->SetGUID(plrTrigger->GetGUID());
                    dwarf->SaySingleTarget("Nikdo mě nebude okrádat!", LANG_UNIVERSAL, plrTrigger);
                    dwarf->UpdateObjectVisibility(true);
                }
            }

            void MoveInLineOfSight(Unit* who) override
            {
                if (me->GetDistance(who) > 1.0f)
                    return;

                // do not allow spawning from floor bellow
                if (!me->IsWithinLOSInMap(who))
                    return;

                TrySpawnDwarf(who->ToPlayer());
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_evil_dwarf_spawnerAI(creature);
        }
    };

    class event_zone_dad : public ParentScript
    {
    public:
        event_zone_dad() : ParentScript("event_zone_dad") { }

        bool OnGossipHello(Player* player, Creature* me) override
        {
            if (player->GetQuestStatus(EVENT_ZONE_QUEST_WEDDING_RINGS) == QUEST_STATUS_REWARDED)
                player->KilledMonsterCredit(me->GetEntry());
            return false;
        }

        bool OnQuestReward(Player* player, Creature* me, const Quest* quest, uint32) override
        {
            if (quest->GetQuestId() != EVENT_ZONE_QUEST_WEDDING_RINGS)
                return false;

            player->KilledMonsterCredit(me->GetEntry());
            me->Whisper("Ziskal si pozehnani od otce!", LANG_UNIVERSAL, player, true);
            me->SaySingleTarget("Doufám, že ta svatba bude stát za to!", LANG_UNIVERSAL, player);
            return false;
        }
    };

    class event_zone_lk : public CreatureScript
    {
    public:
        event_zone_lk() : CreatureScript("event_zone_lk") { }
        struct event_zone_lkAI : public ScriptedAI
        {
            TaskScheduler scheduler;
            uint32 mountId;
            event_zone_lkAI(Creature* creature) : ScriptedAI(creature)
            {
                mountId = 0;
                scheduler.Schedule(Seconds(1), [this](TaskContext task)
                {
                    if (Unit* owner = me->GetOwner())
                    {
                        if (owner->IsMounted())
                        {
                            uint32 newMountId;
                            if (owner->CanFly())
                            {
                                newMountId = EVENT_ZONE_SPELL_INVINCIBLE_FLYING;
                                me->SetCanFly(true);
                            }
                            else
                            {
                                newMountId = EVENT_ZONE_SPELL_INVINCIBLE_GROUND;
                                me->SetCanFly(false);
                            }
                            if (mountId != newMountId)
                            {
                                if (mountId)
                                    me->RemoveAura(mountId);
                                me->AddAura(newMountId, me);
                                mountId = newMountId;
                            }
                        }
                        else
                        {
                            me->SetCanFly(false);
                            me->RemoveAura(mountId);
                            mountId = 0;
                        }
                    }
                    task.Repeat(Seconds(1));
                });
            }
            void UpdateAI(uint32 diff) override
            {
                scheduler.Update(diff);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_lkAI(creature);
        }
    };
}

namespace GuildHouse
{
    class event_zone_inn_dwarf : CreatureScript
    {
    public:
        event_zone_inn_dwarf() : CreatureScript("event_zone_inn_dwarf") { }
        void AddMenuItem(Player* player, std::string text, uint32 action)
        {
            AddGossipItemFor(player, GOSSIP_ICON_CHAT, text, GOSSIP_SENDER_MAIN, action);
        }

        enum InnDwarfActions
        {
            INN_DWARF_ACTION_SEARCH_FOR_JOB = 10001,
            INN_DWARF_ACTION_NOBODY_CARES,
            INN_DWARF_ACTION_BUY_BEER,
            INN_DWARF_ACTION_I_COULD_HELP,
            INN_DWARF_ACTION_I_AM_STALKER,
            INN_DWARF_ACTION_I_AM_SPYING_YOU,
            INN_DWARF_ACTION_I_CANNOT_HELP_YOU_LOL,
            INN_DWARF_ACTION_YES_I_COULD_REALY_HELP,
            INN_DWARF_ACTION_TOO_MUCH_RESOURCES,
            INN_DWARF_ACTION_ASK_WHY_GUILD_KICK,
            INN_DWARF_ACTION_ASK_RESOURCE_TYPES,
            INN_DWARF_ACTION_REFUSE_BECAUSE_EVENT_MARKS,
            INN_DWARF_ACTION_MAYBE_LATER,
            INN_DWARF_ACTION_SHOW_ME_QUEST,
        };

        bool OnGossipHello(Player* player, Creature* me) override
        {
            AddMenuItem(player, "Ne, momentalne se jen porozhlizim po meste a hledam nejakou praci.", INN_DWARF_ACTION_SEARCH_FOR_JOB);
            AddMenuItem(player, "Nikoho nezajimaji tvoje problemy, nechapu proc mi to vubec rikas.", INN_DWARF_ACTION_NOBODY_CARES);
            AddMenuItem(player, "*Objednat trpaslikovi pivo - stoji 2 EM*", INN_DWARF_ACTION_BUY_BEER);
            AddMenuItem(player, "Mozna bych ti mohl pomoci...", INN_DWARF_ACTION_I_COULD_HELP);
            AddMenuItem(player, "Ehh, no... $GRad:Rada; odposlouchavam cizi lidi.", INN_DWARF_ACTION_I_AM_STALKER);
            AddMenuItem(player, "Ano, jsem tajny agent a moji misi je odposlouchavat lidi. A sakra, $Gprozradil:prozradila; jsem sve utajeni.", INN_DWARF_ACTION_I_AM_SPYING_YOU);
            SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_INTRO, me);
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* me, uint32, uint32 action) override
        {
            ClearGossipMenuFor(player);
            switch(action)
            {
                case INN_DWARF_ACTION_SEARCH_FOR_JOB:
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_GOOD_LUCK, me);
                    break;
                case INN_DWARF_ACTION_NOBODY_CARES:
                    // be gone event
                    break;
                case INN_DWARF_ACTION_BUY_BEER:
                    // drunk event
                    break;
                case INN_DWARF_ACTION_I_COULD_HELP:
                    AddMenuItem(player, "Mas pravdu, pomahat nejakemu trpaslikovi jako jsi ty fakt nema smysl. To si radsi necham vytrhat vsechny zuby u zubare!", INN_DWARF_ACTION_I_CANNOT_HELP_YOU_LOL);
                    AddMenuItem(player, "Ano, ja. Mohu ti pomoci s tvymi problemy. Pokud mi reknes, co bys potreboval.", INN_DWARF_ACTION_YES_I_COULD_REALY_HELP);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_YOU_CANNOT_HELP, me);
                    break;
                case INN_DWARF_ACTION_I_AM_STALKER:
                    break;
                case INN_DWARF_ACTION_I_CANNOT_HELP_YOU_LOL:
                    // be gone event
                    break;
                case INN_DWARF_ACTION_YES_I_COULD_REALY_HELP:
                    AddMenuItem(player, "Hodne surovin? Tak na to rovnou zapomen! To ti shanet nebudu ani za zlate prase! Kor pro nekoho jako jsi ty!", INN_DWARF_ACTION_TOO_MUCH_RESOURCES);
                    AddMenuItem(player, "Co se stalo, ze byli vyhozeni z guildy?", INN_DWARF_ACTION_ASK_WHY_GUILD_KICK);
                    AddMenuItem(player, "Jake suroviny potrebujes sehnat?", INN_DWARF_ACTION_ASK_RESOURCE_TYPES);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_TELL_WHAT_I_NEED, me);
                    break;
                case INN_DWARF_ACTION_TOO_MUCH_RESOURCES:
                    // be gone event
                    break;
                case INN_DWARF_ACTION_ASK_WHY_GUILD_KICK:
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_TELL_KICK_REASON, me);
                    break;
                case INN_DWARF_ACTION_ASK_RESOURCE_TYPES:
                    AddMenuItem(player, "EVENT MARKY? TO NEPRICHAZI V UVAHU! Nedam takovemu zebrakovi jako jsi ty moje event marky!", INN_DWARF_ACTION_REFUSE_BECAUSE_EVENT_MARKS);
                    AddMenuItem(player, "Tak s tim ti ted momentalne nepomohu. Mozna jindy.", INN_DWARF_ACTION_MAYBE_LATER);
                    AddMenuItem(player, "To zni jednoduse, kde mohu zacit? Se vsim ti pomohu.", INN_DWARF_ACTION_SHOW_ME_QUEST);
                    SendGossipMenuFor(player, EVENT_ZONE_GOSSIP_INN_DWARF_TELL_REQUIREMENT, me);
                    break;
                case INN_DWARF_ACTION_REFUSE_BECAUSE_EVENT_MARKS:
                    // be gone event
                    break;
                case INN_DWARF_ACTION_MAYBE_LATER:
                    CloseGossipMenuFor(player);
                    break;
                case INN_DWARF_ACTION_SHOW_ME_QUEST:
                    // give quest
                    break;
            }

            return true;
        }

        struct event_zone_inn_dwarfAI : NullCreatureAI
        {
            event_zone_inn_dwarfAI(Creature* creature) : NullCreatureAI(creature) { }
            bool IsNeverVisibleFor(const WorldObject* seer) override
            {
                if (const Player* pseer = seer->ToPlayer())
                    return !pseer->GetGuild() || pseer->GetGuild()->GetLevel() < 10;
                return true;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_inn_dwarfAI(creature);
        }
    };

    class event_zone_beggar : CreatureScript
    {
    public:
        event_zone_beggar() : CreatureScript("event_zone_beggar") { }
        struct event_zone_beggarAI : CreatureAI
        {
            TaskScheduler scheduler;
            std::unordered_set<ObjectGuid> beggedPlayers;
            std::unordered_set<ObjectGuid> ignoredPlayers;
            event_zone_beggarAI(Creature* creature) : CreatureAI(creature)
            {
                me->m_SightDistance = 5.0f;
            }

            void Reset() override
            {
                scheduler.CancelAll();
                beggedPlayers.clear();
                ignoredPlayers.clear();
            }

            void sGossipHello(Player* player) override
            {
                ObjectGuid guid = player->GetGUID();
                if (ignoredPlayers.find(guid) != ignoredPlayers.end())
                {
                    me->Whisper(me->GetName() + " se odmita s tebou bavit!", LANG_UNIVERSAL, player, true);
                    return CloseGossipMenuFor(player);
                }

                if (!player->HasItemCount(Deffender::EVENT_MARK))
                    return;

                AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Venovat 1x Event Mark.", GOSSIP_SENDER_MAIN, 100);
                SendGossipMenuFor(player, 1, me);
            }

            void SendEmoteTo(Player* player, uint32 emoteId)
            {
                WorldPacket data(SMSG_EMOTE, 4 + 8);
                data << uint32(emoteId);
                data << uint64(me->GetGUID());
                player->SendDirectMessage(&data);
            }

            void sGossipSelect(Player* player, uint32, uint32) override
            {
                ObjectGuid guid = player->GetGUID();
                ignoredPlayers.insert(guid);
                scheduler.Schedule(Minutes(30), [this, guid](TaskContext)
                {
                    ignoredPlayers.erase(guid);
                });

                player->DestroyItemCount(Deffender::EVENT_MARK, 1, true);
                scheduler.Schedule(Seconds(1), [this, guid](TaskContext)
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, guid))
                        SendEmoteTo(player, 4);
                });
                CloseGossipMenuFor(player);
            }

            void MoveInLineOfSight(Unit* who) override
            {
                if (who->GetTypeId() != TYPEID_PLAYER)
                    return;

                ObjectGuid guid = who->GetGUID();

                if (beggedPlayers.find(guid) != beggedPlayers.end())
                    return;

                if (ignoredPlayers.find(guid) != ignoredPlayers.end())
                    return;

                beggedPlayers.insert(guid);
                scheduler.Schedule(Minutes(30), [this, guid](TaskContext)
                {
                    beggedPlayers.erase(guid);
                });

                Player* player = who->ToPlayer();
                me->SaySingleTarget("Prosim, dej mi jednu event marku! Dam ti za to darek!", LANG_UNIVERSAL, player);
                SendEmoteTo(player, 20);
            }

            void UpdateAI(uint32 diff) override
            {
                scheduler.Update(diff);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new event_zone_beggarAI(creature);
        }
    };
}

void AddSC_event_zone()
{
    new event_zone_old_priest();
    new event_zone_portal();
    new event_zone_villager();
    new event_zone_cathedral_guardian_dummy();
    new event_zone_inviter();
    new Investigation::EvilsLair::event_zone_witch_vision_orb();
    new Investigation::EvilsLair::event_zone_evil_leader();
    new Investigation::EvilsLair::event_zone_evil_one();
    new Investigation::EvilsLair::event_zone_prophet();
    new Investigation::PumpkinFarm::event_zone_pumpkin_farm_pumpkin();
    new Investigation::PumpkinFarm::event_zone_pumpkin_farm_rabbit();
    new Investigation::TurkeyFarm::event_zone_turkey_farm_farmer();
    new Investigation::TurkeyFarm::event_zone_turkey_farm_turkey();
    new Investigation::Witch::event_zone_witch();
    new Investigation::Witch::event_zone_witch_dog();
    new Investigation::Witch::event_zone_lost_dog();
    new Investigation::Witch::event_zone_lost_moving_dog();
    new Investigation::Tavern::event_zone_innkeeper();
    new Investigation::Tavern::event_zone_inn_guest_spawner();
    new Investigation::Tavern::event_zone_inn_guest();
    new Investigation::Tavern::event_zone_inn_food();
    new Investigation::Blacksmith::event_zone_blacksmith();
    new Investigation::Blacksmith::event_zone_blacksmith_mine_car();
    new Investigation::Blacksmith::event_zone_blacksmith_mine_car_spawner();
    new Investigation::FishingPool::event_zone_fisherman();
    new Investigation::FishingPool::event_zone_fish();
    new Investigation::AppleFarm::event_zone_apple_farm_farmer();
    new Investigation::AppleFarm::event_zone_apple_farm_worm_spawner();
    new Investigation::AppleFarm::event_zone_apple_farm_worm();
    new Investigation::Doctor::event_zone_investigating_doctor();
    new Investigation::Doctor::event_zone_doctor();
    new CryptEvent::event_zone_general();
    new CryptEvent::event_zone_general_soldier();
    new CryptEvent::event_zone_crypt_farmer();
    new CryptEvent::event_zone_crypt_innkeeper();
    new CryptEvent::event_zone_crypt_fisherman();
    new CryptEvent::event_zone_crypt_blacksmith();
    new CryptEvent::event_zone_crypt_event_dummy();
    new CryptEvent::event_zone_crypt_event_portal_opener();
    new CryptEvent::event_zone_crypt_event_portal();
    new CryptEvent::CryptSoldier::event_zone_general_crypt();
    new CryptEvent::CryptSoldier::event_zone_general_crypt_soldier();
    new Execution::event_zone_executioner();
    new Execution::event_zone_prisoner_visibility();
    new EventZoneQuestions::event_zone_library_questions();
    new Revolt::event_zone_revolt_healer();
    new Revolt::event_zone_revolt_leader();
    new Revolt::event_zone_revolt_soldier();
    new Revolt::event_zone_revolt_weak_soldier();
    new Revolt::event_zone_revolt_strong_soldier();
    new Revolt::event_zone_revolt_controllable_soldier();
    new Revolt::event_zone_revolt_brigadier_general();
    new Daily::event_zone_daily_billboard();
    new Daily::Tavern::event_zone_innkeeper_daily();
    new Daily::Lumberjacks::spell_event_zone_chop_wood();
    new Daily::Lumberjacks::event_zone_choppable_tree();
    new Daily::Lumberjacks::event_zone_wood_collector();
    new Daily::Lumberjacks::event_zone_wood_collection_quest_giver();
    new Daily::EvilsLair::event_zone_necromancers_zombie();
    new Daily::EvilsLair::event_zone_necromancer_boss();
    new Daily::EvilsLair::event_zone_mounted_brigadier_general();
    new Wedding::event_zone_cloth_trader();
    new Wedding::event_zone_boxes();
    new Wedding::event_zone_florist();
    new Wedding::event_zone_tailor();
    new Wedding::event_zone_mum();
    new Wedding::event_zone_lost_mare();
    new Wedding::event_zone_labyrint_spawner();
    new Wedding::event_zone_evil_dwarf();
    new Wedding::event_zone_evil_dwarf_spawner();
    new Wedding::event_zone_dad();
    new Wedding::event_zone_lk();
    new GuildHouse::event_zone_inn_dwarf();
    new GuildHouse::event_zone_beggar();
}

#include "ScriptMgr.h"
#include "Player.h"
#include "DatabaseEnv.h"
#include "AchievementMgr.h"
#include "WorldSession.h"
#include "ScriptedGossip.h"
#include "ScriptedCreature.h"

class SeasonalRewards : public PlayerScript
{
public:
    SeasonalRewards() : PlayerScript("SeasonalRewards") { }

    void OnLogin(Player* player, bool) override
    {
        if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(2398))
            player->CompletedAchievement(achievementEntry);
    }

    void OnLogout(Player* player) override
    {
        if (player->GetTotalPlayedTime() > WEEK)
            CharacterDatabase.PExecute("INSERT IGNORE INTO character_seasonal_rewards (guid, rewarded) VALUES(%u, %u)", player->GetGUID().GetCounter(), 0);
    }
};

class npc_seasonal_rewards_handler : public CreatureScript
{
public:
    npc_seasonal_rewards_handler() : CreatureScript("npc_seasonal_rewards_handler") { }

    const std::string subject = "Deffender.eu - 8. Vyroci";
    const std::string text = R"(Dekujeme Ti za podporu serveru.
Za tvoji vernost Ti posilame zaslouzenou odmenu.
S pozdravem
    Deffender.eu Team
)";

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (player->GetSession()->GetSecurity() < SEC_HEAD_EVENTMASTER)
            return true;

        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.ClearMenu();
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "send rewards", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF, "", 0);
        ptc->SendGossipMenu(1, me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player*, Creature* me, uint32, uint32) override
    {
        QueryResult result = CharacterDatabase.Query("SELECT guid FROM character_seasonal_rewards where rewarded = 0");
        if (!result)
            return true;

        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        do
        {
            uint32 guid = result->Fetch()[0].GetUInt32();
                                         // event mark                               // perpetual firework      // banana                    // tallstrider
            std::vector<Item*> items = { Item::CreateItem(Deffender::EVENT_MARK, 50),Item::CreateItem(23714, 1),Item::CreateItem(4537, 200), Item::CreateItem(600072, 1)};
            MailDraft draft(subject, text);
            for (auto item : items)
            {
                draft.AddItem(item);
                item->SaveToDB(trans);
            }
            draft.SendMailTo(trans, MailReceiver(guid), MailSender(MAIL_CREATURE, me->GetEntry()));
            std::string query = "UPDATE character_seasonal_rewards SET rewarded = 1 WHERE guid = " + std::to_string(guid);
            CharacterDatabase.ExecuteOrAppend(trans, query.c_str());

        } while (result->NextRow());
        CharacterDatabase.CommitTransaction(trans);
        return true;
    }
};

void AddSC_seasonal_rewards()
{
    // new SeasonalRewards();
    new npc_seasonal_rewards_handler();
}

#include "teleports_base_class.h"
#include "ScriptedGossip.h"

class event_365_find_port_handler : public CreatureScript
{
public:
    event_365_find_port_handler() : CreatureScript("event_365_find_port_handler") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new find_port_handlerAI(creature, 15);
    }
};

class event_365_helper : public CreatureScript
{
public:
    event_365_helper() : CreatureScript("event_365_helper") { }
    bool OnGossipHello(Player* player, Creature* me) override
    {
        me->Whisper("Vítej na eventu Teleporty. Klikej na vlky a sleduj, který Tě portuje nahoru, a který naopak maří tvé snahy dosáhnout vysněné teleportace. Až se dostaneš nahoru, sedni si na židličku v pořadí v jakém jsi přišel. Hodně štěstí!", LANG_UNIVERSAL, player);
        return true;
    }
};

void AddSC_event_365()
{
    new event_365_find_port_handler();
    new event_365_helper();
}
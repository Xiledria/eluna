#include "ScriptMgr.h"
#include "SpellScript.h"
#include "Auras/SpellAuraEffects.h"
#include "Player.h"
#include "ReputationMgr.h"

class spell_morph_shirt_aura : public SpellScriptLoader
{
public:
    spell_morph_shirt_aura() :  SpellScriptLoader("spell_morph_shirt_aura") { }

    class spell_morph_shirt_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_morph_shirt_aura_AuraScript);

        int32 mapId;
        bool applied = false;
        bool handled = false;
        Position lastPos;

        bool HandleMorph()
        {
            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr)
                return false;

            if (plr->IsLoading())
                return false;

            uint8 race = GetEffect(EFFECT_0)->GetMiscValue();
            uint8 gender = GetEffect(EFFECT_0)->GetMiscValueB();
            plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID, 0);
            plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_FACE_ID, 0);
            plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_STYLE_ID, 0);
            plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_COLOR_ID, 0);
            plr->SetByteValue(PLAYER_BYTES_2, PLAYER_BYTES_2_OFFSET_FACIAL_STYLE, 0);
            plr->SetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER, gender);
            plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_RACE, race);
            plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_GENDER, gender);
            uint8 chrClass = plr->GetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_CLASS);
            plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_CLASS, race == RACE_BLOODELF ? CLASS_PALADIN : CLASS_WARRIOR);
            plr->InitDisplayIds();
            plr->SetSkill(SKILL_LANG_COMMON, 0, 0, 0);
            plr->SetSkill(SKILL_LANG_ORCISH, 0, 0, 0);
            plr->SetSkill(SKILL_LANG_COMMON, 300, 300, 300);
            plr->SetSkill(SKILL_LANG_ORCISH, 300, 300, 300);
            plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_CLASS, chrClass);
            return true;
        }

        void OnApply(AuraEffect const*, AuraEffectHandleModes)
        {
            if (!HandleMorph())
                GetEffect(EFFECT_0)->CalculatePeriodic(GetUnitOwner());
        }

        void CalcPeriodic(AuraEffect const*, bool& isPeriodic, int32& amplitude)
        {
            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr)
                return;

            if (plr->IsLoading())
            {
                isPeriodic = true;
                amplitude = 100;
            }
            else
                isPeriodic = false;
        }

        void HandleEffectPeriodic(AuraEffect const*)
        {
            if (HandleMorph())
                GetEffect(EFFECT_0)->CalculatePeriodic(GetUnitOwner());
        }

        void OnRemove(AuraEffect const*, AuraEffectHandleModes)
        {
            if (Player* plr = GetUnitOwner()->ToPlayer())
            {
                plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID, plr->GetByteValue(PLAYER_ORIGINAL_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID));
                plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_FACE_ID, plr->GetByteValue(PLAYER_ORIGINAL_BYTES, PLAYER_BYTES_OFFSET_FACE_ID));
                plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_STYLE_ID, plr->GetByteValue(PLAYER_ORIGINAL_BYTES, PLAYER_BYTES_OFFSET_HAIR_STYLE_ID));
                plr->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_COLOR_ID, plr->GetByteValue(PLAYER_ORIGINAL_BYTES, PLAYER_BYTES_OFFSET_HAIR_COLOR_ID));
                plr->SetByteValue(PLAYER_BYTES_2, PLAYER_BYTES_2_OFFSET_FACIAL_STYLE, plr->GetByteValue(PLAYER_ORIGINAL_BYTES_2, PLAYER_BYTES_2_OFFSET_FACIAL_STYLE));
                plr->SetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER, plr->GetByteValue(PLAYER_ORIGINAL_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER));
                plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_GENDER, plr->GetByteValue(UNIT_FIELD_ORIGINAL_BYTES_0, UNIT_BYTES_0_OFFSET_GENDER));
                plr->SetByteValue(UNIT_FIELD_BYTES_0, UNIT_BYTES_0_OFFSET_RACE, plr->getOriginalRace());
                plr->SendUpdateToPlayer(plr);
                plr->InitDisplayIds();
            }
        }

        void ApplyLanguage(Player* plr)
        {
            bool apply = false;
            if (static_cast<int32>(plr->GetMapId()) != mapId)
            {
                apply = true;
                if (mapId != -1)
                    plr->GetReputationMgr().SendFakeStates();
                mapId = plr->GetMapId();
                lastPos = plr->GetPosition();
                handled = false;
            }
            else if (!handled && lastPos != plr->GetPosition())
            {
                apply = true;
                // set handled after player has moved his position
                handled = true;
            }

            if (apply)
            {
                GetEffect(EFFECT_1)->HandleEffect(plr, AURA_EFFECT_HANDLE_REAL, false);
                plr->SendUpdateToPlayer(plr);
                GetEffect(EFFECT_1)->HandleEffect(GetUnitOwner(), AURA_EFFECT_HANDLE_REAL, true);
                plr->SendUpdateToPlayer(plr);
            }
        }

        void CalcPeriodicLang(AuraEffect const*, bool& isPeriodic, int32& amplitude)
        {
            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr)
                return;

            isPeriodic = true;
            amplitude = 300;
        }

        void HandleEffectPeriodicLang(AuraEffect const*)
        {
            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr || plr->IsLoading() || plr->IsBeingTeleported())
                return;
            ApplyLanguage(plr);
        }

        void OnApplyLang(AuraEffect const*, AuraEffectHandleModes)
        {
            if (Player* plr = GetUnitOwner()->ToPlayer())
            {
                if (!applied)
                {
                    applied = true;
                    mapId = -1;
                }
                if (plr->IsLoading())
                {
                    GetEffect(EFFECT_1)->CalculatePeriodic(plr);
                    PreventDefaultAction();
                }
            }
        }


        void Register() override
        {
            OnEffectApply += AuraEffectApplyFn(spell_morph_shirt_aura_AuraScript::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_morph_shirt_aura_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_morph_shirt_aura_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_morph_shirt_aura_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            OnEffectApply += AuraEffectApplyFn(spell_morph_shirt_aura_AuraScript::OnApplyLang, EFFECT_1, SPELL_AURA_COMPREHEND_LANGUAGE, AURA_EFFECT_HANDLE_REAL);
            DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_morph_shirt_aura_AuraScript::CalcPeriodicLang, EFFECT_1, SPELL_AURA_COMPREHEND_LANGUAGE);
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_morph_shirt_aura_AuraScript::HandleEffectPeriodicLang, EFFECT_1, SPELL_AURA_COMPREHEND_LANGUAGE);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_morph_shirt_aura_AuraScript();
    }
};

void AddSC_morph_shirt()
{
    new spell_morph_shirt_aura();
}

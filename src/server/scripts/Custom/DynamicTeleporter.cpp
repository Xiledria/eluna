#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "WorldSession.h"
#include "GameEventMgr.h"

struct TeleportData
{
    uint32 entry;
    int32 menu_sub;
    uint8  icon;
    uint32 faction;
    std::string name;
    uint32 realmId;
    uint32 questId;
    int32 eventId;
};

struct TeleportLoc
{
    uint32 map;
    float position_x;
    float position_y;
    float position_z;
    float position_o;
};

class DynamicTeleporterMgr
{
    bool loaded = false;
public:
    uint32 references = 0;
    std::map<uint32, std::vector<TeleportData>> teleportDatas;
    std::map<uint32, TeleportLoc> teleportLocs;

    DynamicTeleporterMgr()
    {
        Load();
    }

    bool IsLoaded()
    {
        return loaded;
    }

    void Reload()
    {
        loaded = false;
        Load();
    }

    void Load()
    {
        teleportDatas.clear();
        teleportLocs.clear();
        QueryResult result = WorldDatabase.Query("SELECT entry, menu_parent, menu_sub, icon, faction, name, map, position_x, position_y, position_z, position_o, realm_id, quest_id, event_id "
            "FROM dynamic_teleporter ORDER BY entry");

        if (!result)
            return;

        do
        {
            Field *fields = result->Fetch();

            TeleportData td;
            td.entry = fields[0].GetUInt32();
            td.menu_sub = fields[2].GetInt32();
            td.icon = std::min(uint8(9), fields[3].GetUInt8());
            td.faction = fields[4].GetUInt32();
            td.name = fields[5].GetString();
            td.realmId = fields[11].GetUInt32();
            td.questId = fields[12].GetUInt32();
            td.eventId = fields[13].GetInt32();

            if (td.menu_sub < 0)
            {
                TeleportLoc tl;
                tl.map = fields[6].GetUInt32();
                tl.position_x = fields[7].GetFloat();
                tl.position_y = fields[8].GetFloat();
                tl.position_z = fields[9].GetFloat();
                tl.position_o = fields[10].GetFloat();
                teleportLocs[td.entry] = tl;
            }
        teleportDatas[fields[1].GetUInt32()].push_back(td);
        } while (result->NextRow());
        loaded = true;
    }
};

std::mutex teleporterMgrLock;
DynamicTeleporterMgr* teleporterMgr = nullptr;
void InitTeleporterMgr()
{
    std::lock_guard<std::mutex> guard(teleporterMgrLock);
    if (!teleporterMgr)
        teleporterMgr = new DynamicTeleporterMgr();
    ++teleporterMgr->references;
}

void RemoveTeleporterReference()
{
    std::lock_guard<std::mutex> guard(teleporterMgrLock);
    if (!--teleporterMgr->references)
    {
        delete teleporterMgr;
        teleporterMgr = nullptr;
    }
}

class npc_dynamic_teleporter : public CreatureScript
{
public:
    npc_dynamic_teleporter() : CreatureScript("npc_dynamic_teleporter")
    {
        InitTeleporterMgr();
    }

    ~npc_dynamic_teleporter()
    {
        RemoveTeleporterReference();
    }

    bool CanShow(Player* plr)
    {
        if (plr->IsInCombat())
        {
            plr->GetSession()->SendNotification("You are in combat!");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        else if (!teleporterMgr->IsLoaded())
        {
            plr->GetSession()->SendNotification("Teleporter reloading, try again!");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        return true;
    }

    void ShowMenu(Player* plr, Creature* me, uint32 menu_id)
    {
        plr->PlayerTalkClass->GetGossipMenu().ClearMenu();
        uint32 count = 0;
        uint32 send_counter = 0;
        if (plr->IsGameMaster() && !menu_id)
        {
            ++count;
            ++send_counter;
            AddGossipItemFor(plr, 0, "~RELOAD TELEPORTER DATA~\n", GOSSIP_SENDER_MAIN, 666);
        }

        for (auto& td : teleporterMgr->teleportDatas[menu_id])
        {
            if ((td.realmId && td.realmId != plr->GetRealmId()) || (td.faction && td.faction != plr->GetTeam()))
                continue;

            if (td.questId && plr->GetQuestStatus(td.questId) != QUEST_STATUS_REWARDED)
                continue;

            if (td.eventId)
            {
                if (td.eventId > 0)
                {
                    if (!sGameEventMgr->IsActiveEvent(td.eventId))
                        continue;
                }
                else if (sGameEventMgr->IsActiveEvent(td.eventId))
                    continue;
            }

            AddGossipItemFor(plr, td.icon, td.name, menu_id, GOSSIP_ACTION_INFO_DEF + td.entry);
            ++count;
            ++send_counter;
            if (send_counter >= 10)
            {
                plr->PlayerTalkClass->SendGossipMenu(1, me->GetGUID());
                send_counter = 0;
            }
        }
        if (send_counter || !count)
            plr->PlayerTalkClass->SendGossipMenu(1, me->GetGUID());
    }

    void TeleportToTarget(Player* plr, uint32 target_id)
    {
        auto itr = teleporterMgr->teleportLocs.find(target_id);
        if (itr != teleporterMgr->teleportLocs.end())
        {
            TeleportLoc& loc = itr->second;
            plr->TeleportTo(loc.map, loc.position_x, loc.position_y, loc.position_z, loc.position_o);
        }
    }

    bool HandleMenuAction(Player* plr, Creature* me, uint32 sender, uint32 action)
    {
        for (auto& td : teleporterMgr->teleportDatas[sender])
        {
            if (td.entry == action)
            {
                if (td.menu_sub < 0)
                {
                    TeleportToTarget(plr, action);
                    return true;
                }
                else
                {
                    ShowMenu(plr, me, td.menu_sub);
                    return true;
                }
            }
        }
        return false;
    }

    bool OnGossipHello(Player* plr, Creature* me)
    {
        if (!CanShow(plr))
            return true;

        ShowMenu(plr, me, 0);
        return true;
    }

    bool OnGossipSelect(Player* plr, Creature* me, uint32 sender, uint32 action)
    {
        if (!CanShow(plr))
            return true;

        if (action == 666 && plr->IsGameMaster())
        {
            teleporterMgr->Reload();
            plr->PlayerTalkClass->SendCloseGossip();
            plr->GetSession()->SendNotification("Reloaded..");
            return true;
        }

        if (!HandleMenuAction(plr, me, sender, action - GOSSIP_ACTION_INFO_DEF))
            plr->PlayerTalkClass->SendCloseGossip();
        return true;
    }
};

void AddSC_npc_dynamic_teleporter()
{
    new npc_dynamic_teleporter();
}

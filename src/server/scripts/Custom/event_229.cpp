#include "grunt_guardian_base_class.h"

class event_229_guardian : public CreatureScript
{
public:
    event_229_guardian() : CreatureScript("event_229_guardian") { }

    struct event_229_guardianAI : public grunt_guardian_base_classAI
    {
        event_229_guardianAI(Creature* creature) : grunt_guardian_base_classAI(creature)
        {
            points = {{ -8866.152344f, -1283.848633f, 8.877116f, 0.012764f }};
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_229_guardianAI(creature);
    }
};

void AddSC_event_229()
{
    new event_229_guardian();
}
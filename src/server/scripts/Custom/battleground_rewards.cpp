#include "ScriptMgr.h"
#include "Player.h"
#include "Guild.h"

class BattlegroundRewards : public PlayerScript
{
public:
    BattlegroundRewards() : PlayerScript("BattlegroundRewards") { }

    struct BGReward
    {
        uint32  gold;
        uint32 quest;
    };

    enum QuestTriggers
    {
        QUEST_3V3_TRIGGER = 450157,
    };

    enum quests
    {
        QUESTS_START    = 40039,
        QUEST_AV,
        QUEST_WS,
        QUEST_AB,
        QUEST_NA,
        QUEST_BE,
        QUEST_EY,
        QUEST_RL,
        QUEST_SA,
        QUEST_DS,
        QUEST_RV,
        QUEST_IC,
        QUEST_3V3       = 40055,
    };

    const std::map<BattlegroundTypeId, BGReward> BGRewards =
    {
        { BATTLEGROUND_AV, { 50, QUEST_AV } },
        { BATTLEGROUND_WS, { 20, QUEST_WS } },
        { BATTLEGROUND_AB, { 30, QUEST_AB } },
        { BATTLEGROUND_NA, {  3, QUEST_NA } },
        { BATTLEGROUND_BE, {  3, QUEST_BE } },
        { BATTLEGROUND_EY, { 30, QUEST_EY } },
        { BATTLEGROUND_RL, {  3, QUEST_RL } },
        { BATTLEGROUND_SA, { 30, QUEST_SA } },
        { BATTLEGROUND_DS, {  3, QUEST_DS } },
        { BATTLEGROUND_RV, {  3, QUEST_RV } },
        { BATTLEGROUND_IC, { 50, QUEST_IC } }
    };

#define MULTIPLIER    1.5f

    enum Rewards
    {
        REWARD_BOX_BG  =  32544,
        REWARD_BOX_FUN =  27590,
        MARK_OF_ARENA  = 400099
    };

    void OnBattlegroundEnd(Player* player, Battleground* battleground, BattlegroundTypeId battlegroundId, bool winner) override
    {
        auto itr = BGRewards.find(battlegroundId);
        if (itr == BGRewards.end())
            return;

        int32 rewardBox = REWARD_BOX_FUN;
        if (player->GetRealmId() == realmID2)
        {
            rewardBox = REWARD_BOX_BG;
            uint32 reward = static_cast<uint32>(itr->second.gold * GOLD * MULTIPLIER);
            if (!winner)
                reward /= 2;

            player->ModifyMoney(reward);
            if (Guild* guild = player->GetGuild())
                guild->GiveXp(itr->second.gold * 10 * MULTIPLIER);
        }

        if (winner)
        {
            if (battleground->isArena())
            {
                player->AddItem(rewardBox, 1, true);
                player->AddItem(MARK_OF_ARENA, 3, true);
                if (battleground->GetArenaType() == ARENA_TYPE_3v3)
                {
                    player->KilledMonsterCredit(QUEST_3V3_TRIGGER);
                    if (roll_chance_i(20)) // extra chance for another treasure
                        player->AddItem(rewardBox, 1, true);
                }
            }

            if (player->IsActiveQuest(itr->second.quest))
                player->CompleteQuest(itr->second.quest);
        }
        else if (battleground->isArena())
            player->AddItem(MARK_OF_ARENA, 1, true);
    }
};

void AddSC_battleground_rewards()
{
    new BattlegroundRewards();
}

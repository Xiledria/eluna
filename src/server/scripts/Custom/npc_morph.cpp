/*
Author: Code edited and stolen by Vi d(T.T)b
Server: Deffender.eu
Web: http://deffender.eu
Script: NPC MORPHER
ScriptName: npc_morph
*/

#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "WorldPacket.h"

#define reward 17202 //reward id
#define pocet 1 //reward count
#define morpher_text_buy "Byl ti odebran 1x[Snowball] za pouziti morphu." //ok
#define morpher_text_err "Nemas dostatecny pocet [Snowball]u. Na morph potrebujes 1x[Snowball]. | You don't have enough [Snowball]s. You need 1x[Snowball]." //you don't have enough tokens

class npc_morph : public CreatureScript
{
public:
    npc_morph() : CreatureScript("npc_morph")
    {
    }
    bool OnGossipHello(Player* plr, Creature* me)
    {
        if (plr->IsInCombat())
        {
            CloseGossipMenuFor(plr);
            me->Whisper("Pro aplikaci morphu nesmis byt v combatu.", LANG_UNIVERSAL, plr, true);
            return true;
        }

        plr->PlayerTalkClass->ClearMenus();
        AddGossipItemFor(plr, 9, "Demorph - zrusit morph...", GOSSIP_SENDER_MAIN, 20001);
        AddGossipItemFor(plr, 6, "Lady Sylvanas Windrunner", GOSSIP_SENDER_MAIN, 20002);
        AddGossipItemFor(plr, 6, "Gul'dan", GOSSIP_SENDER_MAIN, 20003);
        AddGossipItemFor(plr, 6, "Akama", GOSSIP_SENDER_MAIN, 20004);
        AddGossipItemFor(plr, 6, "Maiev Shadowsong", GOSSIP_SENDER_MAIN, 20005);
        AddGossipItemFor(plr, 6, "Medivh", GOSSIP_SENDER_MAIN, 20006);
        AddGossipItemFor(plr, 6, "Tucnak", GOSSIP_SENDER_MAIN, 20007);
        AddGossipItemFor(plr, 6, "Geist", GOSSIP_SENDER_MAIN, 20008);
        AddGossipItemFor(plr, 6, "Undead Pirat", GOSSIP_SENDER_MAIN, 20009);
        AddGossipItemFor(plr, 6, "Velen the Prophet", GOSSIP_SENDER_MAIN, 20010);
        AddGossipItemFor(plr, 6, "Christmas Blood Elf 18+", GOSSIP_SENDER_MAIN, 20011);
        AddGossipItemFor(plr, 6, "Arthas", GOSSIP_SENDER_MAIN, 20012);
        AddGossipItemFor(plr, 6, "Lich king", GOSSIP_SENDER_MAIN, 20013);
        AddGossipItemFor(plr, 6, "Humr", GOSSIP_SENDER_MAIN, 20014);
        AddGossipItemFor(plr, 6, "Terrorfiend", GOSSIP_SENDER_MAIN, 20015);
        AddGossipItemFor(plr, 6, "Troll na netopyrovi", GOSSIP_SENDER_MAIN, 20016);
        AddGossipItemFor(plr, 6, "Teron Gorefiend", GOSSIP_SENDER_MAIN, 20017);
        AddGossipItemFor(plr, 6, "King Varian Wrynn", GOSSIP_SENDER_MAIN, 20018);
        AddGossipItemFor(plr, 6, "Naga", GOSSIP_SENDER_MAIN, 20019);
        AddGossipItemFor(plr, 6, "Santa Goblin", GOSSIP_SENDER_MAIN, 20020);
        AddGossipItemFor(plr, 6, "Christmas Gnome Female", GOSSIP_SENDER_MAIN, 20021);
        AddGossipItemFor(plr, 6, "Aldaron the Reckless", GOSSIP_SENDER_MAIN, 20022);
        AddGossipItemFor(plr, 6, "Ogre", GOSSIP_SENDER_MAIN, 20023);
        AddGossipItemFor(plr, 6, "Christmas Undead", GOSSIP_SENDER_MAIN, 20024);
        AddGossipItemFor(plr, 6, "Christmas Night Elf. 18+", GOSSIP_SENDER_MAIN, 20025);
        AddGossipItemFor(plr, 6, "Gnome Diver", GOSSIP_SENDER_MAIN, 20026);
        AddGossipItemFor(plr, 6, "Santa Claus - Gnome", GOSSIP_SENDER_MAIN, 20027);
        AddGossipItemFor(plr, 6, "Human Diver", GOSSIP_SENDER_MAIN, 20028);
        AddGossipItemFor(plr, 6, "Troll Diver", GOSSIP_SENDER_MAIN, 20029);
        SendGossipMenuFor(plr, DEFAULT_GOSSIP_MESSAGE, me->GetGUID());
        return true;
    }
    void SendDefaultMenu(Player* plr, Creature* me, uint32 uiAction)
    {
        switch (uiAction)
        {
        case 20001://Demorph
            CloseGossipMenuFor(plr);
            plr->DeMorph();
            break;

        case 20002://Lady Sylvanas Windrunner
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(28213);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;
        case 20003://Gul'dan
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(16642);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20004://Akama
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(20681);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20005://Maiev Shadowsong
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(20628);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20006://Medivh
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(18718);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20007://Tucnak
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(24698);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20008://Geist
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(24579);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20009://Undead Pirat
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(25042);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20010://Velen the Prophet
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(17822);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20011://Christmas Blood Elf 18+
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(18785);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20012://Arthas
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(21976);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20013://Lich king
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(22235);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20014://Humr
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(19726);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20015://Terrorfiend
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(18373);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20016://Troll na netopyrovi
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15303);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20017://Teron Gorefiend
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(21576);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20018://King Varian Wrynn
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(28127);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20019://Naga
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(18390);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20020://Santa Goblin
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15698);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20021://Christmas Gnome Female
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15799);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20022://Aldaron the Reckless
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15925);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20023://Ogre
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(1122);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20024://Christmas Undead
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(18811);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20025://Christmas Night Elf. 18+
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15748);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20026://Gnome Diver
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(27657);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20027://Santa Claus Gnom
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(15806);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20028://Human Diver
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(23426);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        case 20029://Troll Diver
            if (plr->HasItemCount(reward, pocet))
            {
                plr->DestroyItemCount(reward, pocet, true, false);
                me->Whisper(morpher_text_buy, LANG_UNIVERSAL, plr, true);
                plr->SetDisplayId(17272);
                CloseGossipMenuFor(plr);
            }
            else
            {
                me->Whisper(morpher_text_err, LANG_UNIVERSAL, plr);
                CloseGossipMenuFor(plr);
            }
            break;

        }
    }


    bool OnGossipSelect(Player* plr, Creature* me, uint32 uiSender, uint32 uiAction)
    {
        if (uiSender == GOSSIP_SENDER_MAIN)
            SendDefaultMenu(plr, me, uiAction);
        return true;
    }

};

void AddSC_npc_morph()
{
    new npc_morph;
}

#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "WorldSession.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "BattlegroundMgr.h"
#include <functional>
#include "PassiveAI.h"

bool CanShow(uint32 two, uint32 three, Creature* me)
{
    // 3v3 team is enough to buy items without 3v3 requirements if it is higher than 2v2 requirements
    if (two < three)
        two = three;
    if (me->GetHealth() >= 1000 && two >= me->GetHealth())
        return true;

    if (me->GetPower(POWER_MANA) >= 1000 && three >= me->GetPower(POWER_MANA))
        return true;

    if (me->GetHealth() <= 1000 && me->GetPower(POWER_MANA) <= 1000)
        return true;

    return false;
}

class npc_bg_vendor : public CreatureScript
{
public:
    npc_bg_vendor(const char* scriptName) : CreatureScript(scriptName) { }

    struct npc_bg_vendorAI : NullCreatureAI
    {
        npc_bg_vendorAI(Creature* creature) : NullCreatureAI(creature) { }

        bool CanTradeWith(Player const* plr) override
        {
            return plr->IsGameMaster() || CanShow(plr->GetArenaPersonalRating(0), plr->GetArenaPersonalRating(1), me);
        }
    };

    bool OnGossipHello(Player* plr, Creature* me) override
    {
        uint32 two = me->GetHealth();
        uint32 three = me->GetPower(POWER_MANA);
        if (!CanShow(plr->GetArenaPersonalRating(0), plr->GetArenaPersonalRating(1), me))
        {
            if (three <= 1000)
                me->Whisper("You need " + std::to_string(two) + " rating to be able to buy anything from me.", LANG_UNIVERSAL, plr, true);
            else if (two <= 1000)
                me->Whisper("You need " + std::to_string(three) + " 3v3 rating to be able to buy anything from me.", LANG_UNIVERSAL, plr, true);
            else
                me->Whisper("You need " + std::to_string(two) + " rating in 2v2 or " + std::to_string(three) + " rating in 3v3 to be able to buy anything from me.", LANG_UNIVERSAL, plr, true);
        }

        plr->GetSession()->SendListInventory(me->GetGUID());
        return true;
    }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_bg_vendorAI(creature);
    }
};

class npc_zan_gladiator : public npc_bg_vendor
{
public:
    npc_zan_gladiator() : npc_bg_vendor("npc_zan_gladiator") { }

    struct npc_zan_gladiatorAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        Creature* me;
        npc_zan_gladiatorAI(Creature* creature) : CreatureAI(creature)
        {
            delay = 5 * MINUTE * IN_MILLISECONDS;
            timer = 0;
            me = creature;
        }

        std::vector<std::function<void(std::list<Player*>&)>> actions
        {
            [this](std::list<Player*>& players)
            {
                auto itr = players.begin();
                std::advance(itr, urand(0, static_cast<uint32>(players.size() - 1)));
                Player* plr = *itr;
                me->Say(plr->GetName() + ", logni.", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                me->Say("Log 2v2!", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                BattlegroundContainer battlegrounds = sBattlegroundMgr->GetBattlegroundsByType(BATTLEGROUND_AA);
                uint32 teams = 0;
                for (auto itr : battlegrounds)
                    if (itr.second->GetArenaType() == ARENA_TYPE_3v3)
                        teams += 2;

                teams += static_cast<uint32>(sBattlegroundMgr->GetBattlegroundQueue(BATTLEGROUND_QUEUE_3v3).m_QueuedPlayers.size() / 3);

                if (!teams)
                    me->Say("Log 3v3!", LANG_UNIVERSAL);
                else if (teams == 1)
                    me->Say("Právě je lognutý 1 3v3 arena team, jdi je pofarmit!", LANG_UNIVERSAL);
                else if (teams < 5)
                    me->Say("Právě logují " + std::to_string(teams) + " 3v3 arena teamy, tak logni taky, neboj se!", LANG_UNIVERSAL);
                else
                    me->Say("Právě loguje " + std::to_string(teams) + " 3v3 arena teamů, logni taky!", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                me->Say("LOL ZA TEN NOVEJ DAILY NA 3v3 ARENY JSOU EVENT MARKY!", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>& players)
            {
                auto itr = players.begin();
                std::advance(itr, urand(0, static_cast<uint32>(players.size() - 1)));
                Player* plr = *itr;
                me->Say(plr->GetName() + ", nedodguj.", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                me->Say("Jednou dosáhneš tak vysokého ratu jako já! Jen musíš chodit areny, tak logni!", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                me->Say("Stop dodge pls.", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                me->Say("Log, jdeme na bodi.", LANG_UNIVERSAL);
            },
            [this](std::list<Player*>&)
            {
                uint32 count = 0;
                uint32 arena3v3 = 0;
                BattlegroundContainer battlegrounds = sBattlegroundMgr->GetBattlegroundsByType(BATTLEGROUND_AA);
                for (auto itr : battlegrounds)
                {
                    Battleground* bg = itr.second;
                    if (bg->GetArenaType() == ARENA_TYPE_3v3)
                        arena3v3 += bg->GetPlayersSize();
                    count += bg->GetPlayersSize();
                }
                if (count > 1)
                {
                    if (count < 5)
                        me->Say("Právě jsou v areně " + std::to_string(count) + " lidi. Logni taky!", LANG_UNIVERSAL);
                    else if (arena3v3)
                        me->Say("Právě je v areně " + std::to_string(count) + " lidí (z toho " + std::to_string(arena3v3) + " ve 3v3). Logni taky!", LANG_UNIVERSAL);
                    else
                        me->Say("Právě je v areně " + std::to_string(count) + " lidí. Logni taky!", LANG_UNIVERSAL);
                }
            }
        };

        void UpdateAI(uint32 diff) override
        {
            if (timer < diff)
            {
                timer += delay;

                const time_t now = time(nullptr);
                tm *tm_struct = localtime(&now);
                uint8 hour = tm_struct->tm_hour;
                if (hour > 2 && hour < 7)
                    return;

                std::list<Player*> players;
                me->GetPlayerListInGrid(players, 15.0f);
                players.remove_if([this](Player* player)
                {
                    if (!me->CanSeeOrDetect(player))
                        return true;

                    // don't spam players already in queue
                    return player->GetBattlegroundQueueIndex(BATTLEGROUND_QUEUE_2v2) != PLAYER_MAX_BATTLEGROUND_QUEUES
                        || player->GetBattlegroundQueueIndex(BATTLEGROUND_QUEUE_3v3) != PLAYER_MAX_BATTLEGROUND_QUEUES
                        || player->GetBattlegroundQueueIndex(BATTLEGROUND_QUEUE_5v5) != PLAYER_MAX_BATTLEGROUND_QUEUES;
                });
                if (!players.empty())
                {
                    auto itr = actions.begin();
                    std::advance(itr, urand(0, static_cast<uint32>(actions.size() - 1)));
                    (*itr)(players);
                }
            }
            else
            {
                timer -= diff;
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_zan_gladiatorAI(creature);
    }
};

void AddSC_npc_bg_vendor()
{
    new npc_bg_vendor("npc_bg_vendor");
    new npc_zan_gladiator();
}

#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "Chat.h"

enum Enchants
{
    //    1Hand Weapons
    ENCHANT_WEP_BERSERKING              = 3789,
    ENCHANT_WEP_BLADE_WARD              = 3869,
    ENCHANT_WEP_BLOOD_DRAINING          = 3870,
    ENCHANT_WEP_ACCURACY                = 3788,
    ENCHANT_WEP_AGILITY_1H              = 1103,
    ENCHANT_WEP_SPIRIT                  = 3844,
    ENCHANT_WEP_BATTLEMASTER            = 2675,
    ENCHANT_WEP_BLACK_MAGIC             = 3790,
    ENCHANT_WEP_ICEBREAKER              = 3239,
    ENCHANT_WEP_LIFEWARD                = 3241,
    ENCHANT_WEP_MIGHTY_SPELL_POWER      = 3834,
    ENCHANT_WEP_EXECUTIONER             = 3225,
    ENCHANT_WEP_POTENCY                 = 3833,
    ENCHANT_WEP_TITANGUARD              = 3851,
    ENCHANT_WEP_WEAPON_CHAIN            = 3731,
    ENCHANT_WEP_SPELLSURGE              = 2674,

    //    2Hand Weapons
    ENCHANT_2WEP_MASSACRE               = 3827,
    ENCHANT_2WEP_SCOURGEBANE            = 3247,
    ENCHANT_2WEP_GIANT_SLAYER           = 3251,
    ENCHANT_2WEP_GREATER_SPELL_POWER    = 3854,
    ENCHANT_2WEP_AGILITY                = 2670,
    ENCHANT_2WEP_MONGOOSE               = 2673,
    ENCHANT_2WEP_SPIRIT                 = 3844,

    //    SHIELDS
    ENCHANT_SHIELD_DEFENSE              = 1952,
    ENCHANT_SHIELD_INTELLECT            = 1128,
    ENCHANT_SHIELD_RESILIENCE           = 3229,
    ENCHANT_SHIELD_BLOCK                = 2655,
    ENCHANT_SHIELD_STAMINA              = 1071,
    ENCHANT_SHIELD_TOUGHSHIELD          = 2653,
    ENCHANT_SHIELD_TITANIUM_PLATING     = 3849,

    //    RANGED WEAPON
    ENCHANT_RANGED_SCOPE                = 3843,
    ENCHANT_RANGED_HIT                  = 2523,
    ENCHANT_RANGED_HASTE                = 3607,
    ENCHANT_RANGED_CRIT                 = 3608,

    //    HEADS
    ENCHANT_HEAD_BLISSFUL_MENDING       = 3819,
    ENCHANT_HEAD_BURNING_MYSTERIES      = 3820,
    ENCHANT_HEAD_DOMINANCE              = 3796,
    ENCHANT_HEAD_SAVAGE_GLADIATOR       = 3842,
    ENCHANT_HEAD_STALWART_PROTECTOR     = 3818,
    ENCHANT_HEAD_TORMENT                = 3817,
    ENCHANT_HEAD_TRIUMPH                = 3795,
    ENCHANT_HEAD_ECLIPSED_MOON          = 3815,
    ENCHANT_HEAD_FLAME_SOUL             = 3816,
    ENCHANT_HEAD_FLEEING_SHADOW         = 3814,
    ENCHANT_HEAD_FROSTY_SOUL            = 3812,
    ENCHANT_HEAD_TOXIC_WARDING          = 3813,

    //    SHOULDERS
    ENCHANT_SHOULDER_MASTERS_AXE        = 3835,
    ENCHANT_SHOULDER_MASTERS_CRAG       = 3836,
    ENCHANT_SHOULDER_MASTERS_PINNACLE   = 3837,
    ENCHANT_SHOULDER_MASTERS_STORM      = 3838,
    ENCHANT_SHOULDER_GREATER_AXE        = 3808,
    ENCHANT_SHOULDER_GREATER_CRAG       = 3809,
    ENCHANT_SHOULDER_GREATER_GLADIATOR  = 3852,
    ENCHANT_SHOULDER_GREATER_PINNACLE   = 3811,
    ENCHANT_SHOULDER_GREATER_STORM      = 3810,
    ENCHANT_SHOULDER_DOMINANCE          = 3794,
    ENCHANT_SHOULDER_TRIUMPH            = 3793,

    //    CLOAKS
    ENCHANT_CLOAK_DARKGLOW_EMBROIDERY   = 3728,
    ENCHANT_CLOAK_SWORDGUARD_EMBROIDERY = 3730,
    ENCHANT_CLOAK_LIGHTWEAVE_EMBROIDERY = 3722,
    ENCHANT_CLOAK_SPRINGY_ARACHNOWEAVE  = 3859,
    ENCHANT_CLOAK_FLEXWEAVE_UNDERLAY    = 3605,
    ENCHANT_CLOAK_WISDOM                = 3296,
    ENCHANT_CLOAK_TITANWEAVE            = 1951,
    ENCHANT_CLOAK_SPELL_PIERCING        = 3243,
    ENCHANT_CLOAK_MIGHTY_ARMOR          = 3294,
    ENCHANT_CLOAK_MAJOR_AGILITY         = 1099,
    ENCHANT_CLOAK_GREATER_SPEED         = 3831,
    ENCHANT_CLOAK_STEALTH_AGI           = 3256,

    ENCHANT_CLOAK_SHADOW_ARMOR          = 1446,
    ENCHANT_CLOAK_FIRE_ARMOR            = 1354,
    ENCHANT_CLOAK_FROST_ARMOR           = 1308,
    ENCHANT_CLOAK_NATURE_ARMOR          = 1400,
    ENCHANT_CLOAK_ARCANE_ARMOR          = 1262,

    //    LEGS
    ENCHANT_LEG_EARTHEN                 = 3853,
    ENCHANT_LEG_FROSTHIDE               = 3822,
    ENCHANT_LEG_ICESCALE                = 3823,
    ENCHANT_LEG_BRILLIANT_SPELLTHREAD   = 3719,
    ENCHANT_LEG_SAPPHIRE_SPELLTHREAD    = 3721,

    //    GLOVES
    ENCHANT_GLOVES_GREATER_BLASTING     = 3249,
    ENCHANT_GLOVES_ARMSMAN              = 3253,
    ENCHANT_GLOVES_CRUSHER              = 1603,
    ENCHANT_GLOVES_AGILITY              = 3222,
    ENCHANT_GLOVES_PRECISION            = 3234,
    ENCHANT_GLOVES_EXPERTISE            = 3231,
    ENCHANT_GLOVES_SPELL_POWER          = 3246,
    ENCHANT_GLOVES_SOCKET_GLOVES        = 3723,
    ENCHANT_GLOVES_PYRO_ROCKET          = 3603,
    ENCHANT_GLOVES_HYPERSPEED           = 3604,
    ENCHANT_GLOVES_ARMOR_WEBBING        = 3860,

    // BRACERS (Wrists)
    ENCHANT_BRACERS_MAJOR_STAMINA       = 3850,
    ENCHANT_BRACERS_SUPERIOR_SP         = 2332,
    ENCHANT_BRACERS_GREATER_ASSUALT     = 3845,
    ENCHANT_BRACERS_MAJOR_SPIRT         = 1147,
    ENCHANT_BRACERS_EXPERTISE           = 3231,
    ENCHANT_BRACERS_GREATER_STATS       = 2661,
    ENCHANT_BRACERS_INTELLECT           = 1119,
    ENCHANT_BRACERS_FURL_ARCANE         = 3763,
    ENCHANT_BRACERS_FURL_FIRE           = 3759,
    ENCHANT_BRACERS_FURL_FROST          = 3760,
    ENCHANT_BRACERS_FURL_NATURE         = 3762,
    ENCHANT_BRACERS_FURL_SHADOW         = 3761,
    ENCHANT_BRACERS_FURL_ATTACK         = 3756,
    ENCHANT_BRACERS_FURL_STAMINA        = 3757,
    ENCHANT_BRACERS_FURL_SPELLPOWER     = 3758,
    ENCHANT_BRACERS_SOCKET_BRACER       = 3717,

    //    CHESTS
    ENCHANT_CHEST_POWERFUL_STATS        = 3832,
    ENCHANT_CHEST_SUPER_HEALTH          = 3297,
    ENCHANT_CHEST_GREATER_MAINA_REST    = 2381,
    ENCHANT_CHEST_EXCEPTIONAL_RESIL     = 3245,
    ENCHANT_CHEST_GREATER_DEFENSE       = 1953,

    //    WAISTS
    ENCHANT_WAISTS_ETERNAL_BELT_BUCKLE  = 3729,
    ENCHANT_WAISTS_FRAG_BELT            = 3601,

    //    BOOTS
    ENCHANT_BOOTS_GREATER_ASSULT        = 1597,
    ENCHANT_BOOTS_TUSKARS_VITLIATY      = 3232,
    ENCHANT_BOOTS_SUPERIOR_AGILITY      = 983,
    ENCHANT_BOOTS_GREATER_SPIRIT        = 1147,
    ENCHANT_BOOTS_GREATER_VITALITY      = 3244,
    ENCHANT_BOOTS_ICEWALKER             = 3826,
    ENCHANT_BOOTS_GREATER_FORTITUDE     = 1075,
    ENCHANT_BOOTS_NITRO_BOOTS           = 3606,

    //    RINGS
    ENCHANT_RING_ASSULT                 = 3839,
    ENCHANT_RING_GREATER_SP             = 3840,
    ENCHANT_RING_STAMINA                = 3791,
};

void Enchant(Player* player, Item* item, uint32 enchantid)
{
    if (!item)
    {
        player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
        return;
    }
    if (!enchantid)
    {
        player->GetSession()->SendNotification("Something went wrong in the code. It has been logged for developers and will be looked into, sorry for the inconvenience.");
        return;
    }
    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, enchantid, 0, 0);
    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
    player->GetSession()->SendNotification("|cff0000FF%s |cffFF0000succesfully enchanted!", item->GetTemplate()->Name1.c_str());
}
class npc_enchantment : public CreatureScript
{
    public:
    npc_enchantment() : CreatureScript("npc_enchantment") {
    }
    bool OnGossipHello(Player* player, Creature* creature)
    {
        if (player->IsInCombat() || player->duel)
        {
            ChatHandler(player->GetSession()).PSendSysMessage("Nesmis byt v combatu ani mít duel!");
        }
        else if (player->getLevel() >= 80) {
            AddGossipItemFor(player, GOSSIP_ICON_TALK, "Welcome to the enchanting NPC!", GOSSIP_SENDER_MAIN, 0);
            AddGossipItemFor(player, 1, "Enchant Head", GOSSIP_SENDER_MAIN, 4);
            AddGossipItemFor(player, 1, "Enchant Shoulders", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 1, "Enchant Cloak", GOSSIP_SENDER_MAIN, 6);
            AddGossipItemFor(player, 1, "Enchant Chest", GOSSIP_SENDER_MAIN, 7);
            AddGossipItemFor(player, 1, "Enchant Waist", GOSSIP_SENDER_MAIN, 14);
            AddGossipItemFor(player, 1, "Enchant Bracers", GOSSIP_SENDER_MAIN, 8);
            AddGossipItemFor(player, 1, "Enchant Gloves", GOSSIP_SENDER_MAIN, 9);
            AddGossipItemFor(player, 1, "Enchant Legs", GOSSIP_SENDER_MAIN, 10);
            AddGossipItemFor(player, 1, "Enchant Feet", GOSSIP_SENDER_MAIN, 11);
            AddGossipItemFor(player, 1, "Enchant 2H Weapon", GOSSIP_SENDER_MAIN, 2);
            AddGossipItemFor(player, 1, "Enchant Main Hand", GOSSIP_SENDER_MAIN, 1);
            AddGossipItemFor(player, 1, "Enchant Off Hand", GOSSIP_SENDER_MAIN, 13);
            AddGossipItemFor(player, 1, "Enchant Shield", GOSSIP_SENDER_MAIN, 3);
            AddGossipItemFor(player, 1, "Enchant Ranged Weapon", GOSSIP_SENDER_MAIN, 15);
            if (player->GetSkillValue(SKILL_ENCHANTING) >= 450)
                AddGossipItemFor(player, 1, "Enchant Rings", GOSSIP_SENDER_MAIN, 12);
        }
        else
        {
            ChatHandler(player->GetSession()).PSendSysMessage("Musis mit level 80!");
        }
        player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
        return true;
    }
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        Item * item;
        player->PlayerTalkClass->ClearMenus();
        switch (action)
        {
            case 0: // Hl. Menu
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000Hello there, I will be enchanting your gear!");
                {
                    if (player->IsInCombat() || player->duel)
                    {
                        ChatHandler(player->GetSession()).PSendSysMessage("Nesmis byt v combatu ani mít duel!");
                    }
                    else if (player->getLevel() >= 80) {
                        AddGossipItemFor(player, GOSSIP_ICON_TALK, "Welcome to the enchanting NPC!", GOSSIP_SENDER_MAIN, 0);
                        AddGossipItemFor(player, 1, "Enchant Head", GOSSIP_SENDER_MAIN, 4);
                        AddGossipItemFor(player, 1, "Enchant Shoulders", GOSSIP_SENDER_MAIN, 5);
                        AddGossipItemFor(player, 1, "Enchant Cloak", GOSSIP_SENDER_MAIN, 6);
                        AddGossipItemFor(player, 1, "Enchant Chest", GOSSIP_SENDER_MAIN, 7);
                        AddGossipItemFor(player, 1, "Enchant Waist", GOSSIP_SENDER_MAIN, 14);
                        AddGossipItemFor(player, 1, "Enchant Bracers", GOSSIP_SENDER_MAIN, 8);
                        AddGossipItemFor(player, 1, "Enchant Gloves", GOSSIP_SENDER_MAIN, 9);
                        AddGossipItemFor(player, 1, "Enchant Legs", GOSSIP_SENDER_MAIN, 10);
                        AddGossipItemFor(player, 1, "Enchant Feet", GOSSIP_SENDER_MAIN, 11);
                        AddGossipItemFor(player, 1, "Enchant 2H Weapon", GOSSIP_SENDER_MAIN, 2);
                        AddGossipItemFor(player, 1, "Enchant Main Hand", GOSSIP_SENDER_MAIN, 1);
                        AddGossipItemFor(player, 1, "Enchant Off Hand", GOSSIP_SENDER_MAIN, 13);
                        AddGossipItemFor(player, 1, "Enchant Shield", GOSSIP_SENDER_MAIN, 3); 
                        AddGossipItemFor(player, 1, "Enchant Ranged Weapon", GOSSIP_SENDER_MAIN, 15);
                        if (player->GetSkillValue(SKILL_ENCHANTING) >= 450)
                        AddGossipItemFor(player, 1, "Enchant Rings", GOSSIP_SENDER_MAIN, 12);
                    }
                    else
                    {
                        ChatHandler(player->GetSession()).PSendSysMessage("Musis mit level 80!");
                    }
                    player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
                    return true;
                    break;
                }

            case 1: // Enchant Main Hand
                AddGossipItemFor(player, 1, "+ 63 Spell Power", GOSSIP_SENDER_MAIN, 112);
                AddGossipItemFor(player, 1, "+ 65 Attack Power", GOSSIP_SENDER_MAIN, 111);
                AddGossipItemFor(player, 1, "+ 26 Agility", GOSSIP_SENDER_MAIN, 100);
                AddGossipItemFor(player, 1, "+ 45 Spirit", GOSSIP_SENDER_MAIN, 101);
                AddGossipItemFor(player, 1, "+ 25 Hit & Critical Strike", GOSSIP_SENDER_MAIN, 105);
                AddGossipItemFor(player, 1, "+ 50 Stamina", GOSSIP_SENDER_MAIN, 110);
                AddGossipItemFor(player, 1, "Berserking", GOSSIP_SENDER_MAIN, 104);
                AddGossipItemFor(player, 1, "Black Magic", GOSSIP_SENDER_MAIN, 106);
                AddGossipItemFor(player, 1, "Battlemaster", GOSSIP_SENDER_MAIN, 107);
                AddGossipItemFor(player, 1, "Icebreaker", GOSSIP_SENDER_MAIN, 108);
                AddGossipItemFor(player, 1, "Lifeward", GOSSIP_SENDER_MAIN, 109);
                AddGossipItemFor(player, 1, "Mongoose", GOSSIP_SENDER_MAIN, 113);
                AddGossipItemFor(player, 1, "Blade Ward", GOSSIP_SENDER_MAIN, 102);
                AddGossipItemFor(player, 1, "Blood Draining", GOSSIP_SENDER_MAIN, 103);
                AddGossipItemFor(player, 1, "Executioner", GOSSIP_SENDER_MAIN, 114);
                AddGossipItemFor(player, 1, "Spellsurge", GOSSIP_SENDER_MAIN, 269);
                AddGossipItemFor(player, 1, "Titanium Weapon Chain", GOSSIP_SENDER_MAIN, 207);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
                return true;
                break;

            case 13: //Enchant Off hand
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
                if (!item || item->GetTemplate()->InventoryType == INVTYPE_HOLDABLE || item->GetTemplate()->InventoryType == INVTYPE_SHIELD)
                {
                    creature->Whisper("This enchant needs a off hand weapon equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    return false;
                }
                else
                {
                    AddGossipItemFor(player, 1, "+ 65 Attack Power", GOSSIP_SENDER_MAIN, 219);
                    AddGossipItemFor(player, 1, "+ 26 Agility", GOSSIP_SENDER_MAIN, 208);
                    AddGossipItemFor(player, 1, "+ 45 Spirit", GOSSIP_SENDER_MAIN, 209);
                    AddGossipItemFor(player, 1, "+ 50 Stamina", GOSSIP_SENDER_MAIN, 218);
                    AddGossipItemFor(player, 1, "Berserking", GOSSIP_SENDER_MAIN, 212);
                    AddGossipItemFor(player, 1, "Battlemaster", GOSSIP_SENDER_MAIN, 215);
                    AddGossipItemFor(player, 1, "Icebreaker", GOSSIP_SENDER_MAIN, 216);
                    AddGossipItemFor(player, 1, "Lifeward", GOSSIP_SENDER_MAIN, 217);
                    AddGossipItemFor(player, 1, "Mongoose", GOSSIP_SENDER_MAIN, 223);
                    AddGossipItemFor(player, 1, "Blade Ward", GOSSIP_SENDER_MAIN, 210);
                    AddGossipItemFor(player, 1, "Blood Draining", GOSSIP_SENDER_MAIN, 211);
                    AddGossipItemFor(player, 1, "Executioner", GOSSIP_SENDER_MAIN, 222);
                    AddGossipItemFor(player, 1, "Titanium Weapon Chain", GOSSIP_SENDER_MAIN, 224);
                    AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                }

                player->PlayerTalkClass->SendGossipMenu(100003, creature->GetGUID());
                return true;
                break;

            case 2: // Enchant 2H Weapon
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
                if (!item)
                {
                    creature->Whisper("This enchant needs a 2H weapon equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    return false;
                }
                if(item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
                {
                    AddGossipItemFor(player, 1, "+ 110 Attack Power", GOSSIP_SENDER_MAIN, 117);
                    AddGossipItemFor(player, 1, "+ 81 Spell Power", GOSSIP_SENDER_MAIN, 115);
                    AddGossipItemFor(player, 1, "+ 35 Agility", GOSSIP_SENDER_MAIN, 116);
                    AddGossipItemFor(player, 1, "+ 45 Spirit", GOSSIP_SENDER_MAIN, 250);
                    AddGossipItemFor(player, 1, "Berserking", GOSSIP_SENDER_MAIN, 104);
                    AddGossipItemFor(player, 1, "Mongoose", GOSSIP_SENDER_MAIN, 113);
                    AddGossipItemFor(player, 1, "Executioner", GOSSIP_SENDER_MAIN, 114);
                    AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                }
                else
                {
                    creature->Whisper("This enchant needs a 2H weapon equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                }
                player->PlayerTalkClass->SendGossipMenu(100003, creature->GetGUID());
                return true;
                break;

            case 3: // Enchant Shield
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
                if (!item)
                {
                    creature->Whisper("This enchant needs a shield equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    return false;
                }
                if (item->GetTemplate()->InventoryType == INVTYPE_SHIELD)
                {
                    AddGossipItemFor(player, 1, "+ 20 Defense Rating", GOSSIP_SENDER_MAIN, 118);
                    AddGossipItemFor(player, 1, "+ 25 Intellect", GOSSIP_SENDER_MAIN, 119);
                    AddGossipItemFor(player, 1, "+ 12 Resilience", GOSSIP_SENDER_MAIN, 120);
                    AddGossipItemFor(player, 1, "+ 15 Block Rating", GOSSIP_SENDER_MAIN, 205);
                    AddGossipItemFor(player, 1, "+ 18 Stamina", GOSSIP_SENDER_MAIN, 122);
                    AddGossipItemFor(player, 1, "+ 36 Block Value", GOSSIP_SENDER_MAIN, 123);
                    AddGossipItemFor(player, 1, "Titanium Plating", GOSSIP_SENDER_MAIN, 121);
                    AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                }
                else
                {
                    creature->Whisper("This enchant needs a shield equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                }
                player->PlayerTalkClass->SendGossipMenu(100004, creature->GetGUID());
                return true;
                break;

            case 15: // Enchant Ranged Weapon
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED);
                if (!item)
                {
                    creature->Whisper("This enchant needs a ranged weapon equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    return false;
                }
                if (item && item->GetTemplate()->Class == ITEM_CLASS_WEAPON && (item->GetTemplate()->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW ||
                item->GetTemplate()->SubClass == ITEM_SUBCLASS_WEAPON_GUN || item->GetTemplate()->SubClass == ITEM_SUBCLASS_WEAPON_BOW))
                {
                    AddGossipItemFor(player, 1, "+ 15 Damage (Scope)", GOSSIP_SENDER_MAIN, 240);
                    AddGossipItemFor(player, 1, "+ 30 Ranged Hit Rating", GOSSIP_SENDER_MAIN, 241);
                    AddGossipItemFor(player, 1, "+ 40 Ranged Haste Rating", GOSSIP_SENDER_MAIN, 242);
                    AddGossipItemFor(player, 1, "+ 40 Ranged Critical Strike", GOSSIP_SENDER_MAIN, 243);
                    AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                }
                else
                {
                    creature->Whisper("This enchant needs a ranged weapon equiped.", LANG_UNIVERSAL, player);
                    CloseGossipMenuFor(player);
                    return false;
                }
                player->PlayerTalkClass->SendGossipMenu(100015, creature->GetGUID());
                return true;
                break;

            case 4: // Enchant Head
                AddGossipItemFor(player, 1, "+ 30 Spell Power & 10 MP5", GOSSIP_SENDER_MAIN, 124);
                AddGossipItemFor(player, 1, "+ 30 Spell Power & 20 Critical Strike", GOSSIP_SENDER_MAIN, 125);
                AddGossipItemFor(player, 1, "+ 29 Spell Power & 20 Resilience", GOSSIP_SENDER_MAIN, 126);
                AddGossipItemFor(player, 1, "+ 30 Stamina & 25 Resilience", GOSSIP_SENDER_MAIN, 127);
                AddGossipItemFor(player, 1, "+ 37 Stamina & 20 Defense", GOSSIP_SENDER_MAIN, 128);
                AddGossipItemFor(player, 1, "+ 50 Attack Power & 20 Critical strike", GOSSIP_SENDER_MAIN, 129);
                AddGossipItemFor(player, 1, "+ 50 Attack Power & 20 Resilience", GOSSIP_SENDER_MAIN, 130);
                AddGossipItemFor(player, 1, "+ 20 Arcane Resist & 30 Stamina", GOSSIP_SENDER_MAIN, 131);
                AddGossipItemFor(player, 1, "+ 25 Fire Resist & 30 Stamina", GOSSIP_SENDER_MAIN, 132);
                AddGossipItemFor(player, 1, "+ 25 Shadow Resist & 30 Stamina", GOSSIP_SENDER_MAIN, 133);
                AddGossipItemFor(player, 1, "+ 20 Frost Resist & 30 Stamina", GOSSIP_SENDER_MAIN, 134);
                AddGossipItemFor(player, 1, "+ 25 Nature Resist & 30 Stamina", GOSSIP_SENDER_MAIN, 135);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100005, creature->GetGUID());
                return true;
                break;

            case 5: // Enchant Shoulders
                if (player->GetSkillValue(SKILL_INSCRIPTION) >= 450)
                {
                    AddGossipItemFor(player, 1, "120 Attack Power & 15 Critical Strike", GOSSIP_SENDER_MAIN, 136);
                    AddGossipItemFor(player, 1, "70 Spell Power & 8 MP5", GOSSIP_SENDER_MAIN, 137);
                    AddGossipItemFor(player, 1, "60 Dodge Rating & 15 Parry Rating", GOSSIP_SENDER_MAIN, 138);
                    AddGossipItemFor(player, 1, "70 Spell Power & 15 Critical Strike", GOSSIP_SENDER_MAIN, 139);
                }
                AddGossipItemFor(player, 1, "+ 40 Attack Power & 15 Critical Strike", GOSSIP_SENDER_MAIN, 140);
                AddGossipItemFor(player, 1, "+ 21 Intellect & 16 Spirit", GOSSIP_SENDER_MAIN, 141);
                AddGossipItemFor(player, 1, "+ 20 Dodge Rating & 22 Stamina", GOSSIP_SENDER_MAIN, 142);
                AddGossipItemFor(player, 1, "+ 30 Stamina & 15 Resilience", GOSSIP_SENDER_MAIN, 143);
                AddGossipItemFor(player, 1, "+ 24 Spell Power & 15 Critical Strike", GOSSIP_SENDER_MAIN, 144);
                AddGossipItemFor(player, 1, "+ 23 Spell Power & 15 Resilience", GOSSIP_SENDER_MAIN, 145);
                AddGossipItemFor(player, 1, "+ 40 Attack Power & 15 Resilience", GOSSIP_SENDER_MAIN, 146);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100006, creature->GetGUID());
                return true;
                break;

            case 6: // Enchant Cloak
                if (player->GetSkillValue(SKILL_TAILORING) >= 450)
                {
                    AddGossipItemFor(player, 1, "Chance to restore 400 Mana", GOSSIP_SENDER_MAIN, 149);
                    AddGossipItemFor(player, 1, "Chance to increase Spell Power by 295", GOSSIP_SENDER_MAIN, 150);
                    AddGossipItemFor(player, 1, "Chance to increase Attack Power by 400", GOSSIP_SENDER_MAIN, 151);
                }
                if (player->GetSkillValue(SKILL_ENGINEERING) >= 450)
                {
                    AddGossipItemFor(player, 1, "Parachute (Spell Power)", GOSSIP_SENDER_MAIN, 147);
                    AddGossipItemFor(player, 1, "Parachute (Agility)", GOSSIP_SENDER_MAIN, 401);
                }
                AddGossipItemFor(player, 1, "+ 20 Shadow Resist", GOSSIP_SENDER_MAIN, 148);
                AddGossipItemFor(player, 1, "+ 20 Arcane Resist", GOSSIP_SENDER_MAIN, 238);
                AddGossipItemFor(player, 1, "+ 20 Nature Resist", GOSSIP_SENDER_MAIN, 235);
                AddGossipItemFor(player, 1, "+ 20 Frost Resist", GOSSIP_SENDER_MAIN, 236);
                AddGossipItemFor(player, 1, "+ 20 Fire Resist", GOSSIP_SENDER_MAIN, 237);
                AddGossipItemFor(player, 1, "+ 10 Spirit", GOSSIP_SENDER_MAIN, 152);
                AddGossipItemFor(player, 1, "+ 16 Dodge Rating", GOSSIP_SENDER_MAIN, 153);
                AddGossipItemFor(player, 1, "+ 35 Spell Penetration", GOSSIP_SENDER_MAIN, 154);
                AddGossipItemFor(player, 1, "+ 225 Armor", GOSSIP_SENDER_MAIN, 155);
                AddGossipItemFor(player, 1, "+ 22 Agility", GOSSIP_SENDER_MAIN, 156);
                AddGossipItemFor(player, 1, "+ 23 Haste Rating", GOSSIP_SENDER_MAIN, 157);
                AddGossipItemFor(player, 1, "+ 10 Agility & Increased stealth", GOSSIP_SENDER_MAIN, 260);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100007, creature->GetGUID());
                return true;
                break;

            case 7: //Enchant chest
                AddGossipItemFor(player, 1, "+ All Stats by 10", GOSSIP_SENDER_MAIN, 158);
                AddGossipItemFor(player, 1, "+ 275 Health", GOSSIP_SENDER_MAIN, 159);
                AddGossipItemFor(player, 1, "+ 10 Mana per 5second", GOSSIP_SENDER_MAIN, 160);
                AddGossipItemFor(player, 1, "+ 20 Resilience", GOSSIP_SENDER_MAIN, 161);
                AddGossipItemFor(player, 1, "+ 22 Dodge Rating", GOSSIP_SENDER_MAIN, 162);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100008, creature->GetGUID());
                return true;
                break;

            case 14: //Enchants Waists
                if (player->GetSkillValue(SKILL_ENGINEERING) >= 450)
                {
                    AddGossipItemFor(player, 1, "Cobalt Frag Bomb", GOSSIP_SENDER_MAIN, 226);
                }
                AddGossipItemFor(player, 1, "+ 1 Prismatic Socket", GOSSIP_SENDER_MAIN, 227);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100014, creature->GetGUID());
                return true;
                break;

            case 8: //Enchant Bracers
                if (player->GetSkillValue(SKILL_BLACKSMITHING) >= 450)
                {
                    AddGossipItemFor(player, 1, "+ 1 Prismatic Socket", GOSSIP_SENDER_MAIN, 231);
                }
                AddGossipItemFor(player, 1, "+ 40 Stamina", GOSSIP_SENDER_MAIN, 163);
                AddGossipItemFor(player, 1, "+ 30 Spell Power", GOSSIP_SENDER_MAIN, 164);
                AddGossipItemFor(player, 1, "+ 50 Attack Power", GOSSIP_SENDER_MAIN, 165);
                AddGossipItemFor(player, 1, "+ 18 Spirit", GOSSIP_SENDER_MAIN, 166);
                AddGossipItemFor(player, 1, "+ 15 Expertise Rating", GOSSIP_SENDER_MAIN, 167);
                AddGossipItemFor(player, 1, "+ All Stats by 6", GOSSIP_SENDER_MAIN, 168);
                AddGossipItemFor(player, 1, "+ 16 Intellect", GOSSIP_SENDER_MAIN, 169);
                if (player->GetSkillValue(SKILL_LEATHERWORKING) >= 450)
                {
                    AddGossipItemFor(player, 1, "+ 70 Arcane Resist", GOSSIP_SENDER_MAIN, 170);
                    AddGossipItemFor(player, 1, "+ 70 Fire Resist", GOSSIP_SENDER_MAIN, 171);
                    AddGossipItemFor(player, 1, "+ 70 Frost Resist", GOSSIP_SENDER_MAIN, 172);
                    AddGossipItemFor(player, 1, "+ 70 Nature Resist", GOSSIP_SENDER_MAIN, 173);
                    AddGossipItemFor(player, 1, "+ 70 Shadow Resist", GOSSIP_SENDER_MAIN, 174);
                    AddGossipItemFor(player, 1, "+ 130 Attack Power", GOSSIP_SENDER_MAIN, 175);
                    AddGossipItemFor(player, 1, "+ 102 Stamina", GOSSIP_SENDER_MAIN, 176);
                    AddGossipItemFor(player, 1, "+ 76 Spell Power", GOSSIP_SENDER_MAIN, 177);
                }
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100009, creature->GetGUID());
                return true;
                break;

            case 9: //Enchant Gloves
                if (player->GetSkillValue(SKILL_BLACKSMITHING) >= 450)
                {
                    AddGossipItemFor(player, 1, "+ 1 Prismatic Socket", GOSSIP_SENDER_MAIN, 229);
                }
                AddGossipItemFor(player, 1, "+ 44 Attack Power", GOSSIP_SENDER_MAIN, 180);
                AddGossipItemFor(player, 1, "+ 28 Spell Power", GOSSIP_SENDER_MAIN, 400);
                AddGossipItemFor(player, 1, "+ 10 Critical Strike", GOSSIP_SENDER_MAIN, 178);
                AddGossipItemFor(player, 1, "+ 2 Percents threat & 10 Parry Rating", GOSSIP_SENDER_MAIN, 179);
                AddGossipItemFor(player, 1, "+ 20 Agility", GOSSIP_SENDER_MAIN, 181);
                AddGossipItemFor(player, 1, "+ 20 Hit Rating", GOSSIP_SENDER_MAIN, 182);
                AddGossipItemFor(player, 1, "+ 15 Expertise Rating", GOSSIP_SENDER_MAIN, 183);
                if (player->GetSkillValue(SKILL_ENGINEERING) >= 450)
                {
                    AddGossipItemFor(player, 1, "Hand-Mounted Pyro Rocket", GOSSIP_SENDER_MAIN, 199);
                    AddGossipItemFor(player, 1, "Haste Rating by 340 for 12sec", GOSSIP_SENDER_MAIN, 200);
                    AddGossipItemFor(player, 1, "250 Armor", GOSSIP_SENDER_MAIN, 201);
                }
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100010, creature->GetGUID());
                return true;
                break;

            case 10: //Enchant legs
                AddGossipItemFor(player, 1, "+ 40 Resilience & 28 Stamina", GOSSIP_SENDER_MAIN, 184);
                AddGossipItemFor(player, 1, "+ 55 Stamina & 22 Agility", GOSSIP_SENDER_MAIN, 185);
                AddGossipItemFor(player, 1, "+ 75 Attack Power & 22 Critical Strike", GOSSIP_SENDER_MAIN, 186);
                AddGossipItemFor(player, 1, "+ 50 Spell Power & 20 Spirit", GOSSIP_SENDER_MAIN, 187);
                AddGossipItemFor(player, 1, "+ 50 Spell Power & 30 Stamina", GOSSIP_SENDER_MAIN, 188);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100011, creature->GetGUID());
                return true;
                break;

            case 11: //Enchant feet
                AddGossipItemFor(player, 1, "+ 32 Attack Power", GOSSIP_SENDER_MAIN, 191);
                AddGossipItemFor(player, 1, "+ 15 Stamina & Movement Speed", GOSSIP_SENDER_MAIN, 192);
                AddGossipItemFor(player, 1, "+ 16 Agility", GOSSIP_SENDER_MAIN, 193);
                AddGossipItemFor(player, 1, "+ 18 Spirit", GOSSIP_SENDER_MAIN, 194);
                AddGossipItemFor(player, 1, "+ 7 Mana per 5 second", GOSSIP_SENDER_MAIN, 195);
                AddGossipItemFor(player, 1, "+ 12 Critical Strike & Hit Rating", GOSSIP_SENDER_MAIN, 196);
                AddGossipItemFor(player, 1, "+ 22 Stamina", GOSSIP_SENDER_MAIN, 197);
                if (player->GetSkillValue(SKILL_ENGINEERING) >= 450)
                {
                    AddGossipItemFor(player, 1, "Nitro Boots", GOSSIP_SENDER_MAIN, 198);
                }
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100012, creature->GetGUID());
                return true;
                break;

            case 12: //Enchant rings
                AddGossipItemFor(player, 1, "40 Attack Power", GOSSIP_SENDER_MAIN, 202);
                AddGossipItemFor(player, 1, "23 Spell Power", GOSSIP_SENDER_MAIN, 203);
                AddGossipItemFor(player, 1, "30 Stamina", GOSSIP_SENDER_MAIN, 204);
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100013, creature->GetGUID());
                return true;
                break;



            //    MAIN HANDS
            case 100:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_AGILITY_1H);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 101:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_SPIRIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 102:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BLADE_WARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 103:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BLOOD_DRAINING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 104:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BERSERKING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 105:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_ACCURACY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 106:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BLACK_MAGIC);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 107:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BATTLEMASTER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 108:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_ICEBREAKER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 109:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_LIFEWARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 110:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_TITANGUARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 111:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_POTENCY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 112:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_MIGHTY_SPELL_POWER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 207:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_WEAPON_CHAIN);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 114:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_EXECUTIONER);
                player->PlayerTalkClass->SendCloseGossip();
                break;
                
            case 269:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_SPELLSURGE);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    OFFHANDS
            case 208:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_AGILITY_1H);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 209:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_SPIRIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 210:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_BLADE_WARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 211:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_BLOOD_DRAINING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 212:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_BERSERKING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 215:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_BATTLEMASTER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 216:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_ICEBREAKER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 217:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_LIFEWARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 218:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_TITANGUARD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 219:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_POTENCY);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            case 221:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_WEAPON_CHAIN);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 222:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_EXECUTIONER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 223:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_2WEP_MONGOOSE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 224:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_WEAPON_CHAIN);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    2HAND WEPS
            case 113:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2WEP_MONGOOSE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 115:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2WEP_GREATER_SPELL_POWER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 116:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2WEP_AGILITY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 117:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2WEP_MASSACRE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 250:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2WEP_SPIRIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    SHIELDS
            case 118:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_DEFENSE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 119:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_INTELLECT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 120:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_RESILIENCE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 121:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_TITANIUM_PLATING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 122:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_STAMINA);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 123:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_TOUGHSHIELD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 205:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_BLOCK);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    RANGED WEAPON
            case 240:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED), ENCHANT_RANGED_SCOPE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 241:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED), ENCHANT_RANGED_HIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 242:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED), ENCHANT_RANGED_HASTE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 243:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED), ENCHANT_RANGED_CRIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    HEADS
            case 124:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_BLISSFUL_MENDING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 125:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_BURNING_MYSTERIES);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 126:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_DOMINANCE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 127:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_SAVAGE_GLADIATOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 128:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_STALWART_PROTECTOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 129:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_TORMENT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 130:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_TRIUMPH);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 131:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_ECLIPSED_MOON);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 132:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_FLAME_SOUL);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 133:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_FLEEING_SHADOW);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 134:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_FROSTY_SOUL);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 135:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_TOXIC_WARDING);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    SHOULDERS
            case 136:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_MASTERS_AXE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 137:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_MASTERS_CRAG);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 138:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_MASTERS_PINNACLE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 139:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_MASTERS_STORM);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 140:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_GREATER_AXE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 141:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_GREATER_CRAG);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 142:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_GREATER_GLADIATOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 143:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_GREATER_PINNACLE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 144:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_GREATER_STORM);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 145:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_DOMINANCE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 146:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_TRIUMPH);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    BACKS
            case 147:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_SPRINGY_ARACHNOWEAVE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 401:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_FLEXWEAVE_UNDERLAY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 148:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_SHADOW_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 149:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_DARKGLOW_EMBROIDERY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 150:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_LIGHTWEAVE_EMBROIDERY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 151:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_SWORDGUARD_EMBROIDERY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 152:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_WISDOM);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 153:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_TITANWEAVE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 154:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_SPELL_PIERCING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 155:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_MIGHTY_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 156:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_MAJOR_AGILITY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 157:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_GREATER_SPEED);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 235:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_NATURE_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 236:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_FROST_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 237:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_FIRE_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 238:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_ARCANE_ARMOR);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 260:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_STEALTH_AGI);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    CHESTS
            case 158:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_POWERFUL_STATS);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 159:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_SUPER_HEALTH);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 160:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_GREATER_MAINA_REST);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 161:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_EXCEPTIONAL_RESIL);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 162:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_GREATER_DEFENSE);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    WAIST
            case 226:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST), ENCHANT_WAISTS_FRAG_BELT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 227:
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST);
                if (!item)
                {
                    player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
                    return false;
                }
                player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST)->SetEnchantment(PRISMATIC_ENCHANTMENT_SLOT, ENCHANT_WAISTS_ETERNAL_BELT_BUCKLE, 0, 0, player->GetGUID());
                player->GetSession()->SendNotification("|cff0000FF+1 Prismatic Socket |cffFF0000succesfully added!");
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    WRISTS
            case 163:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_MAJOR_STAMINA);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 164:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_SUPERIOR_SP);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 165:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_GREATER_ASSUALT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 166:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_MAJOR_SPIRT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 167:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_EXPERTISE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 168:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_GREATER_STATS);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 169:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_INTELLECT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 170:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_ARCANE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 171:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_FIRE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 172:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_FROST);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 173:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_NATURE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 174:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_SHADOW);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 175:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_ATTACK);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 176:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_STAMINA);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 177:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_FURL_SPELLPOWER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 231:
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);
                if (!item)
                {
                    player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
                    return false;
                }
                player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS)->SetEnchantment(PRISMATIC_ENCHANTMENT_SLOT, ENCHANT_BRACERS_SOCKET_BRACER, 0, 0, player->GetGUID());
                player->GetSession()->SendNotification("|cff0000FF+1 Prismatic Socket |cffFF0000succesfully added!");
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    HANDS
            case 178:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_GREATER_BLASTING);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 179:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_ARMSMAN);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 180:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_CRUSHER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 181:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_AGILITY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 182:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_PRECISION);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 183:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_EXPERTISE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 400:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_SPELL_POWER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 229:
                item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);
                if (!item)
                {
                    player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
                    return false;
                }
                player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS)->SetEnchantment(PRISMATIC_ENCHANTMENT_SLOT, ENCHANT_GLOVES_SOCKET_GLOVES, 0, 0, player->GetGUID());
                player->GetSession()->SendNotification("|cff0000FF+1 Prismatic Socket |cffFF0000succesfully added!");
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 199:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_PYRO_ROCKET);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 200:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_HYPERSPEED);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 201:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_ARMOR_WEBBING);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    LEGS
            case 184:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEG_EARTHEN);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 185:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEG_FROSTHIDE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 186:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEG_ICESCALE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 187:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEG_BRILLIANT_SPELLTHREAD);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 188:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEG_SAPPHIRE_SPELLTHREAD);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    FEETS
            case 191:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_GREATER_ASSULT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 192:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_TUSKARS_VITLIATY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 193:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_SUPERIOR_AGILITY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 194:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_GREATER_SPIRIT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 195:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_GREATER_VITALITY);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 196:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_ICEWALKER);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 197:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_GREATER_FORTITUDE);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 198:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_NITRO_BOOTS);
                player->PlayerTalkClass->SendCloseGossip();
                break;


            //    FINGERS
            case 202:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_ASSULT);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_ASSULT);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 203:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_GREATER_SP);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_GREATER_SP);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 204:
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_STAMINA);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_STAMINA);
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 300: {
                AddGossipItemFor(player, GOSSIP_ICON_TALK, "Welcome to the enchanting NPC!", GOSSIP_SENDER_MAIN, 0);
                AddGossipItemFor(player, 1, "Enchant Head", GOSSIP_SENDER_MAIN, 4);
                AddGossipItemFor(player, 1, "Enchant Shoulders", GOSSIP_SENDER_MAIN, 5);
                AddGossipItemFor(player, 1, "Enchant Cloak", GOSSIP_SENDER_MAIN, 6);
                AddGossipItemFor(player, 1, "Enchant Chest", GOSSIP_SENDER_MAIN, 7);
                AddGossipItemFor(player, 1, "Enchant Waist", GOSSIP_SENDER_MAIN, 14);
                AddGossipItemFor(player, 1, "Enchant Bracers", GOSSIP_SENDER_MAIN, 8);
                AddGossipItemFor(player, 1, "Enchant Gloves", GOSSIP_SENDER_MAIN, 9);
                AddGossipItemFor(player, 1, "Enchant Legs", GOSSIP_SENDER_MAIN, 10);
                AddGossipItemFor(player, 1, "Enchant Feet", GOSSIP_SENDER_MAIN, 11);
                AddGossipItemFor(player, 1, "Enchant 2H Weapon", GOSSIP_SENDER_MAIN, 2);
                AddGossipItemFor(player, 1, "Enchant Main Hand", GOSSIP_SENDER_MAIN, 1);
                AddGossipItemFor(player, 1, "Enchant Off Hand", GOSSIP_SENDER_MAIN, 13);
                AddGossipItemFor(player, 1, "Enchant Shield", GOSSIP_SENDER_MAIN, 3);
                AddGossipItemFor(player, 1, "Enchant Ranged Weapon", GOSSIP_SENDER_MAIN, 15);
                if (player->GetSkillValue(SKILL_ENCHANTING) >= 450)
                    AddGossipItemFor(player, 1, "Enchant Rings", GOSSIP_SENDER_MAIN, 12);
                player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
                return true;
                break;

            }
        }
        return true;
    }
};
void AddSC_npc_enchantment()
{
    new npc_enchantment();
}

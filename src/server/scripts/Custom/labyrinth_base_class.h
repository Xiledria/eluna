#include "CreatureAI.h"
#include "TemporarySummon.h"
#include "G3D/Quat.h"

struct labyrint_base_classAI : public CreatureAI
{
    struct Field
    {
        bool connected = false;
        bool walkable = true;
        uint8 x;
        uint8 y;
    };

    struct OrientedPoint
    {
        OrientedPoint()
        {
            x = 0; y = 0; z = 0; o = 0;
        }

        void SetPosition(Position& pos)
        {
            x = pos.m_positionX;
            y = pos.m_positionY;
            z = pos.m_positionZ;
            o = pos.GetOrientation();
        }

        OrientedPoint(float x, float y, float z, float o) { this->x = x; this->y = y; this->z = z; this->o = o; }
        float x, y, z, o;
    };

    Field** fields;
    std::list<Field*> disconnected;
    std::list<Field*> notChecked;
    bool canGetOnRoof;
    bool hasShelter;
    OrientedPoint start;
    OrientedPoint current;
    Map* map;
    ObjectGuid finishGUID;
    uint32 LABYRINT_SIZE;
    uint32 LABYRINT_FLOORS;
    uint32 LABYRINT_FLOOR_PARTS;
    uint32 LABYRINT_FINISH;
#define LABYRINT_PART 600000
#define PART_WIDTH 3.0f
#define PART_HEIGHT 4.0f
#define SHELTER_ID 200332
#define SHELTER_HEIGHT 0.1f
#define STAIRS_PART 2200109
#define STAIRS_SIZE 1.0f
#define STAIRS_HEIGHT 2.0f

    labyrint_base_classAI(Creature* creature, uint32 size, uint32 finishId, bool hasShelter = false, bool walkableRoof = false, uint32 floors = 1, uint32 floorParts = 1)
        : CreatureAI(creature)
    {
        this->hasShelter = hasShelter;
        canGetOnRoof = walkableRoof;
        start.SetPosition(*creature);
        current = start;
        LABYRINT_SIZE = size;
        fields = new Field*[LABYRINT_SIZE];
        for (uint32 i = 0; i < LABYRINT_SIZE; ++i)
            fields[i] = new Field[LABYRINT_SIZE];
        LABYRINT_FLOORS = floors;
        LABYRINT_FLOOR_PARTS = floorParts;
        LABYRINT_FINISH = finishId;
        // if already built, do not change it
        if (!me->GetGameObject(0))
            BuildLabyrint();
        else
        {
            Creature* finish = me->FindNearestCreature(LABYRINT_FINISH, 100.0f);
            if (finish)
                finishGUID = finish->GetGUID();
        }

        map = me->GetMap();
    }

    virtual ~labyrint_base_classAI()
    {
        // deleting
        if (!me->FindMap())
            if (Creature* creature = map->GetCreature(finishGUID))
                creature->DespawnOrUnsummon();

        for (uint32 i = 0; i < LABYRINT_SIZE; ++i)
            delete[] fields[i];
        delete[] fields;
    }

    void Advance(OrientedPoint& p)
    {
        p.x += PART_WIDTH * sin(p.o);
        p.y -= PART_WIDTH * cos(p.o);
    }

    void Rotate(OrientedPoint& p)
    {
        float addx = PART_WIDTH / 2.0f * sin(p.o);
        float addy = PART_WIDTH / 2.0f * cos(p.o);
        bool add = p.o > 3 * G3D::halfPi() || (p.o >= G3D::halfPi() && p.o <= 2 * G3D::halfPi());
        p.o += static_cast<float>(G3D::halfPi());
        if (p.o >= G3D::pif() * 2)
            p.o -= G3D::pif() * 2;
        addx += PART_WIDTH / 2.0f * sin(p.o);
        addy += PART_WIDTH / 2.0f * cos(p.o);
        if (add)
        {
            p.x += addx;
            p.y += addy;
        }
        else
        {
            p.x -= addx;
            p.y -= addy;
        }
    }

    void BuildBlock(OrientedPoint p)
    {
        for (uint32 parts = 0; parts < LABYRINT_FLOOR_PARTS; ++parts)
        {
            for (uint32 sides = 0; sides < 4; ++sides)
            {
                Advance(p);
                me->SummonGameObject(LABYRINT_PART, Position(p.x, p.y, p.z, p.o), G3D::Matrix3::fromEulerAnglesZYX(p.o, 0.0f, 0.0f), 0);
                Rotate(p);
            }
            p.z += PART_HEIGHT;
        }
    }

    void Connect(Field* f)
    {
        if (f->connected)
            return;
        notChecked.push_back(f);
        disconnected.remove(f);
        f->connected = true;
    }

    void TryConnectNeighboars(Field* f)
    {
        if (f->x > 2 && fields[f->x - 1][f->y].walkable)
            Connect(&fields[f->x - 2][f->y]);

        if (f->x < LABYRINT_SIZE - 3 && fields[f->x + 1][f->y].walkable)
            Connect(&fields[f->x + 2][f->y]);

        if (f->y > 2 && fields[f->x][f->y - 1].walkable)
            Connect(&fields[f->x][f->y - 2]);

        if (f->y < LABYRINT_SIZE - 3 && fields[f->x][f->y + 1].walkable)
            Connect(&fields[f->x][f->y + 2]);

    }

    std::list<Field*> GetBlockablePositions(int x, int y)
    {
        std::list<Field*> ret;
        if (fields[x - 1][y].walkable)
            ret.push_back(&fields[x - 1][y]);

        if (fields[x + 1][y].walkable)
            ret.push_back(&fields[x + 1][y]);

        if (fields[x][y - 1].walkable)
            ret.push_back(&fields[x][y - 1]);

        if (fields[x][y + 1].walkable)
            ret.push_back(&fields[x][y + 1]);

        return ret;
    }

    void FixBlockedPaths()
    {
        disconnected.clear();
        notChecked.clear();
        for (uint32 x = 1; x < LABYRINT_SIZE - 1; x += 2)
            for (uint32 y = 1; y < LABYRINT_SIZE - 1; y += 2)
                disconnected.push_back(&fields[x][y]);

        Connect(&fields[1][1]);

        while (!disconnected.empty() /*|| !notChecked.empty()*/)
        {
            while (!notChecked.empty())
            {
                Field* f = *notChecked.begin();
                TryConnectNeighboars(f);
                notChecked.remove(f);
            }

            if (!disconnected.empty())
            {
                bool connected = false;
                std::list<Field*>::iterator itr = disconnected.begin();
                while (!connected)
                {
                    Field* b = *itr;
                    std::list<Field*> validNeighboars = GetNeighboarValidConnections(b);
                    if (!validNeighboars.empty())
                    {
                        auto itr = validNeighboars.begin();
                        if (validNeighboars.size() > 1) // pick random if size > 1
                            std::advance(itr, rand() % (validNeighboars.size() - 1));

                        Field* f = *itr;
                        f->walkable = true;
                        Connect(b);
                        connected = true;
                        itr = disconnected.begin();
                    }
                    else
                        ++itr;
                }
            }
        }
    }

    std::list<Field*> GetNeighboarValidConnections(Field* f)
    {
        std::list<Field*> ret;

        if (f->x > 2 && fields[f->x - 2][f->y].connected)
            ret.push_back(&fields[f->x - 1][f->y]);

        if (f->x < LABYRINT_SIZE - 2 && fields[f->x + 2][f->y].connected)
            ret.push_back(&fields[f->x + 1][f->y]);

        if (f->y > 2 && fields[f->x][f->y - 2].connected)
            ret.push_back(&fields[f->x][f->y - 1]);

        if (f->y < LABYRINT_SIZE - 2 && fields[f->x][f->y + 2].connected)
            ret.push_back(&fields[f->x][f->y + 1]);
        return ret;
    }

    void GenerateLabyrinth(bool firstFloor)
    {
        for (uint32 x = 0; x < LABYRINT_SIZE; ++x)
        {
            for (uint32 y = 0; y < LABYRINT_SIZE; ++y)
            {
                fields[x][y].connected = false; // reset connected for reseting labyrinth
                if (x % 2 == 0 && y % 2 == 0)
                    fields[x][y].walkable = false;
                else
                    fields[x][y].walkable = true;
                fields[x][y].x = x;
                fields[x][y].y = y;
            }
        }

        for (uint32 x = 1; x < LABYRINT_SIZE - 1; x += 2)
        {
            for (uint32 y = 1; y < LABYRINT_SIZE - 1; y += 2)
            {
                std::list<Field*> blockable = GetBlockablePositions(x, y);

                if (blockable.size() < 2)
                    continue;

                int blocked = rand() % blockable.size();

                for (int i = 0; i < blocked; ++i)
                {
                    auto itr = blockable.begin();
                    if (blockable.size() > 1)
                        std::advance(itr, rand() % blockable.size());
                    (*itr)->walkable = false;
                    blockable.erase(itr);
                }
            }
        }
        FixBlockedPaths();
        if (firstFloor)
            fields[0][1].walkable = true;
        else
            fields[0][1].walkable = false;
    }

    void RotateStairs(OrientedPoint& p, bool z = true)
    {
        float addx = STAIRS_SIZE / 2.0f * sin(p.o);
        float addy = STAIRS_SIZE / 2.0f * cos(p.o);
        bool add = p.o > 3 * G3D::halfPi() || (p.o >= G3D::halfPi() && p.o <= 2 * G3D::halfPi());
        p.o += static_cast<float>(G3D::halfPi());
        if (p.o >= G3D::pif() * 2)
            p.o -= G3D::pif() * 2;
        addx += STAIRS_SIZE / 2.0f * sin(p.o);
        addy += STAIRS_SIZE / 2.0f * cos(p.o);
        if (add)
        {
            p.x += addx;
            p.y += addy;
        }
        else
        {
            p.x -= addx;
            p.y -= addy;
        }
        if (z)
            p.z += STAIRS_HEIGHT;
    }

    void AdvanceStairs(OrientedPoint& p)
    {
        p.x += STAIRS_SIZE * sin(p.o);
        p.y -= STAIRS_SIZE * cos(p.o);
    }

    void BuildStairs(OrientedPoint& center)
    {
        OrientedPoint pos(center.x, center.y, center.z - LABYRINT_FLOOR_PARTS * (PART_HEIGHT + SHELTER_HEIGHT), current.o);
        for (uint8 i = 0; i < 2; ++i)
        {
            RotateStairs(pos, false);
            AdvanceStairs(pos);
        }
        pos.o = current.o;
        for (uint32 i = 0; i < PART_HEIGHT / STAIRS_HEIGHT * LABYRINT_FLOOR_PARTS; ++i)
        {
            me->SummonGameObject(STAIRS_PART, Position(pos.x, pos.y, pos.z, pos.o), G3D::Matrix3::fromEulerAnglesZYX(pos.o, 0.0f, 0.0f), 0);
            RotateStairs(pos);
        }
    }

    void BuildBlocks()
    {
        for (uint32 floor = 0; floor < LABYRINT_FLOORS; ++floor)
        {
            GenerateLabyrinth(floor == 0);
            OrientedPoint startCenter(start.x, start.y, floor * LABYRINT_FLOOR_PARTS * (PART_HEIGHT + SHELTER_HEIGHT) + start.z, current.o);
            for (uint32 x = 0; x < LABYRINT_SIZE; ++x)
            {
                OrientedPoint center = startCenter;
                for (uint32 y = 0; y < LABYRINT_SIZE; ++y)
                {
                    if (!fields[x][y].walkable)
                        BuildBlock(center);

                    if (x == LABYRINT_SIZE - 2 && y == LABYRINT_SIZE - 1 && floor == LABYRINT_FLOORS - 1)
                    {
                        if (LABYRINT_FINISH)
                        {
                            float z = center.z + (canGetOnRoof ? LABYRINT_FLOOR_PARTS * (PART_HEIGHT + SHELTER_HEIGHT) + SHELTER_HEIGHT : 0);
                            if (Creature* finish = me->SummonCreature(LABYRINT_FINISH, center.x + PART_WIDTH / 2.0f * cos(center.o), center.y - PART_WIDTH / 2.0f * sin(center.o), z, center.o, TEMPSUMMON_MANUAL_DESPAWN))
                                finishGUID = finish->GetGUID();
                        }
                    }
                    center.y -= PART_WIDTH * cos(center.o);
                    center.x += PART_WIDTH * sin(center.o);
                }
                Rotate(center);
                startCenter.x += PART_WIDTH * sin(center.o);
                startCenter.y -= PART_WIDTH * cos(center.o);
            }
        }
    }

    void BuildShelter()
    {
        current = start;
        for (uint32 floor = 1; floor <= LABYRINT_FLOORS; ++floor)
        {
            OrientedPoint startCenter(start.x, start.y, floor * LABYRINT_FLOOR_PARTS * (PART_HEIGHT + SHELTER_HEIGHT) + start.z, start.o);
            float o = startCenter.o;
            for (uint8 i = 0; i < 3; ++i)
                Rotate(startCenter);
            startCenter.x -= 0.33f * PART_WIDTH * sin(o);
            startCenter.y -= 0.33f * PART_WIDTH * cos(o);
            startCenter.o = o;
            for (uint32 x = 0; x < LABYRINT_SIZE; ++x)
            {
                OrientedPoint center = startCenter;
                for (uint32 y = 0; y < LABYRINT_SIZE; ++y)
                {
                    bool stairs = false;
                    if (canGetOnRoof || floor != LABYRINT_FLOORS)
                    {
                        if ((floor % 2 == 0 && x == 1 && y == 1) || (floor % 2 && x == LABYRINT_SIZE - 2 && y == LABYRINT_SIZE - 2))
                        {
                            BuildStairs(center);
                            stairs = true;
                        }
                    }

                    if (!stairs)
                        me->SummonGameObject(SHELTER_ID, Position(center.x, center.y, center.z, current.o), G3D::Matrix3::fromEulerAnglesZYX(current.o, 0.0f, 0.0f), 0);
                    center.y -= PART_WIDTH * cos(center.o);
                    center.x += PART_WIDTH * sin(center.o);
                }
                Rotate(center);
                startCenter.x += PART_WIDTH * sin(center.o);
                startCenter.y -= PART_WIDTH * cos(center.o);
            }
        }
    }

    void BuildBase()
    {
        for (uint32 floor = 0; floor < LABYRINT_FLOORS; ++floor)
        {
            for (uint32 parts = 0; parts < LABYRINT_FLOOR_PARTS; ++parts)
            {
                for (uint32 sides = 0; sides < 4; ++sides)
                {
                    for (uint32 i = 0; i < LABYRINT_SIZE; ++i)
                    {
                        Advance(current);
                        // skip one block - entrace
                        if (i == 1 && sides == 0)
                            continue;
                        me->SummonGameObject(LABYRINT_PART, Position(current.x, current.y, current.z, current.o), G3D::Matrix3::fromEulerAnglesZYX(current.o, 0.f, 0.f), 0);
                    }
                    Rotate(current);
                }
                current.z += PART_HEIGHT;
            }
            float z = current.z + SHELTER_HEIGHT;
            current = start;
            current.z = z;
        }
    }

    void BuildLabyrint()
    {
        BuildBase();
        BuildBlocks();
        if (hasShelter)
            BuildShelter();
    }

    void UpdateAI(uint32 /*diff*/) override
    {
    }
};

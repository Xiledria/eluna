#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Pet.h"
#include "Player.h"
#include "Creature.h"
#include "WorldSession.h"

#define GOSSIP_ITEM_STABLE "Stable"
#define GOSSIP_ITEM_NEWPET "New Pet"
#define GOSSIP_ITEM_BOAR "Boar"
#define GOSSIP_ITEM_SERPENT "Serpent"
#define GOSSIP_ITEM_SCRAB "Scrab"
#define GOSSIP_ITEM_LION "Lion"
#define GOSSIP_ITEM_WOLF "Wolf"
#define GOSSIP_ITEM_RAVAGER "Ravenger"
#define GOSSIP_ITEM_UNTRAINEPET "Restart Pet"
class npc_hunterpetvendor : public CreatureScript
{
public:
    npc_hunterpetvendor() : CreatureScript("npc_hunterpetvendor") { }
    void CreatePet(Player *player, Creature * m_creature, uint32 entry){
        if(player->getClass() != CLASS_HUNTER) {
            m_creature->Whisper("You are not a Hunter! Shoo!", LANG_UNIVERSAL, player);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            player->PlayerTalkClass->SendCloseGossip();
            return;
        }
        if(player->GetPet()) {
            m_creature->Whisper("Please dismiss your current pet.", LANG_UNIVERSAL, player);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            player->PlayerTalkClass->SendCloseGossip();
            return;
        }
       
        Creature *creatureTarget = m_creature->SummonCreature(entry, player->GetPositionX(), player->GetPositionY()+2, player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 500);
        if(!creatureTarget) return;
       
        Pet* pet = player->CreateTamedPetFrom(creatureTarget, 0);
        if(!pet) return;
        // kill original creature
        creatureTarget->setDeathState(JUST_DIED);
        creatureTarget->RemoveCorpse();
        creatureTarget->SetHealth(0);   // just for nice GM-mode view
        //pet->SetUInt32Value(UNIT_FIELD_PETEXPERIENCE,0);
        //pet->SetUInt32Value(UNIT_FIELD_PETNEXTLEVELEXP, uint32((Trinity::XP::xp_to_level(70))/4));
        // prepare visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel() - 1);
        pet->GetMap()->AddToMap((Creature*)pet);
        // visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());
       
        pet->UpdateAllStats();
       
        // caster have pet now
        player->SetMinion(pet, true);
       
        pet->InitTalentForLevel();
       
        if (player->GetTypeId() == TYPEID_PLAYER)
        {
            // Calc current pet slot
            pet->SavePetToDB(PET_SAVE_AS_CURRENT);
            pet->setPowerType(POWER_FOCUS);
            player->ToPlayer()->PetSpellInitialize();
        }
       
        //end
        player->PlayerTalkClass->SendCloseGossip();
        m_creature->Whisper("Pet added. You might want to feed it and name it somehow.", LANG_UNIVERSAL, player);
        SendGossipMenuFor(player, 1, m_creature->GetGUID());
    }
   
    bool OnGossipHello(Player *player, Creature * m_creature)
    {
        if(player->getClass() != CLASS_HUNTER)
        {
            m_creature->Whisper("You are not a Hunter!", LANG_UNIVERSAL, player);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            player->PlayerTalkClass->SendCloseGossip();
            return true;
        }
        AddGossipItemFor(player, 4, "Get a New Pet.", GOSSIP_SENDER_MAIN, 1000);
        if (player->CanTameExoticPets())
        {
            AddGossipItemFor(player, 4, "Get a New Exotic Pet.", GOSSIP_SENDER_MAIN, 2000);
        }
        AddGossipItemFor(player, 2, "Take me to the Stable.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_STABLEPET);
        AddGossipItemFor(player, 6, "Sell me some Food for my Pet.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_VENDOR);
        AddGossipItemFor(player, 5, "Close Beastmaster Window.", GOSSIP_SENDER_MAIN, 0);
        SendGossipMenuFor(player, 1, m_creature->GetGUID());
        return true;
    }
    bool OnGossipSelect(Player *player, Creature * m_creature, uint32 /*sender*/, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();
        switch (action)
        {
       
        case 5://main menu
            AddGossipItemFor(player, 4, "Get a New Pet.", GOSSIP_SENDER_MAIN, 1000);
            if (player->CanTameExoticPets())
            {
                AddGossipItemFor(player, 4, "Get a New Exotic Pet.", GOSSIP_SENDER_MAIN, 2000);
            }
            AddGossipItemFor(player, 2, "Take me to the Stable.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_STABLEPET);
            AddGossipItemFor(player, 6, "Sell me some Food for my Pet.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_VENDOR);
            AddGossipItemFor(player, 5, "Close Beastmaster Window.", GOSSIP_SENDER_MAIN, 0);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 0://close
            CloseGossipMenuFor(player);
            break;
            /******/case 1000://common pet
            AddGossipItemFor(player, 2, "<- Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 4, "Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 4, "Tenacity Pets.", GOSSIP_SENDER_MAIN, 1500);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
            /*________*/case 1100://common cunning Pets
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "Bat.", GOSSIP_SENDER_MAIN, 1110);
            AddGossipItemFor(player, 4, "Bird of Prey.", GOSSIP_SENDER_MAIN, 1120);
            AddGossipItemFor(player, 4, "Dragonhawk.", GOSSIP_SENDER_MAIN, 1130);
            AddGossipItemFor(player, 4, "Nether Ray.", GOSSIP_SENDER_MAIN, 1150);
            AddGossipItemFor(player, 4, "Spider.", GOSSIP_SENDER_MAIN, 1160);
            AddGossipItemFor(player, 4, "Ravager.", GOSSIP_SENDER_MAIN, 1170);
            AddGossipItemFor(player, 4, "Sporebat.", GOSSIP_SENDER_MAIN, 1180);
            AddGossipItemFor(player, 4, "Serpent.", GOSSIP_SENDER_MAIN, 1190);
            AddGossipItemFor(player, 4, "Wind Serpent.", GOSSIP_SENDER_MAIN, 1200);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 1110://Bat
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Gray.", GOSSIP_SENDER_MAIN, 1111);
            AddGossipItemFor(player, 6, "Brown.", GOSSIP_SENDER_MAIN, 1112);
            AddGossipItemFor(player, 6, "Violet.", GOSSIP_SENDER_MAIN, 1113);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 1114);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 1120://Bird of Prey
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 10000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Eagle.", GOSSIP_SENDER_MAIN, 1121);
            AddGossipItemFor(player, 6, "Owl - Brown.", GOSSIP_SENDER_MAIN, 1123);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1130://Dragonhawk
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Orange.", GOSSIP_SENDER_MAIN, 1131);
            AddGossipItemFor(player, 6, "Pink.", GOSSIP_SENDER_MAIN, 1133);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1134);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;        
        case 1150://Nether Ray
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Black.", GOSSIP_SENDER_MAIN, 1151);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 1152);
            AddGossipItemFor(player, 6, "Green.", GOSSIP_SENDER_MAIN, 1153);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1160://Spider
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Giant Red.", GOSSIP_SENDER_MAIN, 1161);
            AddGossipItemFor(player, 6, "Mine Red.", GOSSIP_SENDER_MAIN, 1163);
            AddGossipItemFor(player, 6, "Mine Olive.", GOSSIP_SENDER_MAIN, 1164);
            AddGossipItemFor(player, 6, "Tarantula Magma.", GOSSIP_SENDER_MAIN, 1165);
            AddGossipItemFor(player, 6, "Tarantula Gray.", GOSSIP_SENDER_MAIN, 1166);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1170://Ravager
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Green.", GOSSIP_SENDER_MAIN, 1171);
            AddGossipItemFor(player, 6, "Orange.", GOSSIP_SENDER_MAIN, 1172);
            AddGossipItemFor(player, 6, "Black-Red.", GOSSIP_SENDER_MAIN, 1173);
            AddGossipItemFor(player, 6, "Blue-Red.", GOSSIP_SENDER_MAIN, 1174);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1180://Sporebat
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 1181);
            AddGossipItemFor(player, 6, "Green.", GOSSIP_SENDER_MAIN, 1182);
            AddGossipItemFor(player, 6, "Yellow.", GOSSIP_SENDER_MAIN, 1183);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1190://Serpent
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<--Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Wyrm Blue.", GOSSIP_SENDER_MAIN, 1191);
            AddGossipItemFor(player, 6, "Cobra Blue.", GOSSIP_SENDER_MAIN, 1194);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1200://Wind Serpent
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 2, "<= Back To Cunning Pets.", GOSSIP_SENDER_MAIN, 1100);
            AddGossipItemFor(player, 6, "Green - Orange.", GOSSIP_SENDER_MAIN, 1201);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1202);
            AddGossipItemFor(player, 6, "Black - Orange", GOSSIP_SENDER_MAIN, 1203);
            AddGossipItemFor(player, 6, "Black - White.", GOSSIP_SENDER_MAIN, 1204);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
            /*________*/case 1300://common Ferocity Pets
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "Carrion Bird.", GOSSIP_SENDER_MAIN, 1310);
            AddGossipItemFor(player, 4, "Dog.", GOSSIP_SENDER_MAIN, 1320);
            AddGossipItemFor(player, 4, "Cat.", GOSSIP_SENDER_MAIN, 1340);
            AddGossipItemFor(player, 4, "Hyena.", GOSSIP_SENDER_MAIN, 1350);
            AddGossipItemFor(player, 4, "Moth.", GOSSIP_SENDER_MAIN, 1360);
            AddGossipItemFor(player, 4, "Raptor.", GOSSIP_SENDER_MAIN, 1370);
            AddGossipItemFor(player, 4, "Wasp.", GOSSIP_SENDER_MAIN, 1380);
            AddGossipItemFor(player, 4, "Tallstrider.", GOSSIP_SENDER_MAIN, 1390);
            AddGossipItemFor(player, 4, "Wolf.", GOSSIP_SENDER_MAIN, 1400);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 1310://Carrion Bird
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Condor - Yellow.", GOSSIP_SENDER_MAIN, 1312);
            AddGossipItemFor(player, 6, "Outland - White.", GOSSIP_SENDER_MAIN, 1313);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1320://Dog
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Mastif - Black.", GOSSIP_SENDER_MAIN, 1325);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1340://Cat
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Lion - Gold.", GOSSIP_SENDER_MAIN, 1341);
            AddGossipItemFor(player, 6, "Lion - White.", GOSSIP_SENDER_MAIN, 1342);
            AddGossipItemFor(player, 6, "Lynx - Red.", GOSSIP_SENDER_MAIN, 1343);
            AddGossipItemFor(player, 6, "Lynx - Yellow.", GOSSIP_SENDER_MAIN, 1344);
            AddGossipItemFor(player, 6, "Tiger - Red.", GOSSIP_SENDER_MAIN, 1345);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1350://Hyena
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Yellow.", GOSSIP_SENDER_MAIN, 1352);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 1353);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1360://Moth
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Beige.", GOSSIP_SENDER_MAIN, 1361);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 1362);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1363);
            AddGossipItemFor(player, 6, "Yellow.", GOSSIP_SENDER_MAIN, 1364);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1370://Raptor
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Gray.", GOSSIP_SENDER_MAIN, 1371);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1372);
            AddGossipItemFor(player, 6, "Obsidian.", GOSSIP_SENDER_MAIN, 1373);
            AddGossipItemFor(player, 6, "Mottled - Blue.", GOSSIP_SENDER_MAIN, 1375);
            AddGossipItemFor(player, 6, "Outland - Yellow.", GOSSIP_SENDER_MAIN, 1376);
            AddGossipItemFor(player, 6, "Outland - Red.", GOSSIP_SENDER_MAIN, 1377);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1390://Tallstrider
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Ivory.", GOSSIP_SENDER_MAIN, 1392);
            AddGossipItemFor(player, 6, "Pink.", GOSSIP_SENDER_MAIN, 1393);
            AddGossipItemFor(player, 6, "Turgoise.", GOSSIP_SENDER_MAIN, 1394);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1400://Wolf
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1300);
            AddGossipItemFor(player, 6, "Dire - Blue / Brown.", GOSSIP_SENDER_MAIN, 1401);
            AddGossipItemFor(player, 6, "Arctic.", GOSSIP_SENDER_MAIN, 1403);
            AddGossipItemFor(player, 6, "Coyote.", GOSSIP_SENDER_MAIN, 1404);
            AddGossipItemFor(player, 6, "Timber Wolf.", GOSSIP_SENDER_MAIN, 1405);
            AddGossipItemFor(player, 6, "Worg - Brown.", GOSSIP_SENDER_MAIN, 1406);
            AddGossipItemFor(player, 6, "Worg - White.", GOSSIP_SENDER_MAIN, 1407);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
            /*________*/case 1500://common Tenacity Pets
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "Turtle.", GOSSIP_SENDER_MAIN, 1510);
            AddGossipItemFor(player, 4, "Crab.", GOSSIP_SENDER_MAIN, 1530);
            AddGossipItemFor(player, 4, "Crocolisk.", GOSSIP_SENDER_MAIN, 1540);
            AddGossipItemFor(player, 4, "Bear.", GOSSIP_SENDER_MAIN, 1550);
            AddGossipItemFor(player, 4, "Boar.", GOSSIP_SENDER_MAIN, 1560);
            AddGossipItemFor(player, 4, "Gorilla.", GOSSIP_SENDER_MAIN, 1570);
            AddGossipItemFor(player, 4, "Warp Stalker.", GOSSIP_SENDER_MAIN, 1580);
            AddGossipItemFor(player, 4, "Scorpid.", GOSSIP_SENDER_MAIN, 1590);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 1510://Turtle
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Green.", GOSSIP_SENDER_MAIN, 1511);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 1512);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1513);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 1514);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;        
        case 1530://Crab
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Bronze.", GOSSIP_SENDER_MAIN, 1532);
            AddGossipItemFor(player, 6, "Saphire.", GOSSIP_SENDER_MAIN, 1533);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1540://Crocolisk
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "River.", GOSSIP_SENDER_MAIN, 1544);
            AddGossipItemFor(player, 6, "Swamp.", GOSSIP_SENDER_MAIN, 1545);
            AddGossipItemFor(player, 6, "Albino.", GOSSIP_SENDER_MAIN, 1546);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1550://Bear
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Brown.", GOSSIP_SENDER_MAIN, 1552);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 1553);
            AddGossipItemFor(player, 6, "Diseased.", GOSSIP_SENDER_MAIN, 1554);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1560://Boar
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Yellow.", GOSSIP_SENDER_MAIN, 1562);
            AddGossipItemFor(player, 6, "Black.", GOSSIP_SENDER_MAIN, 1563);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1570://Gorilla
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Black.", GOSSIP_SENDER_MAIN, 1571);
            AddGossipItemFor(player, 6, "Gray.", GOSSIP_SENDER_MAIN, 1572);
            AddGossipItemFor(player, 6, "Red.", GOSSIP_SENDER_MAIN, 1573);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 1573);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1580://Warp Stalker
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 1583);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
        case 1590://Scorpid
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "<-- Back To Common Pets.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "<= Back To Ferocity Pets.", GOSSIP_SENDER_MAIN, 1500);
            AddGossipItemFor(player, 6, "Golden.", GOSSIP_SENDER_MAIN, 1593);
            AddGossipItemFor(player, 6, "Silver.", GOSSIP_SENDER_MAIN, 1594);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;     
            /******/case 2000://exotic pet
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 4, "Get a New Common Pet.", GOSSIP_SENDER_MAIN, 1000);
            AddGossipItemFor(player, 4, "Cunning Pets.", GOSSIP_SENDER_MAIN, 2100);
            AddGossipItemFor(player, 4, "Ferocity Pets.", GOSSIP_SENDER_MAIN, 2300);
            AddGossipItemFor(player, 4, "Tenacity Pets.", GOSSIP_SENDER_MAIN, 2500);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
            /*________*/case 2100://exotic cunning Pets
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 4, "Chimaera.", GOSSIP_SENDER_MAIN, 2110);
            AddGossipItemFor(player, 4, "Silithid.", GOSSIP_SENDER_MAIN, 2120);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2110://Chimaera
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<- Back To Exotic Cunning Pets.", GOSSIP_SENDER_MAIN, 2100);
            AddGossipItemFor(player, 6, "Beige.", GOSSIP_SENDER_MAIN, 2112);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 2113);
            AddGossipItemFor(player, 6, "Green", GOSSIP_SENDER_MAIN, 2114);
            AddGossipItemFor(player, 6, "Outland - Green.", GOSSIP_SENDER_MAIN, 2115);
            AddGossipItemFor(player, 6, "Outland - Purple.", GOSSIP_SENDER_MAIN, 2116);
            AddGossipItemFor(player, 6, "Outland - White.", GOSSIP_SENDER_MAIN, 2117);
            AddGossipItemFor(player, 6, "Outland - Yellow.", GOSSIP_SENDER_MAIN, 2118);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2120://Silithid
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<- Back To Exotic Cunning Pets.", GOSSIP_SENDER_MAIN, 2100);
            AddGossipItemFor(player, 6, "Violet.", GOSSIP_SENDER_MAIN, 2125);
            AddGossipItemFor(player, 6, "Tan.", GOSSIP_SENDER_MAIN, 2126);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
            /*________*/case 2300://exotic Ferocity Pets
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 4, "Core Hound.", GOSSIP_SENDER_MAIN, 2310);
            AddGossipItemFor(player, 4, "Devilsaur.", GOSSIP_SENDER_MAIN, 2320);
            AddGossipItemFor(player, 4, "Spirit Beast.", GOSSIP_SENDER_MAIN, 2330);
            AddGossipItemFor(player, 4, "Wasp.", GOSSIP_SENDER_MAIN, 2340);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2310://Core Hound
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<- Back To Exotic Ferocity Pets.", GOSSIP_SENDER_MAIN, 2300);
            AddGossipItemFor(player, 6, "Red - Black.", GOSSIP_SENDER_MAIN, 2313);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 2314);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2320://Devilsaur
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<- Back To Exotic Ferocity Pets.", GOSSIP_SENDER_MAIN, 2300);
            AddGossipItemFor(player, 6, "Black.", GOSSIP_SENDER_MAIN, 2321);
            AddGossipItemFor(player, 6, "Green", GOSSIP_SENDER_MAIN, 2322);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 2324);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2330://Spirit Beast
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<- Back To Exotic Ferocity Pets.", GOSSIP_SENDER_MAIN, 2300);
            AddGossipItemFor(player, 6, "Arcturis.", GOSSIP_SENDER_MAIN, 2331);
            AddGossipItemFor(player, 6, "Loque'nahak.", GOSSIP_SENDER_MAIN, 2332);
            AddGossipItemFor(player, 6, "Gondria.", GOSSIP_SENDER_MAIN, 2333);
            AddGossipItemFor(player, 6, "Skoll.", GOSSIP_SENDER_MAIN, 2334);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2340://Wasp
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<= Back To Exotic Ferocity Pets.", GOSSIP_SENDER_MAIN, 2300);
            AddGossipItemFor(player, 6, "FireFly - Green.", GOSSIP_SENDER_MAIN, 2345);
            AddGossipItemFor(player, 6, "FireFly - Black.", GOSSIP_SENDER_MAIN, 2346);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
            /*________*/case 2500://exotic Tenacity Pets
            AddGossipItemFor(player, 2, "<- Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 4, "Rhino.", GOSSIP_SENDER_MAIN, 2510);
            AddGossipItemFor(player, 4, "Worm.", GOSSIP_SENDER_MAIN, 2530);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2510://Rhino
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<= Back To Exotic Tenacity Pets.", GOSSIP_SENDER_MAIN, 2500);
            AddGossipItemFor(player, 6, "Brown.", GOSSIP_SENDER_MAIN, 2512);
            AddGossipItemFor(player, 6, "White.", GOSSIP_SENDER_MAIN, 2514);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case 2530://Worm
            AddGossipItemFor(player, 2, "<== Back to Main Menu.", GOSSIP_SENDER_MAIN, 5);
            AddGossipItemFor(player, 2, "<-- Back To Exotic Pets.", GOSSIP_SENDER_MAIN, 2000);
            AddGossipItemFor(player, 2, "<= Back To Exotic Tenacity Pets.", GOSSIP_SENDER_MAIN, 2500);
            AddGossipItemFor(player, 6, "Blue.", GOSSIP_SENDER_MAIN, 2531);
            AddGossipItemFor(player, 6, "Brown.", GOSSIP_SENDER_MAIN, 2532);
            AddGossipItemFor(player, 6, "Yellow.", GOSSIP_SENDER_MAIN, 2534);
            AddGossipItemFor(player, 6, "Jormungar - Green.", GOSSIP_SENDER_MAIN, 2535);
            AddGossipItemFor(player, 6, "Jormungar - Blue.", GOSSIP_SENDER_MAIN, 2536);
            SendGossipMenuFor(player, 1, m_creature->GetGUID());
            break;
        case GOSSIP_OPTION_STABLEPET:
            player->GetSession()->SendStablePet(m_creature->GetGUID());
            break;
           
        case GOSSIP_OPTION_VENDOR:
            player->GetSession()->SendListInventory(m_creature->GetGUID());
            break;
        /**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**/
        /**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**/
        /*__________ C O M M O N _ _ C U N N I N G _ _ _ P E T S __________*/
        //BAT
        case 1111: //Gray
            CreatePet(player, m_creature, 8927);
            break;
        case 1112: //Brown
            CreatePet(player, m_creature, 1512);
            break;
        case 1113: //Violet
            CreatePet(player, m_creature, 1554);
            break;
        case 1114: //White
            CreatePet(player, m_creature, 28233);
            break;
        //BIRD OF PREY
        case 1121: //Eagle
            CreatePet(player, m_creature, 26369);
            break;
        case 1123: //Owl - Brown
            CreatePet(player, m_creature, 21891);
            break;
        //DRAGONHAWK
        case 1131: //Orange
            CreatePet(player, m_creature, 15649);
            break;
        case 1133: //Pink
            CreatePet(player, m_creature, 20502);
            break;
        case 1134: //Red
            CreatePet(player, m_creature, 18155);
            break;
        //NETHERRAY
        case 1151: //Black
            CreatePet(player, m_creature, 18880);
            break;
        case 1152: //Blue
            CreatePet(player, m_creature, 20196);
            break;
        case 1153: //Green
            CreatePet(player, m_creature, 18130);
            break;
        //SPIDER
        case 1161: //Giant Red
            CreatePet(player, m_creature, 17522);
            break;
        case 1163: //Mine Red
            CreatePet(player, m_creature, 471);
            break;
        case 1164: //Mine Olive
            CreatePet(player, m_creature, 22044);
            break;
        case 1165: //Tarantula Magma
            CreatePet(player, m_creature, 5857);
            break;
        case 1166: //Tarantula Gray
            CreatePet(player, m_creature, 2563);
            break;
        //RAVAGER
        case 1171: //Green
            CreatePet(player, m_creature, 17527);
            break;
        case 1172: //Orange
            CreatePet(player, m_creature, 16933);
            break;
        case 1173: //Black/Red
            CreatePet(player, m_creature, 19350);
            break;
        case 1174: //Blue/Red
            CreatePet(player, m_creature, 23326);
            break;
        //SPOREBAT
        case 1181: //Blue
            CreatePet(player, m_creature, 18128);
            break;
        case 1182: //Green
            CreatePet(player, m_creature, 20387);
            break;
        case 1183: //Yellow
            CreatePet(player, m_creature, 18280);
            break;
        //SERPENT
        case 1191: //Wyrm Blue
            CreatePet(player, m_creature, 26322);
            break;
        case 1194: //Cobra Blue
            CreatePet(player, m_creature, 5048);
            break;
        //WIND SERPENT
        case 1201: //Green - Orange
            CreatePet(player, m_creature, 20797);
            break;
        case 1202: //Red
            CreatePet(player, m_creature, 20749);
            break;
        case 1203: //Black - Orange
            CreatePet(player, m_creature, 20673);
            break;
        case 1204: //Black - White
            CreatePet(player, m_creature, 28477);
            break;
        /*__________ C O M M O N _ _ F E R O C I T Y _ _ P E T S __________*/
        //CARRION BIRD
        case 1312: //Condor - Yellow
            CreatePet(player, m_creature, 428);
            break;
        case 1313: //Outland - White
            CreatePet(player, m_creature, 21515);
            break;
        //DOG
        case 1325: //Mastif - Black
            CreatePet(player, m_creature, 27329);
            break;
            //CAT
        case 1341: //Lion - Gold
            CreatePet(player, m_creature, 3243);
            break;
        case 1342: //Lion - White
            CreatePet(player, m_creature, 3475);
            break;
        case 1343: //Lynx - Red
            CreatePet(player, m_creature, 15372);
            break;
        case 1344: //Lynx - Yellow
            CreatePet(player, m_creature, 15652);
            break;
        case 1345: //Tiger - Red
            CreatePet(player, m_creature, 698);
            break;
        //HYENA
        case 1352: //Yellow
            CreatePet(player, m_creature, 12418);
            break;
        case 1353: //Blue
            CreatePet(player, m_creature, 5427);
            break;
        //MOTH
        case 1361: //Beige
            CreatePet(player, m_creature, 18468);
            break;
        case 1362: //Blue
            CreatePet(player, m_creature, 17349);
            break;
        case 1363: //Red
            CreatePet(player, m_creature, 18437);
            break;
        case 1364: //Yellow
            CreatePet(player, m_creature, 27421);
            break;
        //RAPTOR
        case 1371: //Gray
            CreatePet(player, m_creature, 3634);
            break;
        case 1372: //Red
            CreatePet(player, m_creature, 4356);
            break;
        case 1373: //Obsidian
            CreatePet(player, m_creature, 6508);
            break;
        case 1375: //Mottled - Blue
            CreatePet(player, m_creature, 3633);
            break;
        case 1376: //Outland - Yellow
            CreatePet(player, m_creature, 20634);
            break;
        case 1377: //Outland - Red
            CreatePet(player, m_creature, 20751);
            break;
        //TALLSTRIDER
        case 1392: //Ivory
            CreatePet(player, m_creature, 2957);
            break;
        case 1393: //Pink
            CreatePet(player, m_creature, 3068);
            break;
        case 1394: //Turgoise
            CreatePet(player, m_creature, 2172);
            break;
        //WOLF
        case 1401: //Dire - Blue / Brown
            CreatePet(player, m_creature, 18670);
            break;
        case 1403: //Arctic
            CreatePet(player, m_creature, 704);
            break;
        case 1404: //Coyote
            CreatePet(player, m_creature, 2958);
            break;
        case 1405: //Timber Wolf
            CreatePet(player, m_creature, 299);
            break;
        case 1406: //Worg - Brown
            CreatePet(player, m_creature, 24516);
            break;
        case 1407: //Worg - White
            CreatePet(player, m_creature, 29358);
            break;
        /*__________ C O M M O N _ _ T E N A C I T Y _ _ P E T S __________*/
        //TURTLE
        case 1511: //Green
            CreatePet(player, m_creature, 33710);
            break;
        case 1512: //Blue
            CreatePet(player, m_creature, 3461);
            break;
        case 1513: //Red
            CreatePet(player, m_creature, 14223);
            break;
        case 1514: //White
            CreatePet(player, m_creature, 25482);
            break;
        //CRAB
        case 1532: //Bronze
            CreatePet(player, m_creature, 17217);
            break;
        case 1533: //Saphire
            CreatePet(player, m_creature, 3812);
            break;
        //CROCOLISK
        case 1544: //River
            CreatePet(player, m_creature, 3110);
            break;
        case 1545: //Swamp
            CreatePet(player, m_creature, 4343);
            break;
        case 1546: //Albino
            CreatePet(player, m_creature, 20773);
            break;
        //BEAR
        case 1552: //Brown
            CreatePet(player, m_creature, 3810);
            break;
        case 1553: //White
            CreatePet(player, m_creature, 24547);
            break;
        case 1554: //Diseased
            CreatePet(player, m_creature, 17661);
            break;
        //BOAR
        case 1562: //Yellow
            CreatePet(player, m_creature, 454);
            break;
        case 1563: //Armored - Blue
            CreatePet(player, m_creature, 4512);
            break;
        //GORILLA
        case 1571: //Black
            CreatePet(player, m_creature, 2521);
            break;
        case 1572: //Grey
            CreatePet(player, m_creature, 28098);
            break;
        case 1573: //Red
            CreatePet(player, m_creature, 9622);
            break;
        case 1574: //White
            CreatePet(player, m_creature, 6585);
            break;
        //WARP STALKER
        case 1583: //White
            CreatePet(player, m_creature, 18465);
            break;
        //SCORPID
        case 1593: //Golden
            CreatePet(player, m_creature, 11736);
            break;
        case 1594: //Silver
            CreatePet(player, m_creature, 11735);
            break;
        /**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**/
        /**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**/
        /*__________ E X O T I C _ _ C U N N I N G _ _  P E T S __________*/
        //CHIMAERA
        case 2112: //Beige
            CreatePet(player, m_creature, 8764);
            break;
        case 2113: //Blue
            CreatePet(player, m_creature, 10807);
            break;
        case 2114: //Green
            CreatePet(player, m_creature, 8660);
            break;
        case 2115: //Outland - Green
            CreatePet(player, m_creature, 21879);
            break;
        case 2116: //Outland - Purple
            CreatePet(player, m_creature, 20932);
            break;
        case 2117: //Outland - White
            CreatePet(player, m_creature, 11497);
            break;
        case 2118: //Outland - Yellow
            CreatePet(player, m_creature, 21033);
            break;
        //SILITHID
        case 2125: //Violet
            CreatePet(player, m_creature, 15233);
            break;
        case 2126: //Tan
            CreatePet(player, m_creature, 15230);
            break;
        /*__________ E X O T I C _ _ F E R O C I T Y _ _ P E T S __________*/
        //CORE HOUND               
        case 2313: //Red - Black
            CreatePet(player, m_creature, 11673);
            break;
        case 2314: //White
            CreatePet(player, m_creature, 17447);
            break;
        //DEVILSAUR
        case 2321: //Black
            CreatePet(player, m_creature, 20931);
            break;
        case 2322: //Green
            CreatePet(player, m_creature, 32485);
            break;
        case 2324: //White
            CreatePet(player, m_creature, 6498);
            break;
        //SPIRIT BEAST
        case 2331: //Arcturis
            CreatePet(player, m_creature, 38453);
            break;
        case 2332: //Loque'nahak
            CreatePet(player, m_creature, 32517);
            break;
        case 2333: //Gondria
            CreatePet(player, m_creature, 33776);
            break;
        case 2334: //Skoll
            CreatePet(player, m_creature, 35189);
            break;
        //WASP
        case 2345: //FireFly - Green
            CreatePet(player, m_creature, 18132);
            break;
        case 2346: //FireFly - Black
            CreatePet(player, m_creature, 18283);
            break;
        /*__________ E X O T I C _ _ T E N A C I T Y _ _ P E T S __________*/
        //RHINO
        case 2512: //Brown
            CreatePet(player, m_creature, 25487);
            break;
        case 2514: //White
            CreatePet(player, m_creature, 30445);
            break;
        //WORM
        case 2531: //Blue
            CreatePet(player, m_creature, 11789);
            break;
        case 2532: //Brown
            CreatePet(player, m_creature, 8925);
            break;
        case 2534: //Yellow
            CreatePet(player, m_creature, 14477);
            break;
        case 2535: //Jormungar - Green
            CreatePet(player, m_creature, 26358);
            break;
        case 2536: //Jormungar - Blue
            CreatePet(player, m_creature, 30291);
            break;
        }
        return true;
    }
};
void AddSC_npc_hunterpetvendor()
{
    new npc_hunterpetvendor();
}

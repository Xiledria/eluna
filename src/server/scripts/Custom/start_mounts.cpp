#include "ScriptMgr.h"
#include "Player.h"

class StartMount : public PlayerScript
{
public:
    StartMount() : PlayerScript("StartMount") { }

    void OnLogin(Player* player, bool first) override
    {
        if (!first)
            return;

        uint32 mount;
        switch(player->getOriginalRace())
        {
            case RACE_HUMAN:
                mount = 23229;
                break;
            case RACE_ORC:
                mount = 23250;
                break;
            case RACE_DWARF:
                mount = 23238;
                break;
            case RACE_NIGHTELF:
                mount = 23221;
                break;
            case RACE_UNDEAD_PLAYER:
                mount = 17465;
                break;
            case RACE_TAUREN:
                mount = 23249;
                break;
            case RACE_GNOME:
                mount = 23225;
                break;
            case RACE_TROLL:
                mount = 23241;
                break;
            case RACE_BLOODELF:
                mount = 33660;
                break;
            case RACE_DRAENEI:
                mount = 35713;
                break;
            default:
                return;
        }

        player->LearnSpell(34091, false);
        player->LearnSpell(mount, false);
    }
};

void AddSC_start_mount()
{
    new StartMount();
}

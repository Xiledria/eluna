#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "MapManager.h"
#include "Player.h"
#include "TemporarySummon.h"

#define FIND_PORT_FLOOR_HEIGHT 7

struct find_port_handlerAI : CreatureAI
{
#define CLICK_ME_ENTRY 450034
#define FIND_PORT_FINISH 450035

    std::list<ObjectGuid> teleporterGuids;
    Map* map;
    uint32 floors;

    find_port_handlerAI(Creature* creature, uint8 floors) : CreatureAI(creature)
    {
        this->floors = floors;

        for (uint8 i = 0; i < floors; ++i)
            SpawnFloor(i);

        map = me->GetMap();
    }

    ~find_port_handlerAI()
    {
        for (auto teleporter : teleporterGuids)
            if (Creature* creature = map->GetCreature(teleporter))
                creature->DespawnOrUnsummon();
    }

    void SpawnFloor(uint8 floor)
    {
        std::list<Creature*> teleporters;
        teleporters.push_back(me->SummonCreature(CLICK_ME_ENTRY, me->GetPositionX() + 3, me->GetPositionY() - 0.6f, me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT * floor, 3.115880f, TEMPSUMMON_MANUAL_DESPAWN));
        teleporters.push_back(me->SummonCreature(CLICK_ME_ENTRY, me->GetPositionX() - 2.2f, me->GetPositionY() - 0.5f, me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT * floor, 6.207603f, TEMPSUMMON_MANUAL_DESPAWN));
        teleporters.push_back(me->SummonCreature(CLICK_ME_ENTRY, me->GetPositionX(), me->GetPositionY() + 2, me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT * floor, 4.818233f, TEMPSUMMON_MANUAL_DESPAWN));
        teleporters.push_back(me->SummonCreature(CLICK_ME_ENTRY, me->GetPositionX(), me->GetPositionY() - 3, me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT * floor, 1.494221f, TEMPSUMMON_MANUAL_DESPAWN));
        auto itr = teleporters.begin();
        std::advance(itr, urand(0, static_cast<uint32>(teleporters.size() - 1)));
        (*itr)->SetMaxPower(POWER_HAPPINESS, 1);
        for (auto teleporter : teleporters)
        {
            teleporter->SetMaxPower(POWER_RUNIC_POWER, static_cast<uint32>(me->GetPositionZ()));
            teleporterGuids.push_back(teleporter->GetGUID());
        }
    }

    void UpdateAI(uint32 /*diff*/) override
    {

    }
};

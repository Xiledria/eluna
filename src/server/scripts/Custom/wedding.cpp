#include "CreatureAI.h"
#include "ScriptMgr.h"
#include "Player.h"
#include "Vehicle.h"
#include "SpellAuraEffects.h"
#include "ObjectAccessor.h"

enum WeddingSpells
{
    HEARTH_AURA         = 62002,
};

#define BOAT_WAYPOINTS_COUNT 8
G3D::Vector3 const boatWaypoints[BOAT_WAYPOINTS_COUNT]
{
    { 10244.70f, -7100.87f, 0.00f },
    { 10219.67f, -7084.00f, 0.00f },
    { 10200.50f, -7066.31f, 0.00f },
    { 10195.75f, -7038.00f, 0.00f },
    { 10204.07f, -7015.92f, 0.00f },
    { 10224.54f, -7010.65f, 0.00f },
    { 10233.48f, -7025.51f, 0.00f },
    { 10242.46f, -7028.71f, 0.00f },
};

class wedding_love_boat : public CreatureScript
{
public:
    wedding_love_boat() : CreatureScript("wedding_love_boat") { }

    struct wedding_love_boatAI : CreatureAI
    {
        uint16 timer;
        wedding_love_boatAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 5000;
        }

        void PassengerBoarded(Unit* /*passenger*/, int8 /*seatId*/, bool apply) override
        {
            if (apply)
            {
                if (Vehicle* boat = me->GetVehicleKit())
                {
                    if (!boat->GetAvailableSeatCount())
                    {
                        me->GetMotionMaster()->MoveSmoothPath(1, boatWaypoints, BOAT_WAYPOINTS_COUNT, true);
                    }
                }
            }
        }

        void MovementInform(uint32, uint32 waypoint) override
        {
            if (waypoint != 1)
                return;

            if (Vehicle* boat = me->GetVehicleKit())
            {
                for (SeatMap::const_iterator itr = boat->Seats.begin(); itr != boat->Seats.end(); ++itr)
                {
                    if (Unit* passenger = ObjectAccessor::GetUnit(*me, itr->second.Passenger.Guid))
                    {
                        Position exit(10239.66f, -7031.19f, 1.34f, 4.63f);
                        passenger->_ExitVehicle(&exit);
                    }
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer > diff)
            {
                timer -= diff;
                return;
            }
            timer += 5000;

            if (Vehicle* boat = me->GetVehicleKit())
            {
                if (!boat->GetAvailableSeatCount())
                {
                    for (SeatMap::const_iterator itr = boat->Seats.begin(); itr != boat->Seats.end(); ++itr)
                    {
                        if (Unit* passenger = ObjectAccessor::GetUnit(*me, itr->second.Passenger.Guid))
                        {
                            passenger->AddAura(HEARTH_AURA, passenger);
                        }
                    }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new wedding_love_boatAI(creature);
    }
};


void AddSC_wedding()
{
    new wedding_love_boat();
}

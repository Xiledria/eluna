#include "box_spawner_base_class.h"
#include "teleports_base_class.h"
#include "grunt_guardian_base_class.h"
#include "labyrinth_base_class.h"
#include "Group.h"
#include "GameEventMgr.h"
#include "Vehicle.h"
#include "SpellScript.h"
#include "GridNotifiersImpl.h"
#include "ObjectAccessor.h"
#include "GameObjectAI.h"
#include "ScriptedGossip.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

#define DUMMY_COUNTER_AURA 100004
#define DUMMY_COUNTER_HELPER 100005

enum EventsMask_363
{
    EVENT_MASK_EMPTY         = 0x000,
    EVENT_MASK_PIGS          = 0x001,
    EVENT_MASK_LABYRINT      = 0x002,
    EVENT_MASK_JUMP          = 0x004,
    EVENT_MASK_BOXES         = 0x008,
    EVENT_MASK_FIND_PORT     = 0x010,
    EVENT_MASK_MEADOW        = 0x020,
    EVENT_MASK_DARK_LORD_PVE = 0x040,
    EVENT_MASK_SPIRIT_REALM  = 0x080,
    EVENT_MASK_OBSTACLES     = 0x100,
    EVENT_MASK_ILLUSION      = 0x200,
    EVENT_MASK_DUMMY_COUNTER_DO_NOT_USE_OMG,
    EVENT_MASK_MAX = EVENT_MASK_DUMMY_COUNTER_DO_NOT_USE_OMG - 1
};

EventsMask_363 operator|=(EventsMask_363& a, EventsMask_363 b)
{
    return a = static_cast<EventsMask_363>(static_cast<int32>(a) | static_cast<int32>(b));
}

enum EventId_363 : uint16
{
    EVENT_ID_NO_EVENT           = 0u,
    EVENT_ID_START_ID           = 65530u,
    EVENT_ID_START_ID_PART_1    = EVENT_ID_START_ID,
    EVENT_ID_DARK_LORD_PVE      = 65529u,
    EVENT_ID_MEADOW             = 65530u,
    EVENT_ID_FIND_PORT          = 65531u,
    EVENT_ID_BOXES              = 65532u,
    EVENT_ID_JUMP               = 65533u,
    EVENT_ID_LABYRINT           = 65534u,
    EVENT_ID_PIGS               = 65535u,
    EVENT_ID_MAX_PART_1         = 65535u,
    EVENT_ID_MAX                = EVENT_ID_MAX_PART_1,
};

struct Event_363
{
    Event_363(EventId_363 id, std::vector<EventId_363> dependsOnAnyOf = {}, std::vector<EventId_363> exclusiveWith = {})
    {
        this->id = id;
        this->dependsOnAnyOf = dependsOnAnyOf;
        this->exclusiveWith = exclusiveWith;
    }

    EventId_363 id;
    std::vector<EventId_363> dependsOnAnyOf;
    std::vector<EventId_363> exclusiveWith;
};

bool HasWholeGroupAura(Player* player, uint32 auraId)
{
    if (Group* group = player->GetGroup())
    {
        Group::MemberSlotList const& members = group->GetMemberSlots();
        uint8 groupId = player->GetGroup()->GetMemberGroup(player->GetGUID());
        for (Group::member_citerator itr = members.begin(); itr != members.end(); ++itr)
        {
            if (itr->group != groupId)
                continue;

            Player* groupMember = ObjectAccessor::GetPlayer(*player, itr->guid);
            if (!groupMember)
                return false;

            Aura* aur = groupMember->GetAura(auraId);
            if (!aur)
                return false;
        }
    }
    else
        return false;
    return true;
}

bool HasWholeGroupHelperAuraMask(Player* player, EventsMask_363 mask)
{
    if (Group* group = player->GetGroup())
    {
        Group::MemberSlotList const& members = group->GetMemberSlots();
        uint8 groupId = player->GetGroup()->GetMemberGroup(player->GetGUID());
        for (Group::member_citerator itr = members.begin(); itr != members.end(); ++itr)
        {
            if (itr->group != groupId)
                continue;

            Player* groupMember = ObjectAccessor::GetPlayer(*player, itr->guid);
            if (!groupMember)
                return false;

            Aura* aur = groupMember->GetAura(DUMMY_COUNTER_HELPER);
            if (!aur)
                aur = groupMember->AddAura(DUMMY_COUNTER_HELPER, groupMember);
            if (!aur)
                return false;

            AuraEffect* eff = aur->GetEffect(EFFECT_0);
            if (eff->GetAmount() & mask)
                continue;
            return false;            
        }
    }
    else
        return false;
    return true;
}

int32 GetAuraMask(Player* player, WorldObject* me)
{
    int32 mask = 0;
    if (Group* group = player->GetGroup())
    {
        Group::MemberSlotList const& members = group->GetMemberSlots();
        uint8 groupId = player->GetGroup()->GetMemberGroup(player->GetGUID());
        for (Group::member_citerator itr = members.begin(); itr != members.end(); ++itr)
        {
            if (itr->group != groupId)
                continue;

            Player* groupMember = ObjectAccessor::GetPlayer(*me, itr->guid);
            if (!groupMember)
                continue;

            Aura* aur = groupMember->GetAura(DUMMY_COUNTER_AURA);
            if (!aur)
                aur = groupMember->AddAura(DUMMY_COUNTER_AURA, groupMember);
            if (!aur)
                continue;

            AuraEffect* eff = aur->GetEffect(EFFECT_0);
            if (!eff)
                continue;

            mask |= eff->GetAmount();
        }
    }
    return mask;
}

class event_363_event_handler : public CreatureScript
{
public:
    event_363_event_handler() : CreatureScript("event_363_event_handler") { }

#define DRAGON_VEHICLE_ENTRY 450039

    static EventsMask_363 CalculateEventMask(EventId_363 id)
    {
        return EventsMask_363(1 << (EVENT_ID_MAX - id));
    }

    std::string GenerateRequirementsString(int32 auraMask, int32 eventMask)
    {
        std::string text;
        bool first = true;
        for (int32 i = 1; i <= EVENT_MASK_MAX; i = i << 1)
        {
            if (i & eventMask)
            {
                if (auraMask & i)
                    continue;

                if (!first)
                    text += ", ";
                else
                    text = "Ještě musíš ";
                first = false;

                switch (i)
                {
                case EVENT_MASK_PIGS:
                    text += "najít správné prase na farmě";
                    break;
                case EVENT_MASK_LABYRINT:
                    text += "objevit auru v bludišti";
                    break;
                case EVENT_MASK_JUMP:
                    text += "zachránit mojí pandu nad bludištěm";
                    break;
                case EVENT_MASK_BOXES:
                    text += "přinést mi magické jablko ze stromu u magických krabiček";
                    break;
                case EVENT_MASK_FIND_PORT:
                    text += "ovládnout kouzlo teleportace";
                    break;
                case EVENT_MASK_MEADOW:
                    text += "vyrobit kouzelný elixír";
                    break;
                default:
                    text += " (i = " + std::to_string(i) + ")";
                }
            }
        }

        if (!text.empty())
            text += ".";
        return text;
    }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        int32 mask = me->GetAI()->GetData(0);
        std::string text = GenerateRequirementsString(GetAuraMask(player, me), mask);

        if (!text.empty())
        {
            me->Whisper(text, LANG_UNIVERSAL, player);
            return true;
        }

        Position pos;
        me->GetPositionOffsetTo(*player, pos);
        pos.Relocate(me->GetPositionX() + 2 * pos.GetPositionX(), me->GetPositionY() + 2 * pos.GetPositionY(), me->GetPositionZ() + 2 * pos.GetPositionZ(), pos.GetOrientation());
        TempSummon* dragon = me->SummonCreature(DRAGON_VEHICLE_ENTRY, pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation(), TEMPSUMMON_TIMED_DESPAWN, 2 * IN_MILLISECONDS * MINUTE);
        if (!dragon)
            return true;

        dragon->SetFacingToObject(me);
        dragon->HandleSpellClick(player, 0);
        dragon->Whisper("Odnesu tě až do města!", LANG_UNIVERSAL, player);

        return true;
    }

    struct event_363_event_handlerAI : public CreatureAI
    {
#define EVENT_PARTS 2
#define RECHECK_TIMER 3000
        uint32 timer;
        EventsMask_363 eventMask; // used only in 1st part
        std::map<uint32, uint32> emotes;
        event_363_event_handlerAI(Creature* creature) : CreatureAI(creature)
        {
            timer = RECHECK_TIMER;
            RecalculateEventMask();
            emotes[60]  =  11;
            emotes[76]  =  11;
            emotes[23]  =  11;
            emotes[101] =  70;
            emotes[55]  =  70;
            emotes[53]  =  70;
            emotes[31]  =  18;
            emotes[143] =  18;
            emotes[75]  =  53;
            emotes[22]  =  19;
            emotes[136] =  19;
            emotes[41]  =  23;
            emotes[77]  =  14;
            emotes[183] =  14;
            emotes[59]  =  16;
            emotes[58]  =  17;
            emotes[8]   =  20;
            emotes[5]   =  21;
            emotes[24]  =  21;
            emotes[41]  =  23;
            emotes[17]  =   2;
            emotes[35]  =   7;
            emotes[37]  =   7;
            emotes[34]  =  94;
            emotes[95]  =   6;
            emotes[93]  =   1;
            emotes[94]  =   5;
            emotes[328] =  24;
            emotes[80]  =  24;
            emotes[225] =  24;
            emotes[72]  =  29;
            emotes[264] = 275;
            emotes[66]  = 274;
            emotes[67]  = 273;
            emotes[21]  =   4;
            emotes[102] =  70;
            emotes[303] =  22;
            emotes[97]  =   1;
            emotes[78]  =  66;
        }
#define DEBUG_HEAL 72484
        void ReceiveEmote(Player* player, uint32 emoteId) override
        {
            auto itr = emotes.find(emoteId);
            if (itr != emotes.end())
                me->HandleEmoteCommand(itr->second);
            else if (emoteId == 326)
                me->CastSpell(player, DEBUG_HEAL, true);
        }

        void RecalculateEventMask()
        {
            bool active = false;
            eventMask = EVENT_MASK_EMPTY;
            for (uint32 i = EVENT_ID_START_ID_PART_1; i <= EVENT_ID_MAX_PART_1; ++i)
            {
                if (sGameEventMgr->IsActiveEvent(i))
                {
                    eventMask |= CalculateEventMask(EventId_363(i));
                    active = true;
                }
            }

            if (!active)
            {
                HandleEventGroup1();
                sGameEventMgr->StartEvent(EVENT_ID_DARK_LORD_PVE);
            }
        }

        uint32 GetData(uint32 /*index*/) const override
        {
            return eventMask;
        }

        std::vector<Event_363> HandleEventGroup(std::vector<Event_363>& events)
        {
            std::vector<Event_363> activeEvents;
            activeEvents.reserve(3);

            while (activeEvents.size() < 3)
            {
                auto rngItr = events.begin();
                std::advance(rngItr, urand(0, static_cast<uint32>(events.size() - 1)));
                Event_363& e = *rngItr;
                bool passDep = e.dependsOnAnyOf.empty();
                bool passExc = e.exclusiveWith.empty();
                if (!passDep)
                {
                    for (auto& _e : activeEvents)
                    {
                        for (auto& depends : e.dependsOnAnyOf)
                        {
                            if (_e.id == depends)
                            {
                                passDep = true;
                                break;
                            }
                        }
                    }
                }

                if (!passExc)
                {
                    passExc = true;
                    for (auto& _e : activeEvents)
                    {
                        for (auto& exclusive : e.exclusiveWith)
                        {
                            if (_e.id == exclusive)
                            {
                                passExc = false;
                                break;
                            }
                        }
                    }
                }

                if (passDep && passExc)
                {
                    activeEvents.push_back(e);
                    events.erase(rngItr);
                }
            }

            for (auto& e : activeEvents)
                sGameEventMgr->StartEvent(e.id, true);

            return activeEvents;
        }

        void HandleEventGroup1()
        {
            std::vector<Event_363> events
            {
                { EVENT_ID_FIND_PORT },
                { EVENT_ID_BOXES },
                { EVENT_ID_JUMP, { EVENT_ID_LABYRINT , EVENT_ID_MEADOW } },
                { EVENT_ID_LABYRINT, { }, { EVENT_ID_MEADOW } },
                { EVENT_ID_PIGS },
                { EVENT_ID_MEADOW, { }, { EVENT_ID_LABYRINT } }
            };
            events = HandleEventGroup(events);

            for (auto e : events)
                eventMask |= CalculateEventMask(e.id);
        }

        ~event_363_event_handlerAI()
        {
            if (!me->FindMap())
            {
                for (uint32 i = EVENT_ID_START_ID; i <= EVENT_ID_MAX; ++i)
                {
                    if (sGameEventMgr->IsActiveEvent(i))
                        sGameEventMgr->StopEvent(i);
                }
                if (sGameEventMgr->IsActiveEvent(EVENT_ID_DARK_LORD_PVE))
                    sGameEventMgr->StopEvent(EVENT_ID_DARK_LORD_PVE);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer >= diff)
            {
                timer -= diff;
                return;
            }

            timer += RECHECK_TIMER;
            RecalculateEventMask();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_event_handlerAI(creature);
    }
};

class event_363_door_opener : public CreatureScript
{
public:
    event_363_door_opener() : CreatureScript("event_363_door_opener") { }
    struct event_363_door_openerAI : public CreatureAI
    {
        ObjectGuid doors;
        bool activated;
        Map* map;

        event_363_door_openerAI(Creature* creature) : CreatureAI(creature)
        {
            map = me->GetMap();
            activated = false;
        }

        ~event_363_door_openerAI()
        {
            if (me->FindMap())
                return;

            if (!doors)
                return;

            GameObject* obj = map->GetGameObject(doors);
            if (!obj)
                return;

            obj->ResetDoorOrButton();
        }

        void UpdateAI(uint32 /*diff*/) override
        {
            if (activated)
                return;

            if (GameObject* obj = me->FindNearestGameObject(187770, 0.1f))
            {
                doors = obj->GetGUID();
                obj->Use(me);
                activated = true;
            }
        }

    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_door_openerAI(creature);
    }
};

enum ePigs
{
    PIG_CORRECT_ONE,
    PIG_SLOWING,
    PIG_STUNNING,
    PIG_KNOCKDOWNING,
    PIG_NONE, // must be last
};

void AnnounceGroup(Player* player, Creature* me, std::string text, EventsMask_363 event, std::function<void(Player*)> groupMembersHandlerFn = nullptr)
{
    if (Group* group = player->GetGroup())
    {
        Group::MemberSlotList const& members = group->GetMemberSlots();
        uint8 groupId = player->GetGroup()->GetMemberGroup(player->GetGUID());
        for (Group::member_citerator itr = members.begin(); itr != members.end(); ++itr)
        {
            if (itr->group != groupId)
                continue;

            Player* groupMember = ObjectAccessor::GetPlayer(*me, itr->guid);
            if (!groupMember)
                continue;

            bool pass = event == EVENT_MASK_EMPTY;

            if (event)
            {
                Aura* aur = groupMember->GetAura(DUMMY_COUNTER_AURA);
                if (!aur)
                    aur = groupMember->AddAura(DUMMY_COUNTER_AURA, groupMember);
                if (!aur)
                    continue;

                AuraEffect* eff = aur->GetEffect(EFFECT_0);
                if (!eff)
                    continue;
                if ((eff->GetAmount() & event) == 0)
                {
                    eff->SetAmount(eff->GetAmount() | event);
                    pass = true;
                }
            }

            if (pass)
            {
                me->Whisper(text, LANG_UNIVERSAL, groupMember, true);
                if (groupMembersHandlerFn)
                    groupMembersHandlerFn(groupMember);
            }
        }
    }
}

class event_363_pig_handler : public CreatureScript
{
public:
    event_363_pig_handler() : CreatureScript("event_363_pig_handler") { }
    struct event_363_pig_handlerAI : public CreatureAI
    {
#define PIG_ENTRY 450026
        event_363_pig_handlerAI(Creature* creature) : CreatureAI(creature)
        {
            std::list<Creature*> pigs;
            me->GetCreatureListWithEntryInGrid(pigs, PIG_ENTRY, 80.0f);
            if (pigs.empty())
                return;
            auto pigItr = pigs.begin();
            std::advance(pigItr, urand(0, static_cast<uint32>(pigs.size() - 1)));
            Creature* pig = *pigItr;
            pig->SetMaxPower(POWER_HAPPINESS, PIG_CORRECT_ONE);
            pigs.erase(pigItr);
            for (auto pig : pigs)
                pig->SetMaxPower(POWER_HAPPINESS, urand(PIG_SLOWING, PIG_NONE));
        }

        void UpdateAI(uint32 /*diff*/) override
        {
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_pig_handlerAI(creature);
    }
};

class event_363_pig : public CreatureScript
{
public:
    event_363_pig() : CreatureScript("event_363_pig") { }

    enum PigSpells
    {
        PIG_SLOW = 63004,
        PIG_STUN = 23454,
        PIG_KNOCKDOWN = 42963,
    };

    bool OnGossipHello(Player* player, Creature* me) override
    {
        switch (me->GetMaxPower(POWER_HAPPINESS))
        {
            case PIG_CORRECT_ONE:
            {
                AnnounceGroup(player, me, "Našli jste správné prasátko!!!", EVENT_MASK_PIGS);
                break;
            }
            case PIG_SLOWING:
            {
                me->AddAura(PIG_SLOW, player);
                break;
            }
            case PIG_STUNNING:
            {
                me->AddAura(PIG_STUN, player);
                break;
            }
            case PIG_KNOCKDOWNING:
            {
                player->CastSpell(player, PIG_KNOCKDOWN, true);
                break;
            }
            case PIG_NONE:
            default:
                break;
        }
        return true;
    }
};

class event_363_boxes : public box_spawner_base_class
{
public:

    event_363_boxes() : box_spawner_base_class("event_363_boxes") { }

    struct event_363_boxesAI : public box_spawner_base_classAI
    {
#define BOXES_TOTAL 240
#define OBJECTS_HOLE 20

        event_363_boxesAI(Creature* creature) : box_spawner_base_classAI(creature, 2684, 2728, 131)
        {
            me->RemoveAllGameObjects();
            waypoints.reserve(BOXES_TOTAL);
            for (uint8 i = 0; i < 2; ++i)
            {
                AddWPInDir(0, 1, 1);
                AddWPInDir(0, 1, 0);
                AddWPInDir(0, 1, 0);
                AddWPInDir(1, 0, 0);
                AddWPInDir(1, 0, 0);
                AddWPInDir(1, 0, 0);
                AddWPInDir(0, -1, 0);
                AddWPInDir(0, -1, 0);
                AddWPInDir(0, -1, 0);
                AddWPInDir(-1, 0, 0);
                AddWPInDir(-1, 0, 0);
                AddWPInDir(-1, 0, i);
            }
            Position& p = waypoints[waypoints.size() - 1];
            me->SummonGameObject(BOX_ENTRY, p, G3D::Quat(0.0f, 0.0f, 0.0f, 0.0f), 0);

        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_boxesAI(creature);
    }
};

struct OrientedPoint
{
    OrientedPoint(float x, float y, float z, float o)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->o = o;
    }
    float x;
    float y;
    float z;
    float o;
};

class event_363_labyrint_handler : public CreatureScript
{
public:
    event_363_labyrint_handler() : CreatureScript("event_363_labyrint_handler") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new labyrint_base_classAI(creature, 27, 450029);
    }
};

class event_363_labyrint_finish : public CreatureScript
{
public:
    event_363_labyrint_finish() : CreatureScript("event_363_labyrint_finish") { }
    struct event_363_labyrint_finishAI : CreatureAI
    {
#define LABYRINT_FINISH_AURA 62647
        uint16 timer;
        uint32 delay;

        event_363_labyrint_finishAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 1000;
            delay = timer;
            me->m_SightDistance = 1.0f;
            me->AddAura(LABYRINT_FINISH_AURA, me);
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (me->GetDistance(unit) >= me->m_SightDistance)
                return;

            AnnounceGroup(unit->ToPlayer(), me, "Našli jste auru v labyrintu!!!", EVENT_MASK_LABYRINT);
        }

        void UpdateAI(uint32 diff) override
        {
            if (diff <= delay)
            {
                delay -= diff;
                return;
            }

            delay += timer;
            if (me->HasAura(LABYRINT_FINISH_AURA))
                return;

            me->AddAura(LABYRINT_FINISH_AURA, me);
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_labyrint_finishAI(creature);
    }
};

class event_363_boxes_finish : public CreatureScript
{
public:
    event_363_boxes_finish() : CreatureScript("event_363_boxes_finish") { }
    struct event_363_boxes_finishAI : CreatureAI
    {
#define LABYRINT_FINISH_AURA 62647

        event_363_boxes_finishAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 0.3f;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (me->GetDistance(unit) >= me->m_SightDistance)
                return;

            AnnounceGroup(unit->ToPlayer(), me, "Našli jste kouzelné jablko!!", EVENT_MASK_BOXES);
        }

        void UpdateAI(uint32) override { }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_boxes_finishAI(creature);
    }
};

class event_363_jump_finish : public CreatureScript
{
public:
    event_363_jump_finish() : CreatureScript("event_363_jump_finish") { }
    struct event_363_jump_finishAI : CreatureAI
    {
        uint16 timer;
        uint32 delay;

        event_363_jump_finishAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 2000;
            delay = timer;
            me->m_SightDistance = 5.0f;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (me->GetDistance(unit) >= me->m_SightDistance)
                return;

            AnnounceGroup(unit->ToPlayer(), me, "Gratuluji, zachránili jste pandu!!!", EVENT_MASK_JUMP);
        }

        void UpdateAI(uint32) override { }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_jump_finishAI(creature);
    }
};

#define FLOORS 10

class event_363_find_port_finish : public CreatureScript
{
public:
    event_363_find_port_finish() : CreatureScript("event_363_find_port_finish") { }
#define TELEPORT_VISUAL_FINISH 52233
    bool OnGossipHello(Player* player, Creature* me) override
    {
        AnnounceGroup(player, me, "Ovládli jste teleportaci!!!", EVENT_MASK_FIND_PORT);
        player->CastSpell(player, 52233, true);
        player->NearTeleportTo(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ() - FLOORS * FIND_PORT_FLOOR_HEIGHT, 0);
        return true;
    }
};

class event_363_find_port : public CreatureScript
{
public:
    event_363_find_port() : CreatureScript("event_363_find_port") { }
#define SPELL_TELEPORT_VISUAL 41232
#define SPELL_TELEPORT_DOWN 31046
    bool OnGossipHello(Player* player, Creature* me) override
    {
        Position pos;
        bool owner = false;
        if (TempSummon* tmp = me->ToTempSummon())
        if (Unit* summoner = tmp->GetSummoner())
        {
            pos = summoner->GetPosition();
            owner = true;
        }
        if (!owner)
            pos = me->GetPosition();

        player->CastSpell(player, SPELL_TELEPORT_VISUAL, true);

        if (me->GetMaxPower(POWER_HAPPINESS))
            player->NearTeleportTo(pos.GetPositionX(), pos.GetPositionY(), me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT, player->GetOrientation());
        else
        {
            player->NearTeleportTo(pos.GetPositionX(), pos.GetPositionY(), owner ? pos.GetPositionZ() : me->GetMaxPower(POWER_RUNIC_POWER), player->GetOrientation());
            me->AddAura(SPELL_TELEPORT_DOWN, player);
        }
        return true;
    }
};

class event_363_find_port_handler : public CreatureScript
{
public:
    event_363_find_port_handler() : CreatureScript("event_363_find_port_handler") { }

    struct event_363_find_port_handlerAI : public find_port_handlerAI
    {
        event_363_find_port_handlerAI(Creature* creature, uint8 floors) : find_port_handlerAI(creature, floors)
        {
            Creature* finish = me->SummonCreature(FIND_PORT_FINISH, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ() + FIND_PORT_FLOOR_HEIGHT * floors, me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN);
            teleporterGuids.push_back(finish->GetGUID());
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_find_port_handlerAI(creature, FLOORS);
    }
};

class event_363_drak_smak : public CreatureScript
{
public:
    event_363_drak_smak() : CreatureScript("event_363_drak_smak") { }
    struct event_363_drak_smakAI : CreatureAI
    {
        enum Waypoints
        {
            WP_NONE             = 0,
            WP_TAKEOFF          = 1,
            WP_ABOVE_LABYRINT   = 2,
            WP_ABOVE_WALL       = 3,
            WP_FLY_TO_CITY      = 4,
            WP_FALL_DOWN        = 5,
            WP_FLY_FROM_BUBBLE  = 6,
            WP_FLY_INSIDE_CITY  = 7,
            WP_LAND_IN_CITY     = 8,
        };
#define TROLL_ENTRY         450040
#define TROLL_CAPTAIN_ENTRY 450042
#define SPELL_SHOT           61512

        uint32 timer;
        uint8 wp;
        event_363_drak_smakAI(Creature* creature) : CreatureAI(creature)
        {
            Reset();
        }

        void SetData(uint32, uint32) override
        {
            wp = WP_FLY_FROM_BUBBLE;
        }

        void Reset() override
        {
            timer = 2000;
            wp = WP_TAKEOFF;
        }

        void MovementInform(uint32, uint32 waypoint) override
        {
            if (waypoint == WP_LAND_IN_CITY)
            {
                if (Vehicle* v = me->GetVehicleKit())
                {
                    if (Unit* u = v->GetPassenger(0))
                        if (Player* p = u->ToPlayer())
                            me->Say("Tak jsme tu, " + p->GetName() + "!", LANG_UNIVERSAL);
                    me->GetVehicleKit()->RemoveAllPassengers();
                }
                me->DespawnOrUnsummon(10 * IN_MILLISECONDS);
            }
            else if (waypoint != WP_FALL_DOWN)
                wp = waypoint + 1;
            else
            {
                me->GetMotionMaster()->MoveFall(WP_FALL_DOWN);
                    if (Vehicle* v = me->GetVehicleKit())
                        if (Unit* u = v->GetPassenger(0))
                            if (Player* p = u->ToPlayer())
                            me->Whisper("Běž, utíkej!", LANG_UNIVERSAL, p);
                me->KillSelf();
            }
        }

        void SpellHit(Unit* /*caster*/, const SpellInfo* spell) override
        {
            if (spell->Id != SPELL_SHOT)
                return;

            if (!me->IsFalling())
            {
                me->GetMotionMaster()->MoveFall(WP_FALL_DOWN);
                    if (Vehicle* v = me->GetVehicleKit())
                        if (Unit* u = v->GetPassenger(0))
                            if (Player* p = u->ToPlayer())
                            me->Whisper("Ale ne! Cestu nám zkřížili nepřátelé. Budeš muset cestu do města zvládnout sám.", LANG_UNIVERSAL, p);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!wp)
                return;

            if (timer > diff)
            {
                timer -= diff;
                return;
            }

            switch (wp)
            {
                case WP_TAKEOFF:
                {
                    me->SetDisableGravity(true);
                    Position pos = me->GetPosition();
                    Position offset(0.0f, 0.0f, 10.0f);
                    pos.RelocateOffset(offset);
                    me->GetMotionMaster()->MovePoint(WP_TAKEOFF, pos, false);
                    break;
                }
                case WP_ABOVE_LABYRINT:
                    me->GetMotionMaster()->MovePoint(WP_ABOVE_LABYRINT, 2642.0f, 2637.0f, 169.0f, false);
                    break;
                case WP_ABOVE_WALL:
                    me->GetMotionMaster()->MovePoint(WP_ABOVE_WALL, 2563.0f, 2615.0f, 158.0f, false);
                    break;
                case WP_FLY_TO_CITY:
                {
                    if (Vehicle* v = me->GetVehicleKit())
                        if (Unit* u = v->GetPassenger(0))
                            if (Player* p = u->ToPlayer())
                                me->Whisper("Tohle si nepamatuju, že by tu bylo...", LANG_UNIVERSAL, p);

                    me->GetMotionMaster()->MovePoint(WP_FLY_TO_CITY, 2473.0f, 2628.0f, 169.0f, false);
                    for (uint8 i = 0; i < 6; ++i)
                    {
                        int16 rngx = irand(-5, 5);
                        int16 rngy = irand(-5, 5);
                        me->SummonCreature(TROLL_ENTRY, 2489.6f + rngx, 2610.0f + rngy, 131.42f, 1.632074f, TEMPSUMMON_TIMED_DESPAWN, 30 * IN_MILLISECONDS);
                    }
                    int16 rngx = irand(-5, 5);
                    int16 rngy = irand(-5, 5);
                    me->SummonCreature(TROLL_CAPTAIN_ENTRY, 2489.6f + rngx, 2610.0f + rngy, 131.42f, 1.632074f, TEMPSUMMON_TIMED_DESPAWN, 30 * IN_MILLISECONDS);
                    break;
                }
                case WP_FLY_FROM_BUBBLE:
                    me->GetMotionMaster()->MovePoint(WP_FLY_FROM_BUBBLE, 2504.27f, 2341.94f, 159.60f, false);
                    break;
                case WP_FLY_INSIDE_CITY:
                    me->GetMotionMaster()->MovePoint(WP_FLY_INSIDE_CITY, 2344.69f, 2446.52f, 216.17f, false);
                    break;
                case WP_LAND_IN_CITY:
                    me->GetMotionMaster()->MovePoint(WP_LAND_IN_CITY, 2338.86f, 2447.35f, 147.20f, false);
                    break;
                }

            wp = 0;
            timer = 0;
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_drak_smakAI(creature);
    }
};

class event_363_troll_hunter : public CreatureScript
{
public:
    event_363_troll_hunter() : CreatureScript("event_363_troll_hunter") { }
    struct event_363_troll_hunterAI : CreatureAI
    {
#define BOW_ENTRY     200319
#define DRAGON_ENTRY  450039
#define DUMMY_ENTRY   450041
#define CAPTAIN_ENTRY 450042
#define SPELL_SHOT    61512
#define INTERVAL      2500
        uint32 timer;
        bool first;
        event_363_troll_hunterAI(Creature* creature) : CreatureAI(creature)
        {
            me->LoadEquipment();
            timer = urand(0, INTERVAL);
            first = true;
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer > diff)
            {
                timer -= diff;
                return;
            }
            timer += INTERVAL;

            std::list<Creature*> dragons;
            me->GetCreatureListWithEntryInGrid(dragons, DRAGON_ENTRY, 80.0f);
            Creature* dragon = nullptr;
            if (!dragons.empty())
            {
                auto dragonItr = dragons.begin();
                std::advance(dragonItr, urand(0, static_cast<uint32>(dragons.size() - 1)));
                dragon = *dragonItr;
            }

            if (!dragon)
            {
                std::list<Creature*> dummies;
                me->GetCreatureListWithEntryInGrid(dummies, DUMMY_ENTRY, 80.0f);
                if (dummies.empty())
                    return;
                auto dummyItr = dummies.begin();
                std::advance(dummyItr, urand(0, static_cast<uint32>(dummies.size() - 1)));
                Creature* dummy = *dummyItr;
                me->SetFacingToObject(dummy);
                me->CastSpell(dummy, SPELL_SHOT);
                return;
            }

            if (me->GetEntry() == CAPTAIN_ENTRY && first)
            {
                me->Yell("Pozor drak, sestřelte ho k zemi!", LANG_UNIVERSAL);
                first = false;
            }
            me->SetFacingToObject(dragon);
            me->CastSpell(dragon, SPELL_SHOT);
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_troll_hunterAI(creature);
    }
};

class event_363_guardian : public CreatureScript
{
public:
    event_363_guardian() : CreatureScript("event_363_guardian") { }

    struct event_363_guardianAI : public grunt_guardian_base_classAI
    {
        event_363_guardianAI(Creature* creature) : grunt_guardian_base_classAI(creature)
        {
            points =
            {
                { 2493.83f, 2663.87f, 131.44f, 4.94f },
                { 2500.86f, 2666.40f, 131.42f, 4.85f },
                { 2514.31f, 2665.50f, 131.42f, 4.47f },
                { 2522.17f, 2664.80f, 131.41f, 4.50f }
            };
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_guardianAI(creature);
    }
};

enum FlowerActions
{
    FLOWER_SPAWNER_ON_SPAWN,
    FLOWER_SPAWNER_ON_FLOWER_DESPAWN,
};

enum FlowerIndex
{
    KORINEK_INDEX,
    ZELENKA_INDEX,
    FIALKA_INDEX,
    RUZENKA_INDEX,
    KOUZELNY_LISTEK_INDEX,
    VETVICKA_INDEX,
    ZELENY_LISTEK_INDEX,
    FLOWERS_COUNT
};

class event_363_meadow_handler : public CreatureScript
{
public:
    event_363_meadow_handler() : CreatureScript("event_363_meadow_handler") { }
#define ALCHEMIST_DUMMY_AURA 100006
#define FINISH_AMOUNT 11
    enum FlowersEntry
    {
        FLOWERS_ENTRY_START = 450060,
        KORINEK = KORINEK_INDEX + FLOWERS_ENTRY_START,
        ZELENKA = ZELENKA_INDEX + FLOWERS_ENTRY_START,
        FIALKA = FIALKA_INDEX + FLOWERS_ENTRY_START,
        RUZENKA = RUZENKA_INDEX + FLOWERS_ENTRY_START,
        KOUZELNY_LISTEK = KOUZELNY_LISTEK_INDEX + FLOWERS_ENTRY_START,
        VETVICKA = VETVICKA_INDEX + FLOWERS_ENTRY_START,
        ZELENY_LISTEK = ZELENY_LISTEK_INDEX + FLOWERS_ENTRY_START,
        FLOWERS_END = FLOWERS_ENTRY_START + FLOWERS_COUNT
    };

    void SendBringMe(Player* player, Creature* me, Aura* aur)
    {
        std::string text = "Dones mi ";
        switch (aur->GetStackAmount() - 1)
        {
        case KORINEK_INDEX:
            text += "kořínek";
            break;
        case ZELENKA_INDEX:
            text += "zelenku";
            break;
        case FIALKA_INDEX:
            text += "fialku";
            break;
        case RUZENKA_INDEX:
            text += "růženku";
            break;
        case KOUZELNY_LISTEK_INDEX:
            text += "kouzelný lístek";
            break;
        case VETVICKA_INDEX:
            text += "větvičku";
            break;
        case ZELENY_LISTEK_INDEX:
            text += "zelený lístek";
            break;
        default:
            return;
            break;
        }
        text += ", máš na to " + std::to_string(aur->GetDuration() / IN_MILLISECONDS) + " sekund. (" + std::to_string(aur->GetEffect(EFFECT_0)->GetAmount()) + " / " + std::to_string(FINISH_AMOUNT) + ")";
        me->Whisper(text, LANG_UNIVERSAL, player);
    }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (Aura* aur = player->GetAura(DUMMY_COUNTER_AURA))
        {
            if (aur->GetEffect(EFFECT_0)->GetAmount() & EVENT_MASK_MEADOW)
            {
                me->Whisper("Už máš potřebný elixír, více ti již nepomůžu", LANG_UNIVERSAL, player);
                return true;
            }
        }

        if (Aura* aur = player->GetAura(ALCHEMIST_DUMMY_AURA))
        {
            uint32 flowerEntry = FLOWERS_ENTRY_START + aur->GetStackAmount() - 1;
            ObjectGuid critterGuid = player->GetCritterGUID();

            if (!critterGuid)
            {
                SendBringMe(player, me, aur);
                return true;
            }
            else
            {
                Creature* critter = me->GetMap()->GetCreature(critterGuid);
                bool valid = critter && critter->GetEntry() == flowerEntry;
                if (!valid)
                {
                    me->Whisper("Co to je? To nebylo oč jsem tě žádal! Teď abychom začali odznova!", LANG_UNIVERSAL, player);
                    player->RemoveAura(aur, AURA_REMOVE_BY_ENEMY_SPELL);
                }

                if (critter)
                {
                    if (critter->GetEntry() < FLOWERS_END && critter->GetEntry() >= FLOWERS_ENTRY_START)
                        me->AI()->DoAction(1);
                    else
                    {
                        GameObject* cauldron = me->FindNearestGameObject(2200016, 10.0f);
                        critter->SetFacingToObject(cauldron);
                        critter->GetMotionMaster()->MoveJump(*cauldron, cauldron->GetExactDist2d(critter), 12.0f);
                        critter->DespawnOrUnsummon(1000);
                    }
                }

                if (!valid)
                    return true;
            }

            int32 amount = aur->GetEffect(EFFECT_0)->GetAmount();
            aur->SetStackAmount(urand(1, FLOWERS_COUNT));
            if (amount == FINISH_AMOUNT)
            {
                AnnounceGroup(player, me, "Podařilo se vám vytvořit kouzelný elixír!", EVENT_MASK_MEADOW);
                player->RemoveAura(aur, AURA_REMOVE_BY_ENEMY_SPELL);
                return true;
            }
            aur->GetEffect(EFFECT_0)->SetAmount(amount + 1);
            aur->SetDuration(2 * MINUTE * IN_MILLISECONDS - 6 * amount * IN_MILLISECONDS);
            aur->SetCharges(aur->GetDuration() <= 100 * IN_MILLISECONDS ? 1 : 0);
            SendBringMe(player, me, aur);
        }
        else if (Aura* aur = me->AddAura(ALCHEMIST_DUMMY_AURA, player))
        {
            aur->SetStackAmount(urand(1, FLOWERS_COUNT));
            aur->GetEffect(EFFECT_0)->SetAmount(1);
            aur->SetDuration(2 * MINUTE * IN_MILLISECONDS);
            SendBringMe(player, me, aur);
        }
        return true;
    }

    struct event_363_meadow_handlerAI : CreatureAI
    {
#define TIMER 3 * IN_MILLISECONDS
        uint32 timer;

        struct FlowerSpawnPoint : Position
        {
            FlowerSpawnPoint(float x, float y, float z, ObjectGuid spawner) : Position(x, y, z)
            {
                this->spawner = spawner;
            }
            ObjectGuid spawner;
        };
        std::list<FlowerSpawnPoint> fullSpawn;
        std::list<FlowerSpawnPoint> emptySpawn;
        std::vector<int32> flowersCount;
        event_363_meadow_handlerAI(Creature* creature) : CreatureAI(creature)
        {
            timer = TIMER;
            flowersCount.resize(FLOWERS_COUNT, 0);
        }

        void SetGUID(ObjectGuid guid, int32 action) override
        {
            switch (action)
            {
                case FLOWER_SPAWNER_ON_SPAWN:
                {
                    if (Creature* spawner = me->GetMap()->GetCreature(guid)) // should always be valid
                    {
                        ObjectGuid flowerGuid = spawner->GetCritterGUID();
                        if (flowerGuid)
                            if (Creature* flower = me->GetMap()->GetCreature(flowerGuid))
                                ++flowersCount[flower->GetEntry() - FLOWERS_ENTRY_START];

                        std::list<FlowerSpawnPoint>& spawn = flowerGuid ? fullSpawn : emptySpawn;
                        spawn.push_back(FlowerSpawnPoint(spawner->GetPositionX(), spawner->GetPositionY(), spawner->GetPositionZ(), guid));
                    }
                    break;
                }
                case FLOWER_SPAWNER_ON_FLOWER_DESPAWN:
                {
                    if (Creature* spawner = me->GetMap()->GetCreature(guid))
                    {
                        if (Creature* flower = me->GetMap()->GetCreature(spawner->GetCritterGUID()))
                        {
                            --flowersCount[flower->GetEntry() - FLOWERS_ENTRY_START];
                            emptySpawn.push_back(FlowerSpawnPoint(spawner->GetPositionX(), spawner->GetPositionY(), spawner->GetPositionZ(), spawner->GetGUID()));
                            fullSpawn.remove_if([spawner](FlowerSpawnPoint& p) { return p.spawner == spawner->GetGUID(); });
                        }
                    }
                    break;
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer >= diff)
            {
                timer -= diff;
                return;
            }

            timer += TIMER;
            if (emptySpawn.empty())
                return;

            int32 spawnCount = static_cast<int32>(emptySpawn.size() + fullSpawn.size());
            int32 countPerType = (spawnCount > 3 * FLOWERS_COUNT ? spawnCount / 3 : spawnCount) / FLOWERS_COUNT;
            for (uint8 i = 0; i < FLOWERS_COUNT; ++i)
            {
                int32& count = flowersCount[i];
                if (count < countPerType)
                {
                    auto itr = emptySpawn.begin();
                    std::advance(itr, urand(0, static_cast<uint32>(emptySpawn.size() - 1)));

                    Creature* spawner = me->GetMap()->GetCreature(itr->spawner);
                    if (!spawner)
                    {
                        emptySpawn.erase(itr);
                        continue;
                    }
                    Creature* flower = spawner->SummonCreature(FLOWERS_ENTRY_START + i, *spawner);
                    if (flower)
                    {
                        flower->SetOwnerGUID(spawner->GetGUID());
                        spawner->SetCritterGUID(flower->GetGUID());
                        fullSpawn.push_back(FlowerSpawnPoint(itr->m_positionX, itr->m_positionY, itr->m_positionZ, itr->spawner));
                        emptySpawn.erase(itr);
                       ++count;
                    }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_meadow_handlerAI(creature);
    }
};
#define MEADOW_HANDLER_ENTRY 450067
class event_363_meadow_flower_spawner : public CreatureScript
{
public:
    event_363_meadow_flower_spawner() : CreatureScript("event_363_meadow_flower_spawner") { }

    struct event_363_meadow_flower_spawnerAI : CreatureAI
    {
        ObjectGuid handlerGuid;
        bool hasHandlerGuid;
        Map* map;
        event_363_meadow_flower_spawnerAI(Creature* creature) : CreatureAI(creature)
        {
            hasHandlerGuid = false;
            map = me->GetMap();
        }

        ~event_363_meadow_flower_spawnerAI()
        {
            if (me->FindMap())
                return;

            if (Creature* flower = map->GetCreature(me->GetCritterGUID()))
                flower->DespawnOrUnsummon();
        }

        void SetData(uint32, uint32) override
        {
            Creature* handler = map->GetCreature(handlerGuid);
            if (handler && handler->GetAI())
                handler->GetAI()->SetGUID(me->GetGUID(), FLOWER_SPAWNER_ON_FLOWER_DESPAWN);
            me->SetCritterGUID(ObjectGuid::Empty);
        }

        void UpdateAI(uint32 /*diff*/) override
        {
            if (hasHandlerGuid)
                return;

            Creature* handler = me->FindNearestCreature(MEADOW_HANDLER_ENTRY, 150.0f);
            if (handler && handler->GetAI())
            {
                handler->GetAI()->SetGUID(me->GetGUID(), FLOWER_SPAWNER_ON_SPAWN);
                handlerGuid = handler->GetGUID();
                hasHandlerGuid = true;
            }
            else
                handlerGuid = ObjectGuid::Empty;
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_meadow_flower_spawnerAI(creature);
    }
};

class event_363_meadow_flower : public CreatureScript
{
public:
#define GROWTH 41953
#define MAX_STACKS 30
    event_363_meadow_flower() : CreatureScript("event_363_meadow_flower") { }
    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (Aura* aur = me->GetAura(GROWTH))
        {
            if (aur->GetStackAmount() == MAX_STACKS - 1)
            {
                me->AddAura(GROWTH, me);
                me->GetMotionMaster()->MoveFollow(player, 0.0f, 0.0f);
                me->Whisper("Povedlo se ti vytáhnout rostlinku!", LANG_UNIVERSAL, player);
                me->SetSpeedRate(MOVE_RUN, 2.0f);
                Unit* owner = me->GetOwner();
                if (owner && owner->GetAI())
                    owner->GetAI()->SetData(0, 0);
                me->SetOwnerGUID(player->GetGUID());
                if (ObjectGuid critterGuid = player->GetCritterGUID())
                    if (Creature* critter = player->GetMap()->GetCreature(critterGuid))
                        critter->DespawnOrUnsummon(0);
                player->SetCritterGUID(me->GetGUID());
                me->DespawnOrUnsummon(5 * MINUTE * IN_MILLISECONDS);
                return true;
            }
            else if (aur->GetStackAmount() == MAX_STACKS)
                return true;
        }
        me->AddAura(GROWTH, me);
        return true;
    }

    struct event_363_meadow_flowerAI : CreatureAI
    {
#define DESPAWN_TIMER 2000
        uint32 timer;
        bool checkOwner;
        event_363_meadow_flowerAI(Creature* creature) : CreatureAI(creature) 
        {
            timer = DESPAWN_TIMER;
            checkOwner = true;
            MovementInform(1, 1);
        }

        void DoAction(int32) override
        {
            checkOwner = false;
            if (Unit* owner = me->GetOwner())
            {
                me->SetOwnerGUID(ObjectGuid::Empty);
                owner->SetCritterGUID(ObjectGuid::Empty);
            }
            if (GameObject* cauldron = me->FindNearestGameObject(2200016, 10.0f))
            {
                me->SetFacingToObject(cauldron);
                me->GetMotionMaster()->MoveJump(*cauldron, cauldron->GetExactDist2d(me), 12.0f);
                me->DespawnOrUnsummon(2000);
            }
        }

        void MovementInform(uint32, uint32 waypoint) override
        {
            if (waypoint == 1004)
                me->DespawnOrUnsummon();
        }

        void UpdateAI(uint32 diff) override
        {
            if (!checkOwner)
                return;

            if (timer > diff)
            {
                timer -= diff;
                return;
            }

            timer += DESPAWN_TIMER;
            if (Unit* owner = me->GetOwner())
            {
                if (owner->GetCritterGUID() != me->GetGUID())
                    me->DespawnOrUnsummon();
            }
            else
                me->DespawnOrUnsummon();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_meadow_flowerAI(creature);
    }
};

class spell_event363_alchemist_counter : public SpellScriptLoader
{
public:
    spell_event363_alchemist_counter() : SpellScriptLoader("spell_event363_alchemist_counter") { }

    class spell_event363_alchemist_counter_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_event363_alchemist_counter_AuraScript);

        void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
        {
            Unit* caster = GetCaster();
            Unit* owner = GetUnitOwner();
            if (!caster || !owner || owner->GetTypeId() != TYPEID_PLAYER)
                return;

            std::string text;
            switch (GetCharges())
            {
            case 0:
                if (GetDuration() < 100 * IN_MILLISECONDS)
                {
                    text = "Zbývá ti 100 sekund.";
                    SetCharges(GetCharges() + 1);
                }
                break;
            case 1:
                if (GetDuration() < 50 * IN_MILLISECONDS)
                {
                    text = "Zbývá ti 50 sekund.";
                    SetCharges(GetCharges() + 1);
                }
                break;
            case 2:
                if (GetDuration() < 30 * IN_MILLISECONDS)
                {
                    text = "Zbývá ti 30 sekund.";
                    SetCharges(GetCharges() + 1);
                }
                break;
            case 3:
                if (GetDuration() < 10 * IN_MILLISECONDS)
                {
                    text = "Zbývá ti 10 sekund.";
                    SetCharges(GetCharges() + 1);
                }
                break;
            case 4:
                if (GetDuration() < 5 * IN_MILLISECONDS)
                {
                    text = "Zbývá ti 5 sekund.";
                    SetCharges(GetCharges() + 1);
                }
                break;
            default:
                return;
            }
            if (!text.empty())
                caster->Whisper(text, LANG_UNIVERSAL, owner->ToPlayer());
        }

        void HandleRemove(AuraEffect const*, AuraEffectHandleModes)
        {
            Unit* caster = GetCaster();
            Unit* owner = GetUnitOwner();
            if (!caster || !owner || owner->GetTypeId() != TYPEID_PLAYER)
                return;

            AuraRemoveMode removeMode = GetTargetApplication()->GetRemoveMode();
            if (removeMode == AURA_REMOVE_BY_ENEMY_SPELL)
                return;
            caster->Whisper("POZDĚ! Receptura se narušila. Musíme začít znovu.", LANG_UNIVERSAL, owner->ToPlayer());
        }

        void Register()
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_event363_alchemist_counter_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            OnEffectRemove += AuraEffectRemoveFn(spell_event363_alchemist_counter_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_event363_alchemist_counter_AuraScript();
    }
};

#define HELPER_TIMER 1000
#define HELPER_AI(name) name ## AI
#define HELPER_TXT(x) # x
#define HELPER(name, eventmask, txt_male, txt_female)\
class name : public CreatureScript\
{\
public:\
    name() : CreatureScript(HELPER_TXT(name)) { }\
    bool OnGossipHello(Player* player, Creature* me) override\
    {\
        if (!player->getGender() || Trinity::IsFormatEmptyOrNull(txt_female))\
            me->Whisper(txt_male, LANG_UNIVERSAL, player);\
        else\
            me->Whisper(txt_female, LANG_UNIVERSAL, player);\
        return true;\
    }\
    struct HELPER_AI(name) : CreatureAI\
    {\
        HELPER_AI(name)(Creature* creature) : CreatureAI(creature)\
        {\
            me->m_SightDistance = 3.0f;\
        }\
        void MoveInLineOfSight(Unit* unit) override\
        {\
            if (unit->GetTypeId() != TYPEID_PLAYER)\
                return;\
\
            Player* p = unit->ToPlayer();\
\
            if (Aura* aur = p->GetAura(DUMMY_COUNTER_HELPER))\
            {\
                if (aur->GetEffect(EFFECT_0)->GetAmount() & eventmask)\
                    return;\
            }\
            else\
                p->AddAura(DUMMY_COUNTER_HELPER, p);\
\
            AuraEffect* eff = p->GetAura(DUMMY_COUNTER_HELPER)->GetEffect(EFFECT_0);\
            eff->SetAmount(eff->GetAmount() | eventmask);\
            if (!p->getGender() || Trinity::IsFormatEmptyOrNull(txt_female))\
                me->Whisper(txt_male, LANG_UNIVERSAL, p);\
            else\
                me->Whisper(txt_female, LANG_UNIVERSAL, p);\
        }\
\
        void UpdateAI(uint32) override { }\
    };\
\
    CreatureAI* GetAI(Creature* creature) const override\
    {\
        return new HELPER_AI(name)(creature);\
    }\
}

class event_363_ghost_walker : public CreatureScript
{
public:
    event_363_ghost_walker() : CreatureScript("event_363_ghost_walker") { }
#define GHOST_QUOTES_COUNT 7
#define GHOST_PRIORITIES_TOTAL 10+1+4+3+2+3+3
    const char* quotes[GHOST_QUOTES_COUNT] =
    {
        "Odsud není návratu zpět...",
        "Prý odsud existuje cesta ven, ale nevím o nikom, kdo by jí našel.",
        "Nedotýkej se mě!",
        "Jdi ode mě!",
        "Neznáme se od někud?",
        "V kostele je strážce duší, promluv si s ní, pokud něco potřebuješ.",
        "Na naší tržnici se dají pořídit zajímavé předměty!",
    };

    const uint16 priorities[GHOST_QUOTES_COUNT] =
    {
        10, 1, 4, 3, 2, 3, 3,
    };

    void GenerateQuote(Creature* me)
    {
        uint16 roll = urand(1, GHOST_PRIORITIES_TOTAL);
        uint8 i = 0;
        while (roll > priorities[i])
            roll -= priorities[i++];
        me->SetMaxPower(POWER_HAPPINESS, i + 1);
    }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (!me->GetMaxPower(POWER_HAPPINESS))
            GenerateQuote(me);

        uint8 quote = me->GetMaxPower(POWER_HAPPINESS) - 1;
        me->Whisper(quotes[quote], LANG_UNIVERSAL, player);
        return true;
    }

    struct event_363_ghost_walkerAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_ghost_walkerAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 3000;
            delay = 0;
            me->SetWalk(true);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!me->isMoving())
            {
                if (delay > diff)
                {
                    delay -= diff;
                    return;
                }

                delay += timer;
                if (me->CanFly())
                {
                    if (me->GetDistance(me->GetHomePosition()) > 50.0f)
                    {
                        me->GetMotionMaster()->MovePoint(0, me->GetHomePosition(), false);
                        return;
                    }
                }
                else if (me->GetHomePosition().GetPositionZ() - 50.0f > me->GetPositionZ())
                {
                    me->GetMotionMaster()->MovePoint(0, me->GetHomePosition(), false);
                    return;
                }

                Position pos = *me;
                me->MovePositionToFirstCollision(pos, 10.0f * static_cast<float>(rand_norm()), static_cast<float>(rand_norm() * 2 * G3D::pif()));
                if (!me->CanFly())
                {
                    pos.m_positionZ += 0.1f;
                    if (pos.m_positionZ < me->GetPositionZ() - 1.5f || pos.m_positionZ > me->GetPositionZ() + 1.5f || !me->IsWithinLOS(pos.m_positionX, pos.m_positionY, pos.m_positionZ))
                    {
                        delay = 400;
                        return;
                    }
                }
                me->GetMotionMaster()->MovePoint(0, pos, false);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_ghost_walkerAI(creature);
    }
};

#define SPELL_AURA_GHOST_FORM 100007
#define SPELL_AURA_PHASE_4    100008
#define SPELL_AURA_PHASE_8    100009
#define SPELL_AURA_PHASE_16   100010
#define SPELL_AURA_FLY         34873
#define FREEZE                  9256

void SetFreezeInvisibility(Player* p)
{
    p->m_serverSideVisibility.SetValue(SERVERSIDE_VISIBILITY_GM, SEC_CONSOLE);
    p->UpdateObjectVisibility();
}

void MoveToSpiritRealm(Player* p)
{
    SetFreezeInvisibility(p);
    p->AddAura(SPELL_AURA_GHOST_FORM, p);
    if (Aura* freeze = p->AddAura(FREEZE, p))
        freeze->SetDuration(3000);
    if (Aura* fly = p->AddAura(SPELL_AURA_FLY, p))
        fly->SetDuration(3000);
    p->NearTeleportTo(2443.8f, 2424.0f, 211.0f, 5.4f);
}

class event_363_ghost_portal : public GameObjectScript
{
public:
    event_363_ghost_portal() : GameObjectScript("event_363_ghost_portal") { }

    struct event_363_ghost_portalAI : public GameObjectAI
    {
        uint32 timer;
        event_363_ghost_portalAI(GameObject* go) : GameObjectAI(go)
        {

        }

        void UpdateAI(uint32 /*diff*/)
        {
            std::list<Player*> players;
            go->GetPlayerListInGrid(players, 3.0f);
            for (auto p : players)
            {
                int32 mask = GetAuraMask(p, go);
                if (mask & EVENT_MASK_OBSTACLES)
                {
                    if (p->HasAura(SPELL_AURA_PHASE_16))
                    {
                        p->RemoveAura(SPELL_AURA_GHOST_FORM);
                        p->NearTeleportTo(2595.87f, 2311.26f, 135.19f, 2.54f);
                    }
                    continue;
                }
                Aura* aur = p->GetAura(DUMMY_COUNTER_HELPER);
                if (!aur)
                    continue;

                if (HasWholeGroupHelperAuraMask(p, EVENT_MASK_SPIRIT_REALM))
                    MoveToSpiritRealm(p);
                else
                {
                    p->NearTeleportTo(2592.79f, 2313.6f, 133.6f, 2.5f);
                    p->Unit::Whisper("Musis pockat, nez si ostatni clenove tve skupiny\npromluvi s Ely Layi!", LANG_UNIVERSAL, p, true);
                }
            }
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new event_363_ghost_portalAI(go);
    }
};

class event_363_spirit_realm_welcomer : public CreatureScript
{
public:
    event_363_spirit_realm_welcomer() : CreatureScript("event_363_spirit_realm_welcomer") { }
    struct event_363_spirit_realm_welcomerAI : public CreatureAI
    {
        event_363_spirit_realm_welcomerAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 10.0f;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (unit->GetDistance(me) < 3.2f)
                // apply fly once again, because of slow loading screen
                if (Aura* fly = unit->AddAura(SPELL_AURA_FLY, unit))
                    fly->SetDuration(1000);

            if (Aura* freeze = unit->GetAura(FREEZE))
            {
                freeze->SetDuration(0);
                me->Say("Vítej ve světě mrtvých, " + unit->GetName() + ".", LANG_UNIVERSAL, unit);
            }
        }

        void UpdateAI(uint32) override { }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_spirit_realm_welcomerAI(creature);
    }
};

class event_363_spirit_guard_gossip : public CreatureScript
{
public:
    event_363_spirit_guard_gossip() : CreatureScript("event_363_spirit_guard_gossip") { }
    enum GossipMenuId
    {
        MENU_WELCOME,
        MENU_GHOST_REALM_INFO,
        MENU_CAN_DRAGON_BE_HERE,
        MENU_DRAGON_INFO,
        MENU_GET_LOST_FOOLS,
        MENU_YOU_ARE_DEAD,
        MENU_START = 400000,
    };
    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (player->HasAura(SPELL_AURA_PHASE_4))
        {
            me->Whisper("Strážce duší se s tebou odmítá bavit.", LANG_UNIVERSAL, player, true);
            return true;
        }
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Kde to jsem?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GHOST_REALM_INFO, "", 0);
        ptc->SendGossipMenu(MENU_START + MENU_WELCOME, me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* me, uint32 /*sender*/, uint32 action) override
    {
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.ClearMenu();
        switch (action)
        {
        case GOSSIP_ACTION_INFO_DEF + MENU_GHOST_REALM_INFO:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Ale ja jsem jeste neumrela..." :
                "Ale ja jsem jeste neumrel..."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_YOU_ARE_DEAD, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Je mozne, aby se sem dostal i drak po smrti?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_CAN_DRAGON_BE_HERE, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_GHOST_REALM_INFO, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_YOU_ARE_DEAD:
            ptc->SendCloseGossip();
            me->Whisper((player->getGender() ? "Haha, sice si neumřela, ale už jsi ve světě mrtvých a odsud není návratu zpět!"
                : "Haha, sice si neumřel, ale už jsi ve světě mrtvých a odsud není návratu zpět!"), LANG_UNIVERSAL, player);
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_CAN_DRAGON_BE_HERE:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Potrebuju najit Draka Smaka, kde ho muzu najit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_DRAGON_INFO, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_CAN_DRAGON_BE_HERE, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_DRAGON_INFO:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Musim jej navratit Ely Layi.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GET_LOST_FOOLS, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_DRAGON_INFO, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_GET_LOST_FOOLS:
            ptc->SendCloseGossip();
            me->Whisper("Hlupáku, nikdo nemůže opustit svět mrtvých! Draka Šmaka nikdy nenajdeš, a i kdyby se ti to povedlo, odsud se nikdy nedostanete! A teď mi zmiz z očí!", LANG_UNIVERSAL, player);
            player->AddAura(SPELL_AURA_PHASE_4, player);
            break;
        }
        return true;
    }
};

class event_363_spirit_dragon_gossip : public CreatureScript
{
public:
    event_363_spirit_dragon_gossip() : CreatureScript("event_363_spirit_dragon_gossip") { }
    enum GossipMenuId
    {
        MENU_WTF_URE_DEAD,
        MENU_SORRY,
        MENU_Y_U_HERE_THEN,
        MENU_GOOD_URE_ALIVE,
        MENU_HINT,
        MENU_START_EVENT_GOSSIP,
        MENU_START_EVENT,
        MENU_GTFO_NOW_URE_DEAD,
        MENU_START = 400004,
    };
    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (player->HasAura(SPELL_AURA_PHASE_8))
        {
            Aura* aur = player->GetAura(DUMMY_COUNTER_AURA);
            if (aur && (aur->GetEffect(EFFECT_0)->GetAmount() & EVENT_MASK_SPIRIT_REALM))
            {
                AnnounceGroup(player, me, player->GetName() + (player->getGender() ? " donesla magickou kostku Smakovi" : "donesl magickou kostku Smakovi"), EVENT_MASK_SPIRIT_REALM);
                AnnounceGroup(player, me, "Nyni muzete projit portalem.", EVENT_MASK_OBSTACLES,
                [](Player* player)
                {
                    player->NearTeleportTo(2336.41f, 2472.21f, 264.1f, 0.78f);
                    player->AddAura(SPELL_AURA_PHASE_16, player);
                });
            }
            else
                me->Whisper("Jeste stale nemas magickou tresouci se kostku.", LANG_UNIVERSAL, player, true);
            return true;
        }
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Ano, zabili me.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_SORRY, "", 0);
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (me->getGender() ? "Ne, utekla jsem jim a jsem stale nazivu." :
            "Ne, utekl jsem jim a jsem stale nazivu."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_Y_U_HERE_THEN, "", 0);
        ptc->SendGossipMenu(MENU_START + MENU_WTF_URE_DEAD, me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* me, uint32 /*sender*/, uint32 action) override
    {
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.ClearMenu();
        switch (action)
        {
        case GOSSIP_ACTION_INFO_DEF + MENU_SORRY:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Je to tva vina! Mohla jsem dal zit, kdybys nebyl tak neschopny budizknicemu drak!" :
                "Je to tva vina! Mohl jsem dal zit, kdybys nebyl tak neschopny budizknicemu drak!"), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GTFO_NOW_URE_DEAD, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Ne, promin, delala jsem si legraci. Nejsem po smrti, jsem stale ziva." :
                "Ne, promin, delal jsem si legraci. Nejsem po smrti, jsem stale zivy."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_Y_U_HERE_THEN, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_SORRY, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_GTFO_NOW_URE_DEAD:
            ptc->SendCloseGossip();
            me->Whisper("Za tohle budeš trpět!", LANG_UNIVERSAL, player);
            me->Whisper("Drak Smak ukoncil tvuj posmrtny zivot.", LANG_UNIVERSAL, player, true);
            me->Whisper("Try Again.", LANG_UNIVERSAL, player, true);
            MoveToSpiritRealm(player);
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_Y_U_HERE_THEN:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Posila me Ely Layi, prisla jsem Te zachranit." :
                "Posila me Ely Layi, prisel jsem Te zachranit."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GOOD_URE_ALIVE, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Prisla jsem se ti vysmat. Tomu jak si dopadl. Hahaha." :
                "Prisel jsem se ti vysmat. Tomu jak si dopadl. Hahaha."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GTFO_NOW_URE_DEAD, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_Y_U_HERE_THEN, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_GOOD_URE_ALIVE:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Ano, proto tu take jsem.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_HINT, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Ne, delam si legraci. Tvuj osud mi je lhostejny. Uzij si svoji smrt.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_GTFO_NOW_URE_DEAD, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_GOOD_URE_ALIVE, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_HINT:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Najit magickou tresouci se kostku na letajici lodi, dobre, rozumim.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_START_EVENT_GOSSIP, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_HINT, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_START_EVENT_GOSSIP:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "[Zacit event]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_START_EVENT, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_START_EVENT_GOSSIP, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_START_EVENT:
            ptc->SendCloseGossip();
            player->AddAura(SPELL_AURA_PHASE_8, player);
            player->RemoveAura(SPELL_AURA_PHASE_16);
            break;
        }
        return true;
    }
};

#define ENTRY_TANK 450079
#define ENTRY_DPS  450080
class event_363_spirit_combatant : public CreatureScript
{
public:
    event_363_spirit_combatant() : CreatureScript("event_363_spirit_combatant") { }
    bool OnGossipHello(Player* player, Creature* me) override
    {
        me->HandleSpellClick(player, 0);
        if (player->getGender())
            me->SetDisplayId(me->GetEntry() == ENTRY_TANK ? 2365 : 109);
        else
            me->SetDisplayId(me->GetNativeDisplayId());
        std::string whisper = std::string(player->getGender() ? "Stala ses " : "Stal ses") + std::string(me->GetEntry() == ENTRY_TANK ? "tankem." : "DPSkem.");
        me->Whisper(whisper, LANG_UNIVERSAL, player, true);
        player->PlayerTalkClass->SendCloseGossip();
        return true;
    }
};
#define SPELL_AURA_HELL_DUMMY_CHECK 100011
class event_363_dark_lord_combat : public CreatureScript
{
public:
    event_363_dark_lord_combat() : CreatureScript("event_363_dark_lord_combat") { }
    struct event_363_dark_lord_combatAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        uint32 portTimer;
        ObjectGuid killerGuid;
        bool immune;
        bool talked;
#define SPELL_FAILED 46694
#define PORT_DELAY    2500
        event_363_dark_lord_combatAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 1500;
            delay = 0;
            immune = false;
            talked = false;
            portTimer = 0;
            killerGuid = ObjectGuid::Empty;
        }

        ~event_363_dark_lord_combatAI()
        {
            // deleting creature, else reload
            if (!me->FindMap())
                return;

            if (portTimer)
                TeleportGroup();
        }

        void KilledUnit(Unit* /*victim*/) override
        {
            std::list<Creature*> creatures;
            me->GetCreatureListWithEntryInGrid(creatures, ENTRY_TANK, 30.0f);
            me->GetCreatureListWithEntryInGrid(creatures, ENTRY_DPS, 30.0f);
            creatures.remove_if([](Creature* c) { return c->isDead(); });
            if (creatures.empty())
            {
                std::list<Player*> players;
                me->GetPlayerListInGrid(players, 30.0f);
                if (!players.empty())
                    (*players.begin())->RemoveAura(SPELL_AURA_HELL_DUMMY_CHECK);
                CustomEvade();
            }
        }

        void TeleportGroup()
        {
            Player* nearest = me->SelectNearestPlayer(30.0f);
            if (!nearest)
                return;

            AnnounceGroup(nearest, me, "Promluvte si znovu s Temným Lordem.", EVENT_MASK_EMPTY,
            [](Player* player)
            {
                float xrand = (static_cast<float>(rand_norm()) - 0.5f) * 4.0f;
                float yrand = (static_cast<float>(rand_norm()) - 0.5f) * 4.0f;
                if (Aura* freeze = player->AddAura(FREEZE, player))
                {
                    SetFreezeInvisibility(player);
                    freeze->SetDuration(3000);
                }
                if (Aura* fly = player->AddAura(SPELL_AURA_FLY, player))
                    fly->SetDuration(3000);
                player->NearTeleportTo(2366.94f + xrand, 2327.76f + yrand, 196.1f, 3.85368f);
            });
        }

        const uint32 spells[6] =
        {
            45648, // holy
            45646, // fire
            45647, // nature
            45645, // frost
            45649, // shadow
            45650, // arcane
        };

        uint32 GetActiveSpellId()
        {
            uint32 power = me->GetMaxPower(POWER_HAPPINESS);
            switch (power)
            {
            case SPELL_SCHOOL_HOLY:
            case SPELL_SCHOOL_FIRE:
            case SPELL_SCHOOL_NATURE:
            case SPELL_SCHOOL_FROST:
            case SPELL_SCHOOL_SHADOW:
            case SPELL_SCHOOL_ARCANE:
                return spells[power - 1];
            }
            return 0;
        }

        void ChangeCastSchool(Player* target)
        {
            me->SetMaxPower(POWER_FOCUS, 0);
            uint32 school = urand(SPELL_SCHOOL_HOLY, SPELL_SCHOOL_ARCANE);
            me->SetMaxPower(POWER_HAPPINESS, school);
            if (target)
            {
                std::string text = "Temný Lord sesílá ";
                switch (school)
                {
                case SPELL_SCHOOL_HOLY:
                    text += "holy";
                    break;
                case SPELL_SCHOOL_FIRE:
                    text += "fire";
                    break;
                case SPELL_SCHOOL_NATURE:
                    text += "nature";
                    break;
                case SPELL_SCHOOL_FROST:
                    text += "frost";
                    break;
                case SPELL_SCHOOL_SHADOW:
                    text += "shadow";
                    break;
                case SPELL_SCHOOL_ARCANE:
                    text += "arcane";
                    break;
                }
                text += " spell.";
                AnnounceGroup(target, me, text, EVENT_MASK_EMPTY);
            }
        }

        void ChangeDamageTakenSchool(Player* target)
        {
            me->SetMaxPower(POWER_FOCUS, 0);
            uint32 school = urand(SPELL_SCHOOL_HOLY, SPELL_SCHOOL_ARCANE);
            me->SetMaxPower(POWER_RUNIC_POWER, 1 << school);
            if (target)
            {
                std::string text = "Temný lord je zranitelný pouze ";
                switch (school)
                {
                case SPELL_SCHOOL_HOLY:
                    text += "holy";
                    break;
                case SPELL_SCHOOL_FIRE:
                    text += "fire";
                    break;
                case SPELL_SCHOOL_NATURE:
                    text += "nature";
                    break;
                case SPELL_SCHOOL_FROST:
                    text += "frost";
                    break;
                case SPELL_SCHOOL_SHADOW:
                    text += "shadow";
                    break;
                case SPELL_SCHOOL_ARCANE:
                    text += "arcane";
                    break;
                }
                text += " spellama.";
                AnnounceGroup(target, me, text, EVENT_MASK_EMPTY);
            }
        }

        void ChangeSchool(Creature* target)
        {
            Player* owner = target->GetCharmerOrOwnerPlayerOrPlayerItself();
            ChangeCastSchool(owner);
            ChangeDamageTakenSchool(owner);
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (immune)
            {
                damage = 1;
                immune = false;
            }
            else
                damage = 1000;

            // prevent death
            if (damage >= me->GetHealth())
            {
                Player* killer = attacker->GetCharmerOrOwnerPlayerOrPlayerItself();
                damage = me->GetHealth() - 1;
                if (!killer)
                    return;

                AnnounceGroup(killer, me, "Porazili jste Temného Lorda.", EVENT_MASK_DARK_LORD_PVE,
                [](Player* player)
                {
                    player->AddAura(FREEZE, player);
                    // do not announce lose
                    player->RemoveAura(SPELL_AURA_HELL_DUMMY_CHECK, ObjectGuid::Empty, 0, AURA_REMOVE_BY_ENEMY_SPELL);
                });

                portTimer = PORT_DELAY;
                talked = false;
                CustomEvade();
            }
        }

        void CustomEvade()
        {
            me->SetMaxPower(POWER_RUNIC_POWER, 0);
            me->SetMaxPower(POWER_FOCUS, 0);
            me->SetMaxPower(POWER_HAPPINESS, 0);
            me->SetHealth(me->GetMaxHealth());

            std::list<Creature*> creatures;
            me->GetCreatureListWithEntryInGrid(creatures, ENTRY_TANK, 30.0f);
            me->GetCreatureListWithEntryInGrid(creatures, ENTRY_DPS, 30.0f);
            for (auto c : creatures)
            {
                c->Respawn(true);
                const Position& home = c->GetHomePosition();
                // force relocation
                c->NearTeleportTo(home.m_positionX, home.m_positionY, home.m_positionZ + 0.01f, home.GetOrientation());
            }
        }

        void SpellHit(Unit* caster, const SpellInfo* spell) override
        {
            if ((spell->SchoolMask & me->GetMaxPower(POWER_RUNIC_POWER)) == 0)
            {
                immune = true;
                Player* plr = caster->GetCharmerOrOwnerPlayerOrPlayerItself();
                if (plr)
                {
                    std::string text = "Temny lord je zranitelny pouze ";
                    switch (me->GetMaxPower(POWER_RUNIC_POWER))
                    {
                    case SPELL_SCHOOL_MASK_HOLY:
                        text += "holy";
                        break;
                    case SPELL_SCHOOL_MASK_FIRE:
                        text += "fire";
                        break;
                    case SPELL_SCHOOL_MASK_NATURE:
                        text += "nature";
                        break;
                    case SPELL_SCHOOL_MASK_FROST:
                        text += "frost";
                        break;
                    case SPELL_SCHOOL_MASK_SHADOW:
                        text += "shadow";
                        break;
                    case SPELL_SCHOOL_MASK_ARCANE:
                        text += "arcane";
                        break;
                    }
                    text += " spellama.";
                    me->Whisper(text, LANG_UNIVERSAL, plr, true);
                }
            }
        }

        class DarkLordTargetSelector
        {
        public:
            DarkLordTargetSelector(WorldObject const* obj, float range) : i_obj(obj), i_range(range) { }
            bool operator()(Unit* u)
            {
                if (u->GetEntry() != ENTRY_TANK && u->GetEntry() != ENTRY_DPS)
                    return false;
                if (!u->IsAlive() || !i_obj->IsWithinDistInMap(u, i_range))
                    return false;
                return true;
            }
        private:
            WorldObject const* i_obj;
            float i_range;
        };

        Creature* GetCreatureTarget(float distance)
        {
            std::list<Creature*> creatures;
            DarkLordTargetSelector u_check(me, distance);
            Trinity::CreatureListSearcher<DarkLordTargetSelector> searcher(me, creatures, u_check);
            me->VisitNearbyObject(distance, searcher);
            if (creatures.empty())
                return nullptr;

            for (auto creature : creatures)
                if (creature->GetEntry() == ENTRY_TANK)
                    return creature;
            auto itr = creatures.begin();
            std::advance(itr, urand(0, static_cast<uint32>(creatures.size() - 1)));
            return *itr;
        }

        void UpdateAI(uint32 diff) override
        {
            if (portTimer)
            {
                if (portTimer > diff)
                    portTimer -= diff;
                else
                {
                    if (!talked)
                    {
                        portTimer += PORT_DELAY * 3;
                        me->Talk("Dobrá, vzdávám se, porazili jste mě. Sejdeme se znovu nahoře a povím vám více o Šmakovi.", CHAT_MSG_MONSTER_SAY, LANG_UNIVERSAL, 35.0f, nullptr);
                        talked = true;
                    }
                    else
                    {
                        TeleportGroup();
                        portTimer = 0;
                        talked = false;
                    }
                }
            }

            if (delay > diff)
            {
                delay -= diff;
                return;
            }

            delay += timer;
            Creature* target = GetCreatureTarget(30.0f);
            if (!target)
            {
                CustomEvade();
                return;
            }

            uint32 spell = GetActiveSpellId();
            if (spell)
            {
                me->CastSpell(target, spell, true);
                target->RemoveAurasByType(SPELL_AURA_SCHOOL_ABSORB);
            }

            if (!me->GetMaxPower(POWER_HAPPINESS))
            {
                ChangeSchool(target);
                return;
            }

            uint32 focus = me->GetMaxPower(POWER_FOCUS);
            if (focus == 5)
                ChangeSchool(target);
            else
                me->SetMaxPower(POWER_FOCUS, focus + 1);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_dark_lord_combatAI(creature);
    }
};

class event_363_spirit_dark_lord_gossip : public CreatureScript
{
public:
    event_363_spirit_dark_lord_gossip() : CreatureScript("event_363_spirit_dark_lord_gossip") { }
    enum GossipMenuId
    {
        MENU_LF_DRAGON,
        MENU_WHERE_IS_DRAGON,
        MENU_STALKING_IS_RUDE,
        MENU_MY_HELP_IS_NOT_FREE,
        MENU_TELEPORT_TO_HELL,
        MENU_YOU_ARE_LOST_WITHOUT_ME,
        MENU_WELL_I_COULD_HELP_BYE,
        MENU_START = 400010,
    };
#define TP_POINTS_COUNT 4
    const Position tp[TP_POINTS_COUNT]
    {
        { 2338.0f, 2148.0f, 132.0f },
        { 2401.0f, 2197.0f, 132.0f },
        { 2463.0f, 2248.0f, 132.0f },
        { 2525.0f, 2298.0f, 132.0f }
    };

    uint8 GetFreeTeleportIndex(Creature* me)
    {
        uint32 teleportMask = me->GetMaxPower(POWER_HAPPINESS);
        for (uint8 i = 0; i < TP_POINTS_COUNT; ++i)
        {
            if ((teleportMask & (1 << i)) == 0)
            {
                me->SetMaxPower(POWER_HAPPINESS, teleportMask | (1 << i));
                return i;
            }
        }
        return 255;
    }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (!HasWholeGroupAura(player, SPELL_AURA_PHASE_4))
        {
            me->Whisper("Nemám ti co říct, dokud si ostatní členové tvé skupiny nepromluví se strážcem duší.", LANG_UNIVERSAL, player);
            return true;
        }

        int32 mask = GetAuraMask(player, me);
        if (mask & EVENT_MASK_DARK_LORD_PVE)
        {
            AnnounceGroup(player, me, "Draka Šmaka najdete v magickém stromu až navrchu.", EVENT_MASK_EMPTY,[](Player* player){player->AddAura(SPELL_AURA_PHASE_16, player);});
            return true;
        }
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Ano, shanim, nevis, kde ho mohu najit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_WHERE_IS_DRAGON, "", 0);
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Hahaha, ne, ty bys mi stejne nemohl pomoci.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_WELL_I_COULD_HELP_BYE, "", 0);
        menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Vis, ze je neslusne odposlouchavat cizi hovory?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_STALKING_IS_RUDE, "", 0);
        ptc->SendGossipMenu(MENU_START + MENU_LF_DRAGON, me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* me, uint32 /*sender*/, uint32 action) override
    {
        PlayerMenu* ptc = player->PlayerTalkClass;
        GossipMenu& menu = ptc->GetGossipMenu();
        menu.ClearMenu();
        switch (action)
        {
        case GOSSIP_ACTION_INFO_DEF + MENU_WHERE_IS_DRAGON:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "A kde ho tedy mohu nalezt?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_MY_HELP_IS_NOT_FREE, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Aww, zapomen na to, stejne bys mi nepomohl.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_WELL_I_COULD_HELP_BYE, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_WHERE_IS_DRAGON, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_STALKING_IS_RUDE:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "No dobra, kdyz mi muzes pomoci, kde ho tedy mohu najit?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_WHERE_IS_DRAGON, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Nejen ze odposlouchavas cizi hovory, ale jeste lzes! S tebou nechci mit nic spolecneho!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_WELL_I_COULD_HELP_BYE, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_STALKING_IS_RUDE, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_MY_HELP_IS_NOT_FREE:
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "Dobra, to zni jako fer nabidka.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_TELEPORT_TO_HELL, "", 0);
            menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, (player->getGender() ? "Myslela jsem si, ze za tim bude neco nekaleho. Nemam nejmensi zajem, diky." :
                "Myslel jsem si, ze za tim bude neco nekaleho. Nemam nejmensi zajem, diky."), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + MENU_YOU_ARE_LOST_WITHOUT_ME, "", 0);
            ptc->SendGossipMenu(MENU_START + MENU_MY_HELP_IS_NOT_FREE, me->GetGUID());
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_TELEPORT_TO_HELL:
            {
                uint8 tpIndex = GetFreeTeleportIndex(me);
                if (tpIndex > TP_POINTS_COUNT)
                    AnnounceGroup(player, me, "Event je plný, prosím strpení.", EVENT_MASK_EMPTY);
                else
                {
                    AnnounceGroup(player, me, "Temný Lord Vás teleportoval do svého království.", EVENT_MASK_EMPTY,
                    [&](Player* player)
                    {
                        Aura* aur = me->AddAura(SPELL_AURA_HELL_DUMMY_CHECK, player);
                        if (aur)
                            aur->GetEffect(EFFECT_0)->SetAmount(tpIndex);
                        if (!player->HasAura(SPELL_AURA_GHOST_FORM))
                            player->AddAura(SPELL_AURA_GHOST_FORM, player);
                        player->AddAura(SPELL_AURA_PHASE_4, player);
                        player->NearTeleportTo(tp[tpIndex].GetPositionX(), tp[tpIndex].GetPositionY(), tp[tpIndex].GetPositionZ(), 2.25f);
                    });
                }
            }
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_YOU_ARE_LOST_WITHOUT_ME:
            ptc->SendCloseGossip();
            me->Whisper("Ty to nechápeš, že? Nemáš beze mě nejmenší šanci Draka Šmaka najít, nebo se odsud dostat! Ale je to tvá volba, časem si to rozmyslíš.", LANG_UNIVERSAL, player);
            break;
        case GOSSIP_ACTION_INFO_DEF + MENU_WELL_I_COULD_HELP_BYE:
            ptc->SendCloseGossip();
            me->Whisper("Dobře, jak myslíš, je to tvá volba. Beze mě ale nemáš nejmenší šanci ho najít, hahaha!", LANG_UNIVERSAL, player);
            break;
        }
        return true;
    }
};

class spell_event363_hell_dummy_aura : public SpellScriptLoader
{
public:
    spell_event363_hell_dummy_aura() : SpellScriptLoader("spell_event363_hell_dummy_aura") { }
#define ARLA_DUMMY_NPC       450015
#define DARK_LORD_GOSSIP_NPC 450078
    class spell_event363_hell_dummy_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_event363_hell_dummy_aura_AuraScript);

        void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
        {
            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr)
            {
                SetDuration(0);
                return;
            }

            if (!plr->IsBeingTeleported())
            if (!plr->FindNearestCreature(ARLA_DUMMY_NPC, 30.0f))
            {
                SetDuration(0);
            }
        }

        void HandleRemove(AuraEffect const* eff, AuraEffectHandleModes)
        {
            Unit* caster = GetCaster();
            if (!caster || caster->GetEntry() != DARK_LORD_GOSSIP_NPC)
                return;

            Player* plr = GetUnitOwner()->ToPlayer();
            if (!plr)
                return;

            uint32 happiness = caster->GetMaxPower(POWER_HAPPINESS);
            uint32 amount = 1 << eff->GetAmount();
            if ((happiness & amount) == amount)
                caster->SetMaxPower(POWER_HAPPINESS, happiness - amount);

            AuraRemoveMode removeMode = GetTargetApplication()->GetRemoveMode();
            if (removeMode == AURA_REMOVE_BY_ENEMY_SPELL)
                return;

            AnnounceGroup(plr, caster->ToCreature(), "Selhali jste u temného Lorda. Vaše duše je nyní jeho.\nTry Again.", EVENT_MASK_EMPTY,
            [plr, this](Player* p)
            {
                if (p == plr)
                {
                    MoveToSpiritRealm(p);
                    return;
                }

                Aura* aura = p->GetAura(GetId());
                if (!aura)
                    return;

                if (p != plr)
                {
                    p->RemoveAura(aura, AURA_REMOVE_BY_ENEMY_SPELL);
                    MoveToSpiritRealm(p);
                }
            });
        }

        void Register()
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_event363_hell_dummy_aura_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            OnEffectRemove += AuraEffectRemoveFn(spell_event363_hell_dummy_aura_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_event363_hell_dummy_aura_AuraScript();
    }
};

class event_363_spirit_realm_obstacles_trigger : public CreatureScript
{
public:
#define SPIRIT_GUARD 450091
    event_363_spirit_realm_obstacles_trigger() : CreatureScript("event_363_spirit_realm_obstacles_trigger") { }
    struct event_363_spirit_realm_obstacles_triggerAI : public CreatureAI
    {
        event_363_spirit_realm_obstacles_triggerAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 5.0f;
        }

        bool CanStartEvent(Player* p)
        {
            if (Aura* counter = p->GetAura(DUMMY_COUNTER_AURA))
            {
                if ((counter->GetEffect(EFFECT_0)->GetAmount() & EVENT_MASK_SPIRIT_REALM) == 0)
                    return false;
            }
            else
                return false;

            if (Aura* helper = p->GetAura(DUMMY_COUNTER_HELPER))
            {
                if (helper->GetEffect(EFFECT_0)->GetAmount() & EVENT_MASK_OBSTACLES)
                    return false;
            }
            else return p->AddAura(DUMMY_COUNTER_HELPER, p) != nullptr;
            return true;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (me->GetDistance(unit) >= me->m_SightDistance)
                return;

            Player* p = unit->ToPlayer();

            if (!CanStartEvent(p))
                return;

            AuraEffect* helper = p->GetAura(DUMMY_COUNTER_HELPER)->GetEffect(EFFECT_0);
            helper->SetAmount(helper->GetAmount() | EVENT_MASK_OBSTACLES);

            TempSummon* spirit = me->SummonCreature(SPIRIT_GUARD, 2443.0f, 2411.0f, 266.0f, 3.0f);
            spirit->SetOwnerGUID(p->GetGUID());
            spirit->Whisper("Co to děláš, " + p->GetName() + ", vrať tu kostku zpět!", LANG_UNIVERSAL, p);
            spirit->Whisper("Strazce dusi te pronasleduje!", LANG_UNIVERSAL, p, true);
        }

        void UpdateAI(uint32) override { }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_spirit_realm_obstacles_triggerAI(creature);
    }
};

class event_363_spirit_realm_spirit_guardian : public CreatureScript
{
public:
#define SPIRIT_GUARD 450091
    event_363_spirit_realm_spirit_guardian() : CreatureScript("event_363_spirit_realm_spirit_guardian") { }
    struct event_363_spirit_realm_spirit_guardianAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_spirit_realm_spirit_guardianAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 1000;
            delay = 0;
        }

        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            Player* owner = ObjectAccessor::GetPlayer(*me, me->GetOwnerGUID());
            if (seer->GetTypeId() == TYPEID_PLAYER && seer->ToPlayer()->IsGameMaster())
                return false;

            return seer != owner;
        }

        Player* FollowOwner()
        {
            Player* owner = ObjectAccessor::GetPlayer(*me, me->GetOwnerGUID());
            if (!owner || !owner->HasAura(SPELL_AURA_GHOST_FORM))
            {
                me->DespawnOrUnsummon();
                return nullptr;
            }
            me->GetMotionMaster()->MovePoint(0, *owner, false);
            delay = timer;
            return owner;
        }

        void UpdateAI(uint32 diff) override
        {
            if (!me->isMoving())
                FollowOwner();
            else if (delay > diff)
                delay -= diff;
            else if (Player* owner = FollowOwner())
            {
                if (me->GetDistance(owner) < 1.0f)
                {
                    Aura* helper = owner->GetAura(DUMMY_COUNTER_HELPER);
                    AuraEffect* hEff = helper ? helper->GetEffect(EFFECT_0) : nullptr;
                    Aura* counter = owner->GetAura(DUMMY_COUNTER_AURA);
                    AuraEffect* cEff = counter ? counter->GetEffect(EFFECT_0) : nullptr;
                    if (hEff)
                    {
                        if (hEff->GetAmount() & EVENT_MASK_SPIRIT_REALM)
                            hEff->SetAmount(hEff->GetAmount() - EVENT_MASK_SPIRIT_REALM);
                        if (hEff->GetAmount() & EVENT_MASK_OBSTACLES)
                            hEff->SetAmount(hEff->GetAmount() - EVENT_MASK_OBSTACLES);
                    }
                    if (cEff)
                    {
                        if (cEff->GetAmount() & EVENT_MASK_SPIRIT_REALM)
                            cEff->SetAmount(cEff->GetAmount() - EVENT_MASK_SPIRIT_REALM);
                    }
                    me->Whisper((owner->getGender() ? "Nestihla si donést kostku Smakovi.\nTry Again!" : "Nestihl si donést kostku Smakovi.\nTry Again!"), LANG_UNIVERSAL, owner, true);
                    MoveToSpiritRealm(owner);
                    me->DespawnOrUnsummon();
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_spirit_realm_spirit_guardianAI(creature);
    }
};

class event_363_ghost_shaking_box : public GameObjectScript
{
public:
    event_363_ghost_shaking_box() : GameObjectScript("event_363_ghost_shaking_box") { }

    struct event_363_ghost_shaking_boxAI : public GameObjectAI
    {
        uint32 timer;
        event_363_ghost_shaking_boxAI(GameObject* go) : GameObjectAI(go)
        {

        }

        //@TODO: find another function similiar to CreatureAI::MoveInLineOfSight()
        void UpdateAI(uint32 /*diff*/)
        {
            std::list<Player*> players;
            go->GetPlayerListInGrid(players, 2.0f);
            for (auto p : players)
            {
                Aura* aur = p->GetAura(DUMMY_COUNTER_AURA);
                if (!aur)
                {
                    aur = p->AddAura(DUMMY_COUNTER_AURA, p);
                    if (!aur)
                        continue;
                }

                AuraEffect* eff = aur->GetEffect(EFFECT_0);
                if (!eff || (eff->GetAmount() & EVENT_MASK_SPIRIT_REALM))
                    continue;
                eff->SetAmount(eff->GetAmount() | EVENT_MASK_SPIRIT_REALM);
                p->Unit::Whisper((p->getGender() ? "Ziskala si tresouci se magickou kostku!" : "Ziskal si tresouci se magickou kostku!"), LANG_UNIVERSAL, p, true);
            }
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new event_363_ghost_shaking_boxAI(go);
    }
};


class event_363_catapult : public CreatureScript
{
public:
#define SPELL_BOULDER 59733
    event_363_catapult() : CreatureScript("event_363_catapult") { }
    struct event_363_catapultAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_catapultAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 7000;
            delay = 0;
        }

        void DamageDealt(Unit* target, uint32& damage, DamageEffectType) override
        {
            if (target->GetHealth() <= damage)
                damage = target->GetHealth() - 1;
        }

        void UpdateAI(uint32 diff) override
        {
            if (delay < diff)
            {
                delay += timer + urand(0, 5000);
                std::list<Creature*> targets;
                me->GetCreatureListWithEntryInGrid(targets, ARLA_DUMMY_NPC, 70.0f);
                if (targets.empty())
                    return;

                auto itr = targets.begin();
                std::advance(itr, urand(0, static_cast<uint32>(targets.size() - 1)));
                me->SetFacingToObject(*itr);
                me->CastSpell(*itr, SPELL_BOULDER, false);
            }
            else
                delay -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_catapultAI(creature);
    }
};

class event_363_cannon : public CreatureScript
{
public:
#define SPELL_FIRE_CANNON 57609
#define CATAPULT_ENTRY 450092
#define DESTROYED_CATAPULT_ENTRY 33059
    event_363_cannon() : CreatureScript("event_363_cannon") { }
    struct event_363_cannonAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_cannonAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 7000;
            delay = 0;
        }

        void DamageDealt(Unit* target, uint32& damage, DamageEffectType) override
        {
            if (target->GetHealth() <= damage)
                damage = target->GetHealth() - 1;
        }

        void UpdateAI(uint32 diff) override
        {
            if (delay < diff)
            {
                delay += timer + urand(0, 5000);
                std::list<Creature*> targets;
                me->GetCreatureListWithEntryInGrid(targets, CATAPULT_ENTRY, 70.0f);
                me->GetCreatureListWithEntryInGrid(targets, DESTROYED_CATAPULT_ENTRY, 70.0f);
                if (targets.empty())
                    return;

                auto itr = targets.begin();
                std::advance(itr, urand(0, static_cast<uint32>(targets.size() - 1)));
                me->SetFacingToObject(*itr);
                me->CastSpell(*itr, SPELL_FIRE_CANNON, true);
            }
            else
                delay -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_cannonAI(creature);
    }
};

class event_363_town_repair_worker : public CreatureScript
{
public:
#define SPELL_REPAIR_WALL 57586
    event_363_town_repair_worker() : CreatureScript("event_363_town_repair_worker") { }
    struct event_363_town_repair_workerAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_town_repair_workerAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 2500;
            delay = 0;
        }

        void UpdateAI(uint32 diff) override
        {
            if (delay < diff)
            {
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 1902);
                me->CastSpell(me, SPELL_REPAIR_WALL, true);
                delay += timer + urand(0, 1000);
            }
            else
                delay -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_town_repair_workerAI(creature);
    }
};

class event_363_town_soldier : public CreatureScript
{
public:
#define SPELL_REPAIR_WALL 57586
    event_363_town_soldier() : CreatureScript("event_363_town_soldier") { }
    struct event_363_town_soldierAI : public CreatureAI
    {
        uint32 timer;
        uint32 delay;
        event_363_town_soldierAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 3000;
            delay = 0;
            switch (urand(0, 8))
            {
            case 0:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 18876);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 18825);
                break;
            case 1:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 23456);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 0);
                break;
            case 2:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 12584);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 18825);
                break;
            case 3:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 18830);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 0);
                break;
            case 4:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 18827);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 18825);
                break;
            case 5:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 18865);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 18825);
                break;
            case 6:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 23454);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 18825);
                break;
            case 7:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 23455);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 0);
                break;
            case 8:
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 18867);
                me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID + 1, 0);
                break;
            }
        }

        void UpdateAI(uint32 /*diff*/) override
        {

        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_363_town_soldierAI(creature);
    }
};

class event_363_spirit_realm_helper : public CreatureScript
{
public:
#define realm_helper_txt_male "Propůjčila jsem ti svého draka a tys ho nechal umřít! Musíš projít portálem, tam najít jeho ducha a přivést ho zpět! Já zatím podržím shield proti nepřátelským orkům."
#define realm_helper_txt_female "Propůjčila jsem ti svého draka a tys ho nechala umřít! Musíš projít portálem, tam najít jeho ducha a přivést ho zpět! Já zatím podržím shield proti nepřátelským orkům."
    event_363_spirit_realm_helper() : CreatureScript(HELPER_TXT(event_363_spirit_realm_helper)) { }
    bool OnGossipHello(Player* player, Creature* me) override
    {
        if (GetAuraMask(player, me) & EVENT_MASK_OBSTACLES)
        {
            TempSummon* dragon = me->SummonCreature(DRAGON_VEHICLE_ENTRY, me->GetPositionX() + 1.0f, me->GetPositionY() + 2.0f, me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_TIMED_DESPAWN, 2 * IN_MILLISECONDS * MINUTE);
            if (!dragon)
                return true;

            if (dragon->GetAI())
            {
                dragon->GetAI()->SetData(0, 1);
                dragon->SetFacingToObject(me);
                dragon->HandleSpellClick(player, 0);
                dragon->Whisper("Odnesu tě až do města!", LANG_UNIVERSAL, player);
            }
            else
                dragon->DespawnOrUnsummon(0);
        }
        else
            me->Whisper((player->getGender() ? realm_helper_txt_female : realm_helper_txt_male), LANG_UNIVERSAL, player);
        return true;
    }
    struct HELPER_AI(event_363_spirit_realm_helper) : CreatureAI
    {
        HELPER_AI(event_363_spirit_realm_helper)(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 3.0f;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            Player* p = unit->ToPlayer();
            if (Aura* aur = p->GetAura(DUMMY_COUNTER_HELPER))
            {
                if (aur->GetEffect(EFFECT_0)->GetAmount() & EVENT_MASK_SPIRIT_REALM)
                    return;
            }
            else
                p->AddAura(DUMMY_COUNTER_HELPER, p);

            AuraEffect* eff = p->GetAura(DUMMY_COUNTER_HELPER)->GetEffect(EFFECT_0);
            eff->SetAmount(eff->GetAmount() | EVENT_MASK_SPIRIT_REALM);
            me->Whisper((p->getGender() ? realm_helper_txt_female : realm_helper_txt_male), LANG_UNIVERSAL, p);
        }

        void UpdateAI(uint32) override
        {
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new HELPER_AI(event_363_spirit_realm_helper)(creature);
    }
};


class spell_event363_counter_helper_auras : public SpellScriptLoader
{
public:
    spell_event363_counter_helper_auras() : SpellScriptLoader("spell_event363_counter_helper_auras") { }
    class spell_event363_counter_helper_auras_AuraScript : public AuraScript
    {
#define SPIRIT_WELCOMER_ENTRY 450076
        PrepareAuraScript(spell_event363_counter_helper_auras_AuraScript);
        void TryFreezePlayer(Player* owner)
        {
            if (owner->IsLoading())
            {
                SetFreezeInvisibility(owner);
                if (Aura* freeze = owner->AddAura(FREEZE, owner))
                    freeze->SetDuration(5000);

                if (Aura* fly = owner->AddAura(SPELL_AURA_FLY, owner))
                    fly->SetDuration(5000);
            }
            else if (Aura* fly = owner->GetAura(SPELL_AURA_FLY))
            {
                if (owner->FindNearestCreature(SPIRIT_WELCOMER_ENTRY, 3.2f))
                    return;

                SetFreezeInvisibility(owner);

                if (fly->GetDuration() > 3000)
                    fly->SetDuration(3000);

                else if (Aura* freeze = owner->AddAura(FREEZE, owner))
                    freeze->SetDuration(fly->GetDuration() + 500);

                if (GetId() == DUMMY_COUNTER_HELPER)
                {
                    if (AuraEffect* eff = GetEffect(EFFECT_0))
                    {
                        int32 amount = eff->GetAmount();
                        if (amount & EVENT_MASK_OBSTACLES)
                        {
                            eff->SetAmount(amount - EVENT_MASK_OBSTACLES);
                            owner->Unit::Whisper("Musis dojit znovu pro kostku kvuli relogu!", LANG_UNIVERSAL, owner, true);
                        }
                    }
                }
            }
            else if (!owner->GetAura(FREEZE) && owner->m_serverSideVisibility.GetValue(SERVERSIDE_VISIBILITY_GM) == SEC_CONSOLE)
                owner->m_serverSideVisibility.SetValue(SERVERSIDE_VISIBILITY_GM, SEC_PLAYER);
        }

        void CalcPeriodic(AuraEffect const* /*eff*/, bool& isPeriodic, int32& amplitude)
        {
            Player* owner = GetUnitOwner()->ToPlayer();
            if (!owner)
                return;

            if (isPeriodic)
            {
                amplitude = 1000;
                return;
            }

            isPeriodic = true;
            amplitude = 100;
            TryFreezePlayer(owner);
        }

        void HandleEffectPeriodic(AuraEffect const*)
        {
            Player* owner = GetUnitOwner()->ToPlayer();
            if (!owner)
                return;

            if (AuraEffect* eff = GetEffect(EFFECT_0))
                eff->CalculatePeriodic(owner, false, false);

            if (owner->duel)
                owner->DuelComplete(DUEL_INTERRUPTED);
            
            TryFreezePlayer(owner);
        }

        void HandleRemove(AuraEffect const*, AuraEffectHandleModes)
        {
            if (Aura* freeze = GetUnitOwner()->GetAura(FREEZE))
                freeze->SetDuration(0);
            GetUnitOwner()->m_serverSideVisibility.SetValue(SERVERSIDE_VISIBILITY_GM, SEC_PLAYER);
        }

        void Register()
        {
            OnEffectRemove += AuraEffectRemoveFn(spell_event363_counter_helper_auras_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            DoEffectCalcPeriodic += AuraEffectCalcPeriodicFn(spell_event363_counter_helper_auras_AuraScript::CalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_event363_counter_helper_auras_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_DUMMY);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_event363_counter_helper_auras_AuraScript();
    }
};

class event_363_illusion_vendor : public CreatureScript
{
public:
    event_363_illusion_vendor() : CreatureScript("event_363_illusion_vendor") { }

    bool OnGossipHello(Player* plr, Creature* me) override
    {
        bool interract = false;
        if (Aura* counter = plr->GetAura(DUMMY_COUNTER_AURA))
        {
            if (AuraEffect* eff = counter->GetEffect(EFFECT_0))
            {
                if (eff->GetAmount() & EVENT_MASK_ILLUSION)
                {
                    plr->GetSession()->SendListInventory(me->GetGUID());
                    interract = true;
                }
            }
        }

        if (!interract)
            me->Whisper("Musíš nejprve získat ticket na iluzi!", LANG_UNIVERSAL, plr, true);

        return true;
    }
};

class event_363_illusion_ticket_master : public CreatureScript
{
public:
    event_363_illusion_ticket_master() : CreatureScript("event_363_illusion_ticket_master") { }

    bool OnGossipHello(Player* plr, Creature* me) override
    {
        Aura* counter = plr->GetAura(DUMMY_COUNTER_AURA);
        if (!counter)
            counter = plr->AddAura(DUMMY_COUNTER_AURA, plr);

        if (counter)
        {
            if (AuraEffect* eff = counter->GetEffect(EFFECT_0))
            {
                if ((eff->GetAmount() & EVENT_MASK_ILLUSION) == 0)
                {
                    eff->SetAmount(eff->GetAmount() | EVENT_MASK_ILLUSION);
                    me->Whisper((plr->getGender() ? "Získala si ticket na iluzi!" : "Získal si ticket na iluzi!"), LANG_UNIVERSAL, plr, true);
                }
            }
        }
        return true;
    }
};

HELPER(event_363_pig_helper, EVENT_MASK_PIGS, "Vybral sis event Prasátka. V tomto eventu budeš muset najít správné prasátko. Dávej však pozor! Některá koupou. Hodně štěstí!",
                                              "Vybrala sis event Prasátka. V tomto eventu budeš muset najít správné prasátko. Dávej však pozor! Některá kopou. Hodně štěstí!");
HELPER(event_363_jump_helper, EVENT_MASK_JUMP, "Vybral sis event Skákačku! Vyskákej až nahoru a zachraň mojí pandu, která uvízla nahoře a nemůže se dostat dolu. Hodně štěstí!",
                                               "Vybrala sis event Skákačku! Vyskákej až nahoru a zachraň mojí pandu, která uvízla nahoře a nemůže se dostat dolu. Hodně štěstí!");
HELPER(event_363_teleport_helper, EVENT_MASK_FIND_PORT, "Vybral sis event Teleporty. Klikej na vlky a sleduj, který Tě portuje nahoru, a který naopak maří Tvé snahy dosáhnout vysněné teleportace. Hodně štěstí!",
                                                        "Vybrala sis event Teleporty. Klikej na vlky a sleduj, který Tě portuje nahoru, a který naopak maří Tvé snahy dosáhnout vysněné teleportace. Hodně štěstí!");
HELPER(event_363_labyrint_helper, EVENT_MASK_LABYRINT, "Vybral sis event Bludiště. V tomto eventu musíš najít auru, která se v něm ukrývá. Hodně štěstí!",
                                                       "Vybrala sis event Bludiště. V tomto eventu musíš najít auru, která se v něm ukrývá. Hodně štěstí!");
HELPER(event_363_boxes_helper, EVENT_MASK_BOXES, "Vybral sis event Magické krabičky. V tomto eventu se musíš dostat do koruny stromů. Krabičky Tě dovedou přímo k magickému jablku. Hodně štěstí!",
                                                 "Vybrala sis event Magické krabičky. V tomto eventu se musíš dostat do koruny stromů. Krabičky Tě dovedou přímo k magickému jablku. Hodně štěstí!");
HELPER(event_363_meadow_helper, EVENT_MASK_MEADOW, "Vybral sis event Kouzelný les. V tomto eventu musíš získat elixír. Najdi Mad Alchemistu, který ti elixír vyrobí.",
                                                   "Vybrala sis event Kouzelný les. V tomto eventu musíš získat elixír. Najdi Mad Alchemistu, který ti elixír vyrobí.");
void AddSC_event_363()
{
    new event_363_event_handler();
    new event_363_door_opener();
    new event_363_pig_handler();
    new event_363_pig();
    new event_363_boxes();
    new event_363_labyrint_handler();
    new event_363_labyrint_finish();
    new event_363_boxes_finish();
    new event_363_jump_finish();
    new event_363_find_port_finish();
    new event_363_find_port_handler();
    new event_363_find_port();
    new event_363_drak_smak();
    new event_363_troll_hunter();
    new event_363_pig_helper();
    new event_363_jump_helper();
    new event_363_teleport_helper();
    new event_363_labyrint_helper();
    new event_363_boxes_helper();
    new event_363_meadow_helper();
    new event_363_meadow_handler();
    new event_363_meadow_flower_spawner();
    new event_363_meadow_flower();
    new spell_event363_alchemist_counter();
    new event_363_guardian();
    new event_363_ghost_walker();
    new event_363_ghost_portal();
    new event_363_spirit_realm_helper();
    new event_363_spirit_realm_welcomer();
    new event_363_spirit_guard_gossip();
    new event_363_spirit_dragon_gossip();
    new event_363_spirit_combatant();
    new event_363_ghost_shaking_box();
    new event_363_dark_lord_combat();
    new event_363_spirit_dark_lord_gossip();
    new spell_event363_hell_dummy_aura();
    new event_363_spirit_realm_obstacles_trigger();
    new event_363_spirit_realm_spirit_guardian();
    new event_363_catapult();
    new event_363_cannon();
    new event_363_town_repair_worker();
    new event_363_town_soldier();
    new spell_event363_counter_helper_auras();
    new event_363_illusion_vendor();
    new event_363_illusion_ticket_master();
}

/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: Arena Spectator
%Complete: 100
Comment: Script allow spectate arena games
Category: Custom Script
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Chat.h"
#include "ArenaTeamMgr.h"
#include "BattlegroundMgr.h"
#include "WorldSession.h"
#include "Player.h"
#include "Creature.h"
#include "ObjectAccessor.h"

class arena_spectator_commands : public CommandScript
{
public:
    arena_spectator_commands() : CommandScript("arena_spectator_commands") { }

    static bool HandleSpectateCommand(ChatHandler* handler, char const* args)
    {
        Player* target;
        ObjectGuid target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();

        if (player->isSpectator())
        {
            handler->PSendSysMessage("You are already spectating, leave first!");
            handler->SetSentErrorMessage(true);
            return false;
        }
        if (target == player || target_guid == player->GetGUID())
        {
            handler->PSendSysMessage("Can't spectate self.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->IsInCombat())
        {
            handler->PSendSysMessage("You are in combat.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!target)
        {
            handler->PSendSysMessage("Target does not exist.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->IsMounted())
        {
            handler->PSendSysMessage("Cannot Spectate while mounted.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target && (target->HasAura(32728) || target->HasAura(32727))) // Check Arena Preparation thx XXsupr
        {
            handler->PSendSysMessage("Cant do that. Arena didn't started.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetPet())
        {
            handler->PSendSysMessage("You must hide your pet.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap()->IsBattlegroundOrArena() && !player->isSpectator())
        {
            handler->PSendSysMessage("You are already on battleground or arena.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Map* cMap = target->GetMap();
        if (!cMap->IsBattleArena())
        {
            handler->PSendSysMessage("Player was not found in arena.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap()->IsBattleground())
        {
            handler->PSendSysMessage("Can't do that while you are in battleground.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->isSpectator())
        {
            handler->PSendSysMessage("Can't do that. Your target is spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // all's well, set bg id
        // when porting out from the bg, it will be reset to 0
        player->SetBattlegroundId(target->GetBattlegroundId(), target->GetBattlegroundTypeId());
        // remember current position as entry point for return at bg end teleportation
        if (!player->GetMap()->IsBattlegroundOrArena())
            player->SetBattlegroundEntryPoint();

        // stop flight if need
        if (player->IsInFlight())
        {
            player->GetMotionMaster()->MovementExpired();
            player->CleanupAfterTaxiFlight();
        }
        // save only in non-flight case
        else
            player->SaveRecallPosition();

        // search for two teams
        Battleground *bGround = target->GetBattleground();
        if (bGround->isRated())
        {
            uint32 slot = bGround->GetArenaType() - 2;
            if (bGround->GetArenaType() > 3)
                slot = 2;
            uint32 firstTeamID = target->GetArenaTeamId(slot);
            uint32 secondTeamID = 0;
            Player *firstTeamMember  = target;
            Player *secondTeamMember = nullptr;
            for (Battleground::BattlegroundPlayerMap::const_iterator itr = bGround->GetPlayers().begin(); itr != bGround->GetPlayers().end(); ++itr)
                if (Player* tmplayer = ObjectAccessor::FindPlayer(itr->first))
                {
                    if (tmplayer->isSpectator())
                        continue;

                    uint32 tmpID = tmplayer->GetArenaTeamId(slot);
                    if (tmpID != firstTeamID && tmpID > 0)
                    {
                        secondTeamID = tmpID;
                        secondTeamMember = tmplayer;
                        break;
                    }
                }

            if (firstTeamID > 0 && secondTeamID > 0 && secondTeamMember)
            {
                ArenaTeam *firstTeam  = sArenaTeamMgr->GetArenaTeamById(firstTeamID);
                ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(secondTeamID);
                if (firstTeam && secondTeam)
                {
                    handler->PSendSysMessage("You entered to rated arena.");
                    handler->PSendSysMessage("Teams:");
                    handler->PSendSysMessage("%s - %s", firstTeam->GetName(), secondTeam->GetName());
                    handler->PSendSysMessage("%u(%u) - %u(%u)", firstTeam->GetRating(), firstTeam->GetAverageMMR(firstTeamMember->GetGroup()),
                                             secondTeam->GetRating(), secondTeam->GetAverageMMR(secondTeamMember->GetGroup()));
                }
            }
        }

        // to point to see at target with same orientation
        float x, y, z;
        target->GetContactPoint(player, x, y, z);
        player->TeleportTo(target->GetMapId(), x, y, z, player->GetAngle(target), TELE_TO_GM_MODE);
        player->SetPhaseMask(target->GetPhaseMask(), true);
        player->SetSpectate(true);
        target->GetBattleground()->AddSpectator(player->GetGUID());

        return true;
    }

    static bool HandleSpectateCancelCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player =  handler->GetSession()->GetPlayer();

        if (!player->isSpectator())
        {
            handler->PSendSysMessage("You are not spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        player->CancelSpectate();
        player->TeleportToBGEntryPoint();

        return true;
    }

    static bool HandleSpectateFromCommand(ChatHandler* handler, const char *args)
    {
        Player* target;
        ObjectGuid target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();

        if (!target)
        {
            handler->PSendSysMessage("Cant find player.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isSpectator())
        {
            handler->PSendSysMessage("You are not spectator, spectate someone first.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->isSpectator() && target != player)
        {
            handler->PSendSysMessage("Can`t do that. Your target is spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap() != target->GetMap())
        {
            handler->PSendSysMessage("Cant do that. Different arenas?");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // check for arena preperation
        // if exists than battle didn`t begin
        if (target->HasAura(32728) || target->HasAura(32727))
        {
            handler->PSendSysMessage("Cant do that. Arena didn`t started.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        (target == player && player->getSpectateFrom()) ? player->SetViewpoint(player->getSpectateFrom(), false) :
                                                          player->SetViewpoint(target, true);
        return true;
    }

    static bool HandleSpectateResetCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
        {
            handler->PSendSysMessage("Cant find player.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isSpectator())
        {
            handler->PSendSysMessage("You are not spectator!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Battleground *bGround = player->GetBattleground();
        if (!bGround)
            return false;

        if (bGround->GetStatus() != STATUS_IN_PROGRESS)
            return true;

        for (Battleground::BattlegroundPlayerMap::const_iterator itr = bGround->GetPlayers().begin(); itr != bGround->GetPlayers().end(); ++itr)
            if (Player* tmplayer = ObjectAccessor::FindPlayer(itr->first))
            {
                if (tmplayer->isSpectator())
                    continue;

                uint32 tmpID = bGround->GetPlayerTeam(tmplayer->GetGUID());

                // generate addon massage
                std::string pName = tmplayer->GetName();
                std::string tName = "";

                if (Player *target = tmplayer->GetSelectedPlayer())
                    tName = target->GetName();

                SpectatorAddonMsg msg;
                msg.SetPlayer(pName);
                if (tName != "")
                    msg.SetTarget(tName);
                msg.SetStatus(tmplayer->IsAlive());
                msg.SetClass(tmplayer->getClass());
                msg.SetCurrentHP(tmplayer->GetHealth());
                msg.SetMaxHP(tmplayer->GetMaxHealth());
                Powers powerType = tmplayer->getPowerType();
                msg.SetMaxPower(tmplayer->GetMaxPower(powerType));
                msg.SetCurrentPower(tmplayer->GetPower(powerType));
                msg.SetPowerType(powerType);
                msg.SetTeam(tmpID);
                msg.SendPacket(player->GetGUID());
            }

        return true;
    }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> spectateCommandTable =
        {
            { "player",         150,      true,  &HandleSpectateCommand,        "" },
            { "view",           150,      true,  &HandleSpectateFromCommand,    "" },
            { "reset",          150,      true,  &HandleSpectateResetCommand,   "" },
            { "leave",          150,      true,  &HandleSpectateCancelCommand,  "" },
        };

        static std::vector<ChatCommand> commandTable =
        {
            { "spectate",       150, false,  nullptr, "", spectateCommandTable },
        };
        return commandTable;
    }
};


enum NpcSpectatorAtions {
    // will be used for scrolling
    NPC_SPECTATOR_ACTION_LIST_GAMES         = 1000,
    NPC_SPECTATOR_ACTION_LIST_TOP_GAMES     = 2000,

    // NPC_SPECTATOR_ACTION_SELECTED_PLAYER + player.Guid()
    NPC_SPECTATOR_ACTION_SELECTED_PLAYER    = 3000
};

const uint16 TopGamesRating = 1800;
const uint8  GamesOnPage    = 20;

class npc_arena_spectator : public CreatureScript
{
public:
    npc_arena_spectator() : CreatureScript("npc_arena_spectator") { }

    bool OnGossipHello(Player* player, Creature* creture) override
    {
        AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Games: 2100+mmr", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_TOP_GAMES);
        AddGossipItemFor(player, GOSSIP_ICON_CHAT, "Games: 1000+mmr", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES);
        SendGossipMenuFor(player,DEFAULT_GOSSIP_MESSAGE, creture->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action) override
    {
        player->PlayerTalkClass->ClearMenus();
        if (action >= NPC_SPECTATOR_ACTION_LIST_GAMES && action < NPC_SPECTATOR_ACTION_LIST_TOP_GAMES)
        {
            ShowPage(player, action - NPC_SPECTATOR_ACTION_LIST_GAMES, false);
            SendGossipMenuFor(player,DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
        else if (action >= NPC_SPECTATOR_ACTION_LIST_TOP_GAMES && action < NPC_SPECTATOR_ACTION_SELECTED_PLAYER)
        {
            ShowPage(player, action - NPC_SPECTATOR_ACTION_LIST_TOP_GAMES, true);
            SendGossipMenuFor(player,DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
        else
        {

            uint32 guid = action - NPC_SPECTATOR_ACTION_SELECTED_PLAYER;

            if (Player* target = ObjectAccessor::FindPlayer(ObjectGuid(HighGuid::Player, guid)))
            {
                ChatHandler handler(player->GetSession());
                const char* targetName = target->GetName().c_str();
                arena_spectator_commands::HandleSpectateCommand(&handler, targetName);
            }
        }
        return true;
    }

    std::string GetClassNameById(uint8 id)
    {
        std::string sClass = "";
        switch (id)
        {
            case CLASS_WARRIOR:         sClass = "Warrior ";        break;
            case CLASS_PALADIN:         sClass = "Pala ";           break;
            case CLASS_HUNTER:          sClass = "Hunt ";           break;
            case CLASS_ROGUE:           sClass = "Rogue ";          break;
            case CLASS_PRIEST:          sClass = "Priest ";         break;
            case CLASS_DEATH_KNIGHT:    sClass = "D.K. ";             break;
            case CLASS_SHAMAN:          sClass = "Shama ";          break;
            case CLASS_MAGE:            sClass = "Mage ";           break;
            case CLASS_WARLOCK:         sClass = "Warlock ";        break;
            case CLASS_DRUID:           sClass = "Druid ";          break;
        }
        return sClass;
    }

    std::string GetGamesStringData(Battleground* team, uint16 mmr)
    {
        std::string teamsMember[BG_TEAMS_COUNT];
        uint32 firstTeamId = 0;
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = team->GetPlayers().begin(); itr != team->GetPlayers().end(); ++itr)
            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            {
                if (player->isSpectator())
                    continue;

                uint32 team = itr->second.Team;
                if (!firstTeamId)
                    firstTeamId = team;

                teamsMember[firstTeamId == team] += GetClassNameById(player->getClass());
            }

        std::string data = teamsMember[0] + "(";
        std::stringstream ss;
        ss << mmr;
        data += ss.str();
        data += ") - " + teamsMember[1];
        return data;
    }

    ObjectGuid GetFirstPlayerGuid(Battleground* team)
    {
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = team->GetPlayers().begin(); itr != team->GetPlayers().end(); ++itr)
            if (ObjectAccessor::FindPlayer(itr->first))
                return itr->first;
        return ObjectGuid::Empty;
    }

    void ShowPage(Player* player, uint16 page, bool isTop)
    {
        uint16 highGames  = 0;
        uint16 lowGames   = 0;
        bool haveNextPage = false;
        for (uint8 i = BATTLEGROUND_NA; i <= BATTLEGROUND_RL; ++i)
        {
            if (!sBattlegroundMgr->IsArenaType((BattlegroundTypeId)i))
                continue;

            BattlegroundContainer arenas = sBattlegroundMgr->GetBattlegroundsByType((BattlegroundTypeId)i);
            for (BattlegroundContainer::const_iterator itr = arenas.begin(); itr != arenas.end(); ++itr)
            {
                Battleground* arena = itr->second;

                if (!arena->isRated())
                    continue;

                if (!arena->GetPlayersSize())
                    continue;

                if (arena->GetStartDelayTime())
                    continue;

                uint16 mmr = arena->GetArenaMatchmakerRating(0) + arena->GetArenaMatchmakerRating(1);
                mmr /= 2;

                if (isTop && mmr >= TopGamesRating)
                {
                    highGames++;
                    if (highGames > (page + 1) * GamesOnPage)
                    {
                        haveNextPage = true;
                        break;
                    }

                    if (highGames >= page * GamesOnPage)
                        AddGossipItemFor(player, GOSSIP_ICON_BATTLE, GetGamesStringData(arena, mmr), GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_SELECTED_PLAYER + GetFirstPlayerGuid(arena).GetCounter());
                }
                else if (!isTop && mmr < TopGamesRating)
                {
                    lowGames++;
                    if (lowGames > (page + 1) * GamesOnPage)
                    {
                        haveNextPage = true;
                        break;
                    }

                    if (lowGames >= page * GamesOnPage)
                        AddGossipItemFor(player, GOSSIP_ICON_BATTLE, GetGamesStringData(arena, mmr), GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_SELECTED_PLAYER + GetFirstPlayerGuid(arena).GetCounter());
                }
            }
        }

        if (page > 0)
            AddGossipItemFor(player, GOSSIP_ICON_DOT, "Prev..", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES + page - 1);

        if (haveNextPage)
            AddGossipItemFor(player, GOSSIP_ICON_DOT, "Next..", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES + page + 1);
    }
};


void AddSC_arena_spectator_script()
{
    new arena_spectator_commands();
    new npc_arena_spectator();
}

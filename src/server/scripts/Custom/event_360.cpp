#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "SpellScript.h"
#include "grunt_guardian_base_class.h"

#define MORPH_AURA_ID 100044
class event_360_layi_guardian : public CreatureScript
{
public:
    event_360_layi_guardian() : CreatureScript("event_360_layi_guardian") { }
    struct event_360_layi_guardianAI : CreatureAI
    {
        uint32 timer;
        uint32 delay;
        enum
        {
            SPELL_SLEEP = 42648,
            EMOTE_ATTACK = 35,
        };

        event_360_layi_guardianAI(Creature* creature) : CreatureAI(creature)
        {
            timer = 0;
            delay = 1000;
        }

        std::vector<std::string> quotes =
        {
            "$N, ještě nejsi jeden z nás. Vrať se zpět a najdi Pepina!",
            "$N, já tě nepustím dál, dokud z tebe nebude někdo z nás. Vrať se zpět!",
            "Je mi líto $N, ale dokud nebudeš jako my, nepustím tě dál! Najdi Pepina a vem si od něj morph!",
            "Ještě si nenašel Pepina $N. Až budeš mít morph, pustím tě do cíle.",
            "Mě nepodvedeš $N! Vrať se a najdi Pepina!",
            "$N, kde máš morph? Vrať se zpět do bludiště a najdi Pepina!",
        };

        void TeleportPlayer(Player* player)
        {
            me->SetTarget(player->GetGUID());
            me->SaySingleTarget(Trinity::Containers::SelectRandomContainerElement(quotes), LANG_UNIVERSAL, player);
            player->NearTeleportTo(-8849.0f, -520.0f, 9.0f, 4.88f);
        }

        void UpdateAI(uint32 diff) override
        {
            if (timer >= diff)
            {
                timer -= diff;
                return;
            }

            timer += delay;

            std::list<Player*> players;
            me->GetPlayerListInGrid(players, 25.0f);

            players.remove_if([](Player* player)
            {
                if (player->IsGameMaster())
                    return true;

                if (player->HasAura(MORPH_AURA_ID))
                    return true;

                return false;
            });

            if (players.empty())
            {
                me->SetUInt32Value(UNIT_NPC_EMOTESTATE, 0);
                me->AddAura(SPELL_SLEEP, me);
                return;
            }

            me->SetUInt32Value(UNIT_NPC_EMOTESTATE, EMOTE_ATTACK);
            me->RemoveAura(SPELL_SLEEP);

            players.remove_if([this](Player* player){ return player->GetDistance(me) >= 12.0f; });
            for (auto p : players)
                TeleportPlayer(p);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_360_layi_guardianAI(creature);
    }
};


struct event_360_morpher_baseAI : CreatureAI
{
    std::vector<uint32> morphs =
    {

        23947, 643, 20725, 1536, 368, 785
    };

    event_360_morpher_baseAI(Creature* creature) : CreatureAI(creature)
    {
        me->SetDisplayId(Trinity::Containers::SelectRandomContainerElement(morphs));
    }

    void UpdateAI(uint32) { }
};

class event_360_fake_morpher : public CreatureScript
{
public:
    event_360_fake_morpher() : CreatureScript("event_360_fake_morpher") { }
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_360_morpher_baseAI(creature);
    }
};

class event_360_morpher : public CreatureScript
{
public:
    event_360_morpher() : CreatureScript("event_360_morpher") { }

    bool OnGossipHello(Player* player, Creature* me) override
    {
        player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_CHAT, "Vezmi si morph a utikej do cile...", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF, "", 0);
        player->PlayerTalkClass->SendGossipMenu(140, me->GetGUID());
        return true;
    }
    bool OnGossipSelect(Player* player, Creature* me, uint32 /*sender*/, uint32 /*action*/) override
    {
        me->AddAura(MORPH_AURA_ID, player);
        player->PlayerTalkClass->SendCloseGossip();
        return true;
    }

    struct event_360_morpherAI : event_360_morpher_baseAI
    {
        bool isActive;
        event_360_morpherAI(Creature* creature) : event_360_morpher_baseAI(creature)
        {
            isActive = false;
        }

        void DoAction(int32) override
        {
            isActive = true;
        }

        bool IsNeverVisibleFor(const WorldObject* seer) override
        {
            if (const Player* player = seer->ToPlayer())
                if (player->IsGameMaster())
                    return false;

            return !isActive;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_360_morpherAI(creature);
    }
};

class event_360_flower_guardian : public CreatureScript
{
public:
    event_360_flower_guardian() : CreatureScript("event_360_flower_guardian") { }

    struct event_360_flower_guardianAI : public grunt_guardian_base_classAI
    {
        event_360_flower_guardianAI(Creature* creature) : grunt_guardian_base_classAI(creature)
        {
            me->setActive(true);
            points = {{ -8849.2f, -127.9f, 8.9f, 4.94f }};
        }

        void HandleCatchPlayer(Player* player) override
        {
            if (Aura* aur = player->AddAura(22935, player))
                aur->SetDuration(4000);
            me->CastSpell(player, SPELL_CHARGE, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_360_flower_guardianAI(creature);
    }
};

class event_360_starter : public CreatureScript
{
public:
    event_360_starter() : CreatureScript("event_360_starter") { }

    struct event_360_starterAI : CreatureAI
    {
        struct event_360_labyrint
        {
            std::vector<ObjectGuid::LowType> openedGates;
            ObjectGuid::LowType activeMorpher;
        };

        std::vector<event_360_labyrint> labyrintTypes =
        {
            { { 2464225, 2464211, 2464215, 2464217, 2464214, 2464204, 2464206, 2464219, 2464222, 2464203, 2464212 }, 5032704 },
            { { 2464237, 2464223, 2464224, 2464218, 2464208, 2464228, 2464209, 2464226, 2464230, 2464202, 2464213, 2464212, 2464204 }, 5032705 },
            { { 2464210, 2464227, 2464209, 2464233, 2464228, 2464229, 2464207, 2464208, 2464215, 2464606, 2464204, 2464203, 2464206 }, 5032706 },
            { { 2464205, 2464202, 2464206, 2464203, 2464204, 2464223, 2464229, 2464228, 2464209, 2464226, 2464211, 2464210, 2464225, 2464223, 2464212 }, 5032707 },
            { { 2464221, 2464222, 2464220, 2464208, 2464213, 2464215, 2464206, 2464229, 2464207, 2464234, 2464204, 2464203, 2464226 }, 5032710 }
        };

        bool activated = false;
        event_360_starterAI(Creature* creature) : CreatureAI(creature) { }

        void UpdateAI(uint32) override { }

        bool IsNeverVisibleFor(const WorldObject*) override
        {
            return activated;
        }

        void sGossipHello(Player* player) override
        {
            for (uint32 i = 0; i < labyrintTypes.size(); ++i)
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_CHAT, "Zapnout cestu #" + std::to_string(i), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1 + i, "", 0);
            player->PlayerTalkClass->SendGossipMenu(140, me->GetGUID());
        }

        void sGossipSelect(Player* player, uint32 /*sender*/, uint32 action) override
        {
            if (action >= labyrintTypes.size())
                return player->PlayerTalkClass->SendCloseGossip();

            const event_360_labyrint& labyrint = labyrintTypes[action];
            for (ObjectGuid::LowType gateGuid : labyrint.openedGates)
            {
                // check for existence, maybe someone has deleted it... well we have team that can actually do that :D
                if (sObjectMgr->GetGOData(gateGuid))
                {
                    auto bounds = player->GetMap()->GetGameObjectBySpawnIdStore().equal_range(gateGuid);
                    if (bounds.first != bounds.second)
                    {
                        GameObject* gate = bounds.first->second;
                        gate->SetLootState(GO_READY);
                        gate->UseDoorOrButton(10000, false);
                    }
                }
            }

            // check for existence, maybe someone has deleted it... well we have team that can actually do that :D
            if (sObjectMgr->GetCreatureData(labyrint.activeMorpher))
            {
                auto bounds = player->GetMap()->GetCreatureBySpawnIdStore().equal_range(labyrint.activeMorpher);
                if (bounds.first != bounds.second)
                {
                    Creature* morpher = bounds.first->second;
                    if (morpher->IsAIEnabled)
                        morpher->AI()->DoAction(0);
                }
            }

            player->Unit::Whisper("Path #" + std::to_string(action) + " has been activated!", LANG_UNIVERSAL, player, true);
            activated = true;
            me->UpdateObjectVisibility();
            player->PlayerTalkClass->SendCloseGossip();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_360_starterAI(creature);
    }
};

// 100044
class event_360_morph_aura : public SpellScriptLoader
{
public:
    event_360_morph_aura() : SpellScriptLoader("event_360_morph_aura") { }

    class event_360_morph_AuraScript : public AuraScript
    {
        PrepareAuraScript(event_360_morph_AuraScript);

        std::vector<uint32> morphs =
        {
            614, 633, 599, 18416, 321, 1059, 6805, 9950, 1043, 320,
            954, 616, 10054, 9954, 18167, 15506, 4424, 1973, 1933, 1934
        };

        void OnRemove(AuraEffect const*, AuraEffectHandleModes)
        {
           GetUnitOwner()->SetDisplayId(GetUnitOwner()->GetNativeDisplayId());
        }

        void OnApply(AuraEffect const*, AuraEffectHandleModes)
        {
            GetUnitOwner()->SetDisplayId(Trinity::Containers::SelectRandomContainerElement(morphs));
            GetUnitOwner()->Dismount();
        }

        void Register() override
        {
            OnEffectRemove += AuraEffectRemoveFn(event_360_morph_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_SPEED_NOT_STACK, AURA_EFFECT_HANDLE_REAL);
            OnEffectApply += AuraEffectApplyFn(event_360_morph_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_SPEED_NOT_STACK, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new event_360_morph_AuraScript();
    }
};

// 100045
class event_360_mount_aura : public SpellScriptLoader
{
public:
    event_360_mount_aura() : SpellScriptLoader("event_360_mount_aura") { }

    class event_360_morph_AuraScript : public AuraScript
    {
        PrepareAuraScript(event_360_morph_AuraScript);

        std::vector<uint32> mounts =
        {
            10322, 7322,  10338, 7687 , 35168, 6074, 7690 , 10337, 11021, 7686
        };

        void OnApply(AuraEffect const*, AuraEffectHandleModes)
        {
            PreventDefaultAction();
            uint32 creatureEntry = Trinity::Containers::SelectRandomContainerElement(mounts);
            uint32 displayId = 0;
            uint32 vehicleId = 0;
            if (CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(creatureEntry))
            {
                displayId = ObjectMgr::ChooseDisplayId(creatureInfo);
                sObjectMgr->GetCreatureModelRandomGender(&displayId);
                vehicleId = creatureInfo->VehicleId;
            }

            GetUnitOwner()->Mount(displayId, vehicleId, creatureEntry);
        }

        void Register() override
        {
            OnEffectApply += AuraEffectApplyFn(event_360_morph_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOUNTED, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new event_360_morph_AuraScript();
    }
};

void AddSC_event_360()
{
    new event_360_layi_guardian();
    new event_360_fake_morpher();
    new event_360_morpher();
    new event_360_starter();
    new event_360_flower_guardian();
    new event_360_morph_aura();
    new event_360_mount_aura();
}

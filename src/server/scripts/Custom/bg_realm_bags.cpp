#include "ScriptMgr.h"
#include "Player.h"
#include "Bag.h"

class BagUpgrader : public PlayerScript
{
public:
    BagUpgrader() : PlayerScript("BagUpgrader") { }

    enum
    {
        BAGS_COUNT = 6,
        START_LIFETIME = 400,
        LIFETIME_PER_SLOT = 100,
    };
    uint32 bagsEntries[BAGS_COUNT]
    {
        3914,
        10683,
        14156,
        1977,
        38082,
        51809
    };

    bool TryUpgradeBagsRange(Player* player, uint8 start, uint8 end, uint8 bagIndex)
    {
        bool upgraded = false;
        for (uint8 i = start; i < end; ++i)
        {
            if (Bag* bag = player->GetBagByPos(i))
            {
                if (bag->GetBagSize() < 14 + 2 * bagIndex)
                {
                    bag->SetEntry(bagsEntries[bagIndex]);
                    bag->SetState(ITEM_CHANGED, player);
                    upgraded = true;
                }
            }
            else
            {
                if (Item* bag = player->EquipNewItem(i, bagsEntries[bagIndex], true))
                    bag->SetBinding(true);
                if (start == BANK_SLOT_BAG_START)
                    player->SetBankBagSlotCount(player->GetBankBagSlotCount() + 1);
                upgraded = true;
            }
        }
        return upgraded;
    }

    void UpgradeBags(Player* player, bool login)
    {
        uint32 lifetime = player->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS);
        if (!login && (lifetime % (2 * LIFETIME_PER_SLOT)) == 0)
            return;

        lifetime = std::max<uint32>(START_LIFETIME, lifetime) - START_LIFETIME;
        bool upgraded = false;
        uint8 bagIndex = static_cast<uint8>(std::min<int32>(lifetime / (2 * LIFETIME_PER_SLOT), BAGS_COUNT - 1));

        if (TryUpgradeBagsRange(player, INVENTORY_SLOT_BAG_START, INVENTORY_SLOT_BAG_END, bagIndex))
            upgraded = true;
        if (TryUpgradeBagsRange(player, BANK_SLOT_BAG_START, BANK_SLOT_BAG_END, bagIndex))
            upgraded = true;

        if (upgraded)
            player->Unit::Whisper("Your bags has been upgraded! (relog to see additional bag slots)", LANG_UNIVERSAL, player, true);
    }

    void OnKillCredit(Player* killer, Unit* killed) override
    {
        if (killer->GetRealmId() != realmID2)
            return;

        if (killed->GetTypeId() != TYPEID_PLAYER)
            return;

        UpgradeBags(killer, false);
    }

    void OnLogin(Player* player, bool) override
    {
        if (player->GetRealmId() != realmID2)
            return;

        UpgradeBags(player, true);
    }
};

void AddSC_BagUpgrader()
{
    new BagUpgrader();
}

#include "box_spawner_base_class.h"

class event_381_handler : public box_spawner_base_class
{
public:

    event_381_handler() : box_spawner_base_class("event_381_handler") { }

    struct event_381_handlerAI : public box_spawner_base_classAI
    {
#define BOXES_TOTAL 380
#define OBJECTS_HOLE 20

        event_381_handlerAI(Creature* creature) : box_spawner_base_classAI(creature, 4000.0f, -3500.0f, 532.0f)
        {
            waypoints.reserve(BOXES_TOTAL);
            for (int8 i = 0; i < 4; ++i)
            {
                AddWPInDir(1, 0, 1);
                AddWPInDir(0, 1, 0);
                AddWPInDir(1, 0, 0);
                AddWPInDir(0, 1, 1);
                AddWPInDir(1, 0, 0);
                AddWPInDir(0, 1, 0);
                AddWPInDir(-1, 0, 1);
                AddWPInDir(0, -1, 1);
            }
            AddWPInDir(1, 0, 0);
            AddWPInDir(0, -1, 0);
            AddWPInDir(1, 0, 0);
            AddWPInDir(0, 1, 0);
            AddWPInDir(1, 0, 0);
            AddWPInDir(1, 0, 0);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new event_381_handlerAI(creature);
    }
};

void AddSC_event_381()
{
    new event_381_handler();
}


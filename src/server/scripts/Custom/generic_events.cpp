#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "GameObjectAI.h"
#include "Player.h"
#include "SpellInfo.h"

class evil_noob : public CreatureScript
{
public:
    evil_noob() : CreatureScript("evil_noob") { }

    struct evil_noobAI : CreatureAI
    {
        evil_noobAI(Creature* creature) : CreatureAI(creature) { }

        void SpellHitTarget(Unit* victim, const SpellInfo* spell) override
        {
            if (spell->Id == 10987)
                me->TextEmote(me->GetName() + " smiles at " + victim->GetName() + ". :))");
        }

        void UpdateAI(uint32 diff) override
        {
            uint32 delay = me->GetMaxPower(POWER_HAPPINESS);
            if (delay > diff)
            {
                me->SetMaxPower(POWER_HAPPINESS, delay - diff);
                return;
            }
            me->SetMaxPower(POWER_HAPPINESS, delay + 10000);
            DoCast(10987);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new evil_noobAI(creature);
    }
};

class gurubashi_arena_protector : public CreatureScript
{
public:
    gurubashi_arena_protector() : CreatureScript("gurubashi_arena_protector") { }

    struct gurubashi_arena_protectorAI : CreatureAI
    {
        gurubashi_arena_protectorAI(Creature* creature) : CreatureAI(creature)
        {
            me->m_SightDistance = 5.0f;
        }

        void MoveInLineOfSight(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            if (unit->ToPlayer()->IsGameMaster())
                return;

            if (!unit->IsAlive())
                return;

            if (me->GetDistance(unit) > 5.0f)
                return;

            if (!me->IsWithinLOSInMap(unit))
                return;

            me->TextEmote(me->GetName() + " smiles at " + unit->GetName() + ". :))");
            unit->KillSelf();
        }

        void UpdateAI(uint32) override { }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new gurubashi_arena_protectorAI(creature);
    }
};


class troll_test : public GameObjectScript
{
public:
    troll_test() : GameObjectScript("troll_test") { }

    enum TrollTests
    {
        TROLL_TEST_1 = 2200000,
        TROLL_TEST_2 = 2200001
    };

    struct troll_testAI : public GameObjectAI
    {
        uint32 delay;
        uint16 throwDelay;
        troll_testAI(GameObject* go) : GameObjectAI(go)
        {
            delay = go->GetSpellId();
            if (!delay && go->GetEntry() == TROLL_TEST_2)
                delay = 5000;

            go->SetSpellId(0);
            go->SetSpawnedByDefault(true);
            throwDelay = 0;
            go->setActive(true); // do not break timer if we are out of range
        }

        ~troll_testAI()
        {
            if (!delay)
                delay = 1;
            go->SetSpellId(delay);
        }

        void ThrowDown()
        {
            std::list<Player*> players;
            go->GetPlayerListInGrid(players, 0.1f);

            for (auto p : players) // throw down
            {
                if (!p->isMoving())
                    p->GetMotionMaster()->MovePoint(0, *p, false);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (throwDelay)
            {
                if (throwDelay > diff)
                    throwDelay -= diff;
                else
                {
                    throwDelay = 0;
                    ThrowDown();
                }
            }

            if (delay > diff)
            {
                delay -= diff;
                return;
            }
            delay += 5000;

            if (go->m_serverSideVisibility.GetValue(SERVERSIDE_VISIBILITY_GM) == SEC_PLAYER)
            {
                go->m_serverSideVisibility.SetValue(SERVERSIDE_VISIBILITY_GM, SEC_CONSOLE);
                go->UpdateObjectVisibility();
                throwDelay = 800;
            }
            else
            {
                go->m_serverSideVisibility.SetValue(SERVERSIDE_VISIBILITY_GM, SEC_PLAYER);
                go->UpdateObjectVisibility();
            }
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new troll_testAI(go);
    }
};

class scripted_lootable_gameobject : GameObjectScript
{
public:
    scripted_lootable_gameobject() : GameObjectScript("scripted_lootable_gameobject") { }

    struct scripted_lootable_gameobjectAI : GameObjectAI
    {
        scripted_lootable_gameobjectAI(GameObject* obj) : GameObjectAI(obj) { }

        void OnStateChanged(uint32 state, Unit* unit) override
        {
            if (state == GO_ACTIVATED)
                if (Player* player = unit->ToPlayer())
                    player->KillCreditGO(go->GetEntry());
        }
    };

    GameObjectAI* GetAI(GameObject* obj) const override
    {
        return new scripted_lootable_gameobjectAI(obj);
    }
};

void AddSC_generic_events()
{
    new evil_noob();
    new gurubashi_arena_protector();
    new troll_test();
    new scripted_lootable_gameobject();
}

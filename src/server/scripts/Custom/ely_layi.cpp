#include "ScriptMgr.h"
#include "SpellScript.h"
#include "Player.h"
#include "SpellAuras.h"

#define SHIRT_AURA 44460
#define DUMMY_AURA 100030

class spell_ely_layi_robe : public SpellScriptLoader
{
public:
    spell_ely_layi_robe() : SpellScriptLoader("spell_ely_layi_robe") { }

    class spell_ely_layi_robe_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_ely_layi_robe_AuraScript);

        void HandleEffectApply(AuraEffect const*, AuraEffectHandleModes)
        {
            for (uint32 i = 0; i < 32; ++i)
            {
                if ((1 << i) & IMMUNE_TO_MOVEMENT_IMPAIRMENT_AND_LOSS_CONTROL_MASK)
                    GetUnitOwner()->ApplySpellImmune(GetId(), IMMUNITY_MECHANIC, i, true);
            }
        }

        void HandleEffectRemove(AuraEffect const*, AuraEffectHandleModes)
        {
            for (uint32 i = 0; i < 32; ++i)
            {
                if ((1 << i) & IMMUNE_TO_MOVEMENT_IMPAIRMENT_AND_LOSS_CONTROL_MASK)
                    GetUnitOwner()->ApplySpellImmune(GetId(), IMMUNITY_MECHANIC, i, false);
            }
        }

        void Register()
        {
            OnEffectApply  += AuraEffectApplyFn(spell_ely_layi_robe_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_ely_layi_robe_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_ely_layi_robe_AuraScript();
    }
};

class spell_ely_layi_shirt_aura : public SpellScriptLoader
{
public:
    spell_ely_layi_shirt_aura() : SpellScriptLoader("spell_ely_layi_shirt_aura") { }

    class spell_ely_layi_shirt_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_ely_layi_shirt_aura_AuraScript);

        void HandleEffectPeriodic(AuraEffect const*)
        {
            Unit* owner = GetUnitOwner();
            std::list<Player*> players;
            owner->GetPlayerListInGrid(players, 10.0f);
            players.remove_if([](Player* player)
            {
                return !player->HasAura(DUMMY_AURA);
            });

            if (players.size() > 1)
                owner->AddAura(SHIRT_AURA, owner);
            else if (Aura* aur = owner->GetAura(SHIRT_AURA))
                aur->SetDuration(0);
        }

        void HandleRemove(AuraEffect const*, AuraEffectHandleModes)
        {
            if (Aura* aur = GetUnitOwner()->GetAura(SHIRT_AURA))
                aur->SetDuration(0);
        }

        void Register()
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_ely_layi_shirt_aura_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            OnEffectRemove += AuraEffectRemoveFn(spell_ely_layi_shirt_aura_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_ely_layi_shirt_aura_AuraScript();
    }
};

void AddSC_ely_layi()
{
    new spell_ely_layi_robe();
    new spell_ely_layi_shirt_aura();
}

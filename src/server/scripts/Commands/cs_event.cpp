/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: event_commandscript
%Complete: 100
Comment: All event related commands
Category: commandscripts
EndScriptData */

#include "Chat.h"
#include "GameEventMgr.h"
#include "Language.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "GMStruct.h"
#include "Config.h"
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>


class event_commandscript : public CommandScript
{
public:
    event_commandscript() : CommandScript("event_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> eventCommandTable =
        {
            { "activelist", rbac::RBAC_PERM_COMMAND_EVENT_ACTIVELIST, true, &HandleEventActiveListCommand, "" },
            { "export",     rbac::RBAC_PERM_COMMAND_EVENT_EXPORT,     true, &HandleEventExportCommand,     "" },
            { "info",       rbac::RBAC_PERM_COMMAND_EVENT,            true, &HandleEventInfoCommand,       "" },
            { "import",     rbac::RBAC_PERM_COMMAND_EVENT_IMPORT,     true, &HandleEventImportCommand,     "" },
            { "start",      rbac::RBAC_PERM_COMMAND_EVENT_START,      true, &HandleEventStartCommand,      "" },
            { "stop",       rbac::RBAC_PERM_COMMAND_EVENT_STOP,       true, &HandleEventStopCommand,       "" },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "event", rbac::RBAC_PERM_COMMAND_EVENT, false, nullptr, "", eventCommandTable },
        };
        return commandTable;
    }

    static bool HandleEventActiveListCommand(ChatHandler* handler, char const* /*args*/)
    {
        uint32 counter = 0;

        GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();
        GameEventMgr::ActiveEvents const& activeEvents = sGameEventMgr->GetActiveEventList();

        char const* active = handler->GetTrinityString(LANG_ACTIVE);

        for (auto itr : activeEvents)
        {
            uint32 eventId = itr;
            GameEventData const& eventData = events[eventId];

            if (handler->GetSession())
                handler->PSendSysMessage(LANG_EVENT_ENTRY_LIST_CHAT, eventId, eventId, eventData.description.c_str(), active);
            else
                handler->PSendSysMessage(LANG_EVENT_ENTRY_LIST_CONSOLE, eventId, eventData.description.c_str(), active);

            ++counter;
        }

        if (counter == 0)
            handler->SendSysMessage(LANG_NOEVENTFOUND);
        handler->SetSentErrorMessage(true);

        return true;
    }

    static bool HandleEventImportCommand(ChatHandler* handler, char const* args)
    {
        std::string exportPath = sConfigMgr->GetStringDefault("EventExportDir", "./");
        if (!*args)
        {
            boost::filesystem::path dir(exportPath);
            if (!boost::filesystem::is_directory(dir))
                return false;
            else
            {
                std::string msg = dir.empty() ? "There is no event ready for import." : "List of possible events for load:\n";
                bool hasValidEvents = false;
                for (auto& file : boost::make_iterator_range(boost::filesystem::directory_iterator(dir), {}))
                {
                    std::string filename = file.path().filename().string();
                    if (filename.size() > 6)
                    {
                        filename.erase(0, 6);
                        int32 eventId = atoi(filename.c_str());
                        if (!eventId)
                            continue;

                        auto& eventMap = sGameEventMgr->GetEventMap();
                        if (eventMap.find(eventId) != eventMap.end())
                            continue;

                        msg += std::to_string(eventId) + "\n";
                        hasValidEvents = true;
                    }
                }
                
                if (!hasValidEvents && !dir.empty())
                    msg = "All of the event in directory are already applied in game!";

                handler->SendSysMessage(msg.c_str());
            }
            return true;
        }

        if (!sGameEventMgr->ImportEvent(exportPath + "event_" + args))
        {
            handler->SendSysMessage("Failed to import event! (Check for correct file name or existance of event with specified ID)");
            handler->SetSentErrorMessage(true);
        }
        else
            handler->SendSysMessage("Event has been imported!");

        return true;
    }

    static bool HandleEventExportCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* id =  handler->extractKeyFromLink((char*)args, "Hgameevent");
        if (!id)
            return false;

        uint32 eventId = atoi(id);

        if (!sGameEventMgr->ExportEvent(eventId))
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
        }
        else
        {
            std::string exportPath = sConfigMgr->GetStringDefault("EventExportDir", "./");
            handler->PSendSysMessage("Event %u has been exported to file '%sevent_%u'.", eventId, exportPath, eventId);
        }
        return true;
    }

    static bool HandleEventInfoCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        // id or [name] Shift-click form |color|Hgameevent:id|h[name]|h|r
        char* id =  handler->extractKeyFromLink((char*)args, "Hgameevent");
        if (!id)
            return false;

        uint32 eventId = atoul(id);

        GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();

        if (events.find(eventId) == events.end())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventData const& eventData = events[eventId];
        if (!eventData.isValid())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventMgr::ActiveEvents const& activeEvents = sGameEventMgr->GetActiveEventList();
        bool active = activeEvents.find(eventId) != activeEvents.end();
        char const* activeStr = active ? handler->GetTrinityString(LANG_ACTIVE) : "";

        std::string startTimeStr = TimeToTimestampStr(eventData.start);
        std::string endTimeStr = TimeToTimestampStr(eventData.end);

        uint32 delay = sGameEventMgr->NextCheck(eventId);
        time_t nextTime = time(nullptr) + delay;
        std::string nextStr = nextTime >= eventData.start && nextTime < eventData.end ? TimeToTimestampStr(time(nullptr) + delay) : "-";

        std::string occurenceStr = secsToTimeString(eventData.occurence * MINUTE);
        std::string lengthStr = secsToTimeString(eventData.length * MINUTE);

        handler->PSendSysMessage(LANG_EVENT_INFO, eventId, eventData.description.c_str(), activeStr,
            startTimeStr.c_str(), endTimeStr.c_str(), occurenceStr.c_str(), lengthStr.c_str(),
            nextStr.c_str());
        return true;
    }

    static bool HandleEventStartCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        // id or [name] Shift-click form |color|Hgameevent:id|h[name]|h|r
        char* id =  handler->extractKeyFromLink((char*)args, "Hgameevent");
        if (!id)
            return false;

        uint32 eventId = atoul(id);

        if (WorldSession* session = handler->GetSession())
        {
            if (!session->HasPermission(rbac::RBAC_PERM_START_ALL_EVENTS))
            {
                bool startable = false;
                if (GMStruct* gm = session->GetPlayer()->gm)
                    startable = gm->CanUseEventId(eventId) || gm->CanStartEventId(eventId);

                if (!startable)
                {
                    handler->PSendSysMessage("You do not have permission to start event with id %i",eventId);
                    return true;
                }
            }
        }

        GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();

        if (events.find(eventId) == events.end())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventData const& eventData = events[eventId];
        if (!eventData.isValid())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventMgr::ActiveEvents const& activeEvents = sGameEventMgr->GetActiveEventList();
        if (activeEvents.find(eventId) != activeEvents.end())
        {
            handler->PSendSysMessage(LANG_EVENT_ALREADY_ACTIVE, eventId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        sGameEventMgr->StartEvent(eventId, true);
        return true;
    }

    static bool HandleEventStopCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        // id or [name] Shift-click form |color|Hgameevent:id|h[name]|h|r
        char* id =  handler->extractKeyFromLink((char*)args, "Hgameevent");
        if (!id)
            return false;

        uint32 eventId = atoul(id);

        if (WorldSession* session = handler->GetSession())
        {
            if (!session->HasPermission(rbac::RBAC_PERM_START_ALL_EVENTS))
            {
                bool stoppable = false;
                if (GMStruct* gm = session->GetPlayer()->gm)
                    stoppable = gm->CanUseEventId(eventId) || gm->CanStartEventId(eventId);
                if (!stoppable)
                {
                    handler->PSendSysMessage("You do not have permission to stop event with id %i",eventId);
                    return true;
                }
            }
        }

        GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();

        if (events.find(eventId) == events.end())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventData const& eventData = events[eventId];
        if (!eventData.isValid())
        {
            handler->SendSysMessage(LANG_EVENT_NOT_EXIST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        GameEventMgr::ActiveEvents const& activeEvents = sGameEventMgr->GetActiveEventList();

        if (activeEvents.find(eventId) == activeEvents.end())
        {
            handler->PSendSysMessage(LANG_EVENT_NOT_ACTIVE, eventId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        sGameEventMgr->StopEvent(eventId, true);
        return true;
    }
};

void AddSC_event_commandscript()
{
    new event_commandscript();
}

#include "Chat.h"
#include "ScriptMgr.h"
#include "BattlegroundMgr.h"

class bg_commandscript : public CommandScript
{
public:
    bg_commandscript() : CommandScript("bg_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {

        static std::vector<ChatCommand> bgSetCommandTable =
        {
            { "delay", rbac::RBAC_PERM_COMMAND_BG_SET, false, &HandleBGSetDelay, "" },
        };
        static std::vector<ChatCommand> bgGetCommandTable =
        {
            { "delay", rbac::RBAC_PERM_COMMAND_BG_SET, false, &HandleBGGetDelay, "" },
        };
        static std::vector<ChatCommand> bgCommandTable =
        {
            { "set", rbac::RBAC_PERM_COMMAND_BG_SET, false, nullptr, "", bgSetCommandTable },
            { "get", rbac::RBAC_PERM_COMMAND_BG_GET, false, nullptr, "", bgGetCommandTable },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "bg", rbac::RBAC_PERM_COMMAND_BG,  false, nullptr, "", bgCommandTable },
        };
        return commandTable;
    }

    static bool HandleBGGetDelay(ChatHandler* handler, char const* /*args*/)
    {
        uint32 delay = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_RB)->GetStartDelayTime();
        std::string text = "Delay before RBG starts: " + std::to_string(delay) + ".";
        handler->SendSysMessage(text.c_str(), false);
        return true;
    }

    static bool HandleBGSetDelay(ChatHandler* /*handler*/, char const* args)
    {
        if (!*args)
            return false;

        sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_RB)->SetStartDelayTime(atoi((char*)args));
        return true;
    }
};

void AddSC_bg_commandscript()
{
    new bg_commandscript();
}

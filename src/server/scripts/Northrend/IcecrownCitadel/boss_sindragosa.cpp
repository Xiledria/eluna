/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "GridNotifiers.h"
#include "icecrown_citadel.h"
#include "PlayerAI.h"

enum Texts
{
    SAY_AGGRO                           = 0, // You are fools to have come to this place! The icy winds of Northrend will consume your souls!
    SAY_UNCHAINED_MAGIC                 = 1, // Suffer, mortals, as your pathetic magic betrays you!
    EMOTE_WARN_BLISTERING_COLD          = 2, // %s prepares to unleash a wave of blistering cold!
    SAY_BLISTERING_COLD                 = 3, // Can you feel the cold hand of death upon your heart?
    SAY_RESPITE_FOR_A_TORMENTED_SOUL    = 4, // Aaah! It burns! What sorcery is this?!
    SAY_AIR_PHASE                       = 5, // Your incursion ends here! None shall survive!
    SAY_PHASE_2                         = 6, // Now feel my master's limitless power and despair!
    EMOTE_WARN_FROZEN_ORB               = 7, // %s fires a frozen orb towards $N!
    SAY_KILL                            = 8, // Perish!
                                             // A flaw of mortality...
    SAY_BERSERK                         = 9, // Enough! I tire of these games!
    SAY_DEATH                           = 10, // Free...at last...
    EMOTE_BERSERK_RAID                  = 11
};

enum Spells
{
    // Sindragosa
    SPELL_SINDRAGOSA_S_FURY     = 70608,
    SPELL_TANK_MARKER           = 71039,
    SPELL_FROST_AURA            = 70084,
    SPELL_PERMEATING_CHILL      = 70109,
    SPELL_CLEAVE                = 19983,
    SPELL_TAIL_SMASH            = 71077,
    SPELL_FROST_BREATH_P1       = 69649,
    SPELL_FROST_BREATH_P2       = 73061,
    SPELL_UNCHAINED_MAGIC       = 69762,
    SPELL_BACKLASH              = 69770,
    SPELL_ICY_GRIP              = 70117,
    SPELL_ICY_GRIP_JUMP         = 70122,
    SPELL_BLISTERING_COLD       = 70123,
    SPELL_FROST_BEACON          = 70126,
    SPELL_ICE_TOMB_TARGET       = 69712,
    SPELL_ICE_TOMB_DUMMY        = 69675,
    SPELL_ICE_TOMB_UNTARGETABLE = 69700,
    SPELL_ICE_TOMB_DAMAGE       = 70157,
    SPELL_ASPHYXIATION          = 71665,
    SPELL_FROST_BOMB_TRIGGER    = 69846,
    SPELL_FROST_BOMB_VISUAL     = 70022,
    SPELL_BIRTH_NO_VISUAL       = 40031,
    SPELL_FROST_BOMB            = 69845,
    SPELL_MYSTIC_BUFFET         = 70128,
    SPELL_MYSTIC_BUFFET_TRIGGER = 70127,

    // Spinestalker
    SPELL_BELLOWING_ROAR        = 36922,
    SPELL_CLEAVE_SPINESTALKER   = 40505,
    SPELL_TAIL_SWEEP            = 71370,

    // Rimefang
    SPELL_FROST_BREATH          = 71386,
    SPELL_FROST_AURA_RIMEFANG   = 71387,
    SPELL_ICY_BLAST             = 71376,
    SPELL_ICY_BLAST_AREA        = 71380,

    // Frostwarden Handler
    SPELL_FOCUS_FIRE            = 71350,
    SPELL_ORDER_WHELP           = 71357,
    SPELL_CONCUSSIVE_SHOCK      = 71337,

    // Frost Infusion
    SPELL_FROST_INFUSION_CREDIT = 72289,
    SPELL_FROST_IMBUED_BLADE    = 72290,
    SPELL_FROST_INFUSION        = 72292,
};

enum Events
{
    // Spinestalker
    EVENT_BELLOWING_ROAR            = 13,
    EVENT_CLEAVE_SPINESTALKER       = 14,
    EVENT_TAIL_SWEEP                = 15,

    // Rimefang
    EVENT_FROST_BREATH_RIMEFANG     = 16,
    EVENT_ICY_BLAST                 = 17,
    EVENT_ICY_BLAST_CAST            = 18,

    // Trash
    EVENT_FROSTWARDEN_ORDER_WHELP   = 19,
    EVENT_CONCUSSIVE_SHOCK          = 20,

    // Ice Tomb
    EVENT_CHECK_PLAYER_ALIVE        = 1,
    EVENT_TRIGGER_ASPHYXIATION      = 2,
};

enum Groups
{
    GROUP_LAND_PHASE    = 1,
    GROUP_AIR_PHASE     = 2,
};

enum FrostwingData
{
    DATA_MYSTIC_BUFFET_STACK    = 0,
    DATA_FROSTWYRM_OWNER        = 1,
    DATA_WHELP_MARKER           = 2,
    DATA_LINKED_GAMEOBJECT      = 3,
    DATA_TRAPPED_PLAYER         = 4,
    DATA_IS_THIRD_PHASE         = 5
};

enum MovementPoints
{
    POINT_FROSTWYRM_FLY_IN  = 1,
    POINT_FROSTWYRM_LAND    = 2,
    POINT_AIR_PHASE         = 3,
    POINT_TAKEOFF           = 4,
    POINT_LAND              = 5,
    POINT_AIR_PHASE_FAR     = 6,
    POINT_LAND_GROUND       = 7,
};

enum Shadowmourne
{
    QUEST_FROST_INFUSION        = 24757
};

enum EventDisable
{
    EVENT_DISABLE_ICE_TOMB          = 1,
    EVENT_DISABLE_BLISTERING_COLD   = 2,
};

Position const RimefangFlyPos       = {4413.309f, 2456.421f, 233.3795f, 2.890186f};
Position const RimefangLandPos      = {4413.309f, 2456.421f, 203.3848f, 2.890186f};
Position const SpinestalkerFlyPos   = {4418.895f, 2514.233f, 230.4864f, 3.396045f};
Position const SpinestalkerLandPos  = {4418.895f, 2514.233f, 203.3848f, 3.396045f};
Position const SindragosaSpawnPos   = {4818.700f, 2483.710f, 287.0650f, 3.089233f};
Position const SindragosaFlyPos     = {4475.190f, 2484.570f, 234.8510f, 3.141593f};
Position const SindragosaLandPos    = {4419.190f, 2484.570f, 203.3848f, 3.141593f};
Position const SindragosaAirPos     = {4475.990f, 2484.430f, 247.9340f, 3.141593f};
Position const SindragosaAirPosFar  = {4525.600f, 2485.150f, 245.0820f, 3.141593f};
Position const SindragosaFlyInPos   = {4419.190f, 2484.360f, 232.5150f, 3.141593f};

class FrostwyrmLandEvent : public BasicEvent
{
    public:
        FrostwyrmLandEvent(Creature& owner, Position const& dest) : _owner(owner), _dest(dest) { }

        bool Execute(uint64 /*eventTime*/, uint32 /*updateTime*/) override
        {
            _owner.GetMotionMaster()->MoveLand(POINT_FROSTWYRM_LAND, _dest);
            return true;
        }

    private:
        Creature& _owner;
        Position const& _dest;
};

class FrostBombExplosion : public BasicEvent
{
    public:
        FrostBombExplosion(Creature* owner, ObjectGuid sindragosaGUID) : _owner(owner), _sindragosaGUID(sindragosaGUID) { }

        bool Execute(uint64 /*eventTime*/, uint32 /*updateTime*/) override
        {
            _owner->CastSpell((Unit*)nullptr, SPELL_FROST_BOMB, false, nullptr, nullptr, _sindragosaGUID);
            _owner->RemoveAurasDueToSpell(SPELL_FROST_BOMB_VISUAL);
            return true;
        }

    private:
        Creature* _owner;
        ObjectGuid _sindragosaGUID;
};

class FrostBeaconSelector : NonTankTargetSelector
{
    public:
        FrostBeaconSelector(Unit* source) : NonTankTargetSelector(source, true) { }

        bool operator()(WorldObject* target) const
        {
            if (Unit* unitTarget = target->ToUnit())
                return !NonTankTargetSelector::operator()(unitTarget) ||
                    unitTarget->HasAura(SPELL_ICE_TOMB_UNTARGETABLE);

            return false;
        }
};

class boss_sindragosa : public CreatureScript
{
    public:
        boss_sindragosa() : CreatureScript("boss_sindragosa") { }

        struct boss_sindragosaAI : public BossAI
        {
            boss_sindragosaAI(Creature* creature) : BossAI(creature, DATA_SINDRAGOSA), _summoned(false)
            {
                Initialize();
            }

            void Initialize()
            {
                _mysticBuffetStack = 0;
                _isInAirPhase = false;
                _isThirdPhase = false;
                _airPhaseTime = 0;
                _eventDisable = 0;
                _oldMoveRun = me->GetSpeedRate(MOVE_RUN);
            }

            void ScheduleP1Tasks()
            {
                scheduler.Schedule(Seconds(10), GROUP_LAND_PHASE, [this](TaskContext cleave)
                {
                    if(_eventDisable & EVENT_DISABLE_BLISTERING_COLD) {
                        cleave.Repeat(Seconds(1));
                        return;
                    }

                    DoCastVictim(SPELL_CLEAVE);
                    cleave.Repeat(Seconds(15), Seconds(20));
                })
                .Schedule(Seconds(20), GROUP_LAND_PHASE, [this](TaskContext tail_smash)
                {
                    if(_eventDisable & EVENT_DISABLE_BLISTERING_COLD) {
                        tail_smash.Repeat(Seconds(1));
                        return;
                    }

                    DoCast(me, SPELL_TAIL_SMASH);
                    tail_smash.Repeat(Seconds(27), Seconds(32));
                })
                .Schedule(Seconds(8), Seconds(12), GROUP_LAND_PHASE, [this](TaskContext frost_breath)
                {
                    if(_eventDisable & EVENT_DISABLE_BLISTERING_COLD) {
                        frost_breath.Repeat(Seconds(1));
                        return;
                    }

                    DoCastVictim(_isThirdPhase ? SPELL_FROST_BREATH_P2 : SPELL_FROST_BREATH_P1);
                    frost_breath.Repeat(Seconds(22));
                })
                .Schedule(Seconds(9), Seconds(14), GROUP_LAND_PHASE, [this](TaskContext unchained_magic)
                {
                    Talk(SAY_UNCHAINED_MAGIC);
                    DoCast(me, SPELL_UNCHAINED_MAGIC);
                    unchained_magic.Repeat(Seconds(30), Seconds(35));
                })
                .Schedule(Seconds(30), GROUP_LAND_PHASE, [this](TaskContext before_icy_grip)
                {
                    // we can't grip players if there's ice tomb soon
                    if(_eventDisable & EVENT_DISABLE_ICE_TOMB) {
                        before_icy_grip.Repeat(Seconds(1));
                        return;
                    }

                    _eventDisable |= EVENT_DISABLE_BLISTERING_COLD;

                    before_icy_grip.Schedule(Seconds(2), GROUP_LAND_PHASE, [this](TaskContext icy_grip)
                    {
                        DoCast(me, SPELL_ICY_GRIP);

                        icy_grip.Schedule(Seconds(1), GROUP_LAND_PHASE, [this](TaskContext blistering_cold)
                        {
                            Talk(EMOTE_WARN_BLISTERING_COLD);
                            DoCast(me, SPELL_BLISTERING_COLD);

                            blistering_cold.Schedule(Seconds(5), GROUP_LAND_PHASE, [this](TaskContext /*blistering_yell*/)
                            {
                                Talk(SAY_BLISTERING_COLD);
                            })
                            .Schedule(Seconds(7), [this](TaskContext /*allow_spells*/)
                            {
                                _eventDisable &= ~EVENT_DISABLE_BLISTERING_COLD;
                            });
                        });
                    });

                    if(_isThirdPhase) // let's cast icy grip only once in first phase, the second one could happen right before air phase
                        before_icy_grip.Repeat(Seconds(33), Seconds(38));
                });
            }

            void ScheduleP3Tasks()
            {
                scheduler.Schedule(Seconds(7), Seconds(10), [this](TaskContext ice_tomb)
                {
                    if(_eventDisable & EVENT_DISABLE_BLISTERING_COLD) {
                        ice_tomb.Repeat(Seconds(1));
                        return;
                    }

                    _eventDisable |= EVENT_DISABLE_ICE_TOMB;
                    me->CastCustomSpell(SPELL_ICE_TOMB_TARGET, SPELLVALUE_MAX_TARGETS, 1, nullptr, TRIGGERED_FULL_MASK);

                    ice_tomb.Schedule(Seconds(8), [this](TaskContext /*ice_tomb_hit*/)
                    {
                        _eventDisable &= ~EVENT_DISABLE_ICE_TOMB;
                    });

                    ice_tomb.Repeat(Seconds(12), Seconds(16));
                });
            }

            void ScheduleTasks() override
            {
                ScheduleP1Tasks();

                scheduler.Schedule(Seconds(600), [this](TaskContext /*berserk*/)
                {
                    Talk(EMOTE_BERSERK_RAID);
                    Talk(SAY_BERSERK);
                    DoCast(me, SPELL_BERSERK);
                })
                .Schedule(Seconds(50), GROUP_AIR_PHASE, [this](TaskContext air_phase)
                {
                    _isInAirPhase = true;
                    _airPhaseTime = 44000;

                    Talk(SAY_AIR_PHASE);
                    me->SetReactState(REACT_PASSIVE);
                    me->AttackStop();
                    me->SetSpeedRate(MOVE_RUN, 2.5f);
                    me->SetSpeedRate(MOVE_FLIGHT, 2.5f);

                    Position pos = me->GetPosition();
                    pos.m_positionZ += 30.0f;
                    me->GetMotionMaster()->MoveTakeoff(POINT_TAKEOFF, pos);

                    me->SetCanFly(true);
                    me->SetDisableGravity(true);
                    me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);

                    air_phase.CancelGroup(GROUP_LAND_PHASE);

                    uint32 moveTime = static_cast<uint32>(SindragosaAirPosFar.GetExactDist(&SindragosaFlyInPos) / (me->GetSpeed(MOVE_FLIGHT) * 0.001f));
                    air_phase.Schedule(Milliseconds(_airPhaseTime - moveTime), [this](TaskContext /*land*/)
                    {
                        // cancel other movement in case something goes wrong
                        me->StopMoving();
                        me->GetMotionMaster()->Clear();
                        me->GetMotionMaster()->MovePoint(POINT_LAND, SindragosaFlyInPos);
                    })
                    .Schedule(Seconds(7), [this](TaskContext /*ice_tombs*/)
                    {
                        me->CastCustomSpell(SPELL_ICE_TOMB_TARGET, SPELLVALUE_MAX_TARGETS, RAID_MODE<int32>(2, 5, 2, 6), nullptr, TRIGGERED_FULL_MASK);
                    })
                    .Schedule(Seconds(15), [this](TaskContext frost_bomb)
                    {
                        float destX, destY, destZ;
                        destX = float(rand_norm()) * 75.0f + 4350.0f;
                        destY = float(rand_norm()) * 75.0f + 2450.0f;
                        destZ = 205.0f; // random number close to ground, get exact in next call
                        me->UpdateGroundPositionZ(destX, destY, destZ);
                        me->CastSpell(destX, destY, destZ, SPELL_FROST_BOMB_TRIGGER, false);

                        if(frost_bomb.GetRepeatCounter() < 3) // 4 bombs in air phase
                            frost_bomb.Repeat(Milliseconds(5500));
                    });

                    air_phase.Repeat(Seconds(110));
                });
            }

            void Reset() override
            {
                BossAI::Reset();
                DoCast(me, SPELL_TANK_MARKER, true);
                Initialize();

                me->RemoveAurasDueToSpell(SPELL_FROST_AURA);
                me->RemoveAurasDueToSpell(SPELL_PERMEATING_CHILL);

                if (!_summoned)
                {
                    me->SetCanFly(true);
                    me->SetDisableGravity(true);
                }
            }

            void JustDied(Unit* /* killer */) override
            {
                _JustDied();
                Talk(SAY_DEATH);

                if (Is25ManRaid() && me->HasAura(SPELL_SHADOWS_FATE))
                    DoCastAOE(SPELL_FROST_INFUSION_CREDIT, true);

            }

            void EnterCombat(Unit* victim) override
            {
                if (!instance->CheckRequiredBosses(DATA_SINDRAGOSA, victim->ToPlayer()))
                {
                    EnterEvadeMode(EVADE_REASON_SEQUENCE_BREAK);
                    instance->DoCastSpellOnPlayers(LIGHT_S_HAMMER_TELEPORT);
                    return;
                }

                DoCast(me, SPELL_FROST_AURA);
                DoCast(me, SPELL_PERMEATING_CHILL);
                Talk(SAY_AGGRO);
                instance->SetBossState(DATA_SINDRAGOSA, IN_PROGRESS);
                me->SetCombatPulseDelay(5);
                me->setActive(true);
                DoZoneInCombat();
                ScheduleTasks();
            }

            void EnterEvadeMode(EvadeReason why) override
            {
                if (_isInAirPhase && why == EVADE_REASON_BOUNDARY)
                    return;

                BossAI::EnterEvadeMode(why);
            }

            void JustReachedHome() override
            {
                BossAI::JustReachedHome();
                instance->SetBossState(DATA_SINDRAGOSA, FAIL);

                me->SetCanFly(false);
                me->SetDisableGravity(false);
                me->SetReactState(REACT_DEFENSIVE);
                me->RemoveByteFlag(UNIT_FIELD_BYTES_1, 3, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
            }

            void KilledUnit(Unit* victim) override
            {
                if (victim->GetTypeId() == TYPEID_PLAYER)
                    Talk(SAY_KILL);
            }

            void DoAction(int32 action) override
            {
                if (action == ACTION_START_FROSTWYRM)
                {
                    if (_summoned)
                        return;

                    _summoned = true;
                    if (TempSummon* summon = me->ToTempSummon())
                        summon->SetTempSummonType(TEMPSUMMON_DEAD_DESPAWN);

                    if (me->isDead())
                        return;

                    me->setActive(true);
                    me->SetCanFly(true);
                    me->SetDisableGravity(true);
                    me->SetByteFlag(UNIT_FIELD_BYTES_1, UNIT_BYTES_1_OFFSET_ANIM_TIER, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
                    me->SetSpeedRate(MOVE_FLIGHT, 4.0f);
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                    float moveTime = me->GetExactDist(&SindragosaFlyPos) / (me->GetSpeed(MOVE_FLIGHT) * 0.001f);
                    me->m_Events.AddEvent(new FrostwyrmLandEvent(*me, SindragosaLandPos), me->m_Events.CalculateTime(uint64(moveTime)));
                    me->GetMotionMaster()->MovePoint(POINT_FROSTWYRM_FLY_IN, SindragosaFlyPos);
                    DoCast(me, SPELL_SINDRAGOSA_S_FURY);
                }
            }

            uint32 GetData(uint32 type) const override
            {
                switch (type)
                {
                    case DATA_MYSTIC_BUFFET_STACK:
                        return _mysticBuffetStack;
                    case DATA_IS_THIRD_PHASE:
                        return _isThirdPhase;
                    default:
                        return 0xFFFFFFFF;
                }
            }

            void MovementInform(uint32 type, uint32 point) override
            {
                if (type != POINT_MOTION_TYPE && type != EFFECT_MOTION_TYPE)
                    return;

                switch (point)
                {
                    case POINT_FROSTWYRM_LAND:
                        me->setActive(false);
                        me->SetCanFly(false);
                        me->SetDisableGravity(false);
                        me->RemoveByteFlag(UNIT_FIELD_BYTES_1, UNIT_BYTES_1_OFFSET_ANIM_TIER, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
                        me->SetHomePosition(SindragosaLandPos);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

                        // Sindragosa enters combat as soon as she lands
                        DoZoneInCombat();
                        break;

                    case POINT_TAKEOFF:
                        scheduler.Schedule(Milliseconds(1), [this](TaskContext /*air_movement*/)
                        {
                            me->GetMotionMaster()->MovePoint(POINT_AIR_PHASE, SindragosaAirPos);
                        });
                        break;

                    case POINT_AIR_PHASE:
                        scheduler.Schedule(Milliseconds(1), [this](TaskContext /*air_movement_far*/)
                        {
                            me->GetMotionMaster()->MovePoint(POINT_AIR_PHASE_FAR, SindragosaAirPosFar);
                        });
                        break;

                    case POINT_AIR_PHASE_FAR:
                        scheduler.Schedule(Milliseconds(1), [this](TaskContext /*update_facing*/)
                        {
                            me->SetFacingTo(float(M_PI), true);
                        });
                        break;

                    case POINT_LAND:
                        scheduler.Schedule(Milliseconds(1), [this](TaskContext /*land_ground*/)
                        {
                            ScheduleP1Tasks();
                            me->GetMotionMaster()->MoveLand(POINT_LAND_GROUND, SindragosaLandPos);
                        });
                        break;

                    case POINT_LAND_GROUND:
                    {
                        me->SetCanFly(false);
                        me->SetDisableGravity(false);
                        me->RemoveByteFlag(UNIT_FIELD_BYTES_1, UNIT_BYTES_1_OFFSET_ANIM_TIER, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
                        me->SetReactState(REACT_DEFENSIVE);

                        if (me->GetMotionMaster()->GetCurrentMovementGeneratorType() == POINT_MOTION_TYPE)
                            me->GetMotionMaster()->MovementExpired();

                        me->SetSpeedRate(MOVE_RUN, _oldMoveRun);
                        _isInAirPhase = false;
                        break;
                    }

                    default:
                        break;
                }
            }

            void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/) override
            {
                if (!_isThirdPhase && !HealthAbovePct(35))
                {
                    scheduler.CancelGroup(GROUP_AIR_PHASE);
                    scheduler.Schedule(Seconds(1), [this](TaskContext third_phase_check)
                    {
                        if (!_isInAirPhase)
                        {
                            Talk(SAY_PHASE_2);
                            ScheduleP3Tasks();
                            DoCast(me, SPELL_MYSTIC_BUFFET, true);
                        }
                        else
                            third_phase_check.Repeat(Seconds(5));
                    });

                    _isThirdPhase = true;
                }
            }

            void JustSummoned(Creature* summon) override
            {
                summons.Summon(summon);
                if (summon->GetEntry() == NPC_FROST_BOMB)
                {
                    summon->CastSpell(summon, SPELL_FROST_BOMB_VISUAL, true);
                    summon->CastSpell(summon, SPELL_BIRTH_NO_VISUAL, true);
                    summon->m_Events.AddEvent(new FrostBombExplosion(summon, me->GetGUID()), summon->m_Events.CalculateTime(5500));
                }
            }

            void SummonedCreatureDespawn(Creature* summon) override
            {
                BossAI::SummonedCreatureDespawn(summon);
                if (summon->GetEntry() == NPC_ICE_TOMB)
                    summon->AI()->JustDied(summon);
            }

            void SpellHitTarget(Unit* target, SpellInfo const* spell) override
            {
                if (uint32 spellId = sSpellMgr->GetSpellIdForDifficulty(SPELL_MYSTIC_BUFFET_TRIGGER, me))
                    if (spellId == spell->Id)
                        if (Aura const* mysticBuffet = target->GetAura(spell->Id))
                            _mysticBuffetStack = std::max<uint8>(_mysticBuffetStack, mysticBuffet->GetStackAmount());
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim())
                    return;

                if(_isInAirPhase) {
                    if(_airPhaseTime >= diff)
                        _airPhaseTime -= diff;
                    else
                        _airPhaseTime = 0;
                }

                scheduler.Update(diff, [this] {
                    DoMeleeAttackIfReady();
                });
            }

        private:
            uint8 _mysticBuffetStack;
            bool _isInAirPhase;
            bool _isThirdPhase;
            bool _summoned;
            uint32 _airPhaseTime;
            uint8 _eventDisable;
            float _oldMoveRun;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetIcecrownCitadelAI<boss_sindragosaAI>(creature);
        }
};

class npc_ice_tomb : public CreatureScript
{
    public:
        npc_ice_tomb() : CreatureScript("npc_ice_tomb") { }

        struct npc_ice_tombAI : public ScriptedAI
        {
            npc_ice_tombAI(Creature* creature) : ScriptedAI(creature)
            {
                SetCombatMovement(false);
            }

            void Reset() override
            {
                _events.Reset();
                me->SetReactState(REACT_PASSIVE);
            }

            void SetGUID(ObjectGuid guid, int32 type/* = 0 */) override
            {
                if (type == DATA_TRAPPED_PLAYER)
                {
                    _trappedPlayerGUID = guid;
                    _events.ScheduleEvent(EVENT_CHECK_PLAYER_ALIVE, 1000);
                    _events.ScheduleEvent(EVENT_TRIGGER_ASPHYXIATION, 25000);
                }
            }

            void JustDied(Unit* /*killer*/) override
            {
                me->RemoveAllGameObjects();

                if (Player* player = ObjectAccessor::GetPlayer(*me, _trappedPlayerGUID))
                {
                    _trappedPlayerGUID.Clear();
                    player->RemoveAurasDueToSpell(SPELL_ICE_TOMB_DAMAGE);
                    player->RemoveAurasDueToSpell(SPELL_ASPHYXIATION);
                    player->RemoveAurasDueToSpell(SPELL_ICE_TOMB_UNTARGETABLE);
                }
            }

            void UpdateAI(uint32 diff) override
            {
                if (!_trappedPlayerGUID)
                    return;

                _events.Update(diff);

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_CHECK_PLAYER_ALIVE:
                        {
                            Player* player = ObjectAccessor::GetPlayer(*me, _trappedPlayerGUID);
                            if (!player || player->isDead() || !player->HasAura(SPELL_ICE_TOMB_DAMAGE))
                            {
                                // Remove object
                                JustDied(me);
                                me->DespawnOrUnsummon();
                                return;
                            }

                            _events.ScheduleEvent(EVENT_CHECK_PLAYER_ALIVE, 1000);
                            break;
                        }
                        case EVENT_TRIGGER_ASPHYXIATION:
                            if (Player* player = ObjectAccessor::GetPlayer(*me, _trappedPlayerGUID))
                                player->CastSpell(player, SPELL_ASPHYXIATION, true);
                            break;
                        default:
                            break;
                    }
                }
            }

        private:
            ObjectGuid _trappedPlayerGUID;
            EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetIcecrownCitadelAI<npc_ice_tombAI>(creature);
        }
};

struct npc_sidragosa_dragonAI : public ScriptedAI
{
    npc_sidragosa_dragonAI(Creature* creature, const Position& flyInPos, const Position& landPos) :
    ScriptedAI(creature), _instance(creature->GetInstanceScript()), _summoned(false), _flyInPos(flyInPos), _landPos(landPos)
    {
    }

    void InitializeAI() override
    {
        // Increase add count
        if (!me->isDead())
        {
            _instance->SetData(DATA_SINDRAGOSA_FROSTWYRMS, me->GetSpawnId());  // this cannot be in Reset because reset also happens on evade
            Reset();
        }
    }

    void Reset() override
    {
        _events.Reset();
        ScheduleDragonEvents();

        if (!_summoned)
        {
            me->SetCanFly(true);
            me->SetDisableGravity(true);
        }
    }

    void JustRespawned() override
    {
        ScriptedAI::JustRespawned();
        _instance->SetData(DATA_SINDRAGOSA_FROSTWYRMS, me->GetSpawnId());  // this cannot be in Reset because reset also happens on evade
    }

    void JustDied(Unit* /*killer*/) override
    {
        _events.Reset();
    }

    void DoAction(int32 action) override
    {
        if (action == ACTION_START_FROSTWYRM)
        {
            if (_summoned)
                return;

            _summoned = true;
            if (me->isDead())
                return;

            me->setActive(true);
            me->SetSpeedRate(MOVE_FLIGHT, 2.0f);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            float moveTime = me->GetExactDist(_flyInPos) / (me->GetSpeed(MOVE_FLIGHT) * 0.001f);
            me->m_Events.AddEvent(new FrostwyrmLandEvent(*me, _landPos), me->m_Events.CalculateTime(uint64(moveTime) + 250));
            me->SetDefaultMovementType(IDLE_MOTION_TYPE);
            me->GetMotionMaster()->MoveIdle();
            me->StopMoving();
            me->GetMotionMaster()->MovePoint(POINT_FROSTWYRM_FLY_IN, _flyInPos);
            me->SetReactState(REACT_DEFENSIVE);
        }
    }

    void MovementInform(uint32 type, uint32 point) override
    {
        if (type != EFFECT_MOTION_TYPE || point != POINT_FROSTWYRM_LAND)
            return;

        me->setActive(false);
        me->SetCanFly(false);
        me->SetDisableGravity(false);
        me->RemoveByteFlag(UNIT_FIELD_BYTES_1, UNIT_BYTES_1_OFFSET_ANIM_TIER, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
        me->SetHomePosition(_landPos);
        me->SetFacingTo(_landPos.GetOrientation(), true);
        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
        me->SetReactState(REACT_AGGRESSIVE);
    }

protected:
    virtual void ScheduleDragonEvents() = 0;

    EventMap _events;
    InstanceScript* _instance;
    bool _summoned;

private:
    const Position& _flyInPos;
    const Position& _landPos;
};

class npc_spinestalker : public CreatureScript
{
    public:
        npc_spinestalker() : CreatureScript("npc_spinestalker") { }

        struct npc_spinestalkerAI : public npc_sidragosa_dragonAI
        {
            npc_spinestalkerAI(Creature* creature) : npc_sidragosa_dragonAI(creature, SpinestalkerFlyPos, SpinestalkerLandPos)
            {
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_BELLOWING_ROAR:
                            DoCast(me, SPELL_BELLOWING_ROAR);
                            _events.ScheduleEvent(EVENT_BELLOWING_ROAR, urand(25000, 30000));
                            break;
                        case EVENT_CLEAVE_SPINESTALKER:
                            DoCastVictim(SPELL_CLEAVE_SPINESTALKER);
                            _events.ScheduleEvent(EVENT_CLEAVE_SPINESTALKER, urand(10000, 15000));
                            break;
                        case EVENT_TAIL_SWEEP:
                            DoCast(me, SPELL_TAIL_SWEEP);
                            _events.ScheduleEvent(EVENT_TAIL_SWEEP, urand(22000, 25000));
                            break;
                        default:
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }

        protected:
            void ScheduleDragonEvents() override
            {
                _events.ScheduleEvent(EVENT_BELLOWING_ROAR, urand(20000, 25000));
                _events.ScheduleEvent(EVENT_CLEAVE_SPINESTALKER, urand(10000, 15000));
                _events.ScheduleEvent(EVENT_TAIL_SWEEP, urand(8000, 12000));
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetIcecrownCitadelAI<npc_spinestalkerAI>(creature);
        }
};

class npc_rimefang : public CreatureScript
{
    public:
        npc_rimefang() : CreatureScript("npc_rimefang_icc") { }

        struct npc_rimefangAI : public npc_sidragosa_dragonAI
        {
            npc_rimefangAI(Creature* creature) : npc_sidragosa_dragonAI(creature, RimefangFlyPos, RimefangLandPos)
            {
                Initialize();
            }

            void Initialize()
            {
                _icyBlastCounter = 0;
            }

            void EnterCombat(Unit* /*victim*/) override
            {
                DoCast(me, SPELL_FROST_AURA_RIMEFANG, true);
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_FROST_BREATH_RIMEFANG:
                            DoCast(me, SPELL_FROST_BREATH);
                            _events.ScheduleEvent(EVENT_FROST_BREATH_RIMEFANG, urand(35000, 40000));
                            break;
                        case EVENT_ICY_BLAST:
                        {
                            _icyBlastCounter = RAID_MODE<uint8>(5, 7, 6, 8);
                            me->SetReactState(REACT_PASSIVE);
                            me->AttackStop();
                            me->SetCanFly(true);
                            me->GetMotionMaster()->MovePoint(POINT_FROSTWYRM_FLY_IN, RimefangFlyPos);
                            float moveTime = me->GetExactDist(&RimefangFlyPos)/(me->GetSpeed(MOVE_FLIGHT)*0.001f);
                            _events.ScheduleEvent(EVENT_ICY_BLAST, uint64(moveTime) + urand(60000, 70000));
                            _events.ScheduleEvent(EVENT_ICY_BLAST_CAST, uint64(moveTime) + 250);
                            break;
                        }
                        case EVENT_ICY_BLAST_CAST:
                            if (--_icyBlastCounter)
                            {
                                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true))
                                {
                                    me->SetFacingToObject(target);
                                    DoCast(target, SPELL_ICY_BLAST);
                                }
                                _events.ScheduleEvent(EVENT_ICY_BLAST_CAST, 3000);
                            }
                            else if (Unit* victim = me->SelectVictim())
                            {
                                me->SetReactState(REACT_DEFENSIVE);
                                AttackStart(victim);
                                me->SetCanFly(false);
                            }
                            break;
                        default:
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }

        protected:
            void ScheduleDragonEvents() override
            {
                _events.ScheduleEvent(EVENT_FROST_BREATH_RIMEFANG, urand(12000, 15000));
                _events.ScheduleEvent(EVENT_ICY_BLAST, urand(30000, 35000));
                Initialize();
            }

        private:
            uint8 _icyBlastCounter;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetIcecrownCitadelAI<npc_rimefangAI>(creature);
        }
};

class npc_sindragosa_trash : public CreatureScript
{
    public:
        npc_sindragosa_trash() : CreatureScript("npc_sindragosa_trash") { }

        struct npc_sindragosa_trashAI : public ScriptedAI
        {
            npc_sindragosa_trashAI(Creature* creature) : ScriptedAI(creature)
            {
                Initialize();
                _instance = creature->GetInstanceScript();
                _frostwyrmId = 0;
            }

            void Initialize()
            {
                _isTaunted = false;
            }

            void InitializeAI() override
            {
                _frostwyrmId = (me->GetHomePosition().GetPositionY() < 2484.35f) ? DATA_RIMEFANG : DATA_SPINESTALKER;
                // Increase add count
                if (!me->isDead())
                {
                    if (me->GetEntry() == NPC_FROSTWING_WHELP)
                        _instance->SetData(_frostwyrmId, me->GetSpawnId());  // this cannot be in Reset because reset also happens on evade
                    Reset();
                }
            }

            void Reset() override
            {
                // This is shared AI for handler and whelps
                if (me->GetEntry() == NPC_FROSTWARDEN_HANDLER)
                {
                    _events.ScheduleEvent(EVENT_FROSTWARDEN_ORDER_WHELP, 3000);
                    _events.ScheduleEvent(EVENT_CONCUSSIVE_SHOCK, urand(8000, 10000));
                }

                Initialize();
            }

            void JustRespawned() override
            {
                ScriptedAI::JustRespawned();

                // Increase add count
                if (me->GetEntry() == NPC_FROSTWING_WHELP)
                    _instance->SetData(_frostwyrmId, me->GetSpawnId());  // this cannot be in Reset because reset also happens on evade
            }

            void SetData(uint32 type, uint32 data) override
            {
                if (type == DATA_WHELP_MARKER)
                    _isTaunted = data != 0;
            }

            uint32 GetData(uint32 type) const override
            {
                if (type == DATA_FROSTWYRM_OWNER)
                    return _frostwyrmId;
                else if (type == DATA_WHELP_MARKER)
                    return uint32(_isTaunted);
                return 0;
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_FROSTWARDEN_ORDER_WHELP:
                            DoCast(me, SPELL_ORDER_WHELP);
                            _events.ScheduleEvent(EVENT_FROSTWARDEN_ORDER_WHELP, 3000);
                            break;
                        case EVENT_CONCUSSIVE_SHOCK:
                            DoCast(me, SPELL_CONCUSSIVE_SHOCK);
                            _events.ScheduleEvent(EVENT_CONCUSSIVE_SHOCK, urand(10000, 13000));
                            break;
                        default:
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }

        private:
            EventMap _events;
            InstanceScript* _instance;
            uint32 _frostwyrmId;
            bool _isTaunted; // Frostwing Whelp only
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetIcecrownCitadelAI<npc_sindragosa_trashAI>(creature);
        }
};

class spell_sindragosa_s_fury : public SpellScriptLoader
{
    public:
        spell_sindragosa_s_fury() : SpellScriptLoader("spell_sindragosa_s_fury") { }

        class spell_sindragosa_s_fury_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_sindragosa_s_fury_SpellScript);

        public:
            spell_sindragosa_s_fury_SpellScript()
            {
                _targetCount = 0;
            }

        private:
            bool Load() override
            {
                // This script should execute only in Icecrown Citadel
                if (InstanceMap* instance = GetCaster()->GetMap()->ToInstanceMap())
                    if (instance->GetInstanceScript())
                        if (instance->GetScriptId() == sObjectMgr->GetScriptId(ICCScriptName))
                            return true;

                return false;
            }

            void SelectDest()
            {
                if (Position* dest = const_cast<WorldLocation*>(GetExplTargetDest()))
                {
                    float destX = float(rand_norm()) * 75.0f + 4350.0f;
                    float destY = float(rand_norm()) * 75.0f + 2450.0f;
                    float destZ = 205.0f; // random number close to ground, get exact in next call
                    GetCaster()->UpdateGroundPositionZ(destX, destY, destZ);
                    dest->Relocate(destX, destY, destZ);
                }
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                _targetCount = static_cast<uint32>(targets.size());
            }

            void HandleDummy(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);

                if (!GetHitUnit()->IsAlive() || !_targetCount)
                    return;

                // FIXME: Spell should not even target GMs in the first place
                if(Player* pl = GetHitUnit()->ToPlayer())
                    if(pl->IsGameMaster())
                        return;

                if (GetHitUnit()->IsImmunedToDamage(GetSpellInfo()))
                {
                    GetCaster()->SendSpellDamageImmune(GetHitUnit(), GetId());
                    return;
                }

                float resistance = float(GetHitUnit()->GetResistance(SpellSchoolMask(GetSpellInfo()->SchoolMask)));
                uint32 minResistFactor = uint32((resistance / (resistance + 510.0f)) * 10.0f) * 2;
                uint32 randomResist = urand(0, (9 - minResistFactor) * 100) / 100 + minResistFactor;

                uint32 damage = (uint32(GetEffectValue() / _targetCount) * randomResist) / 10;

                SpellNonMeleeDamage damageInfo(GetCaster(), GetHitUnit(), GetId(), GetSpellInfo()->SchoolMask);
                damageInfo.damage = damage;
                GetCaster()->SendSpellNonMeleeDamageLog(&damageInfo);
                GetCaster()->DealSpellDamage(&damageInfo, false);
            }

            void Register() override
            {
                BeforeCast += SpellCastFn(spell_sindragosa_s_fury_SpellScript::SelectDest);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_sindragosa_s_fury_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENTRY);
                OnEffectHitTarget += SpellEffectFn(spell_sindragosa_s_fury_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
            }

            uint32 _targetCount;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_sindragosa_s_fury_SpellScript();
        }
};

class UnchainedMagicTargetSelector
{
    bool _healerOnly;

    public:
        UnchainedMagicTargetSelector(bool healerOnly) : _healerOnly(healerOnly) { }

        bool operator()(WorldObject* object) const
        {
            if(object->GetTypeId() != TYPEID_PLAYER)
                return true;

            Player* plr = object->ToPlayer();
            if(!plr)
                return true;

            if(plr->getPowerType() != POWER_MANA || plr->getClass() == CLASS_HUNTER)
                return true;

            if(_healerOnly)
                return !PlayerAI::IsPlayerHealer(plr);
            else
                return !PlayerAI::IsPlayerRangedAttacker(plr);
        }
};

class spell_sindragosa_unchained_magic : public SpellScriptLoader
{
    public:
        spell_sindragosa_unchained_magic() : SpellScriptLoader("spell_sindragosa_unchained_magic") { }

        class spell_sindragosa_unchained_magic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_sindragosa_unchained_magic_SpellScript);

            void FilterTargets(std::list<WorldObject*>& unitList)
            {
                std::list<WorldObject*> healerList = unitList;
                unitList.remove_if(UnchainedMagicTargetSelector(false));
                healerList.remove_if(UnchainedMagicTargetSelector(true));

                uint32 maxDpsCount = uint32(GetCaster()->GetMap()->Is25ManRaid() ? 3 : 1), maxHealerCount = maxDpsCount;
                if(unitList.size() < maxDpsCount)
                    maxHealerCount += maxDpsCount - static_cast<uint32>(unitList.size());

                if(healerList.size() < maxHealerCount)
                    maxDpsCount += maxHealerCount - static_cast<uint32>(healerList.size());

                if (unitList.size() > maxDpsCount)
                    Trinity::Containers::RandomResizeList(unitList, maxDpsCount);

                if (healerList.size() > maxHealerCount)
                    Trinity::Containers::RandomResizeList(healerList, maxHealerCount);

                unitList.splice(unitList.end(), healerList);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_sindragosa_unchained_magic_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_sindragosa_unchained_magic_SpellScript();
        }
};

class spell_sindragosa_permeating_chill : public SpellScriptLoader
{
    public:
        spell_sindragosa_permeating_chill() : SpellScriptLoader("spell_sindragosa_permeating_chill") { }

        class spell_sindragosa_permeating_chill_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_sindragosa_permeating_chill_AuraScript);

            bool Load() override
            {
                if (InstanceMap* instance = GetCaster()->GetMap()->ToInstanceMap())
                {
                    if (auto script = instance->GetInstanceScript())
                    {
                        _instance = script;
                        return true;
                    }
                }

                return false;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcTarget() && eventInfo.GetProcTarget()->GetEntry() == NPC_SINDRAGOSA;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                eventInfo.GetActor()->CastSpell(eventInfo.GetActor(), GetSpellInfo()->Effects[EFFECT_0].TriggerSpell, true, nullptr, aurEff, _instance->GetGuidData(DATA_SINDRAGOSA));
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_sindragosa_permeating_chill_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_sindragosa_permeating_chill_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }

        private:
            InstanceScript* _instance;
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_sindragosa_permeating_chill_AuraScript();
        }
};

class spell_sindragosa_frost_breath : public SpellScriptLoader
{
    public:
        spell_sindragosa_frost_breath() : SpellScriptLoader("spell_sindragosa_frost_breath") { }

        class spell_sindragosa_frost_breath_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_sindragosa_frost_breath_SpellScript);

            void HandleInfusion()
            {
                Player* target = GetHitPlayer();
                if (!target)
                    return;

                // Check difficulty and quest status
                if (!(target->GetRaidDifficulty() & RAID_DIFFICULTY_MASK_25MAN) || target->GetQuestStatus(QUEST_FROST_INFUSION) != QUEST_STATUS_INCOMPLETE)
                    return;

                // Check if player has Shadow's Edge equipped and not ready for infusion
                if (!target->HasAura(SPELL_UNSATED_CRAVING) || target->HasAura(SPELL_FROST_IMBUED_BLADE))
                    return;

                Aura* infusion = target->GetAura(SPELL_FROST_INFUSION, target->GetGUID());
                if (infusion && infusion->GetStackAmount() >= 3)
                {
                    target->RemoveAura(infusion);
                    target->CastSpell(target, SPELL_FROST_IMBUED_BLADE, TRIGGERED_FULL_MASK);
                }
                else
                    target->CastSpell(target, SPELL_FROST_INFUSION, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_sindragosa_frost_breath_SpellScript::HandleInfusion);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_sindragosa_frost_breath_SpellScript();
        }
};

class spell_sindragosa_instability : public SpellScriptLoader
{
    public:
        spell_sindragosa_instability() : SpellScriptLoader("spell_sindragosa_instability") { }

        class spell_sindragosa_instability_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_sindragosa_instability_AuraScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_BACKLASH))
                    return false;
                return true;
            }

            void OnRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_EXPIRE)
                    GetTarget()->CastCustomSpell(SPELL_BACKLASH, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), GetTarget(), true, nullptr, aurEff, GetCasterGUID());
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_sindragosa_instability_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_sindragosa_instability_AuraScript();
        }
};

class spell_sindragosa_frost_beacon : public SpellScriptLoader
{
    public:
        spell_sindragosa_frost_beacon() : SpellScriptLoader("spell_sindragosa_frost_beacon") { }

        class spell_sindragosa_frost_beacon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_sindragosa_frost_beacon_AuraScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_ICE_TOMB_DAMAGE))
                    return false;
                return true;
            }

            void PeriodicTick(AuraEffect const* /*aurEff*/)
            {
                PreventDefaultAction();
                if (Unit* caster = GetCaster())
                    caster->CastSpell(GetTarget(), SPELL_ICE_TOMB_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_sindragosa_frost_beacon_AuraScript::PeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_sindragosa_frost_beacon_AuraScript();
        }
};

class spell_sindragosa_ice_tomb : public SpellScriptLoader
{
    public:
        spell_sindragosa_ice_tomb() : SpellScriptLoader("spell_sindragosa_ice_tomb_trap") { }

        class spell_sindragosa_ice_tomb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_sindragosa_ice_tomb_AuraScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sObjectMgr->GetCreatureTemplate(NPC_ICE_TOMB))
                    return false;
                if (!sObjectMgr->GetGameObjectTemplate(GO_ICE_BLOCK))
                    return false;
                return true;
            }

            void PeriodicTick(AuraEffect const* aurEff)
            {
                PreventDefaultAction();

                if (aurEff->GetTickNumber() == 1)
                {
                    if (Unit* caster = GetCaster())
                    {
                        Position pos = GetTarget()->GetPosition();

                        if (TempSummon* summon = caster->SummonCreature(NPC_ICE_TOMB, pos))
                        {
                            summon->AI()->SetGUID(GetTarget()->GetGUID(), DATA_TRAPPED_PLAYER);
                            GetTarget()->CastSpell(GetTarget(), SPELL_ICE_TOMB_UNTARGETABLE);
                            if (GameObject* go = summon->SummonGameObject(GO_ICE_BLOCK, pos, G3D::Quat(), 0))
                            {
                                go->SetSpellId(SPELL_ICE_TOMB_DAMAGE);
                                summon->AddGameObject(go);
                            }
                        }
                    }
                }
            }

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveAurasDueToSpell(SPELL_ICE_TOMB_UNTARGETABLE);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_sindragosa_ice_tomb_AuraScript::PeriodicTick, EFFECT_2, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_sindragosa_ice_tomb_AuraScript::HandleRemove, EFFECT_2, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_sindragosa_ice_tomb_AuraScript();
        }
};

class SindragosaNonTankTargetSelector : NonTankTargetSelector
{
    public:
        SindragosaNonTankTargetSelector(Unit* source) : NonTankTargetSelector(source, true) { }

        bool operator()(WorldObject* target) const
        {
            if (Unit* unitTarget = target->ToUnit())
                return !NonTankTargetSelector::operator()(unitTarget);

            return false;
        }
};

class spell_sindragosa_icy_grip : public SpellScriptLoader
{
    public:
        spell_sindragosa_icy_grip() : SpellScriptLoader("spell_sindragosa_icy_grip") { }

        class spell_sindragosa_icy_grip_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_sindragosa_icy_grip_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_ICY_GRIP_JUMP))
                    return false;
                return true;
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(SindragosaNonTankTargetSelector(GetCaster()));
            }

            void HandleScript(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
                GetHitUnit()->CastSpell(GetCaster(), SPELL_ICY_GRIP_JUMP, true);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_sindragosa_icy_grip_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnEffectHitTarget += SpellEffectFn(spell_sindragosa_icy_grip_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_sindragosa_icy_grip_SpellScript();
        }
};

class MysticBuffetTargetFilter
{
    public:
        explicit MysticBuffetTargetFilter(Unit* caster) : _caster(caster) { }

        bool operator()(WorldObject* unit) const
        {
            return !unit->IsWithinLOSInMap(_caster);
        }

    private:
        Unit* _caster;
};

class spell_sindragosa_mystic_buffet : public SpellScriptLoader
{
    public:
        spell_sindragosa_mystic_buffet() : SpellScriptLoader("spell_sindragosa_mystic_buffet") { }

        class spell_sindragosa_mystic_buffet_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_sindragosa_mystic_buffet_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(MysticBuffetTargetFilter(GetCaster()));
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_sindragosa_mystic_buffet_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_sindragosa_mystic_buffet_SpellScript();
        }
};

class spell_rimefang_icy_blast : public SpellScriptLoader
{
    public:
        spell_rimefang_icy_blast() : SpellScriptLoader("spell_rimefang_icy_blast") { }

        class spell_rimefang_icy_blast_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_rimefang_icy_blast_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_ICY_BLAST_AREA))
                    return false;
                return true;
            }

            void HandleTriggerMissile(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
                if (Position const* pos = GetExplTargetDest())
                    if (TempSummon* summon = GetCaster()->SummonCreature(NPC_ICY_BLAST, *pos, TEMPSUMMON_TIMED_DESPAWN, 40000))
                        summon->CastSpell(summon, SPELL_ICY_BLAST_AREA, true);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_rimefang_icy_blast_SpellScript::HandleTriggerMissile, EFFECT_1, SPELL_EFFECT_TRIGGER_MISSILE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_rimefang_icy_blast_SpellScript();
        }
};

class OrderWhelpTargetSelector
{
    public:
        explicit OrderWhelpTargetSelector(Creature* owner) : _owner(owner) { }

        bool operator()(Creature* creature)
        {
            if (!creature->AI()->GetData(DATA_WHELP_MARKER) && creature->AI()->GetData(DATA_FROSTWYRM_OWNER) == _owner->AI()->GetData(DATA_FROSTWYRM_OWNER))
                return false;
            return true;
        }

    private:
        Creature* _owner;
};

class spell_frostwarden_handler_order_whelp : public SpellScriptLoader
{
    public:
        spell_frostwarden_handler_order_whelp() : SpellScriptLoader("spell_frostwarden_handler_order_whelp") { }

        class spell_frostwarden_handler_order_whelp_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_frostwarden_handler_order_whelp_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_FOCUS_FIRE))
                    return false;
                return true;
            }

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(Trinity::ObjectTypeIdCheck(TYPEID_PLAYER, false));
                if (targets.empty())
                    return;

                WorldObject* target = Trinity::Containers::SelectRandomContainerElement(targets);
                targets.clear();
                targets.push_back(target);
            }

            void HandleForcedCast(SpellEffIndex effIndex)
            {
                // caster is Frostwarden Handler, target is player, caster of triggered is whelp
                PreventHitDefaultEffect(effIndex);
                std::list<Creature*> unitList;
                GetCreatureListWithEntryInGrid(unitList, GetCaster(), NPC_FROSTWING_WHELP, 150.0f);
                if (Creature* creature = GetCaster()->ToCreature())
                    unitList.remove_if(OrderWhelpTargetSelector(creature));

                if (unitList.empty())
                    return;

                Trinity::Containers::SelectRandomContainerElement(unitList)->CastSpell(GetHitUnit(), uint32(GetEffectValue()), true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_frostwarden_handler_order_whelp_SpellScript::HandleForcedCast, EFFECT_0, SPELL_EFFECT_FORCE_CAST);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_frostwarden_handler_order_whelp_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_frostwarden_handler_order_whelp_SpellScript();
        }
};

class spell_frostwarden_handler_focus_fire : public SpellScriptLoader
{
    public:
        spell_frostwarden_handler_focus_fire() : SpellScriptLoader("spell_frostwarden_handler_focus_fire") { }

        class spell_frostwarden_handler_focus_fire_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_frostwarden_handler_focus_fire_SpellScript);

            void HandleScript(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
                GetCaster()->AddThreat(GetHitUnit(), float(GetEffectValue()));
                GetCaster()->GetAI()->SetData(DATA_WHELP_MARKER, 1);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_frostwarden_handler_focus_fire_SpellScript::HandleScript, EFFECT_1, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        class spell_frostwarden_handler_focus_fire_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_frostwarden_handler_focus_fire_AuraScript);

            void PeriodicTick(AuraEffect const* /*aurEff*/)
            {
                PreventDefaultAction();
                if (Unit* caster = GetCaster())
                {
                    caster->AddThreat(GetTarget(), -float(GetSpellInfo()->Effects[EFFECT_1].CalcValue()));
                    caster->GetAI()->SetData(DATA_WHELP_MARKER, 0);
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_frostwarden_handler_focus_fire_AuraScript::PeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_frostwarden_handler_focus_fire_SpellScript();
        }

        AuraScript* GetAuraScript() const override
        {
            return new spell_frostwarden_handler_focus_fire_AuraScript();
        }
};

class spell_sindragosa_ice_tomb_target : public SpellScriptLoader
{
public:
    spell_sindragosa_ice_tomb_target() : SpellScriptLoader("spell_sindragosa_ice_tomb_target") { }

    class spell_sindragosa_ice_tomb_target_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_sindragosa_ice_tomb_target_SpellScript);

        void FilterTargets(std::list<WorldObject*>& unitList)
        {
            Unit* caster = GetCaster();
            unitList.remove_if(FrostBeaconSelector(caster));
        }

        void HandleSindragosaTalk(SpellEffIndex /*effIndex*/)
        {
            if (Creature* creatureCaster = GetCaster()->ToCreature())
                if (creatureCaster->GetAI()->GetData(DATA_IS_THIRD_PHASE))
                    creatureCaster->AI()->Talk(EMOTE_WARN_FROZEN_ORB, GetHitUnit());
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_sindragosa_ice_tomb_target_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            OnEffectLaunchTarget += SpellEffectFn(spell_sindragosa_ice_tomb_target_SpellScript::HandleSindragosaTalk, EFFECT_0, SPELL_EFFECT_DUMMY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_sindragosa_ice_tomb_target_SpellScript();
    }
};

class at_sindragosa_lair : public AreaTriggerScript
{
    public:
        at_sindragosa_lair() : AreaTriggerScript("at_sindragosa_lair") { }

        bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/) override
        {
            if (InstanceScript* instance = player->GetInstanceScript())
            {
                if (!instance->GetData(DATA_SPINESTALKER))
                    if (Creature* spinestalker = ObjectAccessor::GetCreature(*player, instance->GetGuidData(DATA_SPINESTALKER)))
                        spinestalker->AI()->DoAction(ACTION_START_FROSTWYRM);

                if (!instance->GetData(DATA_RIMEFANG))
                    if (Creature* rimefang = ObjectAccessor::GetCreature(*player, instance->GetGuidData(DATA_RIMEFANG)))
                        rimefang->AI()->DoAction(ACTION_START_FROSTWYRM);

                if (!instance->GetData(DATA_SINDRAGOSA_FROSTWYRMS) && !instance->GetGuidData(DATA_SINDRAGOSA) && instance->GetBossState(DATA_SINDRAGOSA) != DONE)
                {
                    if (player->GetMap()->IsHeroic() && !instance->GetData(DATA_HEROIC_ATTEMPTS))
                        return true;

                    player->GetMap()->LoadGrid(SindragosaSpawnPos.GetPositionX(), SindragosaSpawnPos.GetPositionY());
                    if (Creature* sindragosa = player->GetMap()->SummonCreature(NPC_SINDRAGOSA, SindragosaSpawnPos))
                        sindragosa->AI()->DoAction(ACTION_START_FROSTWYRM);
                }
            }

            return true;
        }
};

class achievement_all_you_can_eat : public AchievementCriteriaScript
{
    public:
        achievement_all_you_can_eat() : AchievementCriteriaScript("achievement_all_you_can_eat") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (!target)
                return false;
            return target->GetAI()->GetData(DATA_MYSTIC_BUFFET_STACK) <= 5;
        }
};

void AddSC_boss_sindragosa()
{
    new boss_sindragosa();
    new npc_ice_tomb();
    new npc_spinestalker();
    new npc_rimefang();
    new npc_sindragosa_trash();
    new spell_sindragosa_s_fury();
    new spell_sindragosa_unchained_magic();
    new spell_sindragosa_permeating_chill();
    new spell_sindragosa_frost_breath();
    new spell_sindragosa_instability();
    new spell_sindragosa_frost_beacon();
    new spell_sindragosa_ice_tomb();
    new spell_sindragosa_icy_grip();
    new spell_sindragosa_mystic_buffet();
    new spell_rimefang_icy_blast();
    new spell_frostwarden_handler_order_whelp();
    new spell_frostwarden_handler_focus_fire();
    new spell_trigger_spell_from_caster("spell_sindragosa_ice_tomb", SPELL_ICE_TOMB_DUMMY, TRIGGERED_IGNORE_SET_FACING);
    new spell_trigger_spell_from_caster("spell_sindragosa_ice_tomb_dummy", SPELL_FROST_BEACON);
    new spell_sindragosa_ice_tomb_target();
    new at_sindragosa_lair();
    new achievement_all_you_can_eat();
}

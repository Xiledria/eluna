#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "icecrown_citadel.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Group.h"

class npc_lazy_noob : public CreatureScript
{
public:
    npc_lazy_noob() : CreatureScript("npc_lazy_noob") { }

    struct npc_lazy_noobAI : public ScriptedAI
    {
        npc_lazy_noobAI(Creature* creature) : ScriptedAI(creature)
        {
            _instance = creature->GetInstanceScript();
        }

        struct GridPos
        {
            GridPos(int32 _x, int32 _y)
            {
                x = _x;
                y = _y;
            }

            GridPos()
            {
                x = -999;
                y = -999;
            }

            int32 x;
            int32 y;
        };

        const std::string texts[DATA_THE_LICH_KING] =
        {
            "Lord Marrowgar",
            "Lady Deathwhisper",
            "Gunship Battle",
            "Deathbringer Saurfang",
            "Festergut",
            "Rotface",
            "Professor Putricide",
            "Blood Queen Lana'Thel",
            "Sister Svalna",
            "Valithria Dreamwalker",
            "Sindragosa",
            "The Lich King",
        };

        const GridPos grids[DATA_THE_LICH_KING + 1]
        {
            { 31, 36 },
            { 30, 36 },
            //@TODO: finish grids if needed
            { 31, 36 },
            { 31, 36 },
            { 40, 37 },
            { 40, 37 },
            { 40, 38 },
            { 40, 37 },
            { 40, 37 },
            { 40, 36 },
            { 39, 36 },
            { 40, 36 },
            { },
        };

        void sGossipSelect(Player* player, uint32, uint32 menuId) override
        {
            if (player->GetMoney() < me->GetMaxHealth())
            {
                me->Whisper("You do not have enough gold!", LANG_UNIVERSAL, player);
                return;
            }

            menuId = player->PlayerTalkClass->GetGossipOptionAction(menuId) - GOSSIP_ACTION_INFO_DEF;
            ObjectGuid bossGuid = _instance->GetGuidData(menuId);
            if (bossGuid.Empty)
            {
                me->Whisper("Boss was not found!", LANG_UNIVERSAL, player);
                return;
            }

            const GridPos& g = grids[menuId];
            if (g.x != -999 && g.x != g.y)
                _instance->instance->LoadGrid(static_cast<float>(grids[menuId].x), static_cast<float>(grids[menuId].y));

            if (Creature* boss = _instance->instance->GetCreature(bossGuid))
            {
                boss->AI()->EnterEvadeMode();
                boss->KillSelf();
            }

            _instance->SetBossState(menuId, DONE);
            _instance->instance->ToInstanceMap()->PermBindAllPlayers();
            _instance->SaveToDB();
            player->ModifyMoney(-static_cast<int32>(me->GetMaxHealth()));
            CloseGossipMenuFor(player);
            me->TextEmote(texts[menuId] + " can be skipped for this instance!", nullptr, true);
        }

        void sGossipHello(Player* player) override
        {
            if (!player->IsGameMaster() && (!player->GetGroup() || player->GetGroup()->GetLeaderGUID() != player->GetGUID()))
            {
                me->Whisper("Pouze leader muze se mnou mluvit!", LANG_UNIVERSAL, player, true);
                CloseGossipMenuFor(player);
                return;
            }

            for (uint8 i = 0; i < me->GetMaxPower(POWER_MANA); ++i)
            {
                if (_instance->GetBossState(i) != DONE)
                    AddGossipItemFor(player, GOSSIP_ICON_CHAT, "SKIP " + texts[i], GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i,
                                 "Do you really want to skip " + texts[i] + "?", me->GetMaxHealth(), false);
            }
            SendGossipMenuFor(player, 1, me->GetGUID());
        }
        void UpdateAI(uint32) override { }
        InstanceScript* _instance;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetIcecrownCitadelAI<npc_lazy_noobAI>(creature);
    }
};

void AddSC_npc_lazy_noob()
{
    new npc_lazy_noob();
}

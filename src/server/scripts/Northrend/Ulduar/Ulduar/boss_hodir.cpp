/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "ulduar.h"

enum HodirYells
{
    SAY_AGGRO                                    = 0,
    SAY_SLAY                                     = 1,
    SAY_FLASH_FREEZE                             = 2,
    SAY_STALACTITE                               = 3,
    SAY_DEATH                                    = 4,
    SAY_BERSERK                                  = 5,
    SAY_HARD_MODE_FAILED                         = 6,
    EMOTE_FREEZE                                 = 7,
    EMOTE_BLOWS                                  = 8
};

enum HodirSpells
{
    // Hodir
    SPELL_FROZEN_BLOWS                           = 62478,
    SPELL_FLASH_FREEZE                           = 61968,
    SPELL_FLASH_FREEZE_VISUAL                    = 62148,
    SPELL_BITING_COLD                            = 62038,
    SPELL_BITING_COLD_TRIGGERED                  = 62039, // Needed for Achievement Getting Cold In Here
    SPELL_BITING_COLD_DAMAGE                     = 62188,
    SPELL_FREEZE                                 = 62469,
    SPELL_ICICLE                                 = 62234,
    SPELL_ICICLE_SNOWDRIFT                       = 62462,
    SPELL_FLASH_FREEZE_PLAYER                    = 61969, // Player + Helper
    SPELL_SUMMON_FLASH_FREEZE_PLAYER             = 61970, // Player + Helper
    SPELL_FLASH_FREEZE_HELPER                    = 61990, // Helper
    SPELL_SUMMON_FLASH_FREEZE_HELPER             = 61989, // Helper
    SPELL_FLASH_FREEZE_KILL                      = 62226,
    SPELL_ICICLE_FALL                            = 69428,
    SPELL_FALL_DAMAGE                            = 62236,
    SPELL_FALL_SNOWDRIFT                         = 62460,
    SPELL_BERSERK                                = 47008,
    SPELL_ICE_SHARD                              = 62457,
    SPELL_ICE_SHARD_HIT                          = 65370,

    SPELL_KILL_CREDIT                            = 64899,

    // Druids
    SPELL_WRATH                                  = 62793,
    SPELL_STARLIGHT                              = 62807,

    // Shamans
    SPELL_LAVA_BURST                             = 61924,
    SPELL_STORM_CLOUD                            = 65123,

    // Mages
    SPELL_FIREBALL                               = 61909,
    SPELL_CONJURE_FIRE                           = 62823,
    SPELL_MELT_ICE                               = 64528,
    SPELL_SINGED                                 = 62821,

    // Priests
    SPELL_SMITE                                  = 61923,
    SPELL_GREATER_HEAL                           = 62809,
    SPELL_DISPEL_MAGIC                           = 63499,
};

enum HodirNPC
{
    NPC_FLASH_FREEZE_HELPER                      = 32938,
    NPC_FLASH_FREEZE_PLAYER                      = 32926,
    NPC_SNOWPACKED_ICICLE                        = 33174,
    NPC_ICICLE                                   = 33169,
    NPC_ICICLE_SNOWDRIFT                         = 33173,
    NPC_TOASTY_FIRE                              = 33342,
};

enum HodirGameObjects
{
    GO_TOASTY_FIRE                               = 194300,
    GO_SNOWDRIFT                                 = 194173,
};

enum HodirEvents
{
    // Hodir
    EVENT_FREEZE                                 = 1,
    EVENT_FLASH_FREEZE                           = 2,
    EVENT_FLASH_FREEZE_EFFECT                    = 3,
    EVENT_ICICLE                                 = 4,
    EVENT_BLOWS                                  = 5,
    EVENT_RARE_CACHE                             = 6,
    EVENT_BERSERK                                = 7,

    // Priest
    EVENT_HEAL                                   = 8,
    EVENT_DISPEL_MAGIC                           = 9,

    // Shaman
    EVENT_STORM_CLOUD                            = 10,

    // Druid
    EVENT_STARLIGHT                              = 11,

    // Mage
    EVENT_CONJURE_FIRE                           = 12,
    EVENT_MELT_ICE                               = 13,
};

enum HodirActions
{
    ACTION_FLASH_FREEZE_COMBAT                   = 1,
};

#define FRIENDS_COUNT                            RAID_MODE<uint8>(4, 8)

Position const SummonPositions[8] =
{
    { 1983.75f, -243.36f, 432.767f, 1.57f }, // Field Medic Penny    &&  Battle-Priest Eliza
    { 1999.90f, -230.49f, 432.767f, 1.57f }, // Eivi Nightfeather    &&  Tor Greycloud
    { 2010.06f, -243.45f, 432.767f, 1.57f }, // Elementalist Mahfuun &&  Spiritwalker Tara
    { 2021.12f, -236.65f, 432.767f, 1.57f }, // Missy Flamecuffs     &&  Amira Blazeweaver
    { 2028.10f, -244.66f, 432.767f, 1.57f }, // Field Medic Jessi    &&  Battle-Priest Gina
    { 2014.18f, -232.80f, 432.767f, 1.57f }, // Ellie Nightfeather   &&  Kar Greycloud
    { 1992.90f, -237.54f, 432.767f, 1.57f }, // Elementalist Avuun   &&  Spiritwalker Yona
    { 1976.60f, -233.53f, 432.767f, 1.57f }, // Sissy Flamecuffs     &&  Veesha Blazeweaver
};

std::array<uint32, 8> Helpers =
{{
    NPC_FIELD_MEDIC_PENNY,
    NPC_EIVI_NIGHTFEATHER,
    NPC_ELEMENTALIST_MAHFUUN,
    NPC_MISSY_FLAMECUFFS,
    NPC_FIELD_MEDIC_JESSI,
    NPC_ELLIE_NIGHTFEATHER,
    NPC_ELEMENTALIST_AVUUN,
    NPC_SISSY_FLAMECUFFS,
}};

struct npc_flash_freezeAI : public ScriptedAI
{
    npc_flash_freezeAI(Creature* creature, uint32 freezeSpellId) : ScriptedAI(creature), _freezeSpellId(freezeSpellId)
    {
        Initialize();
        instance = me->GetInstanceScript();
        me->SetDisplayId(me->GetCreatureTemplate()->Modelid2);
        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED | UNIT_FLAG_PACIFIED);
        me->SetControlled(true, UNIT_STATE_ROOT);
    }

    void Initialize()
    {
        targetGUID.Clear();
        checkDespawnTimer = 5000;
    }

    InstanceScript* instance;

    ObjectGuid targetGUID;
    uint32 checkDespawnTimer;

    void Reset() override
    {
        Initialize();
    }

    void UpdateAI(uint32 diff) override
    {
        if (checkDespawnTimer <= diff)
        {
            Unit* unit = ObjectAccessor::GetUnit(*me, targetGUID);
            if (!unit || unit->isDead() || !unit->HasAura(_freezeSpellId))
            {
                me->DespawnOrUnsummon();
                return;
            }

            if (me->IsInCombat())
                checkDespawnTimer = 2500;
            else
                checkDespawnTimer = 10000;
        }
        else
            checkDespawnTimer -= diff;
    }

    void IsSummonedBy(Unit* summoner) override
    {
        if (Creature* hodir = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_HODIR)))
            hodir->AI()->JustSummoned(me);

        targetGUID = summoner->GetGUID();

        if (Unit* target = ObjectAccessor::GetUnit(*me, targetGUID))
        {
            DoCast(target, _freezeSpellId, true);
            // Prevents to have Ice Block on other place than target is
            me->NearTeleportTo(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), target->GetOrientation());

            if (target->GetTypeId() == TYPEID_PLAYER)
                instance->SetData(DATA_HODIR_CHEESE_THE_FREEZE, 0);
        }
    }

private:
    uint32 _freezeSpellId;
};

class npc_flash_freeze_helper : public CreatureScript
{
public:
    npc_flash_freeze_helper() : CreatureScript("npc_flash_freeze_helper") { }

    struct npc_flash_freeze_helperAI : public npc_flash_freezeAI
    {
        npc_flash_freeze_helperAI(Creature* creature) : npc_flash_freezeAI(creature, SPELL_FLASH_FREEZE_HELPER) {}

        void DamageTaken(Unit* who, uint32& /*damage*/) override
        {
            if (instance->GetBossState(BOSS_HODIR) == NOT_STARTED)
            {
                if (Creature* Hodir = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_HODIR)))
                {
                    if (!Hodir->IsInCombat())
                    {
                        Hodir->AI()->DoZoneInCombat();
                        Hodir->AI()->AttackStart(who);
                    }
                }
            }
        }

        void DoAction(int32 action) override
        {
            if (action != ACTION_FLASH_FREEZE_COMBAT)
                return;

            if (Creature* helper = ObjectAccessor::GetCreature(*me, targetGUID))
                if (Creature* hodir = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_HODIR)))
                    helper->AI()->AttackStart(hodir);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetInstanceAI<npc_flash_freeze_helperAI>(creature);
    }
};

class npc_flash_freeze_player : public CreatureScript
{
public:
    npc_flash_freeze_player() : CreatureScript("npc_flash_freeze_player") { }

    struct npc_flash_freeze_playerAI : public npc_flash_freezeAI
    {
        npc_flash_freeze_playerAI(Creature* creature) : npc_flash_freezeAI(creature, SPELL_FLASH_FREEZE_PLAYER) {}
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetInstanceAI<npc_flash_freeze_playerAI>(creature);
    }
};

class HodirFlashFreezeCheck : public Trinity::AnyUnfriendlyUnitInObjectRangeCheck
{
public:
    HodirFlashFreezeCheck(WorldObject const* obj, Unit const* funit, float range) :
        Trinity::AnyUnfriendlyUnitInObjectRangeCheck(obj, funit, range) { }

    bool operator()(Unit* u) const
    {
        if (!Trinity::AnyUnfriendlyUnitInObjectRangeCheck::operator()(u))
            return false;

        // players on snow piles
        if (u->GetTypeId() == TYPEID_PLAYER && u->FindNearestCreature(NPC_SNOWPACKED_ICICLE, 5.0f))
            return false;

        bool isHodirHelper = false;
        if (u->GetTypeId() == TYPEID_UNIT)
            isHodirHelper = std::find(Helpers.begin(), Helpers.end(), u->ToCreature()->GetEntry()) != Helpers.end();

        // only target players and helper NPCs
        if(u->GetTypeId() != TYPEID_PLAYER && !isHodirHelper)
            return false;

        return true;
    }
};

class HodirWipeCheck : public Trinity::AnyPlayerInObjectRangeCheck
{
public:
    HodirWipeCheck(WorldObject const* obj, float range) :
        Trinity::AnyPlayerInObjectRangeCheck(obj, range, true) { }

    bool operator()(Player* p) const
    {
        if(!Trinity::AnyPlayerInObjectRangeCheck::operator()(p))
            return false;

        if(p->IsGameMaster())
            return false;

        if(p->HasAura(SPELL_FLASH_FREEZE_PLAYER))
            return false;

        return true;
    }
};

class boss_hodir : public CreatureScript
{
    public:
        boss_hodir() : CreatureScript("boss_hodir") { }

        struct boss_hodirAI : public BossAI
        {
            boss_hodirAI(Creature* creature) : BossAI(creature, BOSS_HODIR), _encounterFinished(false) { }

            void Reset() override
            {
                if (_encounterFinished)
                    return;

                BossAI::Reset();
                _hardModeFailed = false;

                instance->SetData(DATA_HODIR_GETTING_COLD_IN_HERE, 1);
                instance->SetData(DATA_HODIR_CHEESE_THE_FREEZE, 1);
                instance->SetData(DATA_HODIR_I_HAVE_THE_COOLEST_FRIENDS, 1);

                me->SetReactState(REACT_PASSIVE);

                for (uint8 n = 0; n < FRIENDS_COUNT; ++n)
                    if (Creature* FrozenHelper = me->SummonCreature(Helpers[n], SummonPositions[n], TEMPSUMMON_MANUAL_DESPAWN))
                        FrozenHelper->CastSpell(FrozenHelper, SPELL_SUMMON_FLASH_FREEZE_HELPER, true);
            }

            void ScheduleTasks() override
            {
                scheduler.Schedule(Seconds(2), [this](TaskContext icicle)
                {
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
                        DoCast(target, SPELL_ICICLE);

                    icicle.Repeat(Milliseconds(RAID_MODE(5500, 3500)));
                })
                .Schedule(Seconds(3), [this](TaskContext wipe_check)
                {
                    Player* player = nullptr;
                    HodirWipeCheck check(me, 100.0f);
                    Trinity::PlayerSearcher<HodirWipeCheck> searcher(me, player, check);
                    me->VisitNearbyWorldObject(100.0f, searcher);

                    if (!player)
                    {
                        EnterEvadeMode(EVADE_REASON_NO_HOSTILES);
                        return;
                    }

                    wipe_check.Repeat(Seconds(3));
                })
                .Schedule(Seconds(25), [this](TaskContext freeze)
                {
                    DoCastAOE(SPELL_FREEZE);
                    freeze.Repeat(Seconds(30), Seconds(45));
                })
                .Schedule(Seconds(60), Seconds(65), [this](TaskContext freezing_blows)
                {
                    Talk(SAY_STALACTITE);
                    Talk(EMOTE_BLOWS);
                    DoCast(me, SPELL_FROZEN_BLOWS);
                    freezing_blows.Repeat(Seconds(60), Seconds(65));
                })
                .Schedule(Seconds(45), [this](TaskContext flash_freeze)
                {
                    Talk(SAY_FLASH_FREEZE);
                    Talk(EMOTE_FREEZE);
                    for (uint8 n = 0; n < RAID_MODE(2, 3); ++n)
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
                            target->CastSpell(target, SPELL_ICICLE_SNOWDRIFT, true);

                    DoCast(SPELL_FLASH_FREEZE);

                    // this is executed after cast, because we are not executing events while casting
                    flash_freeze.Schedule(Milliseconds(500), [this](TaskContext /*flash_freeze_effect*/)
                    {
                        // if hodir casts it client lags for some reason
                        if (Creature* trigger = GetClosestCreatureWithEntry(me, NPC_INVISIBLE_STALKER, 50.0f))
                            trigger->CastSpell((Unit*)nullptr, SPELL_FLASH_FREEZE_VISUAL, true);

                        std::list<Unit*> TargetList;
                        HodirFlashFreezeCheck checker(me, me, 100.0f);
                        Trinity::UnitListSearcher<HodirFlashFreezeCheck> searcher(me, TargetList, checker);
                        me->VisitNearbyObject(100.0f, searcher);

                        for (Unit* target : TargetList)
                        {
                            if (target->HasAura(SPELL_FLASH_FREEZE_HELPER) || target->HasAura(SPELL_FLASH_FREEZE_PLAYER))
                            {
                                me->CastSpell(target, SPELL_FLASH_FREEZE_KILL, true);
                                continue;
                            }

                            target->CastSpell(target, SPELL_SUMMON_FLASH_FREEZE_PLAYER, true);
                        }
                    });

                    flash_freeze.Repeat(Seconds(25), Seconds(35));
                })
                .Schedule(Seconds(180), [this](TaskContext /*hard_mode_fail*/)
                {
                    Talk(SAY_HARD_MODE_FAILED);
                    _hardModeFailed = true;
                    instance->SetData(DATA_HODIR_RARE_CACHE, 0);
                })
                .Schedule(Seconds(480), [this](TaskContext /*berserk*/)
                {
                    Talk(SAY_BERSERK);
                    DoCast(me, SPELL_BERSERK, true);
                });
            }

            void EnterCombat(Unit* /*who*/) override
            {
                if (_encounterFinished)
                    return;

                _EnterCombat();
                Talk(SAY_AGGRO);
                DoCast(me, SPELL_BITING_COLD, true);
                me->SetReactState(REACT_AGGRESSIVE);

                std::list<Creature*> flashFreezeList;
                GetCreatureListWithEntryInGrid(flashFreezeList, me, NPC_FLASH_FREEZE_HELPER, 100.0f);
                for (Creature* creature : flashFreezeList)
                    creature->AI()->DoAction(ACTION_FLASH_FREEZE_COMBAT);
            }

            void KilledUnit(Unit* who) override
            {
                if (who->GetTypeId() == TYPEID_PLAYER)
                    Talk(SAY_SLAY);
            }

            void DamageTaken(Unit* who, uint32& damage) override
            {
                if (_encounterFinished) {
                    damage = 0;
                    return;
                }

                if (damage >= me->GetHealth())
                {
                    damage = 0;
                    _encounterFinished = true;

                    if (!_hardModeFailed)
                        instance->SetData(DATA_HODIR_RARE_CACHE, 1);

                    me->RemoveAllAuras();
                    me->SetReactState(REACT_PASSIVE);
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_RENAME);
                    me->StopMoving();
                    me->CombatStop(true);
                    me->DeleteThreatList();

                    DoCastAOE(SPELL_KILL_CREDIT, true); // before change faction, targets enemies only
                    me->setFaction(35);

                    Talk(SAY_DEATH);
                    _JustDied();

                    me->m_Events.AddEvent(new KeeperDespawnEvent(me), me->m_Events.CalculateTime(10000));
                }
                else if (!me->IsInCombat())
                {
                    me->AI()->DoZoneInCombat();
                    me->AI()->AttackStart(who);
                }
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim())
                    return;

                scheduler.Update(diff, [this] {
                    DoMeleeAttackIfReady();
                });
            }

        private:
            bool _encounterFinished;
            bool _hardModeFailed;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<boss_hodirAI>(creature);
        };
};

class npc_icicle : public CreatureScript
{
    public:
        npc_icicle() : CreatureScript("npc_icicle") { }

        struct npc_icicleAI : public ScriptedAI
        {
            npc_icicleAI(Creature* creature) : ScriptedAI(creature)
            {
                Initialize();
                me->SetDisplayId(me->GetCreatureTemplate()->Modelid1);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_PACIFIED | UNIT_FLAG_NOT_SELECTABLE);
                me->SetControlled(true, UNIT_STATE_ROOT);
                me->SetReactState(REACT_PASSIVE);
            }

            void Initialize()
            {
                icicleTimer = 2500;
            }

            uint32 icicleTimer;

            void Reset() override
            {
                Initialize();
            }

            void UpdateAI(uint32 diff) override
            {
                if (icicleTimer <= diff)
                {
                    if (me->GetEntry() == NPC_ICICLE_SNOWDRIFT)
                    {
                        DoCast(me, SPELL_FALL_SNOWDRIFT);
                        DoCast(me, SPELL_ICICLE_FALL);
                    }
                    else if (me->GetEntry() == NPC_ICICLE)
                    {
                        DoCast(me, SPELL_ICICLE_FALL);
                        DoCast(me, SPELL_FALL_DAMAGE, true);
                    }
                    icicleTimer = 10000;
                }
                else
                    icicleTimer -= diff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_icicleAI>(creature);
        };
};

class npc_snowpacked_icicle : public CreatureScript
{
    public:
        npc_snowpacked_icicle() : CreatureScript("npc_snowpacked_icicle") { }

        struct npc_snowpacked_icicleAI : public ScriptedAI
        {
            npc_snowpacked_icicleAI(Creature* creature) : ScriptedAI(creature)
            {
                Initialize();
                me->SetDisplayId(me->GetCreatureTemplate()->Modelid2);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_PACIFIED);
                me->SetControlled(true, UNIT_STATE_ROOT);
                me->SetReactState(REACT_PASSIVE);
            }

            void Initialize()
            {
                despawnTimer = 12000;
            }

            uint32 despawnTimer;

            void Reset() override
            {
                Initialize();
            }

            void UpdateAI(uint32 diff) override
            {
                if (despawnTimer <= diff)
                {
                    if (GameObject* Snowdrift = me->FindNearestGameObject(GO_SNOWDRIFT, 2.0f))
                        me->RemoveGameObject(Snowdrift, true);
                    me->DespawnOrUnsummon();
                }
                else
                    despawnTimer -= diff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_snowpacked_icicleAI>(creature);
        };
};

class npc_hodir_priest : public CreatureScript
{
    public:
        npc_hodir_priest() : CreatureScript("npc_hodir_priest") { }

        struct npc_hodir_priestAI : public ScriptedAI
        {
            npc_hodir_priestAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = me->GetInstanceScript();
            }

            void Reset() override
            {
                events.Reset();
                events.ScheduleEvent(EVENT_HEAL, urand(4000, 8000));
                events.ScheduleEvent(EVENT_DISPEL_MAGIC, urand(15000, 20000));
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_STUNNED) || me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED))
                    return;

                events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if (HealthBelowPct(30))
                    DoCast(me, SPELL_GREATER_HEAL);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_HEAL:
                            DoCastAOE(SPELL_GREATER_HEAL);
                            events.ScheduleEvent(EVENT_HEAL, urand(7500, 10000));
                            break;
                        case EVENT_DISPEL_MAGIC:
                        {
                            std::list<Unit*> TargetList;
                            Trinity::AnyFriendlyUnitInObjectRangeCheck checker(me, me, 30.0f);
                            Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(me, TargetList, checker);
                            me->VisitNearbyObject(30.0f, searcher);
                            for (std::list<Unit*>::iterator itr = TargetList.begin(); itr != TargetList.end(); ++itr)
                                if ((*itr)->HasAura(SPELL_FREEZE))
                                    DoCast(*itr, SPELL_DISPEL_MAGIC, true);
                            events.ScheduleEvent(EVENT_DISPEL_MAGIC, urand(15000, 20000));
                            break;
                        }
                        default:
                            break;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;
                }

                DoSpellAttackIfReady(SPELL_SMITE);
            }

            void JustDied(Unit* killer) override
            {
                ScriptedAI::JustDied(killer);
                instance->SetData(DATA_HODIR_I_HAVE_THE_COOLEST_FRIENDS, 0);
            }

        private:
            InstanceScript* instance;
            EventMap events;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_hodir_priestAI>(creature);
        };
};

class npc_hodir_shaman : public CreatureScript
{
    public:
        npc_hodir_shaman() : CreatureScript("npc_hodir_shaman") { }

        struct npc_hodir_shamanAI : public ScriptedAI
        {
            npc_hodir_shamanAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = me->GetInstanceScript();
            }

            void Reset() override
            {
                events.Reset();
                events.ScheduleEvent(EVENT_STORM_CLOUD, urand(10000, 12500));
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_STUNNED) || me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED))
                    return;

                events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_STORM_CLOUD:
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
                                DoCast(target, SPELL_STORM_CLOUD, true);
                            events.ScheduleEvent(EVENT_STORM_CLOUD, urand(15000, 20000));
                            break;
                        default:
                            break;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;
                }

                DoSpellAttackIfReady(SPELL_LAVA_BURST);
            }

            void JustDied(Unit* killer) override
            {
                ScriptedAI::JustDied(killer);
                instance->SetData(DATA_HODIR_I_HAVE_THE_COOLEST_FRIENDS, 0);
            }

        private:
            InstanceScript* instance;
            EventMap events;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_hodir_shamanAI>(creature);
        };
};

class npc_hodir_druid : public CreatureScript
{
    public:
        npc_hodir_druid() : CreatureScript("npc_hodir_druid") { }

        struct npc_hodir_druidAI : public ScriptedAI
        {
            npc_hodir_druidAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = me->GetInstanceScript();
            }

            void Reset() override
            {
                events.Reset();
                events.ScheduleEvent(EVENT_STARLIGHT, urand(15000, 17500));
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_STUNNED) || me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED))
                    return;

                events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_STARLIGHT:
                            DoCast(me, SPELL_STARLIGHT, true);
                            events.ScheduleEvent(EVENT_STARLIGHT, urand(25000, 35000));
                            break;
                        default:
                            break;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;
                }

                DoSpellAttackIfReady(SPELL_WRATH);
            }

            void JustDied(Unit* killer) override
            {
                ScriptedAI::JustDied(killer);
                instance->SetData(DATA_HODIR_I_HAVE_THE_COOLEST_FRIENDS, 0);
            }

        private:
            InstanceScript* instance;
            EventMap events;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_hodir_druidAI>(creature);
        };
};

class npc_hodir_mage : public CreatureScript
{
    public:
        npc_hodir_mage() : CreatureScript("npc_hodir_mage") { }

        struct npc_hodir_mageAI : public ScriptedAI
        {
            npc_hodir_mageAI(Creature* creature) : ScriptedAI(creature), summons(me)
            {
                instance = me->GetInstanceScript();
            }

            void Reset() override
            {
                events.Reset();
                summons.DespawnAll();
                events.ScheduleEvent(EVENT_CONJURE_FIRE, urand(10000, 12500));
                events.ScheduleEvent(EVENT_MELT_ICE, 5000);
            }

            void JustSummoned(Creature* summoned) override
            {
                if (summoned->GetEntry() == NPC_TOASTY_FIRE)
                {
                    if (Creature* hodir = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_HODIR)))
                        hodir->AI()->JustSummoned(summoned); // for fire despawn on evade

                    summons.Summon(summoned);
                }
            }

            void SummonedCreatureDespawn(Creature* summoned) override
            {
                if (summoned->GetEntry() == NPC_TOASTY_FIRE)
                    summons.Despawn(summoned);
            }

            void UpdateAI(uint32 diff) override
            {
                if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_STUNNED) || me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED))
                    return;

                events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_CONJURE_FIRE:
                            if (summons.size() >= RAID_MODE<uint64>(2, 4))
                                break;
                            DoCast(me, SPELL_CONJURE_FIRE, true);
                            events.ScheduleEvent(EVENT_CONJURE_FIRE, urand(15000, 20000));
                            break;
                        case EVENT_MELT_ICE:
                            if (Creature* FlashFreeze = me->FindNearestCreature(NPC_FLASH_FREEZE_PLAYER, 50.0f, true))
                                DoCast(FlashFreeze, SPELL_MELT_ICE, true);
                            events.ScheduleEvent(EVENT_MELT_ICE, urand(10000, 15000));
                            break;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;
                }

                DoSpellAttackIfReady(SPELL_FIREBALL);
            }

            void JustDied(Unit* killer) override
            {
                ScriptedAI::JustDied(killer);
                instance->SetData(DATA_HODIR_I_HAVE_THE_COOLEST_FRIENDS, 0);
            }

        private:
            InstanceScript* instance;
            EventMap events;
            SummonList summons;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_hodir_mageAI>(creature);
        };
};

class npc_toasty_fire : public CreatureScript
{
    public:
        npc_toasty_fire() : CreatureScript("npc_toasty_fire") { }

        struct npc_toasty_fireAI : public ScriptedAI
        {
            npc_toasty_fireAI(Creature* creature) : ScriptedAI(creature)
            {
                me->SetDisplayId(me->GetCreatureTemplate()->Modelid2);
            }

            void Reset() override
            {
                DoCast(me, SPELL_SINGED, true);
            }

            void SpellHit(Unit* /*who*/, const SpellInfo* spell) override
            {
                if (spell->Id == SPELL_FLASH_FREEZE_PLAYER || spell->Id == SPELL_ICE_SHARD || spell->Id == SPELL_ICE_SHARD_HIT)
                {
                    if (GameObject* ToastyFire = me->FindNearestGameObject(GO_TOASTY_FIRE, 1.0f))
                        me->RemoveGameObject(ToastyFire, true);
                    me->DespawnOrUnsummon();
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetUlduarAI<npc_toasty_fireAI>(creature);
        };
};

class spell_biting_cold : public SpellScriptLoader
{
    public:
        spell_biting_cold() : SpellScriptLoader("spell_biting_cold") { }

        class spell_biting_cold_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_biting_cold_AuraScript);

            void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
            {
                Unit* target = GetTarget();

                auto itr = listOfTargets.find(target->GetGUID());
                if (itr == listOfTargets.end())
                {
                    listOfTargets.emplace(target->GetGUID(), 1);
                    return;
                }

                if (itr->second >= 4)
                {
                    target->CastSpell(target, SPELL_BITING_COLD_TRIGGERED, true);
                    itr->second = 1;
                }
                else
                {
                    if (target->isMoving())
                        itr->second = 1;
                    else
                        itr->second++;
                }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_biting_cold_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }

        private:
            std::map<ObjectGuid, uint8> listOfTargets;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_biting_cold_AuraScript();
        }
};

class spell_biting_cold_dot : public SpellScriptLoader
{
public:
    spell_biting_cold_dot() : SpellScriptLoader("spell_biting_cold_dot") { }

    class spell_biting_cold_dot_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_biting_cold_dot_AuraScript);

        void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
        {
            Unit* caster = GetCaster();
            if (!caster)
                return;

            if (GetStackAmount() > 2)
                if (InstanceScript* instance = GetCaster()->GetInstanceScript())
                    instance->SetData(DATA_HODIR_GETTING_COLD_IN_HERE, 0);

            int32 damage = int32(200 * std::pow(2.0f, GetStackAmount()));
            caster->CastCustomSpell(caster, SPELL_BITING_COLD_DAMAGE, &damage, nullptr, nullptr, true);

            if (caster->isMoving())
                caster->RemoveAuraFromStack(SPELL_BITING_COLD_TRIGGERED);
        }

        void Register() override
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_biting_cold_dot_AuraScript::HandleEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_biting_cold_dot_AuraScript();
    }
};

void AddSC_boss_hodir()
{
    new boss_hodir();
    new npc_icicle();
    new npc_snowpacked_icicle();
    new npc_hodir_priest();
    new npc_hodir_shaman();
    new npc_hodir_druid();
    new npc_hodir_mage();
    new npc_toasty_fire();
    new npc_flash_freeze_helper();
    new npc_flash_freeze_player();
    new spell_biting_cold();
    new spell_biting_cold_dot();
}

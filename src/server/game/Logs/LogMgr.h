#ifndef __LOG_MGR_H
#define __LOG_MGR_H

#include <atomic>
#include <SharedDefines.h>

enum LogType
{
    LOG_TYPE_ARENA,
    LOG_TYPE_BOSS,
    LOG_TYPES_MAX
};

class LogMgr
{
public:
    static void Initialize();
    uint32 GetNextLogId(LogType type);
private:
    LogMgr();
    void _Initialize();
    std::atomic<uint32> logIds[LOG_TYPES_MAX];
};

extern LogMgr* sLogMgr;

#endif

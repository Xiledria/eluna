#ifndef __GAME_EVENT_DUMP
#define __GAME_EVENT_DUMP

#include <string>
#include <fstream>
#include "SharedDefines.h"

class GameEventDump
{
public:
    GameEventDump();
    ~GameEventDump();
    bool Import(std::string path);
    bool Export(int32 eventId);
private:
    void ImportGO(std::ifstream* file, int32 eventId);
    void ImportNPC(std::ifstream* file, int32 eventId);
};

#endif

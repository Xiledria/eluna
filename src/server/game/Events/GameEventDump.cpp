#include "GameEventDump.h"
#include "GameEventMgr.h"
#include "ObjectMgr.h"
#include "MapManager.h"
#include <fstream>
#include "Config.h"
#include <boost/filesystem.hpp>

/*
 * MISSING THINGS:
 * _______________
 * conditions
 * prerequisite events
 * pools
 * waypoints
 * quests
 * new custom quests
 * new custom creatures
 * new custom Equipment
 * new custom gameobjects
 */
GameEventDump::GameEventDump()
{

}

GameEventDump::~GameEventDump()
{

}

bool GameEventDump::Export(int32 eventId)
{
    GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();
    if (events.find(eventId) == events.end())
        return false;
    std::string exportPath = sConfigMgr->GetStringDefault("EventExportDir", "./");
    boost::filesystem::path dir(exportPath);
    if (!(boost::filesystem::exists(dir)))
    {
        if (!boost::filesystem::create_directory(dir))
            return false; // unable to save to specified directory
    }
    // path is not directory, but file
    else if (!boost::filesystem::is_directory(dir))
        return false;

    std::string fileName = exportPath + "event_" + std::to_string(eventId);
    std::ofstream file;
    file.open(fileName, std::ios::out | std::ios::binary);

    // Info about event
    {
        GameEventData& eventData = events[eventId];
        file.write(reinterpret_cast<char*>(&eventId), 4);
        file.write(reinterpret_cast<char*>(&eventData.announce), 1);
        file.write(reinterpret_cast<char*>(&eventData.holiday_id), 4);
        file.write(reinterpret_cast<char*>(&eventData.length), 4);
        file.write(reinterpret_cast<char*>(&eventData.nextstart), 4);
        file.write(reinterpret_cast<char*>(&eventData.occurence), 4);
        file.write(reinterpret_cast<char*>(&eventData.start), 4);
        file.write(reinterpret_cast<char*>(&eventData.state), 4);
        uint32 size = static_cast<uint32>(eventData.description.size());
        file.write(reinterpret_cast<char*>(&size), 4);
        file.write(eventData.description.c_str(), eventData.description.size());
        bool isActive = sGameEventMgr->IsActiveEvent(eventId);
        file.write(reinterpret_cast<char*>(&isActive), 1);
    }
    // copy all gameobjects
    {
        uint32 objectCount = 0;
        for (auto itr :sGameEventMgr->mGameEventGameobjectGuids[eventId])
            if (itr > 0 && sObjectMgr->GetGOData(itr)) // filter invalid guids
                ++objectCount;
        file.write(reinterpret_cast<char*>(&objectCount), 4);
        for (auto itr :sGameEventMgr->mGameEventGameobjectGuids[eventId])
        {
            if (itr > 0)
            if (GameObjectData* data = const_cast<GameObjectData*>(sObjectMgr->GetGOData(itr)))
            {
                file.write(reinterpret_cast<char*>(&data->animprogress), 4);
                file.write(reinterpret_cast<char*>(&data->artKit), 1);
                file.write(reinterpret_cast<char*>(&data->dbData), 1);
                file.write(reinterpret_cast<char*>(&data->go_state), 4);
                file.write(reinterpret_cast<char*>(&data->id), 4);
                file.write(reinterpret_cast<char*>(&data->mapid), 2);
                file.write(reinterpret_cast<char*>(&data->orientation), 4);
                file.write(reinterpret_cast<char*>(&data->phaseMask), 4);
                file.write(reinterpret_cast<char*>(&data->posX), 4);
                file.write(reinterpret_cast<char*>(&data->posY), 4);
                file.write(reinterpret_cast<char*>(&data->posZ), 4);
                file.write(reinterpret_cast<char*>(&data->rotation.x), 4);
                file.write(reinterpret_cast<char*>(&data->rotation.y), 4);
                file.write(reinterpret_cast<char*>(&data->rotation.z), 4);
                file.write(reinterpret_cast<char*>(&data->rotation.w), 4);
                file.write(reinterpret_cast<char*>(&data->spawnMask), 1);
                file.write(reinterpret_cast<char*>(&data->spawntimesecs), 4);
            }
        }
    }
    // copy all creatures
    {
        uint32 creatureCount = 0;
        for (auto itr :sGameEventMgr->mGameEventCreatureGuids[eventId])
            if (sObjectMgr->GetCreatureData(itr)) // filter invalid guids
                ++creatureCount;
        file.write(reinterpret_cast<char*>(&creatureCount), 4);
        for (auto itr :sGameEventMgr->mGameEventCreatureGuids[eventId])
        {
            if (CreatureData* data = const_cast<CreatureData*>(sObjectMgr->GetCreatureData(itr)))
            {
                file.write(reinterpret_cast<char*>(&data->curhealth), 4);
                file.write(reinterpret_cast<char*>(&data->curmana), 4);
                file.write(reinterpret_cast<char*>(&data->currentwaypoint), 4);
                file.write(reinterpret_cast<char*>(&data->dbData), 1);
                file.write(reinterpret_cast<char*>(&data->displayid), 4);
                file.write(reinterpret_cast<char*>(&data->dynamicflags), 4);
                file.write(reinterpret_cast<char*>(&data->equipmentId), 1);
                file.write(reinterpret_cast<char*>(&data->id), 4);
                file.write(reinterpret_cast<char*>(&data->mapid), 2);
                file.write(reinterpret_cast<char*>(&data->movementType), 1);
                file.write(reinterpret_cast<char*>(&data->npcflag), 4);
                file.write(reinterpret_cast<char*>(&data->orientation), 4);
                file.write(reinterpret_cast<char*>(&data->phaseMask), 4);
                file.write(reinterpret_cast<char*>(&data->posX), 4);
                file.write(reinterpret_cast<char*>(&data->posY), 4);
                file.write(reinterpret_cast<char*>(&data->posZ), 4);
                file.write(reinterpret_cast<char*>(&data->spawndist), 4);
                file.write(reinterpret_cast<char*>(&data->spawnMask), 1);
                file.write(reinterpret_cast<char*>(&data->spawntimesecs), 4);
                file.write(reinterpret_cast<char*>(&data->unit_flags), 4);
            }
        }
    }
    // @TODO: create new waypoints if needed
    // @TODO: missing quests
    file.close();

    return true;
}

bool GameEventDump::Import(std::string path)
{
    std::ifstream* file = new std::ifstream;
    file->open(path, std::ios::in | std::ios::binary);
    if (!file->is_open())
        return false;
    file->seekg(0);
    int32 eventId;
    uint32 count;
    file->read(reinterpret_cast<char*>(&eventId), 4);
    GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();
    if (events.find(eventId) != events.end())
    {
        file->close();
        return false;
    }
    GameEventData& eventData = events[eventId];
    file->read(reinterpret_cast<char*>(&eventData.announce), 1);
    file->read(reinterpret_cast<char*>(&eventData.holiday_id), 4);
    file->read(reinterpret_cast<char*>(&eventData.length), 4);
    file->read(reinterpret_cast<char*>(&eventData.nextstart), 4);
    file->read(reinterpret_cast<char*>(&eventData.occurence), 4);
    file->read(reinterpret_cast<char*>(&eventData.start), 4);
    file->read(reinterpret_cast<char*>(&eventData.state), 4);
    file->read(reinterpret_cast<char*>(&count), 4);
    char* descr = new char[count + 1];
    descr[count] = '\0';
    file->read(descr, count);
    eventData.description = descr;
    eventData.description.resize(count);
    bool isActive;
    file->read(reinterpret_cast<char*>(&isActive), 1);
    if (isActive)
        sGameEventMgr->StartEvent(eventId);

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT);
    stmt->setInt32(0, eventId);
    stmt->setString(1, descr);
    WorldDatabase.Execute(stmt);

    file->read(reinterpret_cast<char*>(&count), 4);
    for (uint32 i = 0; i < count; ++i)
        ImportGO(file, eventId);

    file->read(reinterpret_cast<char*>(&count), 4);
    for (uint32 i = 0; i < count; ++i)
        ImportNPC(file, eventId);
    file->close();

    return true;
}

void GameEventDump::ImportGO(std::ifstream* file, int32 eventId)
{
    GameObjectData* data = new GameObjectData;
    file->read(reinterpret_cast<char*>(&data->animprogress), 4);
    file->read(reinterpret_cast<char*>(&data->artKit), 1);
    file->read(reinterpret_cast<char*>(&data->dbData), 1);
    file->read(reinterpret_cast<char*>(&data->go_state), 4);
    file->read(reinterpret_cast<char*>(&data->id), 4);
    file->read(reinterpret_cast<char*>(&data->mapid), 2);
    file->read(reinterpret_cast<char*>(&data->orientation), 4);
    file->read(reinterpret_cast<char*>(&data->phaseMask), 4);
    file->read(reinterpret_cast<char*>(&data->posX), 4);
    file->read(reinterpret_cast<char*>(&data->posY), 4);
    file->read(reinterpret_cast<char*>(&data->posZ), 4);
    file->read(reinterpret_cast<char*>(&data->rotation.x), 4);
    file->read(reinterpret_cast<char*>(&data->rotation.y), 4);
    file->read(reinterpret_cast<char*>(&data->rotation.z), 4);
    file->read(reinterpret_cast<char*>(&data->rotation.w), 4);
    file->read(reinterpret_cast<char*>(&data->spawnMask), 1);
    file->read(reinterpret_cast<char*>(&data->spawntimesecs), 4);

    Map* map = sMapMgr->FindBaseNonInstanceMap(data->mapid);

    GameObject* object = new GameObject;
    uint32 guidLow = map->GenerateLowGuid<HighGuid::GameObject>();

    const Position pos(data->posX, data->posY, data->posZ, data->orientation);
    if (!object->Create(guidLow, data->id, map, data->phaseMask, pos, data->rotation, data->animprogress, data->go_state, data->artKit))
    {
        delete object;
        delete data;
        return;
    }

    object->SetRespawnTime(data->spawntimesecs);

    // fill the gameobject data and save to the db
    object->SaveToDB(map->GetId(), (1 << map->GetSpawnMode()), data->phaseMask);

    uint32 spawnId = object->GetSpawnId();
    if (eventId)
    {
        PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT_GAMEOBJECT);
        stmt->setInt32(0,eventId);
        stmt->setUInt32(1,object->GetSpawnId());
        WorldDatabase.Execute(stmt);
        sGameEventMgr->AddGameObjectToEvent(object->GetSpawnId(), eventId);
    }
    // delete the old object and do a clean load from DB with a fresh new GameObject instance.
    // this is required to avoid weird behavior and memory leaks
    delete object;

    bool addToMap = !eventId || sGameEventMgr->IsActiveEvent(eventId);
    object = new GameObject();
    // this will generate a new guid if the object is in an instance
    if (!object->LoadGameObjectFromDB(spawnId, map, addToMap))
    {
        delete object;
        delete data;
        return;
    }

    if (addToMap) // add only in case event is in progress
        sObjectMgr->AddGameobjectToGrid(guidLow, sObjectMgr->GetGOData(spawnId));
    delete data;
}

void GameEventDump::ImportNPC(std::ifstream* file, int32 eventId)
{
    CreatureData* data = new CreatureData;
    file->read(reinterpret_cast<char*>(&data->curhealth), 4);
    file->read(reinterpret_cast<char*>(&data->curmana), 4);
    file->read(reinterpret_cast<char*>(&data->currentwaypoint), 4);
    file->read(reinterpret_cast<char*>(&data->dbData), 1);
    file->read(reinterpret_cast<char*>(&data->displayid), 4);
    file->read(reinterpret_cast<char*>(&data->dynamicflags), 4);
    file->read(reinterpret_cast<char*>(&data->equipmentId), 1);
    file->read(reinterpret_cast<char*>(&data->id), 4);
    file->read(reinterpret_cast<char*>(&data->mapid), 2);
    file->read(reinterpret_cast<char*>(&data->movementType), 1);
    file->read(reinterpret_cast<char*>(&data->npcflag), 4);
    file->read(reinterpret_cast<char*>(&data->orientation), 4);
    file->read(reinterpret_cast<char*>(&data->phaseMask), 4);
    file->read(reinterpret_cast<char*>(&data->posX), 4);
    file->read(reinterpret_cast<char*>(&data->posY), 4);
    file->read(reinterpret_cast<char*>(&data->posZ), 4);
    file->read(reinterpret_cast<char*>(&data->spawndist), 4);
    file->read(reinterpret_cast<char*>(&data->spawnMask), 1);
    file->read(reinterpret_cast<char*>(&data->spawntimesecs), 4);
    file->read(reinterpret_cast<char*>(&data->unit_flags), 4);

    if (!sObjectMgr->GetCreatureTemplate(data->id))
        return;
    Map* map = sMapMgr->FindBaseNonInstanceMap(data->mapid);
    bool addToMap = !eventId || sGameEventMgr->IsActiveEvent(eventId);

    Creature* creature = new Creature();
    uint32 guidLow = map->GenerateLowGuid<HighGuid::Unit>();
    if (!creature->Create(guidLow, map, data->phaseMask, data->id, data->posX, data->posY, data->posZ, data->orientation, data))
    {
        delete data;
        delete creature;
        return;
    }

    creature->SetRespawnDelay(data->spawntimesecs);
    creature->SetRespawnRadius(data->spawndist);
    creature->SetUInt32Value(UNIT_FIELD_FLAGS, data->unit_flags);
    creature->SetUInt32Value(UNIT_DYNAMIC_FLAGS, data->dynamicflags);
    creature->SetHealth(data->curhealth);
    creature->SetPower(POWER_MANA, data->curmana);
    creature->SetDisplayId(data->displayid);
    creature->SetNativeDisplayId(data->displayid);
    creature->SetDefaultMovementType(MovementGeneratorType(data->movementType));
    creature->SetCurrentEquipmentId(data->equipmentId);

    creature->SaveToDB(map->GetId(), (1 << map->GetSpawnMode()), data->phaseMask);

    uint32 db_guid = creature->GetSpawnId();
    if (eventId)
    {
        PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT_CREATURE);
        stmt->setInt32(0,eventId);
        stmt->setUInt32(1,db_guid);
        WorldDatabase.Execute(stmt);
        sGameEventMgr->AddCreatureToEvent(db_guid, eventId);
    }
    // To call _LoadGoods(); _LoadQuests(); CreateTrainerSpells()
    // current "creature" variable is deleted and created fresh new, otherwise old values might trigger asserts or cause undefined behavior
    creature->CleanupsBeforeDelete();
    delete creature;
    creature = new Creature();
    if (!creature->LoadCreatureFromDB(db_guid, map, addToMap))
    {
        delete data;
        delete creature;
        return;
    }

    if (addToMap) // add only in case if event is in progress
        sObjectMgr->AddCreatureToGrid(guidLow, sObjectMgr->GetCreatureData(db_guid));
    delete data;
}

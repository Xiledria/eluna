#include "ScriptMgr.h"
#include "Unit.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

template<uint32 HealValue, uint8 EffId, uint16 AuraType = SPELL_AURA_PERIODIC_DAMAGE>
class spell_gen_on_healed : public SpellScriptLoader
{
public:
    spell_gen_on_healed(const char* scriptName) : SpellScriptLoader(scriptName) { }

    class spell_gen_on_healed_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_gen_on_healed_AuraScript);

        void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
        {
            if (GetUnitOwner()->HealthAboveOrEqualPct(HealValue))
            {
                PreventDefaultAction();
                Remove(AURA_REMOVE_BY_DEFAULT);
            }
        }

        void Register() override
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_gen_on_healed_AuraScript::HandleEffectPeriodic, EffId, AuraType);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_gen_on_healed_AuraScript();
    }
};

template<uint8 EffIdAura, uint8 EffIdValue, uint16 AuraType = SPELL_AURA_PERIODIC_DAMAGE>
class spell_gen_on_healed_effect : public SpellScriptLoader
{
public:
    spell_gen_on_healed_effect(const char* scriptName) : SpellScriptLoader(scriptName) { }

    class spell_gen_on_healed_effect_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_gen_on_healed_effect_AuraScript);

        void HandleEffectPeriodic(AuraEffect const* /*aurEff*/)
        {
            if (GetUnitOwner()->HealthAboveOrEqualPct(GetSpellInfo()->Effects[EffIdValue].CalcValue()))
            {
                PreventDefaultAction();
                Remove(AURA_REMOVE_BY_DEFAULT);
            }
        }

        void Register() override
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_gen_on_healed_effect_AuraScript::HandleEffectPeriodic, EffIdAura, AuraType);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_gen_on_healed_effect_AuraScript();
    }
};

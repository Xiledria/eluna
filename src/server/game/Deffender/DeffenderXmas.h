#ifndef __DEFFENDER_XMAS_H
#define __DEFFENDER_XMAS_H
#include "Define.h"

class Player;
namespace Deffender
{
    namespace Xmas
    {
        enum DeffenderXmasEnum
        {
            DEFFENDER_GIFT_LOOT = 150000,
            DEFFENDER_GIFT_AURA = 100046,
            DEFFENDER_GIFT_ITEM =  46740,
        };

        TC_GAME_API bool GenerateGift(Player* player);
    }
}
#endif

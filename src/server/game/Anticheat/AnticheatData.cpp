#include "AnticheatData.h"

AnticheatData::AnticheatData()
{
    airWalkCounter = 0;
    lastOpcode = 0;
    totalReports = 0;
    for (uint8 i = 0; i < MAX_REPORT_TYPES; i++)
    {
        typeReports[i] = 0;
        tempReports[i] = 0;
        tempReportsTimer[i] = 0;
    }
    for (uint8 i = 0; i < MAX_HISTORY_STEPS; ++i)
    {
        speedHistory[i] = 0.0f;
    }
    average = 0;
    creationTime = 0;
    hasDailyReport = false;
    PQRCheck = new PQR();
}

AnticheatData::~AnticheatData()
{
    delete PQRCheck;
}

void AnticheatData::UpdateSpeedHistory(float speed)
{
    for (uint8 i = 0; i < MAX_HISTORY_STEPS -1; ++i)
        speedHistory[i] = speedHistory[i+1];
    speedHistory[MAX_HISTORY_STEPS - 1] = speed;
}

float AnticheatData::GetMaxSpeed()
{
    float max = 0;
    for (uint8 i = 0; i < MAX_HISTORY_STEPS; ++i)
        if (speedHistory[i] > max)
            max = speedHistory[i];
    return max;
}


void AnticheatData::SetDailyReportState(bool b)
{
    hasDailyReport = b;
}

bool AnticheatData::GetDailyReportState()
{
    return hasDailyReport;
}

void AnticheatData::SetLastOpcode(uint32 opcode)
{
    lastOpcode = opcode;
}

void AnticheatData::SetPosition(float x, float y, float z, float o)
{
    lastMovementInfo.pos.m_positionX = x;
    lastMovementInfo.pos.m_positionY = y;
    lastMovementInfo.pos.m_positionZ = z;
    lastMovementInfo.pos.SetOrientation(o);
}

uint32 AnticheatData::GetLastOpcode() const
{
    return lastOpcode;
}

const MovementInfo& AnticheatData::GetLastMovementInfo() const
{
    return lastMovementInfo;
}

void AnticheatData::SetLastMovementInfo(MovementInfo& moveInfo)
{
    lastMovementInfo = moveInfo;
}

uint32 AnticheatData::GetTotalReports() const
{
    return totalReports;
}

void AnticheatData::SetTotalReports(uint32 _totalReports)
{
    totalReports = _totalReports;
}

void AnticheatData::SetTypeReports(uint32 type, uint32 amount)
{
    typeReports[type] = amount;
}

uint32 AnticheatData::GetTypeReports(uint32 type) const
{
    return typeReports[type];
}

float AnticheatData::GetAverage() const
{
    return average;
}

void AnticheatData::SetAverage(float _average)
{
    average = _average;
}

uint32 AnticheatData::GetCreationTime() const
{
    return creationTime;
}

void AnticheatData::SetCreationTime(uint32 _creationTime)
{
    creationTime = _creationTime;
}

void AnticheatData::SetTempReports(uint32 amount, uint8 type)
{
    tempReports[type] = amount;
}

uint32 AnticheatData::GetTempReports(uint8 type)
{
    return tempReports[type];
}

void AnticheatData::SetTempReportsTimer(uint32 time, uint8 type)
{
    tempReportsTimer[type] = time;
}

uint32 AnticheatData::GetTempReportsTimer(uint8 type)
{
    return tempReportsTimer[type];
}

uint16 AnticheatData::UpdateAirWalkCounter()
{
    return ++airWalkCounter;
}

void AnticheatData::ResetAirWalkCounter()
{
    airWalkCounter = 0;
}

void AnticheatData::UpdatePQRInterruptLogs(int32 cast, int32 start, int32 end, int32 time, uint32 spellId)
{
    PQRCheck->interrupts.push_back(new AnticheatData::PQR::Spell(start, end, cast, time, spellId));
}

void AnticheatData::UpdatePQRDispellLogs(int32 delay, int32 duration, int32 time, uint32 spellId)
{
    PQRCheck->dispells.push_back(new AnticheatData::PQR::Aura(duration, delay, time, spellId));
}

AnticheatData::PQR* AnticheatData::GetPQRData()
{
    return PQRCheck;
}

AnticheatData::PQR::~PQR()
{
    interrupts.clear();
    dispells.clear();
}

AnticheatData::PQR::Spell::Spell(int32 start, int32 end, int32 cast, int32 time, uint32 spellId)
{
    this->start = start;
    this->end = end;
    this->cast = cast;
    this->time = time;
    this->spellId = spellId;
}

AnticheatData::PQR::Aura::Aura(int32 duration, int32 delay, int32 time, uint32 spellId)
{
    this->duration = duration;
    this->delay = delay;
    this->time = time;
    this->spellId = spellId;
}
